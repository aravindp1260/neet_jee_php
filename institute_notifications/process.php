<?php
ini_set('display_errors','0');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}
$imagepath="https://admin.rizee.in/files/";
//$imagepath="http://localhost:81/neetjee/files/";
//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
		if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
		?>
			 <script>
				$('html, body').animate({
				   scrollTop: $("#myDiv").offset().top
			   }, 2000);
			  </script>
			 <?php
			$data_sel = $database->query1("SELECT * FROM institute_notifications_days WHERE id = '".$_POST['editform']."'");
			if(mysqli_num_rows($data_sel) > 0){
			$data = mysqli_fetch_array($data_sel);
			$_POST = array_merge($_POST,$data);
			$_POST['short_description']=$_POST['description'];
			
			$_POST['image']=str_replace($imagepath,"",$_POST['image']);
			if($data['web_image']!='' || $data['mobile_image']!=''){
				$_POST['webview_image']=str_replace($imagepath,"",$_POST['web_image']);
				$_POST['mobileview_image']=str_replace($imagepath,"",$_POST['mobile_image']);

				$ldesc=str_replace('<div class="webimage" ><img src="'.$data['web_image'].'"  width="100%" height="auto" /><br /><br /></div><div class="mobileimage" ><img src="'.$data['mobile_image'].'" width="100%" height="auto" /><br /><br /></div>','',$data['long_description']);
			}else{
				$ldesc=$data['long_description'];
			}
			$_POST['long_description'] = preg_replace('#<div style="text-align: center;padding-top:60px;" id="actionbuttondiv">(.*?)</div>#', '', $ldesc);
		 ?>
		 <script type="text/javascript">
		 $('#adminForm').slideDown();
		 </script>
		 
		 <?php
			}
		 }
		 if($_POST['addForm'] == 2){
			 $_POST['long_description']=$_POST['long_description'];
		 }else{
			$_POST['long_description']=urldecode($_POST['long_description']);
		 }
	
	?>
		<style>
			.subjectanalysis {
				padding: .75rem 1.25rem;
				margin-bottom: 0;
				background-color: rgba(0,0,0,.03);
				border-bottom: 1px solid rgba(0,0,0,.125);
			}
			.subject-wise-analysis.nav-pills .nav-link.active {
				background-color:#000;
				color:#fff;
			}
			.subject-analysis-content label, h6 {
				color:#000;
			}
			.subject-analysis-content .table td p {
				color:#000000c7;
				font-weight:bold;
			}
			.subject-analysis-content .table th, .subject-analysis-content .table td {
				font-size:12px !important;
				padding:0.7rem 0.1rem !important;
			}

			  .multiselect-item.multiselect-filter .input-group-btn{
	display:none;
  }
  .multiselect-native-select .multiselect.dropdown-toggle {
  	border: 1px solid #ced4da;
  }
		</style>
		<style>
		
		</style>
	
	<script>
		('#class_ids').selectpicker3();
		$('#exam_ids').selectpicker1();
		$('#username').selectpicker2();
		//$('#chapter').selectpicker();
		/*$('#username').multiselect({
			enableFiltering: true,
			includeSelectAllOption: true,
			maxHeight: 300,
			 buttonWidth: "100%",
			dropDown: true
		});*/
	 </script>
	<script type="text/javascript">
      $('#notification_time').datetimepicker({
		  format: 'LT'
	 });
	 
    </script>

	<div class="col-lg-12 col-md-12" id="myDiv">
		<div class="col-lg-12 col-md-12">
			<?php
			$_POST['class_ids']=explode(",",$_POST['class_ids']);
			?>
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group row">
						<label class="col-sm-4 col-form-label">Class <span style="color:red;">*</span></label>
						<div class="col-sm-8">
							<select class="form-control selectpicker3" multiple data-actions-box="true"  data-live-search="true" name="class_ids" value=""  id="class_ids" >
								<?php
								$selexam =$database->query("select * from class where estatus='1' ");
								
								while($data = mysqli_fetch_array($selexam))
								{
									?>
								<option value="<?php echo $data['id'];?>"   <?php if(isset($_POST['class_ids'])) { if(in_array($data['id'], $_POST['class_ids'])) { echo 'selected="selected"'; } } ?> ><?php echo $data['class'];?></option>
								
									<?php
								}
								?>
							</select>
							<span class="error text-danger"><?php if(isset($_SESSION['error']['class_ids'])){ echo $_SESSION['error']['class_ids'];}?></span>
						</div>
					</div>
				</div>
				
				<?php
				$_POST['exam_ids']=explode(",",$_POST['exam_ids']);
				?>
							
				<div class="col-lg-6">
					<div class="form-group row">
						<label class="col-sm-4 col-form-label">Exam <span style="color:red;">*</span></label>
						<div class="col-sm-8">
							<select class="form-control selectpicker1"  multiple name="exam_ids" value="" data-actions-box="true"  data-live-search="true"  id="exam_ids" onChange="setState('aaa','<?php echo SECURE_PATH;?>notifyusers/ajax1.php','getsubject=1&class='+$('#class_ids').val()+'&exam='+$('#exam_ids').val()+'');">
								<?php
								$selexam =$database->query("select * from exam where estatus='1' and id IN (1,2,5) ");
								
								while($data = mysqli_fetch_array($selexam))
								{
									?>
									<option value="<?php echo $data['id'];?>"   <?php if(isset($_POST['exam_ids'])) { if(in_array($data['id'], $_POST['exam_ids'])) { echo 'selected="selected"'; } } ?> ><?php echo $data['exam'];?></option>
								<?php
								}
								?>
							</select>
							<span class="error text-danger"><?php if(isset($_SESSION['error']['exam_ids'])){ echo $_SESSION['error']['exam_ids'];}?></span>
						</div>
					</div>
				</div>
			
				<div class="col-lg-6">
					<div class="form-group row">
						<label class="col-sm-4 col-form-label">Uselevel <span style="color:red;">*</span></label>
						<div class="col-sm-8">
							<select class="form-control" name="userlevel" value=""  id="userlevel"   >
								<option value=''>-- Select --</option>
								
								<option value="2" <?php if(isset($_POST['userlevel'])) { if($_POST['userlevel']==2) { echo 'selected="selected"'; }  } ?>>Registered Users</option>
								<option value="1" <?php if(isset($_POST['userlevel'])) { if($_POST['userlevel']==1) { echo 'selected="selected"'; }  } ?>>Institute Users</option>
							</select>
							<span class="error text-danger"><?php if(isset($_SESSION['error']['userlevel'])){ echo $_SESSION['error']['userlevel'];}?></span>
						</div>
					</div>
				</div>
				<div class="col-lg-6" >
					<div class="form-group row" >
						<label class="col-sm-4 col-form-label"> Actions Type</label>
						<div class="col-sm-8">
						<select class="form-control" name="app_actions" value=""   id="app_actions"  onchange="appactionsdiv();">
								<option value=''>-- Select --</option>
								<option value="1" <?php if(isset($_POST['app_actions'])) { if($_POST['app_actions']==1) { echo 'selected="selected"'; }  } ?>>Learn</option>
								<option value="2" <?php if(isset($_POST['app_actions'])) { if($_POST['app_actions']==2) { echo 'selected="selected"'; }  } ?>>Practice</option>
								<option value="3" <?php if(isset($_POST['app_actions'])) { if($_POST['app_actions']==3) { echo 'selected="selected"'; }  } ?>>Action</option>
							</select>
							<span class="error text-danger"><?php if(isset($_SESSION['error']['app_actions'])){ echo $_SESSION['error']['app_actions'];}?></span>
						</div>
					</div>
				</div>
				<?php
				if(isset($_POST['app_actions'])){
					if($_POST['app_actions']!=''){
						if($_POST['app_actions']=='1' || $_POST['app_actions']=='2'){
							$styled='';
							$styled1='style="display:none;"';
							$styled3='';
						}else if($_POST['app_actions']=='3'){
							
							$styled='style="display:none;"';
							$styled1='';
							$styled3='';
						}else{
							$styled='style="display:none;"';
							$styled1='style="display:none;"';
							$styled3='';
						}
					}else{
						$styled='style="display:none;"';
						$styled1='style="display:none;"';
						$styled3='style="display:none;"';
					}
				}else{
					$styled='style="display:none;"';
					$styled1='style="display:none;"';
					$styled3='style="display:none;"';
				}
				?>
				<div class="col-lg-6 appactiondatadiv"  <?php echo $styled; ?>>
						<div class="form-group row" id="aaa">
					<label class="col-sm-4 col-form-label">Subject</label>
					<div class="col-sm-8">
					<select class="form-control" name="subject" value=""   id="subject" onChange="setState('bbb','<?php echo SECURE_PATH;?>notifyusers/ajax1.php','getsubchapter=1&class='+$('#class_ids').val()+'&subject='+$('#subject').val()+'');">
						<option value=''>-- Select --</option>
						<?php
						$row = $database->query("select * from subject where estatus='1'");
						while($data = mysqli_fetch_array($row))
						{
							?>
						<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['subject'])) { if($_POST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['subject'];?></option>
						<?php
						}
						?>
					</select>
						<span class="error text-danger"><?php if(isset($_SESSION['error']['subject'])){ echo $_SESSION['error']['subject'];}?></span>
					</div>
				</div>
			</div>
			<div class="col-lg-6 appactiondatadiv" <?php echo $styled; ?>>
				<div class="form-group row" id="bbb">
					<label class="col-sm-4 col-form-label">Chapter</label>
					<div class="col-sm-8">
					<select class="form-control" name="chapter" value=""   id="chapter" onChange="setState('ccc','<?php echo SECURE_PATH;?>notifyusers/ajax1.php','getsubtopic=1&class='+$('#class_ids').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'');">
							<option value=''>-- Select --</option>
							<?php
							$row = $database->query("select * from chapter where estatus='1'");
							
							while($data = mysqli_fetch_array($row))
							{
								?>
							<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['chapter'])) { if($_POST['chapter']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['chapter'];?></option>
							<?php
							}
							?>
						</select>
						<span class="error text-danger"><?php if(isset($_SESSION['error']['chapter'])){ echo $_SESSION['error']['chapter'];}?></span>
					</div>
				</div>
			</div>
			<div class="col-lg-6 appactiondatadiv" <?php echo $styled; ?>>
				<div class="form-group row" id="ccc">
					<label class="col-sm-4 col-form-label">Topic</label>
					<div class="col-sm-8">
					<select class="form-control" name="topic" value=""   id="topic" >
							<option value=''>-- Select --</option>
							<?php
							$row = $database->query("select * from topic where estatus='1'");
							
							while($data = mysqli_fetch_array($row))
							{
								?>
							<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['topic'])) { if($_POST['topic']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['topic'];?></option>
							<?php
							}
							?>
						</select>
						<span class="error text-danger"><?php if(isset($_SESSION['error']['topic'])){ echo $_SESSION['error']['topic'];}?></span>
					</div>
				</div>
			</div>

			
			
			<div class="col-lg-6 appactiondatadiv1" <?php echo $styled1; ?>>
				<div class="form-group row" id="ccc">
					<label class="col-sm-4 col-form-label">App Action Data</label>
					<div class="col-sm-8">
					<select id="app_action_id" class="form-control" name="app_action_id">
						<option value="">-Select-</option>
						<?php
						$sql=$database->query1("select * from app_modules where estatus='1'");
						while($row=mysqli_fetch_array($sql)){
						?>
							
							<option value="<?php echo $row['id'];?>" <?php if(isset($_POST['app_action_id'])) { if($_POST['app_action_id']==$row['id']) { echo 'selected="selected"'; }  } ?>><?php echo $row['title'];?></option>
						<?php
						}
						?>
					</select>
						<span class="error text-danger"><?php if(isset($_SESSION['error']['app_action_id'])){ echo $_SESSION['error']['app_action_id'];}?></span>
					</div>
				</div>
			</div>
			
			<div class="col-lg-6 appactiondatadiv4" <?php echo $styled3; ?>>
				<div class="form-group row" >
					<label class="col-sm-4 col-form-label">Button Name</label>
					<div class="col-sm-8">
					<input type="text" id="button_name" class="form-control" value="<?php if(isset($_POST['button_name'])){ if($_POST['button_name']!=''){ echo $_POST['button_name']; }else{ echo 'Explore More'; }}else{ ''; } ?>">
						<span class="error text-danger"><?php if(isset($_SESSION['error']['button_name'])){ echo $_SESSION['error']['button_name'];}?></span>
					</div>
				</div>
			</div>
				<div class="col-lg-6">
					<div class="form-group row">
						<label class="col-sm-4 col-form-label">Days Wise Notifications <span style="color:red;">*</span></label>
						<div class="col-sm-8">
							<select class="form-control" name="day" value=""  id="day"   >
								<option value=''>-- Select --</option>
								
								<option value="1" <?php if(isset($_POST['day'])) { if($_POST['day']==1) { echo 'selected="selected"'; }  } ?>>Day-1</option>
								<option value="2" <?php if(isset($_POST['day'])) { if($_POST['day']==2) { echo 'selected="selected"'; }  } ?>>Day-2</option>
								<option value="3" <?php if(isset($_POST['day'])) { if($_POST['day']==3) { echo 'selected="selected"'; }  } ?>>Day-3</option>
								<!-- <option value="4" <?php if(isset($_POST['day'])) { if($_POST['day']==4) { echo 'selected="selected"'; }  } ?>>Day-4</option>
								<option value="5" <?php if(isset($_POST['day'])) { if($_POST['day']==5) { echo 'selected="selected"'; }  } ?>>Day-5</option>
								<option value="6" <?php if(isset($_POST['day'])) { if($_POST['day']==6) { echo 'selected="selected"'; }  } ?>>Day-6</option><option value="7" <?php if(isset($_POST['day'])) { if($_POST['day']==7) { echo 'selected="selected"'; }  } ?>>Day-7</option> -->
							</select>
							<span class="error text-danger"><?php if(isset($_SESSION['error']['day'])){ echo $_SESSION['error']['day'];}?></span>
						</div>
					</div>
				</div>
				
			
			<div class="col-lg-6"  >
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Notification Time <span style="color:red;">*</span></label>
					<div class="col-sm-8">
						
							<input type="text" name="notification_time" id="notification_time" class="form-control " value="<?php if(isset($_POST['notification_time'])) { if($_POST['notification_time']!=''){ echo $_POST['notification_time']; }else { echo '';} }else{ echo ''; }?>">
							<span class="error text-danger"><?php if(isset($_SESSION['error']['notification_time'])){ echo $_SESSION['error']['notification_time'];}?></span>
						
					</div>
				</div>
			</div>
			
				<div class="col-lg-6" >
					<div class="form-group row">
						<label class="col-sm-4 col-form-label">Title <span style="color:red;">*</span></label>
						<div class="col-sm-8">
						<textarea id="title" id="title" class="form-control" ><?php if(isset($_POST['title'])){ if($_POST['title']!=''){ echo $_POST['title']; }} ?></textarea>
							<span class="error text-danger"><?php if(isset($_SESSION['error']['title'])){ echo $_SESSION['error']['title'];}?></span>
						</div>
					</div>
				</div>

				<div class="col-lg-6" >
					<div class="form-group row">
						<label class="col-sm-4 col-form-label">Short Description <span style="color:red;">*</span></label>
						<div class="col-sm-8">
							<textarea id="short_description" id="short_description" class="form-control" rows="5"><?php if(isset($_POST['short_description'])){ if($_POST['short_description']!=''){ echo $_POST['short_description']; }} ?></textarea>
							<span class="error text-danger"><?php if(isset($_SESSION['error']['short_description'])){ echo $_SESSION['error']['short_description'];}?></span>
						</div>
					</div>
				</div>

			</div>
		</div>

			<div class="row">
				<div class="col-lg-12" >
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Long Description <span style="color:red;">*</span></label>
						<div class="col-sm-10">

							<div class="wrs_container">
								<div class="wrs_row">
									<div class="wrs_col wrs_s12">
										<div id="editorContainer">
											<div id="toolbarLocation"></div>
											
											<textarea id="description" class="customcontent wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['long_description'])) { echo $_POST['long_description']; }else{  } ?></textarea>

										</div>
										<a class="btn btn-link"  data-toggle="modal" data-target="#previewModal" style="pointer:cursor;" onClick="mobilePreview()">Mobile Preview</a>
									</div>

								</div>
							</div>


						</div>
					</div>
				</div>

			</div>
				<div class="row">
								<div class="col-lg-6">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Web View Image</label>
										<div class="col-sm-8">

											<div id="file-uploader1" style="display:inline">
												<noscript>
													<p>Please enable JavaScript to use file uploader.</p>
													<!-- or put a simple form for upload here -->
												</noscript>

											</div>
											<script>

												function createUploader(){

													var uploader = new qq.FileUploader({
														element: document.getElementById('file-uploader1'),
														action: '<?php echo SECURE_PATH;?>frame/js/upload/php2.php?upload=webview_image&upload_type=single&filetype=file',
														debug: true,
														multiple:false
													});
												}

												createUploader();



												// in your app create uploader as soon as the DOM is ready
												// don't wait for the window to load

											</script>
											<input type="hidden" name="webview_image" id="webview_image" value="<?php if(isset($_POST['webview_image'])) { echo $_POST['webview_image']; } ?>"/>
											
											<?php
											if(isset($_POST['webview_image']))
											{
												if($_POST['webview_image']!=''){
													?>
													<img src="<?php echo SECURE_PATH."files/".$_POST['webview_image'];?>" style="width:20%;height:auto;" />
													<?php
												}

											}
											?>

											<span class="error" style="color:red;" ><?php if(isset($_SESSION['error']['webview_image'])){ echo $_SESSION['error']['webview_image'];}?></span>
										</div>
									</div>

								</div>
								<div class="col-lg-6">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Mobile View Image</label>
										<div class="col-sm-8">

											<div id="file-uploader4" style="display:inline">
												<noscript>
													<p>Please enable JavaScript to use file uploader.</p>
													<!-- or put a simple form for upload here -->
												</noscript>

											</div>
											<script>

												function createUploader(){

													var uploader = new qq.FileUploader({
														element: document.getElementById('file-uploader4'),
														action: '<?php echo SECURE_PATH;?>frame/js/upload/php2.php?upload=mobileview_image&upload_type=single&filetype=file',
														debug: true,
														multiple:false
													});
												}

												createUploader();



												// in your app create uploader as soon as the DOM is ready
												// don't wait for the window to load

											</script>
											<input type="hidden" name="mobileview_image" id="mobileview_image" value="<?php if(isset($_POST['mobileview_image'])) { echo $_POST['mobileview_image']; } ?>"/>
											
											<?php
											if(isset($_POST['mobileview_image']))
											{
												if($_POST['mobileview_image']!=''){
													?>
													<img src="<?php echo SECURE_PATH."files/".$_POST['mobileview_image'];?>" style="width:20%;height:auto;" />
													<?php
												}

											}
											?>

											<span class="error" style="color:red;" ><?php if(isset($_SESSION['error']['mobileview_image'])){ echo $_SESSION['error']['mobileview_image'];}?></span>
										</div>
									</div>

								</div>
							</div>

			<div class="row">
				<div class="col-lg-6">
					<div class="form-group row">
						<label class="col-sm-4 col-form-label">Image</label>
						<div class="col-sm-8">

							<div id="file-uploader2" style="display:inline">
								<noscript>
									<p>Please enable JavaScript to use file uploader.</p>
									<!-- or put a simple form for upload here -->
								</noscript>

							</div>
							<script>

								function createUploader(){

									var uploader = new qq.FileUploader({
										element: document.getElementById('file-uploader2'),
										action: '<?php echo SECURE_PATH;?>frame/js/upload/php2.php?upload=image&upload_type=single&filetype=file',
										debug: true,
										multiple:false
									});
								}

								createUploader();



								// in your app create uploader as soon as the DOM is ready
								// don't wait for the window to load

							</script>
							<input type="hidden" name="image" id="image" value="<?php if(isset($_POST['image'])) { echo $_POST['image']; } ?>"/>
							<!-- <div class="pics"></div> -->
							<?php
							if(isset($_POST['image']))
							{
								if($_POST['image']!=''){
									?>
									<img src="<?php echo SECURE_PATH."files/".$_POST['image'];?>" style="width:20%;height:auto;" />
									<?php
								}

							}
							?>

							<span class="error" style="color:red;" ><?php if(isset($_SESSION['error']['image'])){ echo $_SESSION['error']['image'];}?></span>
						</div>
					</div>

				</div>



				<script type="text/javascript" >
					createEditorInstance("en", {});
				</script>
					<script>
						function rand(){

							var conttype=$('#conttype').val();
							var data2;
							data2 = '{ "description" : "' + encodeURIComponent(tinymce.get('description').getContent())+ '"}';



							$.ajax({
								type: 'POST',
								url: '<?php echo SECURE_PATH;?>institute_notifications/img.php',
								data: data2,
								contentType: 'application/json; charset=utf-8',
								dataType: 'json',

								success: function (result,xhr) {
									console.log("reply");
									var shortdesc=$('#short_description').val();

									

									setState('adminForm','<?php echo SECURE_PATH;?>institute_notifications/process.php','validateFormc=1&class_ids='+$('#class_ids').val()+'&exam_ids='+$('#exam_ids').val()+'&title='+escape($('#title').val())+'&day='+$('#day').val()+'&notification_time='+$('#notification_time').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&app_actions='+$('#app_actions').val()+'&app_action_id='+$('#app_action_id').val()+'&button_name='+$('#button_name').val()+'&short_description='+escape($('#short_description').val())+'&userlevel='+$('#userlevel').val()+'&webview_image='+$('#webview_image').val()+'&mobileview_image='+$('#mobileview_image').val()+'&image='+$('#image').val()+'<?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>');
								},
								error: function(e){

									console.log("ERROR: ", e);
								}
							});
						}


						function mobilePreview(){

							var conttype=$('#conttype').val();
							var data2;
							data2 = '{ "description" : "' + encodeURIComponent(tinymce.get('description').getContent())+ '"}';


							$.ajax({
								type: 'POST',
								url: '<?php echo SECURE_PATH;?>institute_notifications/img.php',
								data: data2,
								contentType: 'application/json; charset=utf-8',
								dataType: 'json',

								success: function (result,xhr) {


									console.log('description',tinymce.get('description').getContent());

									var html = data2.description;
									var iframe = $('#displayframe');

									iframe[0].srcdoc =    (tinymce.get('description').getContent());

									console.log("iframe",iframe);

								},
								error: function(e){

									console.log("ERROR: ", e);
								}
							});

						}


					</script>




			<div class="form-group row pl-3">
		
				<div class="col-lg-12">
					<a class="radius-20 btn btn-theme px-5" style="cursor:pointer" onClick="rand();">Save Notification</a>
				</div>
			</div>
		</div>
	</div>

<?php
	unset($_SESSION['error']);
	unset($_SESSION['description']);
}

if(isset($_POST['validateFormc'])){
	$_SESSION['error'] = array();
  $post = $session->cleanInput($_POST);
  $id = 'NULL';

  if(isset($post['editform'])){
	  $id = $post['editform'];
  }
	
  
	$field = 'exam_ids';
	if(!$post['exam_ids'] || strlen(trim($post['exam_ids'])) == 0){
	  $_SESSION['error'][$field] = "* Exam name cannot be empty";
	}
	$field = 'class_ids';
	if(!$post['class_ids'] || strlen(trim($post['class_ids'])) == 0){
	  $_SESSION['error'][$field] = "* Class name cannot be empty";
	}
	$field = 'title';
	if(!$post['title'] || strlen(trim($post['title'])) == 0){
	  $_SESSION['error'][$field] = "* Title cannot be empty";
	}

	$field = 'day';
	if(!$post['day'] || strlen(trim($post['day'])) == 0){
	  $_SESSION['error'][$field] = "* Please Select Day";
	}
	$field = 'notification_time';
	if(!$post['notification_time'] || strlen(trim($post['notification_time'])) == 0){
	  $_SESSION['error'][$field] = "* Notification Time can not be empty";
	}

	/*$field = 'app_action_id';
	if(!$post['app_action_id'] || strlen(trim($post['app_action_id'])) == 0){
	  $_SESSION['error'][$field] = "* App Action can not be empty";
	} */
	$field = 'short_description';
	if(!$post['short_description'] || strlen(trim($post['short_description'])) == 0){
	  $_SESSION['error'][$field] = "* Short Description cannot be empty";
	}

	
	if(!isset($_SESSION['description']))
        $_SESSION['description'] = "";

	
		
  //Check if any errors exist
	if(count($_SESSION['error']) > 0 || $post['validateFormc'] == 2){
		
		//print_r($_SESSION['error']);
		$desc=str_replace("'","%27",$post['short_description']);
		$title=str_replace("'","%27",$post['title']);
	?>
    <script type="text/javascript">
      $('#adminForm').slideDown();
	  
	 
      setState('adminForm','<?php echo SECURE_PATH;?>institute_notifications/process.php','addForm=1&class_ids=<?php echo $post['class_ids'];?>&exam_ids=<?php echo $post['exam_ids'];?>&email=<?php echo $post['email'];?>&webview_image=<?php echo $post['webview_image'];?>&mobileview_image=<?php echo $post['mobileview_image'];?>&title=<?php echo $title;?>&short_description=<?php echo $desc;?>&file=<?php echo $post['file'];?>&userlevel=<?php echo $post['userlevel'];?>&day=<?php echo $post['day'];?>&subject=<?php echo $post['subject'];?>&chapter=<?php echo $post['chapter'];?>&topic=<?php echo $post['topic'];?>&app_actions=<?php echo $post['app_actions'];?>&app_actions=<?php echo $post['app_actions'];?>&button_name=<?php echo $post['button_name'];?>&notification_time=<?php echo $post['notification_time'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?>')
    </script>
  <?php
	}
	else{
		
		$description=str_replace("'","&#39;",$post['short_description']);
		$ldesc=str_replace("'","&#39;",$_SESSION['description']);
		$headerdata='<!DOCTYPE html><html lang="en"><head><style>
		.floatingButton { color: #fff: ;background-color: #232F73;border-color: #12183A;font-size:16px;}
		.floatingButton:hover,.floatingButton.active,.floatingButton:active {color: #fff;background-color: #232F73;border-color: #12183A;}
		@media (max-width:991.98px) { .webimage { display: none;} } @media (min-width:992px) { .mobileimage { display: none;} }@media screen and (min-width: 992px) {.floatingButton { color: #fff;background-color: #232F73;border-color: #12183A;width: 100%;max-width: 250px;font-weight: bold;display: block;position: fixed;bottom: 4px;left: 0;right: -220px;margin: 0 auto;padding: 16px 12px;border-radius: 30.5px;text-align: center;}}
		@media screen and (max-width: 991.98px) {.floatingButton {color:#fff;background-color: #232F73;border-color: #12183A;width: 100%;max-width: 250px;font-weight: bold;display: block;position: fixed;bottom: 4px;left: 0;right: 0;margin: 0 auto;padding: 16px 12px;border-radius: 30.5px;text-align: center;}}</style></head>';
		$header=urlencode($headerdata);
		$var1=str_replace("%3C!DOCTYPE%20html%3E%0A%3Chtml%3E%0A%3Chead%3E%0A%3C%2Fhead%3E",$header,$ldesc);
			if(strlen($post['webview_image']) > 0){
				$web_image =$imagepath.$post['webview_image'];
		  }else{
			  $web_image ="";
		  }
		  if(strlen($post['mobileview_image']) > 0){
				$mobile_image =$imagepath.$post['mobileview_image'];
		  }else{
			  $mobile_image ="";
		  }
		if(strlen($web_image)>0 || strlen($mobile_image)>0 ){
			$var2='<div class="webimage" ><img src="'.$web_image.'"  width="100%" height="auto" /><br /><br /></div><div class="mobileimage" ><img src="'.$mobile_image.'" width="100%" height="auto" /><br /><br /></div>';
			$var3=urlencode($var2);
			$var4=str_replace('+','%20',$var3);
			$var6=explode($header."%0A",$var1);
			$var6count=count($var6);
			if($var6count>1){
				$data=$var6[1];
			}else{
				$data=$var6[0];
				
			}
			//$data=$var6[1];
			$dataval=$header;
			$long_desc=$dataval.$var4.$data;
		 }else{
			 $long_desc=$ldesc;
		 }
		
		
		
			if($post['app_actions']!=''){
			
			if($post['topic']!=''){
				$topic=$post['topic'];
			}else{
				$topic=0;
			}	
			if($post['app_actions']=='1'){
				if($post['subject']!='' && $post['chapter']!=''){
					
					$long_desc2=explode("%3C%2Fbody%3E",$long_desc);
					$link='<div style="text-align: center;padding-top:60px;" id="actionbuttondiv"><a href="https://rizee.in/student/learn/:'.$post['subject'].'/:'.$post['chapter'].'/:'.$topic.'" class="floatingButton">'.$post['button_name'].'</a></div>';
					$link1=urlencode($link);
					$link2=str_replace('+','%20',$link1);
					$long_desc11=$long_desc2[0].$link2.$long_desc2[1];
					$long_desc1=filterqa($long_desc11);
					$app_action_id=0;
				}else{
					$long_desc1=filterqa($long_desc);
					$app_action_id=0;
				}
			}else if($post['app_actions']=='2'){
				if($post['subject']!='' && $post['chapter']!=''){
					$long_desc2=explode("%3C%2Fbody%3E",$long_desc);
					$link='<div style="text-align: center;padding-top:60px;" id="actionbuttondiv"><a href="https://rizee.in/student/practice/:'.$post['subject'].'/:'.$post['chapter'].'/:'.$topic.'" class="floatingButton">'.$post['button_name'].'</a></div>';
					$link1=urlencode($link);
					$link2=str_replace('+','%20',$link1);
					$long_desc11=$long_desc2[0].$link2.$long_desc2[1];
					$long_desc1=filterqa($long_desc11);
					$app_action_id=0;
				}else{
					$long_desc1=filterqa($long_desc);
					$app_action_id=0;
				}
			}else if($post['app_actions']=='3'){
				if($post['app_action_id']==15){
					$long_desc2=explode("%3C%2Fbody%3E",$long_desc);
					$link='<div style="text-align: center;padding-top:60px;" id="actionbuttondiv"><a href="https://rizee.in/student/LiveMockTest" class="floatingButton" >'.$post['button_name'].'</a></div>';
					$link1=urlencode($link);
					$link2=str_replace('+','%20',$link1);
					$long_desc11=$long_desc2[0].$link2.$long_desc2[1];
					$long_desc1=filterqa($long_desc11);
				
				}else{
					$sell=$database->query1("select * from app_modules where estatus='1' and id='".$post['app_action_id']."'");
					$row=mysqli_fetch_array($sell);
					$long_desc2=explode("%3C%2Fbody%3E",$long_desc);
					$link='<div style="text-align: center;padding-top:60px;" id="actionbuttondiv"><a href="https://rizee.in/student/action/'.$row['module_name'].'" class="floatingButton">'.$post['button_name'].'</a></div>';
					$link1=urlencode($link);
					$link2=str_replace('+','%20',$link1);
					$long_desc11=$long_desc2[0].$link2.$long_desc2[1];
					$long_desc1=filterqa($long_desc11);
				}
				$app_action_id=$post['app_action_id'];
			}else{
				$long_desc1=filterqa($long_desc);
				$app_action_id=0;
			}
			
		}else{
			$long_desc1=filterqa($long_desc);
			$app_action_id=0;
		}
		$long_desc2=mysqli_real_escape_string($database->connection1,$long_desc1);
		
		 
		  if(strlen($post['image']) > 0){
		   $file = $imagepath.$post['image'];
		  }else{
			  $file = "";
		  }
		if($id=='NULL')
		{
			
			$file = "";
					
					 
				$title=str_replace("'","&#39;",$post['title']);
				$description=str_replace("'","&#39;",$post['short_description']);
				$result=$database->query1("INSERT INTO  institute_notifications_days  set title='".$title."',description='".$description."', long_description = '".$long_desc2."', image = '".$file."',exam_ids='".$post['exam_ids']."',class_ids='".$post['class_ids']."',subject='".$post['subject']."',chapter='".$post['chapter']."',topic='".$post['topic']."',app_actions='".$post['app_actions']."',app_action_id='".$app_action_id."',student_ids='',userlevel='".$post['userlevel']."',button_name='".$post['button_name']."',day='".$post['day']."',notification_time='".$post['notification_time']."',web_image ='".$web_image."',mobile_image ='".$mobile_image."',section_ids=0,institution_id='0',is_admin='1',estatus='1',timestamp='".time()."'" );
				

				
				$sid= mysqli_insert_id($con);
				
				if($result){
					
					
				 ?>
				  <div class="col-lg-12 col-md-12">
					<div class="form-group">
						<div class="alert alert-success">
						<i class="fa fa-thumbs-up fa-2x"></i>Welcome Notification Created Successfully
						</div>
					</div>
				</div>
				<script type="text/javascript">
				alert("Welcome Notification Created Successfully");
				animateForm('<?php echo SECURE_PATH;?>institute_notifications/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
			</script>
				
		 <?php
			}else{
				?>
				<div class="col-lg-12 col-md-12">
					<div class="form-group">
						<div class="alert alert-success">
						<i class="fa fa-thumbs-up fa-2x"></i> Notification Creation Failed
						</div>
					</div>
				</div>
				<script type="text/javascript">
				alert("Notification Failed");
				animateForm('<?php echo SECURE_PATH;?>institute_notifications/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
			</script>
				
			<?php

			}
		}else{
			$description=str_replace("'","&#39;",$post['short_description']);
			$title=str_replace("'","&#39;",$post['title']);
			$file = "";
					 if(strlen($post['image']) > 0)
					   $file = "https://admin.rizee.in/files/".$post['image'];
			if($title!='undefined'){
				$result=$database->query1("Update  institute_notifications_days  set title='".$title."',description='".$description."', long_description = '".$long_desc2."', image = '".$file."',exam_ids='".$post['exam_ids']."',class_ids='".$post['class_ids']."',subject='".$post['subject']."',chapter='".$post['chapter']."',topic='".$post['topic']."',app_actions='".$post['app_actions']."',app_action_id='".$app_action_id."',student_ids='',button_name='".$post['button_name']."',userlevel='".$post['userlevel']."',day='".$post['day']."',web_image = '".$web_image."',mobile_image = '".$mobile_image."',notification_time='".$post['notification_time']."' where id='".$id."'" );

			}

		
				

			
			?>
				<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Notification Updated Successfully</div>
				
					<script type="text/javascript">
				
					animateForm('<?php echo SECURE_PATH;?>institute_notifications/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
				</script>
			<?php
		}
    
		
	}
}


function filterqa($str){
  $str = urldecode($str);
//$res = str_replace("&nbsp;","",$str);

  return $res= getAllMath($str);

}


function getAllMath($text){



  $result = "";
  $part1 = explode("<math",$text);

  if(count($part1) > 0){

      $result.= $part1[0];//strip_tags($part1[0],'<p><img><b>');

      foreach($part1 as $first){
          $part2 = explode("</math>",$first);

          if(count($part2) > 1){
              $mml = "<math ".$part2[0]."</math>";
             
              $fields = array(
                  'mml' => $mml,
                  'lang'=>urlencode('en')
          );
          
          
          $url="http://imgsl.mylearningplus.in/neetjee/edut/tinymce4/plugins/tiny_mce_wiris/integration/showimagenew.php";
          
         $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         $retval = curl_exec($ch);
        curl_close($ch);
  
        $svg = json_decode($retval,true);


        $alt = "MathML Question";
        if(isset($svg['result']['alt']))
            $alt = $svg['result']['alt'];

		  $imgtext=str_replace(" ","+",$svg['result']['content']);
		 
       // $img= '<img src="'.($svg['result']['content']).'"  class="Wirisformula" role="math" alt="'.$alt.'" style="vertical-align: -5px; " width="'.$svg['result']['width'].'" height="'.$svg['result']['height'].'"></img>';
       $img= "<img src='".($imgtext)."'  class='Wirisformula' role='math' alt='.$alt.' style='vertical-align: -5px; ' width='".$svg['result']['width']."' height='".$svg['result']['height']."'></img>";
        $result.= $img.$part2[1];//strip_tags($part2[1],'<p><img>');
          
         
          }
      }
  }

  return $result;
}
?>


 
	<?php
	


if(isset($_GET['rowDelete'])){
	$database->query1("update  institute_notifications_days set estatus='0'  WHERE id = '".$_GET['rowDelete']."'");
	?>
    <div class="alert alert-success"> Notification deleted successfully!</div>

  <script type="text/javascript">

animateForm('<?php echo SECURE_PATH;?>institute_notifications/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>>
   <?php
}
?>
<?php


//Display bulkreport
if(isset($_GET['tableDisplay'])){
	?>
	
	<?php
	//Pagination code
  $limit=50;
  if(isset($_GET['page']))
  {
    $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
    $page=$_GET['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
  //Search Form
?>
<?php
   $tableName = 'institute_notifications_days';
  $condition = "estatus='1' and is_admin='1'";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  $q = "SELECT count(*) as count FROM $tableName ".$condition." ORDER BY timestamp DESC";
  $result_sel = $database->query1($q);
  $numdata = mysqli_fetch_array($result_sel);
  $numres=$numdata['count'];
    $query = "SELECT * FROM $tableName ".$condition."  ORDER BY timestamp DESC";
  //echo $query;
  $data_sel = $database->query1($query);
  if($numres > 0){
	?>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						
						<div class="card border-0">
										<div class="card-body table-responsive table_data p-0">
											<table class="table table-bordered dashboard-table mb-0" id="dataTable">
												<thead class="thead-light">
													<tr>
														
														<th class="text-left" nowrap>Date & Time</th>
														<th class="text-left" nowrap>Class</th>
														<th class="text-left" nowrap>Exam</th>
														<th class="text-left" nowrap>Title</th>
														<th class="text-left" nowrap>Description</th>
														<th class="text-left" nowrap>Day</th>
														<th class="text-left" nowrap>Options</th>
													</tr>
												</thead>
												<tbody>
													<?php
														
													$i=1;
													while($value=mysqli_fetch_array($data_sel)){
														$time=$value['timestamp'];
														$class='';
														$classsel=$database->query("select * from class where estatus='1' and id in (".$value['class_ids'].")");
														while($rowclass=mysqli_fetch_array($classsel)){
															$class.=$rowclass['class'].",";
														}

														$exam='';
														$examsel=$database->query("select * from exam where estatus='1' and id in (".$value['exam_ids'].")");
														while($rowclass=mysqli_fetch_array($examsel)){
															$exam.=$rowclass['exam'].",";
														}

														?>
														<tr>

															<td class="text-left" data-order="<?php echo $time; ?>"><?php echo date('d/m/Y  H:i:s',$value['timestamp']);?></td>
															<td class="text-left"><?php echo rtrim($class,","); ?></td>
															<td class="text-left"><?php echo rtrim($exam,","); ?></td>
															<td class="text-left"><?php echo $value['title']; ?></td>
															<td class="text-left"><?php echo $value['description']; ?></td>
															<td>Day- <?php echo $value['day']; ?></td>
															<td>
																<a style="cursor:pointer;" data-toggle="modal" data-target="#exampleModal1"onClick="setStateGet('viewDetails1','<?php echo SECURE_PATH;?>institute_notifications/prnt.php','viewdetails=1&id=<?php echo $value['id'];?>')"><i class="fa fa-eye" ></i></a>&nbsp;
																<a class=""  onClick="setState('adminForm','<?php echo SECURE_PATH;?>institute_notifications/process.php','addForm=2&editform=<?php echo $value['id'];?>')"><span style="color:blue;"><i class="fa fa-pencil-square-o" style="color:blue;"></i></a>&nbsp;&nbsp;&nbsp;
																<a class=" " onClick="confirmDelete('adminForm','<?php echo SECURE_PATH;?>institute_notifications/process.php','rowDelete=<?php echo $value['id'];?>')"><i class="fa fa-trash" style="color:red;"></i></a>
															</td>	
													  </tr>
													<?php
													$i++;
													}
													
													?>
												
												</tbody>
											</table>
									
										</div>
					</div>
				</div>
			</div>
		</div>

  </section>
 	<script type="text/javascript">
  $('#dataTable').DataTable({
	"pageLength": 50,
	"order": [[ 0, "desc" ]],
   
    
  });
   
</script>
	<?php
	}else{
	
}

}
?>
	<script>
	function appactionsdiv(){
		var app_actions=$('#app_actions').val();
		
		if(app_actions=='1'){
			
			$('.appactiondatadiv').show();
			$('.appactiondatadiv1').hide();
			$('.appactiondatadiv4').show();
			$('#button_name').val('Learn More');
		}else if(app_actions=='2'){
			$('.appactiondatadiv').show();
			$('.appactiondatadiv1').hide();
			$('.appactiondatadiv4').show();
			$('#button_name').val('Explore More');
		}else if(app_actions=='3'){
			$('.appactiondatadiv').hide();
			$('.appactiondatadiv1').show();
			$('.appactiondatadiv4').show();
			$('#button_name').val('Explore More');
		}

	}
		</script>

<script>
	 $(".selectpicker1").selectpicker('refresh');
	$(".selectpicker2").selectpicker('refresh');
	$(".selectpicker3").selectpicker('refresh');
	</script>



