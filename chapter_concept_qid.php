<?php

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");
/*define("DB_SERVER","localhost");
define("DB_USER","root");
define("DB_PWD","");
define("DB_NAME","qsbank");*/
$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = "Concept formula1 Report".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";
header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
						<th style="text-align:center;">Sr.No.</th>
							<th style="text-align:center;">Subject</th>
							<th style="text-align:center;">Chapter Name</th>
							<th style="text-align:center;"> Question ID</th>
							<th style="text-align:center;"> Verification Status</th>
							<th style="text-align:center;"> Verified Username</th>
							<th style="text-align:center;"> Verified Date</th>
							<th style="text-align:center;"> Review Status</th>
							<th style="text-align:center;"> Reviewed Username</th>
							<th style="text-align:center;"> Reviewed Date</th>
						</tr>
                        <?php
							$j=1;
                                $chapter_sel = query("SELECT id,class,subject,chapter FROM chapter WHERE estatus=1   ORDER BY subject ASC");

                                while($chapter = mysqli_fetch_array($chapter_sel)){
									
								   $selctot1=query("select id,vstatus1,vusername1,vtimestamp1,review_status,rusername,rtimestamp from createquestion where estatus='1' and subject='".$chapter['subject']."' and find_in_set(".$chapter['id'].",chapter)>0 and question_theory='1' and find_in_set(10,inputquestion)");
								  while($rowctot1=mysqli_fetch_array($selctot1)){
									  $selusers=query("select * from users where  userlevel='3' and username='".$rowctot1['vusername1']."'");
									  $rowuser=mysqli_fetch_array($selusers);
									  $time=$rowctot1['vtimestamp1'];
										if($rowctot1['vstatus1']=='1'){
											$status="Verfied";
										}else if($rowctot1['vstatus1']=='0'){
											$status="Pending";
										}else if($rowctot1['vstatus1']=='2'){
											$status="Rejected";
										} 
										if($rowctot1['review_status']=='1'){
											$rstatus="Reviewed";
										}else if($rowctot1['review_status']=='0'){
											$rstatus="Review Pending";
										}else if($rowctot1['review_status']=='2'){
											$rstatus="Rejected";
										} 
										$sub= query("SELECT id,subject FROM subject WHERE estatus=1 AND id = '".$chapter['subject']."'");
										$rowsub = mysqli_fetch_array($sub);
                                                echo "<tr>";
                                                    ?>	
                                                    
													<td><?php echo $j;?></td>
                                                        <td><?php echo $rowsub['subject']; ?></td>
                                                        <td ><?php echo $chapter['chapter'];?></td>
														
                                                       <td ><?php echo $rowctot1['id'];?></td>
													   <td><?php echo $status;?></td>
													   <td><?php echo $rowctot1['vusername1'];?></td>
													   <td class="text-left" data-order="<?php echo $time; ?>"><?php echo date('d/m/Y H:i:s',$rowctot1['vtimestamp1']);?></td>
													   <td><?php echo $rstatus;?></td>
													   <td><?php echo $rowctot1['rusername'];?></td>
													   <td class="text-left" ><?php echo date('d/m/Y H:i:s',$rowctot1['rtimestamp']);?></td>
                                                       
                                                    <?php
                                                    echo "</tr>";
                                                    
                                                            
                                     $j++;           
                                            
								  }
									
									
                                }
                                
                            

                        ?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>