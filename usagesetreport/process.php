<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
?>


	<?php
	if(isset($_REQUEST['tableDisplay'])){
		
		if(isset($_REQUEST['subject'])){
			if($_REQUEST['subject']!=""){
				$sub=" AND subject='".$_REQUEST['subject']."'";
			} else {
				$sub="";
			}
		}else{
			$sub="";
		}
	
	?>
		<style>
			.subjectanalysis {
				padding: .75rem 1.25rem;
				margin-bottom: 0;
				background-color: rgba(0,0,0,.03);
				border-bottom: 1px solid rgba(0,0,0,.125);
			}
			.subject-wise-analysis.nav-pills .nav-link.active {
				background-color:#000;
				color:#fff;
			}
			.subject-analysis-content label, h6 {
				color:#000;
			}
			.subject-analysis-content .table td p {
				color:#000000c7;
				font-weight:bold;
			}
			.subject-analysis-content .table th, .subject-analysis-content .table td {
				font-size:12px !important;
				padding:0.7rem 0.1rem !important;
			}
		</style>
		<style>
	
	 </style>
	<script>
	$('.chapter').selectpicker1();
	 </script>
		<section class="content-area">
			<div class="container">
				<div class="row Data-Tables">
					<div class="col-xl-12 col-lg-12"> 
						<div class="card border-0 shadow mb-4">
							
							<div class="card-body">
							

						
								
									
									<script type="text/javascript">


									  $('#dataTable').DataTable({
										"pageLength": 50,
										"order": [[ 1, 'asc' ]]
										
									  });

									</script>
									
									
									<?php
									//$_REQUEST['chapter3']=$_REQUEST['chapter'];
									$_REQUEST['chapter1']=explode(",",$_REQUEST['chapter']); ?>
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Subject</label>
												<div class="p-1 col-sm-4">
													
													<select class="form-control" name="subject" value=""   id="subject" onChange="search_report1();setState('aaa','<?php echo SECURE_PATH;?>usagesetreport/process.php','getchapter=1&subject='+$('#subject').val()+'');">
														<option value=''>-- Select --</option>
														<?php
														if($session->userlevel=='6'){
															$row = $database->query("select * from subject where estatus='1'  and id in (".rtrim($row['subject'],",").") ");
														}else{
															$row = $database->query("select * from subject where estatus='1' ");
														}
														while($data = mysqli_fetch_array($row))
														{
															?>
														<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['subject'])) { if($_REQUEST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['subject'];?></option>
														<?php
														}
														?>
													</select>
												</div>
											</div>
										</div>

										<?php 
										$data_sel1 =  $database->query("select * from users where username='".$session->username."'");
										$rowll1 = mysqli_fetch_array($data_sel1); 
										
										?>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Chapter</label>
												<div class="p-1 col-sm-4" id="aaa">
													
													
													<select class="form-control selectpicker1 chapter" name="chapter" value=""  multiple data-live-search="true"  id="chapter"  onChange="search_report1()" >
														<option value=''>-- Select --</option>
														<?php
														if($session->userlevel=='6'){
															$row = $database->query("select * from chapter where estatus='1'  and id  in (".rtrim($rowll1['chapter'],",").")  ");
														}else{
															$row = $database->query("select * from chapter where estatus='1' ".$sub." ");
														}
														
														while($data = mysqli_fetch_array($row))
														{
															?>
														<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['chapter1'])) { if(in_array($data['id'], $_REQUEST['chapter1'])) { echo 'selected="selected"'; } } ?>  ><?php echo $data['chapter'];?></option>
														
														<?php
														}
														?>
													</select>
												</div>
											</div>
										</div>
									</div>
									<?php
									if(isset($_REQUEST['chapter_topic'])) {
										if($_REQUEST['chapter_topic']!='' && $_REQUEST['chapter_topic']!='undefined'){
											$_REQUEST['chapter_topic']=$_REQUEST['chapter_topic'];
										}else{
											$_REQUEST['chapter_topic']=1;
										}
									}else{
										$_REQUEST['chapter_topic']=1;
									}
									?>
									<div class="row">
										
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Chapter/Topic</label>
												<div class="p-1 col-sm-4" id="aaa">
													
													
													<select class="form-control" name="chapter_topic" value=""   id="chapter_topic"  onChange="search_report1()" >
														<option value=''>-- Select --</option>
														<option value="1" <?php if(isset($_REQUEST['chapter_topic'])) { if($_REQUEST['chapter_topic']=='1') { echo 'selected="selected"'; }  } ?>  >Chapter</option>
														<option value="2" <?php if(isset($_REQUEST['chapter_topic'])) { if($_REQUEST['chapter_topic']=='2') { echo 'selected="selected"'; }  } ?>  >Topic</option>
														
													</select>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Exam Papers</label>
												<div class="p-1 col-sm-4" id="aaa">
													
													
													<select class="form-control " name="qset" value=""    id="qset"  onChange="search_report1()" >
														<option value=''>-- Select --</option>
														<option value='All' <?php if(isset($_REQUEST['qset'])) { if($_REQUEST['qset']=='All') { echo 'selected="selected"'; }  } ?>>All</option>
														<?php
														$row = $database->query("select * from previous_sets where estatus='1'  ");
														
														
														while($data = mysqli_fetch_array($row))
														{
															?>
														<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['qset'])) { if($_REQUEST['qset']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['qset'];?></option>
														<?php
														}
														?>
													</select>
												</div>
											</div>
										</div>
									</div>
									
									
									<div class="card border-0">
										<div class="card-body table-responsive table_data p-0">
											<table class="table table-bordered dashboard-table mb-0" id="dataTable">
												<thead class="thead-light">
													<tr>
														<th scope="col" >Sr.No.</th>
														<th scope="col">Subject</th>
														<th scope="col" >Chapter</th>
														<?php
														if(isset($_REQUEST['chapter_topic'])){
															if($_REQUEST['chapter_topic']=="2"){
																?>
																<th scope="col" >Topic</th>
																<?php
															}
														}
														?>
														
														<th scope="col" >Total Questions</th>
														<th scope="col" >Verified Questions</th>
														<th scope="col">Practice Test</th>
														<th scope="col">Practice Test(%)</th>
														<th scope="col">Cumulative Test</th>
														<th scope="col">Cumulative Test(%)</th>
														<th scope="col">Semi Grand Test</th>
														<th scope="col">Semi Grand Test(%)</th>
														<th scope="col">Grand Test</th>
														<th scope="col">Grand Test(%)</th>
														
														
													</tr>
													
												</thead>
											<tbody>
												<?php
												if(isset($_REQUEST['chapter'])){
													if($_REQUEST['chapter']!=""){
														$chapter=" AND id in (".$_REQUEST['chapter'].")";
														$chapter1=" AND chapter in (".$_REQUEST['chapter'].")";
													} else {
														$chapter="";
														$chapter1="";
													}
												}else{
													$chapter="";
													$chapter1="";
												}

												if(isset($_REQUEST['subject'])){
													if($_REQUEST['subject']!=""){
														$subject=" AND subject='".$_REQUEST['subject']."'";
													} else {
														$subject="";
													}
												}else{
													$subject="";
												}
												if(isset($_REQUEST['qset'])){
													if($_REQUEST['qset']!=""){
														if($_REQUEST['qset']!="All"){
															$qset=" AND ppaper='1' AND qset='".$_REQUEST['qset']."'";
														}else{
															$qset=" AND ppaper='1'";
														}
													} else {
														$qset="";
													}
												}else{
													$qset="";
												}
												$cond=$chapter.$subject;
												 $k=1;
												
													$sel = $database->query("SELECT id,subject,chapter FROM chapter WHERE estatus=1 ".$cond."  ORDER BY subject ASC");
													while($row = mysqli_fetch_array($sel)){
														if(isset($_REQUEST['chapter_topic'])){
															if($_REQUEST['chapter_topic']!=1){
																$topic_sel =  $database->query("SELECT id,topic FROM topic WHERE estatus=1  and chapter='".$row['id']."' ORDER BY id ASC");
																while( $topic_row = mysqli_fetch_array($topic_sel)){

																	$sub_sel =  $database->query("SELECT id,subject FROM subject WHERE estatus=1  and id ='".$row['subject']."' ORDER BY id ASC");

																$subject = mysqli_fetch_array($sub_sel);
																	if(strlen($qset)==0){
																		
																		$sql1= $database->query("select sum(total) as cnt from syllabus_totals_new where chapter='".$row['id']."' and topic='".$topic_row['id']."'  ");
																		$row1=mysqli_fetch_array($sql1);
																		
																		

																		$sql1v= $database->query("select ctotal as cnt from syllabus_totals_usageset1 where chapter='".$row['id']."'  and find_in_set(".$topic_row['id'].",topic)>0  group by topic");
																		$row1v=mysqli_fetch_array($sql1v);

																		

																		$sqll1= $database->query("select total  as cnt  from syllabus_totals_usageset1 where  chapter='".$row['id']."' and topic='".$topic_row['id']."'  and usage_set='1' ");
																		$rowl1=mysqli_fetch_array($sqll1);
																		
																		$sqll2= $database->query("select total as cnt  from syllabus_totals_usageset1 where chapter='".$row['id']."' and topic='".$topic_row['id']."'  and usage_set='3' ");
																		$rowl2=mysqli_fetch_array($sqll2);

																		$sqll3= $database->query("select total  as cnt from syllabus_totals_usageset1 where chapter='".$row['id']."' and topic='".$topic_row['id']."'  and usage_set='4' ");
																		$rowl3=mysqli_fetch_array($sqll3);

																		$sqll4= $database->query("select total as cnt  from syllabus_totals_usageset1 where chapter='".$row['id']."' and topic='".$topic_row['id']."'  and usage_set='5' ");
																		$rowl4=mysqli_fetch_array($sqll4);
																	}else{
																	
																		$sql1= $database->query("select count(id) as cnt from createquestion where  estatus='1' and find_in_set(".$row['id'].",chapter)>0 and find_in_set(".$topic_row['id'].",topic)>0 ".$qset."  and usageset!='0' ");
																		$row1=mysqli_fetch_array($sql1);

																		$sql1v= $database->query("select count(id) as cnt from createquestion where  estatus='1' and vstatus1='1'  and find_in_set(".$row['id'].",chapter)>0 and find_in_set(".$topic_row['id'].",topic)>0 ".$qset." and usageset!='0' ");
																		$row1v=mysqli_fetch_array($sql1v);

																		$sqll1= $database->query("select count(id) as cnt from createquestion where  estatus='1' and vstatus1='1'  and find_in_set(".$row['id'].",chapter)>0 and find_in_set(".$topic_row['id'].",topic)>0 ".$qset."  and usageset='1' ");
																		$rowl1=mysqli_fetch_array($sqll1);
																		
																		$sqll2= $database->query("select count(id) as cnt from createquestion where  estatus='1' and vstatus1='1'  and find_in_set(".$row['id'].",chapter)>0 and find_in_set(".$topic_row['id'].",topic)>0 ".$qset." and usageset='3' ");
																		$rowl2=mysqli_fetch_array($sqll2);

																		$sqll3= $database->query("select count(id) as cnt from createquestion where  estatus='1' and vstatus1='1'    and find_in_set(".$row['id'].",chapter)>0 and find_in_set(".$topic_row['id'].",topic)>0 ".$qset."  and usageset='4' ");
																		$rowl3=mysqli_fetch_array($sqll3);

																		$sqll4= $database->query("select count(id) as cnt from createquestion where  estatus='1' and vstatus1='1'  and find_in_set(".$row['id'].",chapter)>0 and find_in_set(".$topic_row['id'].",topic)>0 ".$qset."  and usageset='5' ");
																		$rowl4=mysqli_fetch_array($sqll4);
																	}

																	$percentp = $rowl1[0]/$row1v[0];
																	if(is_nan($percentp))
																		$percentp=0;
																	else
																		$percentp=$percentp;
																	$percent_vp= number_format( $percentp * 100) . '%';

																	$percentc = $rowl2[0]/$row1v[0];
																	if(is_nan($percentc))
																		$percentc=0;
																	else
																		$percentc=$percentc;
																	$percent_cp= number_format( $percentc * 100) . '%';

																	$percentsg = $rowl3[0]/$row1v[0];
																	if(is_nan($percentsg))
																		$percentsg=0;
																	else
																		$percentsg=$percentsg;
																	$percent_sg= number_format( $percentsg * 100) . '%';

																	$percentg = $rowl4[0]/$row1v[0];
																	if(is_nan($percentg))
																		$percentg=0;
																	else
																		$percentg=$percentg;
																	$percent_g= number_format( $percentg * 100) . '%';
																	echo "<tr>";
																	?>	
																	
																	<td ><?php echo $k;?></td>
																		<td ><?php echo $subject['subject'];?></td>
																		<td><?php echo $row['chapter']; ?></td>
																		<td><?php echo $topic_row['topic']; ?></td>
																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>usagesetreport/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $row['id'];?>&topic=<?php echo $topic_row['id'];?>&qset=<?php echo $_REQUEST['qset'];?>")' ><?php echo $row1['cnt']; ?></a></td>
																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>usagesetreport/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $row['id'];?>&topic=<?php echo $topic_row['id'];?>&status=1&qset=<?php echo $_REQUEST['qset'];?>")' ><?php echo $row1v['cnt']; ?></a></td>
																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank' onClick='window.open("<?php echo SECURE_PATH;?>usagesetreport/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $row['id'];?>&topic=<?php echo $topic_row['id'];?>&qset=<?php echo $_REQUEST['qset'];?>&usageset=1")' ><?php echo $rowl1['cnt']; ?></a></td>
																		<td><?php echo $percent_vp; ?></td>
																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank' onClick='window.open("<?php echo SECURE_PATH;?>usagesetreport/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $row['id'];?>&topic=<?php echo $topic_row['id'];?>&qset=<?php echo $_REQUEST['qset'];?>&usageset=3")' ><?php echo $rowl2['cnt']; ?></a></td>
																		<td><?php echo $percent_cp; ?></td>
																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank' onClick='window.open("<?php echo SECURE_PATH;?>usagesetreport/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $row['id'];?>&topic=<?php echo $topic_row['id'];?>&qset=<?php echo $_REQUEST['qset'];?>&usageset=4")' ><?php echo $rowl3['cnt']; ?></a></td>
																		<td><?php echo $percent_sg; ?></td>
																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank' onClick='window.open("<?php echo SECURE_PATH;?>usagesetreport/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $row['id'];?>&topic=<?php echo $topic_row['id'];?>&qset=<?php echo $_REQUEST['qset'];?>&usageset=5")' ><?php echo $rowl4['cnt']; ?></a></td>
																		<td><?php echo $percent_g; ?></td>
																		
																	<?php
																	echo "</tr>";
																	$k++;
																}
															}else{

																$sub_sel =  $database->query("SELECT id,subject FROM subject WHERE estatus=1  and id ='".$row['subject']."' ORDER BY id ASC");

																$subject = mysqli_fetch_array($sub_sel);
																	if(strlen($qset)==0){
																		$verify=0;
																		//$sql1= $database->query("select count(id) as cnt from createquestion where  estatus='1'  and find_in_set(".$row['id'].",chapter)>0   and usageset!='0' ");
																		//$row1=mysqli_fetch_array($sql1);
																		$sql1= $database->query("select chapter_total as cnt from syllabus_totals_new where chapter='".$row['id']."'   ");
																		$row1=mysqli_fetch_array($sql1);
																		$sql1v= $database->query("select sum(ctotal) as cnt from syllabus_totals_usageset1 where chapter='".$row['id']."'    group by topic");
																		$row1v=mysqli_fetch_array($sql1v);

																		$sqll1= $database->query("select ctotal  as cnt  from syllabus_totals_usageset1 where  chapter='".$row['id']."'   and usage_set='1' ");
																		$rowl1=mysqli_fetch_array($sqll1);
																		
																		$sqll2= $database->query("select ctotal as cnt  from syllabus_totals_usageset1 where chapter='".$row['id']."'   and usage_set='3' ");
																		$rowl2=mysqli_fetch_array($sqll2);

																		$sqll3= $database->query("select ctotal  as cnt from syllabus_totals_usageset1 where chapter='".$row['id']."'  and usage_set='4' ");
																		$rowl3=mysqli_fetch_array($sqll3);

																		$sqll4= $database->query("select ctotal as cnt  from syllabus_totals_usageset1 where chapter='".$row['id']."'   and usage_set='5' ");
																		$rowl4=mysqli_fetch_array($sqll4);
																	}else{
																	
																		$sql1= $database->query("select count(id) as cnt from createquestion where  estatus='1' and find_in_set(".$row['id'].",chapter)>0  ".$qset."  and usageset!='0' ");
																		$row1=mysqli_fetch_array($sql1);

																		$sql1v= $database->query("select count(id) as cnt from createquestion where  estatus='1' and vstatus1='1' and find_in_set(".$row['id'].",chapter)>0  ".$qset."  and usageset!='0' ");
																		$row1v=mysqli_fetch_array($sql1v);
																		$sqll1= $database->query("select count(id) as cnt from createquestion where  estatus='1' and vstatus1='1' and find_in_set(".$row['id'].",chapter)>0  ".$qset."  and usageset='1' ");
																		$rowl1=mysqli_fetch_array($sqll1);
																		
																		$sqll2= $database->query("select count(id) as cnt from createquestion where  estatus='1' and vstatus1='1' and find_in_set(".$row['id'].",chapter)>0  ".$qset." and usageset='3' ");
																		$rowl2=mysqli_fetch_array($sqll2);

																		$sqll3= $database->query("select count(id) as cnt from createquestion where  estatus='1' and vstatus1='1' and find_in_set(".$row['id'].",chapter)>0  ".$qset."  and usageset='4' ");
																		$rowl3=mysqli_fetch_array($sqll3);

																		$sqll4= $database->query("select count(id) as cnt from createquestion where  estatus='1'  and vstatus1='1' and find_in_set(".$row['id'].",chapter)>0  ".$qset."  and usageset='5' ");
																		$rowl4=mysqli_fetch_array($sqll4);
																	}

																	$percentp = $rowl1[0]/$row1v[0];
																	if(is_nan($percentp))
																		$percentp=0;
																	else
																		$percentp=$percentp;
																	$percent_vp= number_format( $percentp * 100) . '%';

																	$percentc = $rowl2[0]/$row1v[0];
																	if(is_nan($percentc))
																		$percentc=0;
																	else
																		$percentc=$percentc;
																	$percent_cp= number_format( $percentc * 100) . '%';

																	$percentsg = $rowl3[0]/$row1v[0];
																	if(is_nan($percentsg))
																		$percentsg=0;
																	else
																		$percentsg=$percentsg;
																	$percent_sg= number_format( $percentsg * 100) . '%';

																	$percentg = $rowl4[0]/$row1v[0];
																	if(is_nan($percentg))
																		$percentg=0;
																	else
																		$percentg=$percentg;
																	$percent_g= number_format( $percentg * 100) . '%';
																	echo "<tr>";
																	?>	
																	
																	<td ><?php echo $k;?></td>
																		<td ><?php echo $subject['subject'];?></td>
																		<td><?php echo $row['chapter']; ?></td>
																		
																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>usagesetreport/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $row['id'];?>&topic=<?php echo $topic_row['id'];?>&qset=<?php echo $_REQUEST['qset'];?>")' ><?php echo $row1['cnt']; ?></a></td>
																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>usagesetreport/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $row['id'];?>&topic=<?php echo $topic_row['id'];?>&status=1&qset=<?php echo $_REQUEST['qset'];?>")' ><?php echo $row1v['cnt']; ?></a></td>
																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank' onClick='window.open("<?php echo SECURE_PATH;?>usagesetreport/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $row['id'];?>&topic=<?php echo $topic_row['id'];?>&qset=<?php echo $_REQUEST['qset'];?>&usageset=1")' ><?php echo $rowl1['cnt']; ?></a></td>
																		<td><?php echo $percent_vp; ?></td>
																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank' onClick='window.open("<?php echo SECURE_PATH;?>usagesetreport/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $row['id'];?>&topic=<?php echo $topic_row['id'];?>&qset=<?php echo $_REQUEST['qset'];?>&usageset=3")' ><?php echo $rowl2['cnt']; ?></a></td>
																		<td><?php echo $percent_cp; ?></td>
																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank' onClick='window.open("<?php echo SECURE_PATH;?>usagesetreport/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $row['id'];?>&topic=<?php echo $topic_row['id'];?>&qset=<?php echo $_REQUEST['qset'];?>&usageset=4")' ><?php echo $rowl3['cnt']; ?></a></td>
																		<td><?php echo $percent_sg; ?></td>
																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank' onClick='window.open("<?php echo SECURE_PATH;?>usagesetreport/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $row['id'];?>&topic=<?php echo $topic_row['id'];?>&qset=<?php echo $_REQUEST['qset'];?>&usageset=5")' ><?php echo $rowl4['cnt']; ?></a></td>
																		<td><?php echo $percent_g; ?></td>
																		
																	<?php
																	echo "</tr>";
																	$k++;

															}
														}
														
													}
											?>
												
											</tbody>
										</table>
									
									
									</div>	
								</div>
								
								
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>
		
			
	<?php
	}
	if(isset($_REQUEST['getchapter'])){
		$sql=$database->query("select * from users where username='".$session->username."'");
		$rowl=mysqli_fetch_array($sql);
		if(isset($_REQUEST['subject'])){
			if($_REQUEST['subject']!=''){
				$subject= " AND subject='".$_REQUEST['subject']."'";
			}else{
				$subject='';
			}
		}else{
			$subject='';
		}
		?>
			<select class="form-control selectpicker1 chapter" name="chapter" value=""  multiple data-live-search="true"  id="chapter"  onChange="search_report1()" >
				<option value=''>-- Select --</option>
				<?php
				if($session->userlevel=='6'){
					$row = $database->query("select * from chapter where estatus='1'  and subject in (".rtrim($row['subject'],",").") and id in (".rtrim($rowl['chapter'],",").") ".$subject."");
				}else{
					echo "select * from chapter where estatus='1'".$subject." ";
					$row = $database->query("select * from chapter where estatus='1'".$subject."  ");
				}
				
				while($data = mysqli_fetch_array($row))
				{
					?>
				<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['chapter'])) { if(in_array($data['id'], $_REQUEST['chapter'])) { echo 'selected="selected"'; } } ?>  ><?php echo $data['chapter'];?></option>
				<?php
				}
				?>
			</select>
		<?php

	}
	?>
	<script>
	$(function () {
		$('.datepicker').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});

	
	</script>
	<script>
	function search_report1() {
	var subject = $('#subject').val();
	var chapter = $('#chapter').val();
	var qset = $('#qset').val();
	var chapter_topic = $('#chapter_topic').val();
	setStateGet('adminTable','<?php echo SECURE_PATH;?>usagesetreport/process.php','tableDisplay=1&chapter='+$('#chapter').val()+'&subject='+$('#subject').val()+'&qset='+$('#qset').val()+'&chapter_topic='+$('#chapter_topic').val()+'');
	
}
</script>

	<script>
	$(".selectpicker1").selectpicker('refresh');
	</script>