<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
?>

<style>
.card-header h6{
font-family: 'Varela Round', sans-serif !important;
}
.card{
	font-family: 'Varela Round', sans-serif !important;
}
</style>
	<?php
	$con = mysqli_connect("localhost","rspace","Rsp@2019","neetjee");
	//$con = mysqli_connect("localhost","root","","neetjee");
		function query($sql){
		global $con;
		 return mysqli_query($con,$sql);
		}
	if(isset($_REQUEST['addForm'])){
		
		
	
	?>
		
	
	
	
		<section class="content-area">
			<div class="container">
				<div class="row Data-Tables">
					<div class="col-xl-12 col-lg-12"> 
						<div class="card border-0 shadow mb-4">
							<div class="card-header py-3">
								<h6 class="m-0 font-weight-bold text-primary"> Institute Exam Paper Analysis</h6>
							</div>
							<div class="card-body">
								
								<div class="form-row">
									<div class="col-md-3 mb-3">
									  <label for="validationCustom01">Institute</label>
									
									  	<select id="institute_id" name="institute_id" class="form-control" onchange="setState('institutediv','<?php echo SECURE_PATH;?>institute_epaper/process.php','institutedata=1&institute_id='+$('#institute_id').val()+'');" >
											<option value=''>-Select-</option>
											<?php
											$sql=$database->query("select id,institute_name from institute where estatus='1'");
											while($row=mysqli_fetch_array($sql)){
											?>
												<option value='<?php echo $row['id']; ?>' <?php if(isset($_REQUEST['institute_id'])) { if($_REQUEST['institute_id']==$row['id']) { echo 'selected="selected"'; }  } ?>><?php echo $row['institute_name']; ?></option>
											<?php
												
											}
											?>
										</select>
								
									</div>
									<div class="col-md-3 mb-3">
									  <label for="validationCustom02">Exam</label>
										<select id="exam" name="exam" class="form-control"  onclick="examtypediv();" onchange="setState('institutediv','<?php echo SECURE_PATH;?>institute_epaper/process.php','institutedata=1&institute_id='+$('#institute_id').val()+'&exam='+$('#exam').val()+'');" >
											<option value=''>-Select-</option>
											<?php
											if($session->userlevel==3){
												if($rowsel['subject']=='4'){
													$sql=$database->query("select id,exam from exam where estatus='1' and id in (2,3,4) order by id asc");
												}else if($rowsel['subject']=='1' OR $rowsel['subject']=='5'){
													$sql=$database->query("select id,exam from exam where estatus='1' and id in (1,3,4) order by id asc");
												}else{
													$sql=$database->query("select id,exam from exam where estatus='1'  order by id asc");
												}
											}else{
												$sql=$database->query("select id,exam from exam where estatus='1' order by id asc");
											}
											while($row=mysqli_fetch_array($sql)){
											?>
												<option value='<?php echo $row['id']; ?>' <?php if(isset($_REQUEST['exam'])) { if($_REQUEST['exam']==$row['id']) { echo 'selected="selected"'; }  } ?>><?php echo $row['exam']; ?></option>
											<?php
												
											}
											?>
										</select>
									  
									</div>
									<?php
									if(isset($_REQUEST['examtype'])){
										if($_REQUEST['examtype']!=''){
											if($_REQUEST['examtype']=='2'){
												$style_examtype='';
											}else{
												$style_examtype='style="display:none;"';
											}
											
										}else{
											$style_examtype='style="display:none;"';
										}
									}else{
										$style_examtype='style="display:none;"';
									}
									?>
									<div class="col-md-3 mb-3 " <?php echo $style_examtype; ?>  id="examtypedatadiv" onchange="setState('institutediv','<?php echo SECURE_PATH;?>institute_epaper/process.php','institutedata=1&institute_id='+$('#institute_id').val()+'&exam='+$('#exam').val()+'&examtype='+$('#examtype').val()+'');" >
									  <label for="validationCustom02">Exam Type</label>
									  <select id="examtype" name="examtype" class="form-control" >
											<option value=''>-Select-</option>
											<option value='1' <?php if(isset($_REQUEST['examtype'])) { if($_REQUEST['examtype']==1) { echo 'selected="selected"'; }  } ?>>Mains</option>
											<option value='2' <?php if(isset($_REQUEST['examtype'])) { if($_REQUEST['examtype']==2) { echo 'selected="selected"'; }  } ?>>Advance</option>
										</select>
									  
									</div>
									<div class="col-md-3 mb-3"  id="institutediv" >
									  <label for="validationCustom02"><span style="color:red;">*</span>Exam Name</label>
									 <select id="exampaper" name="exampaper" class="form-control"  >
									 	<option value=''>-Select-</option>
											<?php
											$sql=query("select id,exam_name from exam_paper where estatus='1' and exam_name!=''");
											while($row=mysqli_fetch_array($sql)){
											?>
												<option value='<?php echo $row['id']; ?>' <?php if(isset($_REQUEST['exampaper'])) { if($_REQUEST['exampaper']==$row['id']) { echo 'selected="selected"'; }  } ?>><?php echo $row['exam_name']; ?></option>
											<?php
												
											}
											?>
										</select>
										<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['exampaper'])){ echo $_SESSION['error']['exampaper'];}?></span>
									</div>
								 </div>
								 <div class="text-right col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<a type="button" class="btn-blue my-2 px-5 btn btn-primary text-white" onclick="setState('adminTable','<?php echo SECURE_PATH;?>institute_epaper/process.php','tableDisplay=1&institute_id='+$('#institute_id').val()+'&exam='+$('#exam').val()+'&examtype='+$('#examtype').val()+'&exampaper='+$('#exampaper').val()+'');">Submit</a>
								</div> 
								 
									
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>
		
		<?php
			unset($_SESSION['error']);
	}
		?>
	<?php
		if(isset($_REQUEST['tableDisplay'])){
			$field = 'exampaper';
			if(!$_REQUEST['exampaper'] || strlen(trim($_REQUEST['exampaper'])) == 0){
			  $_SESSION['error'][$field] = "* Please select Exampaper";
			}
			
			if(count($_SESSION['error']) > 0){
			?>
				<script type="text/javascript">
				$('#adminForm').slideDown();

				setState('adminForm','<?php echo SECURE_PATH;?>institute_epaper/process.php','addForm=1&exampaper=<?php echo $_REQUEST['exampaper'];?>&exam=<?php echo $_REQUEST['exam'];?>&institute_id=<?php echo $_REQUEST['institute_id']; ?>&examtype=<?php echo $_REQUEST['examtype']; ?>')
				</script>

			<?php
			}else{
				
						if(isset($_REQUEST['institute_id'])){
							if($_REQUEST['institute_id']!=''){
								$inst_con=" AND institution_id='".$_REQUEST['institute_id']."'";
								
							}else{
								$inst_con="";
								
							}
						}else{
							$inst_con="";
							
						}
						if(isset($_REQUEST['exam'])){
							if($_REQUEST['exam']!=''){
								$exam=" AND exam_type='".$_REQUEST['exam']."'";
								
							}else{
								$exam="";
								
							}
						}else{
							$exam="";
							
						}
						if(isset($_REQUEST['examtype'])){
							if($_REQUEST['examtype']!=''){
								$examtype=" AND sub_exam_type='".$_REQUEST['examtype']."'";
								
							}else{
								$examtype="";
								
							}
						}else{
								$examtype="";
								
								
							}

						if(isset($_REQUEST['exampaper'])){
							if($_REQUEST['exampaper']!=''){
								$exampaper=" AND exam_paper_id IN (".$_REQUEST['exampaper'].")";
								
							}else{
								$exampaper="";
								
							}
						}else{
								$exampaper="";
								
						}
						$sqle=query("select exam_type from exam_paper where estatus='1' and id='".$_REQUEST['exampaper']."'");
						$rowe=mysqli_fetch_array($sqle);
						if($rowe['exam_type']=='1'){
							$sub="1,2,3,5";
						}else if($rowe['exam_type']=='2'){
							$sub="2,3,4";
						}else{
							$sub="1,2,3,4,5";
						}
						
						?>
						<script type="text/javascript">
						$('#example').DataTable({
							"paging":   false,
							"searching": false,
							"bInfo": false
							
						});

						$('#example1').DataTable({
							"paging":   false,
							"searching": false,
							"bInfo": false
							
						});

						$('#example2').DataTable({
							"paging":   false,
							"searching": false,
							"bInfo": false
							
						});
						
						</script>
						<div class="container">
							<div class="text-right pt-2 pb-2" >
							<a href="<?php echo SECURE_PATH;?>institute_epaper/report.php?exampaper=<?php echo $_REQUEST['exampaper']; ?>&userlevel=<?php echo $session->userlevel; ?>&username=<?php echo $session->username; ?>" style="float:center" target="_blank"  title="PDF Export" ><i class="text-center fa fa-file-pdf-o" style="font-size: 25px;color:red;"></i></a>
							</div>
						</div>
						<section class="content-area">
							<div class="container">
								<div class="row Data-Tables">
									<div class="col-xl-12 col-lg-12"> 
										<div class="card border-0 shadow mb-4">
											<div class="card-header py-3">
												<h6 class="m-0 font-weight-bold text-primary">Chapter Wise Weightage</h6>
											</div>
											<div class="card-body">
												
												<div class="accordion" id="accordionExample">
													<?php
													$i=1;
														$sql=$database->query("select * from subject where estatus='1'  and id in (".$sub.")");
														while($row=mysqli_fetch_array($sql)){
															if($i==1){
																$classd="show";
																$icon="fa-minus";
															}else{
																$classd="";
																$icon="fa-plus";
															}
															if($row['id']=='1'){
																$image=SECURE_PATH."images/botany.png";
															}else if($row['id']=='2'){
																$image=SECURE_PATH."images/physics.png";
															}else if($row['id']=='3'){
																$image=SECURE_PATH."images/chemistry.png";
															}else if($row['id']=='4'){
																$image=SECURE_PATH."images/maths.png";
															}else if($row['id']=='5'){
																$image=SECURE_PATH."images/zoology.png";
															}
													?>
													<script>
													$('#dataTable<?php echo $row['id']; ?>').DataTable({
														"paging":   false,
														"searching": false,
														"bInfo": false
														
													});
													</script>
													
													<div class="card">
														<div class="card-header "  id="heading<?php echo $i; ?>" onclick="hideshow();">
														<h2 class="mb-0">
															<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="true" aria-controls="collapseOne">
															<?php echo $row['subject']; ?>
															</button><i 
														</h2>
														</div>

														<div id="collapse<?php echo $i; ?>" class="collapse <?php echo $classd; ?>" aria-labelledby="heading<?php echo $i; ?>" data-parent="#accordionExample">
														<div class="card-body">
														<table id="dataTable<?php echo $row['id']; ?>" class="table table-striped table-bordered" style="width:100%">
																	<thead>
																		<tr>
																			
																			<th>Class</th>
																			<th>Chapter</th>
																			<th>Qstn</th>
																			<th>Percentage</th>
																		</tr>
																		</tr>
																	</thead>
																	<tbody>
																		
																		
																	
																		
																		<?php
																		$i=1;
																		$sqlcom=$database->query("select * from chapter where  estatus='1' and subject='".$row['id']."'");
																		while($rowcom=mysqli_fetch_array($sqlcom)){
																			$sqlqs=query("select count(id) as cnt from question_paper where estatus='1'   and subject_id='".$row['id']."' ".$exampaper."");
																			$rowqs=mysqli_fetch_array($sqlqs);

																			if($rowcom['class']=='1'){
																				$class="XI";
																			}else{
																				$class="XII";
																			}
																			$sqlq1_tot=query("select count(id) as cnt from question_paper where estatus='1'  and  find_in_set(".$rowcom['id'].",chapter)>0 and subject_id='".$row['id']."' ".$exampaper."");
																			$rowq1_tot=mysqli_fetch_array($sqlq1_tot);
																			$per=round(($rowq1_tot['cnt']/$rowqs['cnt'])*100);
																			
																			if(is_nan($per)){
																				$per1=0;
																			}else{
																				$per1=$per;
																			}
																			if($rowq1_tot['cnt']>0){
																				?>
																				<tr>
																					<td><?php echo $class; ?></td>
																					<td><?php echo $rowcom['chapter']; ?></td>
																					<td><a class="text-primary"  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>institute_epaper/process1.php','viewDetails=1&exampaper=<?php echo $_REQUEST['exampaper']; ?>&subject=<?php echo $row['id']; ?>&chapter=<?php echo $rowcom['id']; ?>')"><?php echo $rowq1_tot['cnt']; ?></a></td>
																					<?php
																					
																				
																					//echo $rowq1_tot['cnt'];
																					
																					
																					
																					?>
																						<td><?php echo $per1; ?> %</td>
																				</tr>
																				
																				<?php
																					$i++;
																				}
																			}
																		?>
																	
																	
																	</tbody>	
																	
																	
																</table>
														</div>
														</div>
													</div>
													
													<?php $i++; } ?>  
													
												</div>
												
											</div>
											
											
										</div>
									</div>
								</div>

							</div>
						</section>
						<section class="content-area">
							<div class="container">
								<div class="row Data-Tables">
									<div class="col-xl-12 col-lg-12"> 
										<div class="card border-0 shadow mb-4">
											<div class="card-header py-3">
												<h6 class="m-0 font-weight-bold text-primary">Complexity</h6>
											</div>
											<div class="card-body">
											<table id="example" class="table table-striped table-bordered" style="width:100%">
													<thead>
														<tr>
															<th>Complexity</th>
															<th>Qstn</th>
															<?php
															$sql=$database->query("select * from subject where estatus='1' and id in (".$sub.")");
															while($row=mysqli_fetch_array($sql)){
																echo '<th>'.$row['subject'].'</th>';
															}
															?>
														</tr>
													</thead>
													<tbody>
														
														<?php
															$i=1;
															$tcount='';
														$sqlcom=$database->query("select * from complexity where  estatus='1'");
														while($rowcom=mysqli_fetch_array($sqlcom)){
															
															$q1='';

															$sqlq=query("select question_id from question_paper where estatus='1' and exam_paper_id='".$_REQUEST['exampaper']."' and subject_id!=''");
															while($rowqcom=mysqli_fetch_array($sqlq)){
																$q1.=$rowqcom['question_id'].",";
															}
															
															$sqlee=$database->query("select count(id) as cnt from createquestion where estatus='1'  and id in (".rtrim($q1,",").") and complexity='".$rowcom['id']."' and subject!='' ");
															$rowcount=mysqli_fetch_array($sqlee);
															$tcount=$tcount+$rowcount['cnt'];
															
														?>
														<tr class="<?php echo $style; ?>">
															<td><?php echo $rowcom['complexity']; ?></td>
															<td><a class="text-primary"  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>institute_epaper/process2.php','viewDetails=1&exampaper=<?php echo $_REQUEST['exampaper']; ?>&complexity=<?php echo $rowcom['id']; ?>')"><?php echo $rowcount['cnt']; ?></a></td>
															<?php
															
															$sql=$database->query("select * from subject where estatus='1' and id in (".$sub.")");
															while($row=mysqli_fetch_array($sql)){
																$q2='';

																$sqlq1=query("select question_id from question_paper where estatus='1' and exam_paper_id='".$_REQUEST['exampaper']."' and subject_id='".$row['id']."'");
																while($rowqcom1=mysqli_fetch_array($sqlq1)){
																	$q2.=$rowqcom1['question_id'].",";
																}

																$sqlee2=$database->query("select count(id) as cnt from createquestion where estatus='1'  and id in (".rtrim($q2,",").")  and subject='".$row['id']."' ");
																$rowcount2=mysqli_fetch_array($sqlee2);
																
																$sqlee1=$database->query("select count(id) as cnt from createquestion where estatus='1'  and id in (".rtrim($q2,",").") and complexity='".$rowcom['id']."' and subject='".$row['id']."' ");
																$rowcount1=mysqli_fetch_array($sqlee1);
																$per=round(($rowcount1['cnt']/$rowcount2['cnt'])*100);
																if(is_nan($per)){
																	$per1=0;
																}else{
																	$per1=$per;
																}
																?>
																<td><a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>institute_epaper/process2.php','viewDetails=1&exampaper=<?php echo $_REQUEST['exampaper']; ?>&complexity=<?php echo $rowcom['id']; ?>&subject=<?php echo $row['id']; ?>')"><?php echo $rowcount1['cnt']; ?></a> <small style="color:blue;">(<?php echo $per1; ?> %)</small></td>
																<?php
																
															}
															?>
														</tr>
														
														<?php
															$i++;
														}
														
														?>
													
													<tr> 
															<td>Total</td>
															<td><a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>institute_epaper/process2.php','viewDetails=1&exampaper=<?php echo $_REQUEST['exampaper']; ?>')"><?php echo $tcount; ?></a></td>
															<?php
															$sql=$database->query("select * from subject where estatus='1' and id in (".$sub.")");
															while($row=mysqli_fetch_array($sql)){
																$sqlq1=query("select question_id from question_paper where estatus='1' and exam_paper_id='".$_REQUEST['exampaper']."' and subject_id='".$row['id']."'");
																while($rowqcom1=mysqli_fetch_array($sqlq1)){
																	$q2.=$rowqcom1['question_id'].",";
																}
																$sqlee2=$database->query("select count(id) as cnt from createquestion where estatus='1'  and id in (".rtrim($q2,",").")  and subject='".$row['id']."' ");
																$rowcount2=mysqli_fetch_array($sqlee2);
																
																$sqlee1=$database->query("select count(id) as cnt from createquestion where estatus='1'  and id in (".rtrim($q2,",").") and complexity!='' and subject='".$row['id']."' ");
																$rowcount1=mysqli_fetch_array($sqlee1);
																$per=round(($rowcount1['cnt']/$rowcount2['cnt'])*100);
																if(is_nan($per)){
																	$per1=0;
																}else{
																	$per1=$per;
																}
																?>
																<td><a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>institute_epaper/process2.php','viewDetails=1&exampaper=<?php echo $_REQUEST['exampaper']; ?>&subject=<?php echo $row['id']; ?>')"><?php echo $rowcount1['cnt']; ?></a> <small style="color:blue;">(<?php echo $per1; ?> %)</small></td>
																<?php
															}
															?>
														</tr>
												</table>
												
												
											</div>
											
											
										</div>
									</div>
								</div>

							</div>
						</section>
						<section class="content-area">
							<div class="container">
								<div class="row Data-Tables">
									<div class="col-xl-12 col-lg-12"> 
										<div class="card border-0 shadow mb-4">
											<div class="card-header py-3">
												<h6 class="m-0 font-weight-bold text-primary">Question Type</h6>
											</div>
											<div class="card-body">
											<table id="example" class="table table-striped table-bordered" style="width:100%">
													<thead>
														<tr>
															<th>Question Type</th>
															<th>Qstn</th>
															<?php
															$sql=$database->query("select * from subject where estatus='1' and id in (".$sub.")");
															while($row=mysqli_fetch_array($sql)){
																echo '<th>'.$row['subject'].'</th>';
															}
															?>
														</tr>
													</thead>
													<tbody>
														
														<?php
															$i=1;
														$sqlcom=$database->query("select * from questiontype where  estatus='1'");
														while($rowcom=mysqli_fetch_array($sqlcom)){
															$qtype='';

															$sqlq=query("select question_id from question_paper where estatus='1' and exam_paper_id='".$_REQUEST['exampaper']."' and subject_id!=''");
															while($rowqcom=mysqli_fetch_array($sqlq)){
																$qtype.=$rowqcom['question_id'].",";
															}
															
															$sqleeqt=$database->query("select count(id) as cnt from createquestion where estatus='1'  and id in (".rtrim($qtype,",").") and find_in_set(".$rowcom['id'].",inputquestion)>0 and subject!='' and inputquestion!='' ");
															$rowcountqt=mysqli_fetch_array($sqleeqt);
														if($rowcountqt['cnt']>0){	
														?>
														<tr class="<?php echo $style; ?>">
															<td><?php echo $rowcom['questiontype']; ?></td>
															<td><a class="text-primary"  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>institute_epaper/process2.php','viewDetails=1&exampaper=<?php echo $_REQUEST['exampaper']; ?>&questiontype=<?php echo $rowcom['id']; ?>')"><?php echo $rowcountqt['cnt']; ?></a></td>
															<?php
															$sql=$database->query("select * from subject where estatus='1' and id in (".$sub.")");
															while($row=mysqli_fetch_array($sql)){
																$qqt1='';
																$sqlq1=query("select question_id from question_paper where estatus='1' and exam_paper_id='".$_REQUEST['exampaper']."' and subject_id='".$row['id']."' order by question_id asc");
																while($rowqcom1=mysqli_fetch_array($sqlq1)){
																	$qqt1.=$rowqcom1['question_id'].",";
																}
																$sqlee21=$database->query("select count(id) as cnt from createquestion where estatus='1'  and id in (".rtrim($qqt1,",").")  and subject='".$row['id']."' and inputquestion!=''");
																$rowcount21=mysqli_fetch_array($sqlee21);
																$sqlee1=$database->query("select count(id) as cnt from createquestion where estatus='1'  and id in (".rtrim($qqt1,",").") and find_in_set(".$rowcom['id'].",inputquestion)>0 and subject='".$row['id']."' ");
																$rowcountee=mysqli_fetch_array($sqlee1);
																
																$per=round(($rowcountee['cnt']/$rowcount21['cnt'])*100);
																if(is_nan($per)){
																	$per1=0;
																}else{
																	$per1=$per;
																}
																?>
																<td><a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>institute_epaper/process2.php','viewDetails=1&exampaper=<?php echo $_REQUEST['exampaper']; ?>&subject=<?php echo $row['id']; ?>&questiontype=<?php echo $rowcom['id']; ?>')"><?php echo $rowcountee['cnt']; ?></span> <small style="color:blue;">(<?php echo $per1; ?> %)</small></td>
																<?php
																
															}
															?>
														</tr>
														
														<?php
															$i++;
															}
														}
														?>
													
													
												</table>
												
												
											</div>
											
											
										</div>
									</div>
								</div>

							</div>
						</section>

						

						<section class="content-area">
							<div class="container">
								<div class="row Data-Tables">
									<div class="col-xl-12 col-lg-12"> 
										<div class="card border-0 shadow mb-4">
											<div class="card-header py-3">
												<h6 class="m-0 font-weight-bold text-primary">Question Theory</h6>
											</div>
											<div class="card-body">
												
												<table id="example1" class="table table-striped table-bordered" style="width:100%">
													<thead>
														<tr>
															<th>Question Theory</th>
															<th>Qstn</th>
															<?php
															$sql=$database->query("select * from subject where estatus='1' and id in (".$sub.")");
															while($row=mysqli_fetch_array($sql)){
																echo '<th>'.$row['subject'].'</th>';
															}
															?>
														</tr>
													</thead>
													<tbody>
														
														<?php
															$i=1;
														$sqlcom=$database->query("SELECT * FROM question_theory where  estatus='1'");
														while($rowcom=mysqli_fetch_array($sqlcom)){
															
															$qt='';

															$sqlq=query("select question_id from question_paper where estatus='1' and exam_paper_id='".$_REQUEST['exampaper']."' and subject_id!='' ");
															while($rowqcom=mysqli_fetch_array($sqlq)){
																$qt.=$rowqcom['question_id'].",";
															}
															
															$sqlee=$database->query("select count(id) as cnt from createquestion where estatus='1'  and id in (".rtrim($qt,",").") and question_theory='".$rowcom['id']."' and subject!='' ");
															$rowcount=mysqli_fetch_array($sqlee);
														?>
														<tr class="<?php echo $style; ?>">
															<td><?php echo $rowcom['question_theory']; ?></td>
															<td><a class="text-primary"  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>institute_epaper/process2.php','viewDetails=1&exampaper=<?php echo $_REQUEST['exampaper']; ?>&question_theory=<?php echo $rowcom['id']; ?>')"><?php echo $rowcount['cnt']; ?></a></td>
															<?php
															$sql=$database->query("select * from subject where estatus='1' and id in (".$sub.")");
															while($row=mysqli_fetch_array($sql)){
																
																$qth='';

																$sqlq1=query("select question_id from question_paper where estatus='1' and exam_paper_id='".$_REQUEST['exampaper']."' and subject_id='".$row['id']."'");
																while($rowqcom1=mysqli_fetch_array($sqlq1)){
																	$qth.=$rowqcom1['question_id'].",";
																}
																$sqlee2=$database->query("select count(id) as cnt from createquestion where estatus='1'  and id in (".rtrim($qth,",").")  and subject='".$row['id']."' ");
																$rowcount2=mysqli_fetch_array($sqlee2);
																$sqlee1=$database->query("select count(id) as cnt from createquestion where estatus='1'  and id in (".rtrim($qth,",").") and question_theory='".$rowcom['id']."' and subject='".$row['id']."' ");
																$rowcount1=mysqli_fetch_array($sqlee1);
																$per=round(($rowcount1['cnt']/$rowcount2['cnt'])*100);
																if(is_nan($per)){
																	$per1=0;
																}else{
																	$per1=$per;
																}	

																
																?>
																<td><a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>institute_epaper/process2.php','viewDetails=1&exampaper=<?php echo $_REQUEST['exampaper']; ?>&question_theory=<?php echo $rowcom['id']; ?>&subject=<?php echo $row['id']; ?>')"><?php echo $rowcount1['cnt']; ?> </a><small style="color:blue;">(<?php echo $per1; ?> %)</small></td>
																<?php
															}
															?>
														</tr>
														
														<?php
															$i++;
														}
														?>
													
													
												</table>
												
											</div>
											
											
										</div>
									</div>
								</div>

							</div>
						</section>
						
						
				<?php	
				
			}
	}
	
	
	if(isset($_REQUEST['institutedata'])){
	
		if(isset($_REQUEST['institute_id'])){
			if($_REQUEST['institute_id']!=''){
				$_REQUEST['institute_id']=$_REQUEST['institute_id'];
				$insti_con=" AND institution_id='".$_REQUEST['institute_id']."'";
			}else{
				$insti_con="";
			}
		}else{
			$insti_con="";
		}
		if(isset($_REQUEST['exam'])){
			if($_REQUEST['exam']!=''){
				$_REQUEST['exam']=$_REQUEST['exam'];
				$exam=" AND exam_type='".$_REQUEST['exam']."'";
			}else{
				$exam="";
			}
		}else{
			$exam="";
		}
			if(isset($_REQUEST['examtype'])){
			if($_REQUEST['examtype']!='' && $_REQUEST['examtype']!='0'){
				$_REQUEST['examtype']=$_REQUEST['examtype'];
				$examtype=" AND sub_exam_type='".$_REQUEST['examtype']."'";
			}else{
				$examtype="";
			}
		}else{
			$examtype="";
		}
		?>
			<label for="validationCustom02">Exam Name</label>
			 <select id="exampaper" name="exampaper" class="form-control" >
				<option value=''>-Select-</option>
				<?php
				$sql=query("select id,exam_name from exam_paper where estatus='1' and exam_name!='' ".$insti_con.$exam.$examtype." order by timestamp desc");
				if(mysqli_num_rows($sql)){
					while($row=mysqli_fetch_array($sql)){
					
					?>
						<option value='<?php echo $row['id']; ?>' <?php if(isset($_REQUEST['exampaper'])) { if($_REQUEST['exampaper']==$row['id']) { echo 'selected="selected"'; }  } ?>><?php echo $row['exam_name']; ?></option>
					<?php
					}
				}
				?>
			</select>
		<?php
	}
	?>
	<script>
	function examtypediv(){
		var exam=$('#exam').val();
		if(exam=='2'){
			$('#examtypedatadiv').show();
		}else{
			$('#examtypedatadiv').hide();
		}

	}
	
	</script>
