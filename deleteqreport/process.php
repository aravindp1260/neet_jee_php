<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}

?>
<script>

    $(function () {
		$('.datepicker').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});
	function search_report() {
		
			var class1 = $('#class1').val();
			var subject1 = $('#subject1').val();
			var chapter1 = $('#chapter1').val();
			var date = $('#date').val();
			var exam = $('#exam').val();
			var explanation = $('#explanation').val();
			var complexity = $('#complexity').val();
			var usageset = $('#usageset').val();
			var pagerows = $('#pagerows').val();
			var question_id = $('#question_id').val();
			var page = $('#page').val();
			var set = $('#set').val();
			setStateGet('adminTable','<?php echo SECURE_PATH;?>deleteqreport/process.php','tableDisplay=1&class1='+class1+'&date='+date+'&exam='+exam+'&pagerows='+pagerows+'&subject1='+subject1+'&explanation='+explanation+'&set='+$('#set').val()+'&complexity='+complexity+'&usageset='+usageset+'&question_id='+question_id+'&chapter1='+chapter1+'&page='+page);
		}
	</script>
<?php
//Delete createticket
//Display bulkreport
if(isset($_GET['tableDisplay'])){
	$sql=$database->query("select * from users where username='".$_SESSION['username']."'");
	$rowsel=mysqli_fetch_array($sql);
	$subject=explode(",",$rowsel['subject']);
	if($_REQUEST['exam']!=''){
		$_REQUEST['exam']=$_REQUEST['exam'];
	}else{
		$_REQUEST['exam']=$_REQUEST['exam1'];
	}
	if($_REQUEST['complexity']!=''){
		$_REQUEST['complexity']=$_REQUEST['complexity'];
	}else{
		$_REQUEST['complexity']=$_REQUEST['complexity1'];
	}
	if($_REQUEST['usageset']!=''){
		$_REQUEST['usageset']=$_REQUEST['usageset'];
	}else{
		$_REQUEST['usageset']=$_REQUEST['usageset1'];
	}
	if($_REQUEST['question_theory']!=''){
		$_REQUEST['question_theory']=$_REQUEST['question_theory'];
	}else{
		$_REQUEST['question_theory']=$_REQUEST['question_theory1'];
	}
	?>
	
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12"> 
					<div class="card border-0 shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary"> Deleted  Question Details</h6>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Date</label>
										<div class="col-sm-8">
											
											<input type="text" name="date" id="date" class="datepicker form-control" value="<?php echo $_REQUEST['date'];?>" onblur="search_report();" placeholder="Select Date" >
											<span class="error"><?php if(isset($_SESSION['error']['class1'])){ echo $_SESSION['error']['class1'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Class</label>
										<div class="col-sm-8">
											<select class="form-control" name="class1" value=""  id="class1" onchange="search_report();setState('aaa1','<?php echo SECURE_PATH;?>deleteqreport/process.php','getsubject1=1&class='+$('#class1').val()+'&exam='+$('#exam').val()+'');"">
												<option value=''>-- Select --</option>
												<?php
												$row = $database->query("select * from class where estatus='1'");
												while($data = mysqli_fetch_array($row))
												{
													?>
												<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['class1'])) { if($_REQUEST['class1']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['class'];?></option>
												<?php
												}
												?>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['class1'])){ echo $_SESSION['error']['class1'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Exam</label>
										<div class="col-sm-8">
											<select class="form-control" name="exam" value=""  id="exam" onchange="search_report();setState('aaa1','<?php echo SECURE_PATH;?>deleteqreport/process.php','getsubject1=1&class='+$('#class1').val()+'&exam='+$('#exam').val()+'');"">
												<option value=''>-- Select --</option>
												<option value='1' <?php if(isset($_REQUEST['exam'])) { if($_REQUEST['exam']=='1') { echo 'selected="selected"'; }  } ?>>NEET</option>
												<option value='2' <?php if(isset($_REQUEST['exam'])) { if($_REQUEST['exam']=='2') { echo 'selected="selected"'; }  } ?>>JEE</option>
												
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['exam'])){ echo $_SESSION['error']['exam'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4" >
									<div class="form-group row" id="aaa1">
										<label class="col-sm-4 col-form-label">Subject</label>
										<div class="col-sm-8">
										<select class="form-control" name="subject1" value=""   id="subject1" onChange="search_report();setState('fff','<?php echo SECURE_PATH;?>deleteqreport/process.php','getsubchapter=1&class='+$('#class1').val()+'&subject='+$('#subject1').val()+'');">
											<option value=''>-- Select --</option>
											<?php
											$row = $database->query("select * from subject where estatus='1'");
											while($data = mysqli_fetch_array($row))
											{
												?>
											<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['subject1'])) { if($_REQUEST['subject1']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['subject'];?></option>
											<?php
											}
											?>
										</select>
											<span class="error"><?php if(isset($_SESSION['error']['subject1'])){ echo $_SESSION['error']['subject1'];}?></span>
										</div>
									</div>
								</div>							
								<div class="col-lg-4" >
									<div class="form-group row" id="fff">
										<label class="col-sm-4 col-form-label">Chapter</label>
										<div class="col-sm-8">
										<select class="form-control" name="chapter1" value=""   id="chapter1" onChange="search_report();">
												<option value=''>-- Select --</option>
												<?php
												if($_REQUEST['class1']!='' && $_REQUEST['subject1']=='' ){
													$row = $database->query("select * from chapter where estatus='1' and class='".$_REQUEST['class1']."'");
												}else if($_REQUEST['class1']=='' && $_REQUEST['subject1']!='' ){
													$row = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and subject = '".$_REQUEST['subject1']."' ");
													
												}else if($_REQUEST['subject1']!='' && $_REQUEST['class1']!==''){
													
													$row = $database->query("select * from chapter where estatus='1' and class='".$_REQUEST['class1']."' and subject='".$_REQUEST['subject1']."'");

												}else{
													$row = $database->query("select * from chapter where estatus='1'");
												}
												while($data = mysqli_fetch_array($row))
												{
													?>
												<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['chapter1'])) { if($_REQUEST['chapter1']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['chapter'];?></option>
												<?php
												}
												?>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['chapter1'])){ echo $_SESSION['error']['chapter1'];}?></span>
										</div>
									</div>
								</div>
								
								<div class="col-lg-4" >
									<div class="form-group row" id="ccc1">
										<label class="col-sm-4 col-form-label">Explanation</label>
										<div class="col-sm-8">
										<select class="form-control" name="explanation" value=""   id="explanation" onChange="search_report();">
												<option value=''>-- Select --</option>
												<option value='1' <?php if(isset($_REQUEST['explanation'])) { if($_REQUEST['explanation']=='1') { echo 'selected="selected"'; }  } ?>>With Explanation</option>
												<option value='2' <?php if(isset($_REQUEST['explanation'])) { if($_REQUEST['explanation']=='2') { echo 'selected="selected"'; }  } ?>>With Out Explanation</option>
												
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4" >
									<div class="form-group row" id="ccc1">
										<label class="col-sm-4 col-form-label">Usage Set</label>
										<div class="col-sm-8">
										<select class="form-control" name="usageset" value=""   id="usageset" onChange="search_report();">
											<option value=''>-- Select --</option>
												<?php
												$uageset = $database->query("SELECT * FROM question_useset WHERE estatus='1' "); 
												
												while($datauageset = mysqli_fetch_array($uageset))
												{
													?>
												<option value="<?php echo $datauageset['id'];?>" <?php if(isset($_REQUEST['usageset'])) { if($_REQUEST['usageset']==$datauageset['id']) { echo 'selected="selected"'; }  } ?>><?php echo $datauageset['usageset'];?></option>
												<?php
												}
												?>
												
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['usageset'])){ echo $_SESSION['error']['usageset'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4" >
									<div class="form-group row" id="ccc1">
										<label class="col-sm-4 col-form-label">Complexity</label>
										<div class="col-sm-8">
										<select class="form-control" name="complexity" value=""   id="complexity" onChange="search_report();">
											<option value=''>-- Select --</option>
												<?php
												$dcomplexity = $database->query("SELECT * FROM complexity WHERE estatus='1' "); 
												
												while($datacomplexity = mysqli_fetch_array($dcomplexity))
												{
													?>
												<option value="<?php echo $datacomplexity['id'];?>" <?php if(isset($_REQUEST['complexity'])) { if($_REQUEST['complexity']==$datacomplexity['id']) { echo 'selected="selected"'; }  } ?>><?php echo $datacomplexity['complexity'];?></option>
												<?php
												}
												?>
												
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['usageset'])){ echo $_SESSION['error']['usageset'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4" >
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Previous Paper Set</label>
										<div class="col-sm-8">
										<select class="form-control" name="set" value=""   id="set" onChange="search_report();">
											<option value=''>-- Select --</option>
											<?php
											$row = $database->query("select b.id,b.qset from previous_questions as a inner join previous_sets as b on a.estatus='1' and b.estatus='1' and a.id=b.pid order by a.year,b.id ASC ");
											while($data = mysqli_fetch_array($row))
											{
												?>
											<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['set'])) { if($_REQUEST['set']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['qset'];?></option>
											<?php
											}
											?>
										</select>
											
										</div>
									</div>
								</div>
								<div class="col-lg-4" >
									<div class="form-group row" >
										<label class="col-sm-4 col-form-label">Question Id</label>
										<div class="col-sm-8">
										
											<input type="text" name="question_id" id="question_id" class="form-control" onChange="search_report();"  value="<?php echo $_REQUEST['question_id'];?>" >
											<span class="error"><?php if(isset($_SESSION['error']['usageset'])){ echo $_SESSION['error']['usageset'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4" >
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Page No.</label>
										<div class="col-sm-8">
										
											<input type="text" name="page" id="page" class="form-control" onChange="search_report();"  value="<?php echo $_REQUEST['page'];?>" >
											<span class="error"><?php if(isset($_SESSION['error']['page'])){ echo $_SESSION['error']['page'];}?></span>
										</div>
									</div>
								</div>
								
							</div>
										<?php
								//Pagination code
										//$_REQUEST['exam']=$_REQUEST['exam1'];
										if(isset($_REQUEST['pagerows'])){
											if($_REQUEST['pagerows']!="" && $_REQUEST['pagerows']!="undefined"){
												$limit1=$_REQUEST['pagerows'];
											} else {
												$limit1="25";
											}
										}else{
											$limit1="25";

										}
										$limit=$limit1;
										if(isset($_GET['page']))
										{
											if($_GET['page']!='0'){
												$start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
												$page=$_GET['page'];
											}else{
												$start = 0;      //if no page var is given, set start to 0
												$page=0;
											}
										}
										else
										{
											$start = 0;      //if no page var is given, set start to 0
											$page=0;
										}
										
										
										//Search Form

										$tableName = 'createquestion';
										
										if(isset($_GET['keyword'])){
										}
										
										if(isset($_REQUEST['class1'])){
											if($_REQUEST['class1']!=""){
												$class=" AND class LIKE '%".$_REQUEST['class1']."%'";
											} else {
												$class="";
											}
										}else{
											$class="";
										}
										if(isset($_REQUEST['subject1'])){
											if($_REQUEST['subject1']!=""){
												$subject=" AND subject='".$_REQUEST['subject1']."'";
											} else {
												$subject="";
											}
										}else{
											$subject="";
										}
										if(isset($_REQUEST['chapter1'])){
											if($_REQUEST['chapter1']!=""){
												$chapter=" AND chapter IN (".$_REQUEST['chapter1'].")";
											} else {
												$chapter="";
											}
										}else{
											$chapter="";
										}
										if(isset($_REQUEST['explanation'])){
											if($_REQUEST['explanation']=="1"){
												$explanation=" AND explanation!='%3Cp%3ENA%3C%2Fp%3E' AND explanation != '%3Cp%3ENA%3C%2Fp%3E%0A%3Cp%3E%26nbsp%3B%3C%2Fp%3E' AND LENGTH(explanation) > 30";
											} else if($_REQUEST['explanation']=='2'){
												$explanation=" AND (explanation='%3Cp%3ENA%3C%2Fp%3E' OR   explanation = '%3Cp%3ENA%3C%2Fp%3E%0A%3Cp%3E%26nbsp%3B%3C%2Fp%3E' OR explanation='' OR LENGTH(explanation) < 30)";
											}else {


												$explanation="";
											}
										}else{
											$explanation="";
										}
										if(isset($_REQUEST['set'])){
											if($_REQUEST['set']!=""){
												$set=" AND ppaper='1' and qset ='".$_REQUEST['set']."'";
											} else {
												$set="";
											}
										}else{
											$set="";
										}
										if(isset($_REQUEST['complexity'])){
											if($_REQUEST['complexity']!=""){
												$complexity=" AND complexity ='".$_REQUEST['complexity']."'";
											} else {
												$complexity="";
											}
										}else{
											$complexity="";
										}
										if(isset($_REQUEST['usageset'])){
											if($_REQUEST['usageset']!=""){
												$usageset=" AND usageset ='".$_REQUEST['usageset']."'";
											} else {
												$usageset="";
											}
										}else{
											$usageset="";
										}
										
										if(isset($_REQUEST['exam'])){
											if($_REQUEST['exam']!=""){
												$exam=" AND exam LIKE '%".$_REQUEST['exam']."%'";
											} else {
												$exam="";
											}
										}else{
											$exam="";
										}
										if(isset($_REQUEST['exam1'])){
											if($_REQUEST['exam1']!=""){
												$exam1=" AND exam LIKE '%".$_REQUEST['exam1']."%'";
											} else {
												$exam1="";
											}
										}else{
											$exam1="";
										}
										if(isset($_REQUEST['question_id'])){
											if($_REQUEST['question_id']!=""){
												$question_id=" AND id='".$_REQUEST['question_id']."'";
											} else {
												$question_id="";
											}
										}else{
											$question_id="";
										}
										if(isset($_REQUEST['date'])){
											if($_REQUEST['date']!=""){
												$date=" and dtimestamp between ".strtotime($_REQUEST['date']. ' 00:00:01')." and ".strtotime($_REQUEST['date']. ' 23:59:59')."";
											} else {
												$date="";
											}
										}else{
											$date="";
										}
										
											
											$cond = $date.$class.$subject.$chapter.$exam.$exam1.$explanation.$set.$complexity.$usageset.$question_id;
											
											$condition="estatus='0'";
											if(strlen($condition) > 0){
												$condition ="where ".$condition.$cond;
											}
											$pagination = $session->showPagination(SECURE_PATH."deleteqreport/process.php?tableDisplay=1&class1=".$_REQUEST['class1']."&exam=".$_REQUEST['exam']."&subject1=".$_REQUEST['subject1']."&chapter1=".$_REQUEST['chapter1']."&topic1=".$_REQUEST['topic1']."&date=".$_REQUEST['date']."&explanation=".$_REQUEST['explanation']."&complexity=".$_REQUEST['complexity']."&set=".$_REQUEST['set']."&usageset=".$_REQUEST['usageset']."&question_id=".$_REQUEST['question_id']."&pagerows=".$_REQUEST['pagerows']."&start=".$_REQUEST['start']."&limit=".$_REQUEST['limit']."&",$tableName,$start,$limit,$page,$condition);
											$q = "SELECT * FROM $tableName ".$condition." ";
											$result_sel = $database->query($q);
											$numres = mysqli_num_rows($result_sel);
											$query = "SELECT * FROM $tableName ".$condition."  order by timestamp DESC LIMIT ".$start.",".$limit." ";
											$data_sel = $database->query($query);
										
										if(($start+$limit) > $numres){
											$onpage = $numres;
											}
											else{
											$onpage = $start+$limit;
											}
										if($numres > 0){
											?>
								<br />
								<div class="d-flex justify-content-between">
									<label class="d-flex align-items-center">Show <select name="" id="pagerows" onchange="search_report()" aria-controls="dtBasicExample" class=" form-control">
										
										<option value="25" <?php if(isset($_REQUEST['pagerows'])) { if($_REQUEST['pagerows']=='25') { echo 'selected="selected"'; }  }else{ echo 'selected="selected"'; }  ?>>25</option>
										<option value="50" <?php if(isset($_REQUEST['pagerows'])) { if($_REQUEST['pagerows']=='50') { echo 'selected="selected"'; }  } ?>>50</option>
										<option value="100" <?php if(isset($_REQUEST['pagerows'])) { if($_REQUEST['pagerows']=='100') { echo 'selected="selected"'; }  } ?>>100</option>
									</select> entries</label>
									
								</div>
								<?php
								 echo '<div class="d-flex justify-content-end"><p>Showing '.($start+1).' - '.($onpage).' results out of '.$numres.'</p></div>';
										?>
								<div class="card border-0">
									<div class="card-body table-responsive table_data p-0">
										<table class="table table-bordered dashboard-table mb-0" >
											<thead class="thead-light">
												<tr>
													<th scope="col">Sln.</th>
													<th scope="col">Qn Id.</th>
													<th scope="col">Deleted Date</th>
													<th scope="col">Deleted By</th>
													<th scope="col">Deleted Remarks</th>
													<th scope="col">Question</th>
													<th scope="col">Class</th>
													<th scope="col">Exam</th>
													<th scope="col">Subject - Chapter</th>
													<th scope="col">Topic</th>
                                                    <th scope="col">Created By</th>
													<th scope="col">Actions</th>
                                                 </tr>
											</thead>
										<tbody>
											<?php
											if(isset($_GET['page'])){
												if($_GET['page']!='0'){
													if($_GET['page']==1)
													$i=1;
													else
													$i=(($_GET['page']-1)*($limit))+1;
												}else{
													$i=1;
												}
											}else $i=1;

											$k = 1;
											//$sel=$database->query("select * from createquestion ".$condition." ");
											//echo "select * from createquestion ".$condition." ";
											while($value = mysqli_fetch_array($data_sel)){



                                                                                         //    $exp = filterqa($value['explanation']);

$display=1;
/*
echo strtolower($exp).'<br />';


if(isset($_REQUEST['explanation'])){

if($_REQUEST['explanation']==1){

if(strtolower($exp) == 'na')
  $display = 0;

}

}

echo $display.'<br />';
*/

if($display==1 ){


												$value['exam']=rtrim($value['exam'],',');
												$value['class']=rtrim($value['class'],',');
												$value['chapter']=rtrim($value['chapter'],',');
												if($value['exam']=='1'){
													$exam="NEET";
												}else if($value['exam']=='2'){
													$exam="JEE";
												}else if($value['exam']=='1,2'){
													$exam="NEET<br />JEE";
												}
												if($value['class']=='1'){
													$class='XI';
												}else if($value['class']=='2'){
													$class='XII';
												}else  if($value['class']=='1,2'){
													$class='XI<br />XII';
												}
												$dtime=$value['timestamp'];
												//$date=date("d-m-Y  H:i:s ", $value["time"]);
												$date1 = new DateTime("@$dtime");
												$dt=$date1->format('d/m/Y  H:i:s');
												$dt1=date('d/m/Y H:i:s',$value['dtimestamp']);
												$chapter ='';
												$m=1;
												$zSql = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and id IN(".$value['chapter'].")"); 
												while($row=mysqli_fetch_array($zSql)){
													$chapter .= $row['chapter'].",";
													$m++;
												}
												$topic ='';
												$m=1;
												$zSql = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$value['topic'].")"); 
												while($row=mysqli_fetch_array($zSql)){
													$topic .= $row['topic'].",";
													$m++;
												}

												if($value['dremarks']!=''){
													$remarks='';
													$sql=$database->query("select * from delete_qtype where estatus='1' and id IN (".$value['dremarks'].")");
													$rowc=mysqli_num_rows($sql);
													if($rowc>0){
														while($rowl=mysqli_fetch_array($sql)){
															$remarks.=$rowl['delete_qtype'].",";
														}
													}else{
														$remarks=$value['dremarks'];
													}
													
												}else{
													$remarks='';
												}
												
												$question2=urldecode($value['question']);
												$question=preg_replace('/<table>.*<\/table>/', '', $question2);
												
										?>
											<tr>
												<td class="text-left"><?php echo $i; ?></td>
												<td class="text-left" ><?php echo $value['id']; ?></td>
												<td class="text-left" ><?php echo $dt1; ?></td>
												 <td class="text-left" ><?php echo $value['dusername']; ?></td>
												  <td class="text-left" ><?php echo rtrim($remarks,","); ?></td>
												<?php
												if($value['qtype']=='7'){
													$question=urldecode($value['question']);
												?>
													<td class="text-left" width="33%"><div   style="overflow:hidden;max-width:350px;" id="tableborder-none"><?php echo $question; ?></div></td>
												<?php
												}else if($value['qtype']=='5'){
													$question=urldecode($value['question']);
												?>
													<td class="text-left" width="33%"><div   style="overflow:hidden;max-width:350px;" id="tableborder-none"><?php echo $question; ?></div></td>
												<?php
												}else if($value['qtype']=='8'){
													$question=urldecode($value['question']);
												?>
													<td class="text-left" width="33%"><div   style="overflow:hidden;max-width:350px;" id="tableborder-none"><?php echo $question; ?></div></td>
												<?php
												}else if($value['qtype']=='3'){
												
													$list1='';
													$list2='';
													$obj=json_decode($value['question'],true);
													foreach($obj as $rowpost2)
													{
														if(strlen($rowpost2)>0)
														{
															$rowpost = explode("_",$rowpost2);
															$list1.=$rowpost[0]."_";
															$list2.=$rowpost[1]."_";
														}
													}
													$qlist1 = explode("_",$list1);
													$qlist2 = explode("_",$list2);
												?>
													<td class="text-left" width="33%">
														<div   style="overflow:hidden;max-width:350px;" id="tableborder-none">
														<div class="d-flex justify-content-between">
														<div>
														<h5>List1</h5>
														<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
														<?php
															foreach($obj as $qqlist)
															{
															
																if(strlen($qqlist['qlist1'])>0)
																{
																	echo '<li>'.urldecode($qqlist['qlist1']).'</li>';
																}
															}
															?>
														
														</ul>
														</div>
														<div>
														<h5>List2</h5>
														<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
														<?php
															foreach($obj as $qqlist1)
															{
															
																if(strlen($qqlist1['qlist2'])>0)
																{
																	echo '<li>'.urldecode($qqlist1['qlist2']).'</li>';
																}
															}
															?>
														</ol>
														</div>
														</div>
														</div>
													</td>
												<?php
												}else if($value['qtype']=='9'){
													$list1='';
													$list2='';
													$obj=json_decode($value['question'],true);
													foreach($obj as $rowpost2)
													{
														if(strlen($rowpost2)>0)
														{
															$rowpost = explode("_",$rowpost2);
															$list1.=$rowpost[0]."_";
															$list2.=$rowpost[1]."_";
														}
													}
													$qlist1 = explode("_",$list1);
													$qlist2 = explode("_",$list2);
													
													?>	
													<td class="text-left" width="33%"><div   style="overflow:hidden;max-width:350px;" id="tableborder-none">
														<div class="d-flex justify-content-between">
														<div>
														<h5>List1</h5>
														<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
														<?php
															foreach($obj as $qqlist)
															{
															
																if(strlen($qqlist['qlist1'])>0)
																{
																	echo '<li>'.urldecode($qqlist['qlist1']).'</li>';
																}
															}
															?>
														
														
														</ul>
														</div>
														<div>
														<h5>List2</h5>
														<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
														<?php
															foreach($obj as $qqlist1)
															{
															
																if(strlen($qqlist1['qlist2'])>0)
																{
																	echo '<li>'.urldecode($qqlist1['qlist2']).'</li>';
																}
															}
															?>
														</ol>
														</div>
														</div>
														</div>
													</td>
													<?php
												}else{
													$question=urldecode($value['question']);
												?>
													<td class="text-left"  width="33%"><div   style="overflow:hidden;max-width:350px;" id="tableborder-none"><?php echo urldecode($question); ?></div></td>
												<?php
												}
												?>
												
												<td class="text-left"><?php echo $class; ?></td>
												<td class="text-left"><?php echo $exam; ?></td>
												<td class="text-left"><?php echo $database->get_name('subject','id',$value['subject'],'subject'); ?> - <?php echo rtrim($chapter,','); ?></td>
												<td class="text-left"> <?php echo rtrim($topic,','); ?></td>
                                                <td class="text-left" ><?php echo $value['username']; ?></td>
												
                                                <td class="text-left" nowrap>
													<a class="text-success" style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>deleteqreport/process1.php','viewDetails=1&class1=<?php echo $_REQUEST['class1']; ?>&exam=<?php echo $_REQUEST['exam']; ?>&subject1=<?php echo $_REQUEST['subject1']; ?>&chapter1=<?php echo $_REQUEST['chapter1']; ?>&topic1=<?php echo $_REQUEST['topic1']; ?>&question_id=<?php echo $_REQUEST['question_id']; ?>&date=<?php echo $_REQUEST['date']; ?>&userlevel=<?php echo $session->userlevel;?>&explanation=<?php echo $_REQUEST['explanation'];?>&start=<?php echo $start; ?>&limit=<?php echo $limit; ?>&pagerows=<?php echo $_REQUEST['pagerows']; ?>&id=<?php echo $value['id'];?>&usageset=<?php echo $_REQUEST['usageset']; ?>&complexity=<?php echo $_REQUEST['complexity']; ?>&status=<?php echo $_REQUEST['status'];?>&page=<?php echo $_REQUEST['page']; ?>')"><i class="fa fa-eye"  ></i></a> &nbsp;&nbsp;&nbsp;
													
												</td>
                                             </tr>
											<?php 
												$i++;

}
												}
												?>
										</tbody>
									</table>
									
									
									
								</div>
							</div>
							
							<div class="my-3 footer-pagination d-flex justify-content-between align-items-center">
									<p class="mb-0">Showing <?php echo ($start+1); ?> To <?php echo ($onpage); ?> results out of <?php echo $numres; ?></p>
									<?php echo $pagination ;?>
											
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</section>
  
	<?php
	}
	else{
	?>
		<div class="text-danger text-center">No Results Found</div>
  <?php
	}

}
?>
<?php
	if(isset($_REQUEST['getsubchapter']))
	{	
		
	
	?>
 
	
		<label class="col-sm-4 col-form-label">Chapter<span style="color:red;">*</span></label>
		 <div class="col-sm-8">
   			<select name="chapter1" id="chapter1"  class="form-control" onChange="search_report();" >
    			<option value=''>-- Select --</option>

            		<?php
					if($_REQUEST['class']!=''){
						$zSql = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and class='".$_REQUEST['class']."' and subject = '".$_REQUEST['subject']."' ");
						
					}else{
						$zSql = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and subject = '".$_REQUEST['subject']."' ");
					}
					
					if(mysqli_num_rows($zSql)>0)
					{ 
						while($data = mysqli_fetch_array($zSql))
						{ 
							?> 
							<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['chapter1'])) { if($_REQUEST['chapter1']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['chapter'];?></option>
							
							<?php
						}
					}
					?>
   				</select>
			 </div>
					
    <?php
	}
	
	if(isset($_REQUEST['getsubject1']))
	{	
	?>
 
	
		<label class="col-sm-4 col-form-label">Subject</label>
		 <div class="col-sm-8"> 
   			<select class="form-control" name="subject1" value=""   id="subject1" onChange="setState('fff','<?php echo SECURE_PATH;?>deleteqreport/process.php','getsubchapter=1&class='+$('#class1').val()+'&subject='+$('#subject1').val()+'')" >
				<option value=''>-- Select --</option>
				<?php
				if($_REQUEST['exam']=='1'){
					$row1 = $database->query("select * from subject where estatus='1'  and id!='4'");
					echo "select * from subject where estatus='1' and id!='4'";
				}else if($_REQUEST['exam']=='2'){
					$row1 = $database->query("select * from subject where estatus='1'  and id!=1 and id!=5 ");
				}else{
					$row1 = $database->query("select * from subject where estatus='1' ");
				}
				while($data = mysqli_fetch_array($row1))
				{
					?>
				<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['subject'])) { if($_POST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['subject']);?></option>
				<?php
				}
				?>
			</select>
			 </div>
					
    <?php
	}
	?>