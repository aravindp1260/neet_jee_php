<style>
/*Fixed table side
-----------------------------------*/

.table-scroll {
  position: relative;
  /* max-width: 600px; */
  margin: auto;
  overflow: hidden;
  border: 1px solid #dee2e6;
  border-top: none;
  margin-left: -1px;
}

.table-wrap {
  width: 100%;
  overflow: auto;
}

.table-scroll table {
  width: 100%;
  margin: auto;
  border-collapse: separate;
  border-spacing: 0;
}

.table-scroll th,
.table-scroll td {
  padding: 5px 10px;
  border: 1px solid #dee2e6;
  ;
  /* background: #fff; */
  white-space: nowrap;
  vertical-align: top;
}

.table-scroll thead,
.table-scroll tfoot {
  background: #f9f9f9;
}

.clone {
  position: absolute;
  top: 0;
  left: 0;
  pointer-events: none;
}

.clone th,
.clone td {
  visibility: hidden
}

.clone td,
.clone th {
  border-color: transparent
}

.clone tbody th {
  visibility: visible;
}

.clone .fixed-side {
  border: 1px solid #dee2e6;
  ;
  background: #FFFFFF;
  visibility: visible;
}

.clone thead,
.clone tfoot {
  background: transparent;
}

/*Fixed table side
---------------------------------*/

</style>
<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
?>

<?php
	//Display bulkreport
if(isset($_REQUEST['tableDisplay'])){
	$sql=$database->query("select * from users where username='".$session->username."'");
	$rowsel=mysqli_fetch_array($sql);
	$chapter=rtrim($rowsel['chapter'],",");
	//echo "select * from chapter where estatus='1' and id in (".$chapter.")  order by id asc limit 0,1";
	if($session->userlevel!=9){
		$selchapter=$database->query("select * from chapter where estatus='1' and id in (".$chapter.")  order by id asc limit 0,1");
	}else{
		$selchapter=$database->query("select * from chapter where estatus='1'   order by id asc limit 0,1");
	}
	$rowchapter=mysqli_fetch_array($selchapter);
	if(isset($_POST['chapter'])){
		if($_POST['chapter']!=''){
			$_POST['chapter']=$_POST['chapter'];
			$chaptercon=" AND id IN (".$_POST['chapter'].") ";
			$chaptercon1="  AND chapter IN (".$_POST['chapter'].") ";
		}else{
			$_POST['chapter']=$rowchapter['id'];
			//$chaptercon="  AND id IN (".$chapter.") ";
			$chaptercon="  AND id IN (".$rowchapter['id'].") ";
			$chaptercon1="  AND chapter IN (".$rowchapter['id'].") ";
		}
	}else{
		$_POST['chapter']=$rowchapter['id'];
		//$chaptercon="  AND id IN (".$chapter.") ";
		$chaptercon="  AND id IN (".$rowchapter['id'].") ";
		$chaptercon1="  AND chapter IN (".$rowchapter['id'].") ";
	}
	if(isset($_POST['topic'])){
		if($_POST['topic']!=''){
			$_POST['topic']=$_POST['topic'];
			$topiccon="  AND id='".$_POST['topic']."'";
			
		}else{
			$_POST['topic']="";
			$topiccon="";
		}
	}else{
		$_POST['topic']="";
		$topiccon="";
	}
	if(isset($_POST['comp_tquestion'])){
		if($_POST['comp_tquestion']!=''){
			$_POST['comp_tquestion']=$_POST['comp_tquestion'];
			
		}else{
			$_POST['comp_tquestion']="complexity";
		}
	}else{
		$_POST['comp_tquestion']="complexity";
	
	}
	
	?>
	
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12"> 
					<div class="card border-0 shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary">FITMENT REPORT OF THE CHAPTER  - <?php echo $database->get_name('chapter','id',$_POST['chapter'],'chapter'); ?> 	</h6>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Chapter</label>
										<div class="col-sm-8">
											
											<select id="chapter" class="form-control" name="chapter" onchange="setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&comp_tquestion='+$('#comp_tquestion').val()+'')">
												<option value="">Select</option>
												<?php
												if($session->userlevel!=9){
													$sell=$database->query("select * from chapter where estatus='1' and id and id in (".$chapter.")");
												}else{
													$sell=$database->query("select * from chapter where estatus='1'");
												}
												while($rowll=mysqli_fetch_array($sell)){
												?>
													<option value='<?php echo $rowll['id']; ?>' <?php if(isset($_POST['chapter'])){ if($_POST['chapter']!=''){ if($_POST['chapter'] == $rowll['id']){ echo 'selected'; } } } ?>><?php echo $rowll['chapter']; ?></option>
												<?php
												}
												?>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['chapter'])){ echo $_SESSION['error']['chapter'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Topic</label>
										<div class="col-sm-8">
											
											<select id="topic" class="form-control" name="topic" onchange="setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&comp_tquestion='+$('#comp_tquestion').val()+'')">
												<option value="">Select</option>
												<?php
												$sell=$database->query("select * from topic where estatus='1' ".$chaptercon1."");
												while($rowll=mysqli_fetch_array($sell)){
												?>
													<option value='<?php echo $rowll['id']; ?>' <?php if(isset($_POST['topic'])){ if($_POST['topic']!=''){ if($_POST['topic'] == $rowll['id']){ echo 'selected'; } } } ?>><?php echo $rowll['topic']; ?></option>
												<?php
												}
												?>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['topic'])){ echo $_SESSION['error']['topic'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Type</label>
										<div class="col-sm-8">
											
											<select id="comp_tquestion" class="form-control" name="comp_tquestion" onchange="setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&comp_tquestion='+$('#comp_tquestion').val()+'&topic_report='+$('#topic_report').val()+'')">
												<option value="">Select</option>
												<option value="complexity" <?php if(isset($_POST['comp_tquestion'])){ if($_POST['comp_tquestion'] == 'complexity'){ echo ' selected="selected"';}}else { echo ' selected="selected"'; }?>>Complexity</option>
												<option value="inputquestion" <?php if(isset($_POST['comp_tquestion'])){ if($_POST['comp_tquestion'] == 'inputquestion'){ echo ' selected="selected"';};}?>>Type Of Question</option>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['topic'])){ echo $_SESSION['error']['topic'];}?></span>
										</div>
									</div>
								</div>
								
								
								
							</div>
							<?php
							if($_POST['chapter']!='' && $_POST['topic']==''){ ?>
								<div class="row pt-2">
									
									 <div class="form-group row pl-3">
										<div class="col-md-12 topic_report" >
											<div class="form-group">
											 <div class="custom-control custom-checkbox">
											  <input type="checkbox" class="topic_report custom-control-input" id="topic_report"  onchange="getFields();setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&comp_tquestion='+$('#comp_tquestion').val()+'&topic_report='+$('#topic_report').val()+'');"  value="<?php if(isset($_POST['topic_report'])){ if($_POST['topic_report']!=''){ echo $_POST['topic_report'];}else{ echo '1'; }}else{  } ?>" <?php if(isset($_POST['topic_report'])) { if($_POST['topic_report']==1) { echo 'checked'; }}else{ echo ''; } ?> >
											  <label class="custom-control-label pr-5" for="topic_report" > &nbsp;&nbsp;&nbsp;Show Topic wise Report</label>
											</div>
											</div>
										</div>	
									</div>
								</div>
							<?php } ?>
							<div class="d-flex justify-content-between">
									<label class="d-flex align-items-center"> </label>
									<a onclick="setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&comp_tquestion='+$('#comp_tquestion').val()+'&topic_report='+$('#topic_report').val()+'')"  style="float:right" target="_blank"  title="PDF Export" ><i class="fa fa-refresh" title="Refresh Page" style="font-size: 25px;color:blue;"></i></a>
								</div>
							<div class="table-responsive pt-4">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
											<th colspan="3" ></th>
											<th>Typing Mistakes</th>
											<th>Question inappropriate</th>
											<th >Answer Options are not correct</th>
											<th >Explanation is not Correct</th>
											<th >Explanation is Required</th>
                                            
                                           
                                        </tr>
										<?php
										
										$sqlt=$database->query("select count(id) as cnt  from createquestion where estatus='1' and vstatus1=1 and find_in_set(".$_POST['chapter'].",chapter)>0  and vusername1='".$session->username."'");
										$rowt=mysqli_fetch_array($sqlt);
										$sqlrt=$database->query("select count(id) as cnt  from createquestion where estatus='1' and vstatus1=1 and reiew_status=1 and find_in_set(".$_POST['chapter'].",chapter)>0   and vusername1='".$session->username."' ");
										$rowrt=mysqli_fetch_array($sqlrt);
										$sqlt1=$database->query("select count(id) as cnt  from createquestion where estatus='0'  and find_in_set(".$_POST['chapter'].",chapter)>0 and dremarks='Typing Mistakes'   and dusername='".$session->username."'");
										$rowt1=mysqli_fetch_array($sqlt1);
										$sqltr1=$database->query("select count(id) as cnt  from createquestion where estatus='0' and vstatus1='1'  and find_in_set(".$_POST['chapter'].",chapter)>0 and dremarks='Typing Mistakes' and vusername1='".$session->username."'  ");
										$rowrt1=mysqli_fetch_array($sqltr1);
										$sqlt2=$database->query("select count(id) as cnt  from createquestion where estatus='0'   and find_in_set(".$_POST['chapter'].",chapter)>0 and dremarks='Question inappropriate' and dusername='".$session->username."' ");
										$rowt2=mysqli_fetch_array($sqlt2);
										$sqltr2=$database->query("select count(id) as cnt  from createquestion where estatus='0'  and  vstatus1='1' and find_in_set(".$_POST['chapter'].",chapter)>0 and dremarks='Question inappropriate' and vusername1='".$session->username."'");
										$rowtr2=mysqli_fetch_array($sqltr2);
										$sqlt3=$database->query("select count(id) as cnt  from createquestion where estatus='0'  and find_in_set(".$_POST['chapter'].",chapter)>0 and dremarks='Answer Options are not correct' and dusername='".$session->username."' ");
										$rowt3=mysqli_fetch_array($sqlt3);
										$sqltr3=$database->query("select count(id) as cnt  from createquestion where estatus='0'  and vstatus1=1 and find_in_set(".$_POST['chapter'].",chapter)>0 and dremarks='Answer Options are not correct' and vusername1='".$session->username."'    ");
										$rowtr3=mysqli_fetch_array($sqltr3);
										$sqlt4=$database->query("select count(id) as cnt  from createquestion where estatus='0'  and find_in_set(".$_POST['chapter'].",chapter)>0 and dremarks='Explanation is not Correct' and dusername='".$session->username."'");
										$rowt4=mysqli_fetch_array($sqlt4);
										$sqltr4=$database->query("select count(id) as cnt  from createquestion where estatus='0'  and vstatus1=1 and find_in_set(".$_POST['chapter'].",chapter)>0 and dremarks='Explanation is not Correct' and vusername1='".$session->username."'  ");
										$rowtr4=mysqli_fetch_array($sqltr4);
										$sqlt5=$database->query("select count(id) as cnt  from createquestion where estatus='0'  and find_in_set(".$_POST['chapter'].",chapter)>0 and dremarks='Explanation is Required' and dusername='".$session->username."' ");
										$rowt5=mysqli_fetch_array($sqlt5);
										$sqltr5=$database->query("select count(id) as cnt  from createquestion where estatus='0' and vstatus1=1  and find_in_set(".$_POST['chapter'].",chapter)>0 and dremarks='Explanation is Required'  and vusername1='".$session->username."'  ");
										$rowtr5=mysqli_fetch_array($sqltr5);
										$errort1=$rowt1['cnt']+$rowrt1['cnt'];
										$errort2=$rowt2['cnt']+$rowtr2['cnt'];
										$errort3=$rowt3['cnt']+$rowtr3['cnt'];
										$errort4=$rowt4['cnt']+$rowtr4['cnt'];
										$errort5=$rowt5['cnt']+$rowtr5['cnt'];
										?>
                                        <tr>
                                            <td>Total Question</td>
											<td><?php  if($rowt['cnt']!=''){ echo $rowt['cnt']; } else{ echo '0'; }; ?></td>
                                            <td>Deleted</td>
											<td><?php if($rowt1['cnt']!=''){ echo $rowt1['cnt']; } else{ echo '0'; }; ?></td>
											<td><?php  if($rowt2['cnt']!=''){ echo $rowt2['cnt']; } else{ echo '0'; }; ?></td>
											<td><?php  if($rowt3['cnt']!=''){ echo $rowt3['cnt']; } else{ echo '0'; }; ?> </td>
											<td><?php  if($rowt4['cnt']!=''){ echo $rowt4['cnt']; } else{ echo '0'; }; ?> </td>
											<td><?php  if($rowt5['cnt']!=''){ echo $rowt5['cnt']; } else{ echo '0'; }; ?></td>
                                        </tr>
										<tr>
                                            <td>Reviewed</td>
											<td><?php if($rowrt['cnt']!=''){ echo $rowrt['cnt']; } else{ echo '0'; }; ?></td>
                                            <td>Deleted byReviewer</td>
											<td><?php if($rowrt1['cnt']!=''){ echo $rowrt1['cnt']; } else{ echo '0'; };  ?></td>
											<td><?php  if($rowtr2['cnt']!=''){ echo $rowtr2['cnt']; } else{ echo '0'; }; ?></td>
											<td><?php  if($rowtr3['cnt']!=''){ echo $rowtr3['cnt']; } else{ echo '0'; }; ?></td>
											<td><?php  if($rowtr4['cnt']!=''){ echo $rowtr4['cnt']; } else{ echo '0'; }; ?></td>
											<td><?php  if($rowtr5['cnt']!=''){ echo $rowtr5['cnt']; } else{ echo '0'; }; ?></td>
                                        </tr>
										<tr>
											<td colspan="2" ></td>
                                            <td>Total Errors</td>
											<td><?php echo $errort1;  ?></td>
											<td><?php echo $errort2; ?></td>
											<td><?php echo $errort3; ?></td>
											<td><?php echo $errort4; ?></td>
											<td><?php echo $errort5; ?></td>
                                        </tr>

                                    </thead>
                                    
                                </table>
                            </div>
							<?php
							if($_POST['chapter']!='' && $_POST['topic']==''){
								$sqll1=$database->query("select count(id) as cnt from createquestion where estatus='1' and find_in_set(".$_POST['chapter'].",chapter) >0  ");
								$rowl1=mysqli_fetch_array($sqll1);
								$sqll1v=$database->query("select count(id) as cnt from createquestion where estatus='1' and find_in_set(".$_POST['chapter'].",chapter) >0 and vstatus1=1 and vusername1='".$session->username."'");
								$rowl1v=mysqli_fetch_array($sqll1v);
								$sqll1p=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$_POST['chapter'].",chapter) >0 and vstatus1='0'");
								$rowl1p=mysqli_fetch_array($sqll1p);
								$sqll1r=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$_POST['chapter'].",chapter) >0 and vstatus1='2' and vusername1='".$session->username."' ");
								$rowl1r=mysqli_fetch_array($sqll1r);
								?>
								<div class="table-responsive pt-3" >
									<h6 class="my-4"><b><u>Verification Overview</u></b></h6>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>Chapter</th>
												<th>Total Questions</th>
												<th>Verified</th>
												<th>Pending</th>
												<th>Rejected</th>
												
											</tr>
											<tr>
												<td><?php echo $database->get_name('chapter','id',$_POST['chapter'],'chapter'); ?></td>
												<td><?php echo $rowl1['cnt']; ?></td>
												<td><?php echo $rowl1v['cnt']; ?></td>
												<td><?php echo $rowl1p['cnt']; ?></td>
												<td><?php echo $rowl1r['cnt']; ?></td>
											</tr>
											

										</thead>
										<tbody>
											
										</tbody>
									</table>
								</div>
								<?php
								$sqll1vt=$database->query("select count(id) as cnt from createquestion where estatus='1' and find_in_set(".$_POST['chapter'].",chapter) >0  and vstatus1='1' ");
								$rowlvt=mysqli_fetch_array($sqll1vt);
								$sqll1vv=$database->query("select count(id) as cnt from createquestion where estatus='1' and find_in_set(".$_POST['chapter'].",chapter) >0 and vstatus1=1 and review_status='1' and vusername1='".$session->username."'");
								$rowl1vv=mysqli_fetch_array($sqll1vv);
								$sqll1vp=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$_POST['chapter'].",chapter) >0 and vstatus1='1' and review_status='0' and vusername1='".$session->username."' ");
								$rowl1vp=mysqli_fetch_array($sqll1vp);

								$wc=0;
								$woutc=0;
								$timing=0;
								$qtype=0;
								$comple=0;
								$qtheory=0;
								$usage=0;
								$class=0;
								$exam=0;
								$chapter=0;
								$topic=0;
								$rselrc=$database->query("select b.changes_count,b.changes from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1' and review_status='1'   and b.estatus='1' and a.id=b.question_id    and  a.vusername1='".$session->username."'  and b.message='1'    and find_in_set(".$_POST['chapter'].",chapter) >0 group by a.id");
								$rowcn=mysqli_num_rows($rselrc);
								if($rowcn>0) {
									while($row_rselrc=mysqli_fetch_array($rselrc)){
										if($row_rselrc['changes_count']!=0){
											$wc++;
										}
										if($row_rselrc['changes_count']==0){
											$woutc++;
										}


										if($row_rselrc['changes']!='No Changes'){
											$changes=$row_rselrc['changes'];
											$data=explode("^",$changes);
											$a=1;
											
											if(count($data)>0){
												foreach($data as $dataa){
													$data1=explode("_",$dataa);
													if($data1[0]!=''){
														$new='';
														$old='';
														
														if($data1[0]=='complexity'){
															
															$comple++;
														}
														if($data1[0]=='timeduration'){
															
															$timing++;
															
														}
														if($data1[0]=='usageset'){
															
															$usage++;
														}
														if($data1[0]=='questiontheory'){
															
															$qtheory++;
														}
														if($data1[0]=='inputquestion'){
															
															$qtype++;
														}
														if($data1[0]=='class'){
															
															$class++;
														}
														if($data1[0]=='exam'){
															
															$exam++;
														}
														if($data1[0]=='chapter'){
															
															$chapter++;
														}
														if($data1[0]=='topic'){
															
															$topic++;
														}
														
														
														
														?>
															
														<?php
														
													
													$a++;
													}
												}
											}
										}
											
										
									}
													
								}
								
								
								?>
								<div class="table-responsive pt-3" >
									<h6 class="my-4"><b><u>Reviewer  Overview</u></b></h6>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>Chapter</th>
												<th>Total Questions</th>
												<th>Reviewed Questions</th>
												<th>Pending</th>
												<th>Reviewed with corrections</th>
												<th>Reviewed with  out corrections</th>
												
											</tr>
											<tr>
												<td><?php echo $database->get_name('chapter','id',$_POST['chapter'],'chapter'); ?></td>
												<td><?php echo $rowlvt['cnt']; ?></td>
												<td><?php echo $rowl1vv['cnt']; ?></td>
												<td><?php echo $rowl1vp['cnt']; ?></td>
												<td><?php echo $wc; ?></td>
												<td><?php echo $woutc; ?></td>
											</tr>
											

										</thead>
										<tbody>
											
										</tbody>
									</table>
								</div>
								<div class="table-responsive pt-3" >
									<h6 class="my-4"><b><u>Reviewer Overview With Corrections</u></b></h6>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>Chapter</th>
												<th>Timing Issue</th>
												<th>QuestionType Issue</th>
												<th>Complexity Issue</th>
												<th>Question Theory Issue</th>
												<th>Usageset Issue</th>
												<th>Class Issue</th>
												<th>Exam Issue</th>
												<th>Chapter Issue</th>
												<th>Topic Issue</th>
												
											</tr>
											<tr>
												<td><?php echo $database->get_name('chapter','id',$_POST['chapter'],'chapter'); ?></td>
												<td><?php echo $timing; ?></td>
												<td><?php echo $qtype; ?></td>
												<td><?php echo $comple; ?></td>
												<td><?php echo $qtheory; ?></td>
												<td><?php echo $usage; ?></td>
												<td><?php echo $class; ?></td>
												<td><?php echo $exam; ?></td>
												<td><?php echo $chapter; ?></td>
												<td><?php echo $topic; ?></td>
											</tr>
											

										</thead>
										<tbody>
											
										</tbody>
									</table>
								</div>
								<div class="table-responsive pt-3" >
									<h6 class="my-4"><b><u>Usageset wise Overview</u></b></h6>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th rowspan="2" colspan="2"></th>
												<th>Total</th>
												<th>Verified </th>
												<th>Verified (%)</th>
												<th>Pending</th>
												<th>Pending (%)</th>
												<th>Reviewed</th>
												<th>Reviewed (%)</th>
												<th>Review Pending</th>
												<th>Review Pending (%)</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th rowspan="6"><?php echo $database->get_name('chapter','id',$_POST['chapter'],'chapter'); ?>  Chapter</th>
											</tr>
											<?php
											$sqllo=$database->query("select * from question_useset where estatus='1' ");
											$i=1;
											while($rowllo=mysqli_fetch_array($sqllo)){
												$seltotal=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$_POST['chapter'].",chapter) >0 and usageset='".$rowllo['id']."'");
												$rowtotal=mysqli_fetch_array($seltotal);

												$seltotal1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$_POST['chapter'].",chapter) >0 and usageset='".$rowllo['id']."'");
												$rowtotal1=mysqli_fetch_array($seltotal1);
												
												$seltotalv=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1=1 and find_in_set(".$_POST['chapter'].",chapter) >0 and usageset='".$rowllo['id']."'");
												$rowtotalv=mysqli_fetch_array($seltotalv);

												$percentpv = $rowtotalv['cnt']/$rowtotal['cnt'];
												if(is_nan($percentpv))
													$percentpv=0;
												else
													$percentpv=$percentpv;
												$percent_vpv= number_format( $percentpv * 100) . '%';

												$seltotalp=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1=0 and find_in_set(".$_POST['chapter'].",chapter) >0 and usageset='".$rowllo['id']."'");
												$rowtotalp=mysqli_fetch_array($seltotalp);

												$percentpvp = $rowtotalp['cnt']/$rowtotal['cnt'];
												if(is_nan($percentpvp))
													$percentpvp=0;
												else
													$percentpvp=$percentpvp;
												$percent_vpvp= number_format( $percentpvp * 100) . '%';


												

												$seltotalr=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1=1 and review_status='1' and find_in_set(".$_POST['chapter'].",chapter) >0 and usageset='".$rowllo['id']."'");
												$rowtotalr=mysqli_fetch_array($seltotalr);

												$seltotalpr=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1=1 and review_status='0' and find_in_set(".$_POST['chapter'].",chapter) >0 and usageset='".$rowllo['id']."'");
												$rowtotalpr=mysqli_fetch_array($seltotalpr);

												$percentpr = $rowtotalr['cnt']/$rowtotal1['cnt'];
												if(is_nan($percentpr))
													$percentpr=0;
												else
													$percentpr=$percentpr;
												$percent_vr= number_format( $percentpr * 100) . '%';

												$percentpr1 = $rowtotalpr['cnt']/$rowtotal1['cnt'];
												if(is_nan($percentpr1))
													$percentpr1=0;
												else
													$percentpr1=$percentpr1;
												$percent_vr1= number_format( $percentpr1 * 100) . '%';
												?>
												<tr>
													<td><?php  if($rowllo['usageset']!=''){ echo $rowllo['usageset']; } else{ echo '0'; } ?></td>
													<td><?php  if($rowtotal['cnt']!=''){ echo $rowtotal['cnt']; } else{ echo '0'; } ?></td>
													<td><?php  if($rowtotalv['cnt']!=''){ echo $rowtotalv['cnt']; } else{ echo '0'; } ?></td>
													<td><?php echo $percent_vpv; ?></td>
													<td><?php  if($rowtotalp['cnt']!=''){ echo $rowtotalp['cnt']; } else{ echo '0'; } ?></td>
													<td><?php echo $percent_vpvp; ?></td>
													<td><?php  if($rowtotalr['cnt']!=''){ echo $rowtotalr['cnt']; } else{ echo '0'; } ?></td>
													<td><?php echo $percent_vr; ?></td>
													<td><?php  if($rowtotalpr['cnt']!=''){ echo $rowtotalpr['cnt']; } else{ echo '0'; } ?></td>
													<td><?php echo $percent_vr1; ?></td>
												<?php
											}
											?>
											</tr>
										</tbody>
									</table>
								</div>
							<?php
							}else if($_POST['chapter']!='' && $_POST['topic']!=''){
								$sqll1=$database->query("select count(id) as cnt from createquestion where estatus='1' and find_in_set(".$_POST['chapter'].",chapter) >0 and find_in_set(".$_REQUEST['topic'].",topic) >0  ");
									$rowl1=mysqli_fetch_array($sqll1);
									$sqll1v=$database->query("select count(id) as cnt from createquestion where estatus='1' and find_in_set(".$_POST['chapter'].",chapter) >0 and vstatus1=1 and vusername1='".$session->username."' and find_in_set(".$_POST['topic'].",topic) >0 ");
									$rowl1v=mysqli_fetch_array($sqll1v);
									
									$sqll1p=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$_POST['chapter'].",chapter) >0 and  find_in_set(".$_POST['topic'].",topic) >0 and vstatus1='0'");
									$rowl1p=mysqli_fetch_array($sqll1p);
									$sqll1r=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$_POST['chapter'].",chapter) >0 and vstatus1='2' and find_in_set(".$_POST['topic'].",topic) >0 and vusername1='".$session->username."' ");
									$rowl1r=mysqli_fetch_array($sqll1r);
							?>
									<div class="table-responsive pt-3" >
										<h6 class="my-4"><b><u>Topic Verification Overview</u></b></h6>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>Topic</th>
													<th>Total Questions</th>
													<th>Verified</th>
													<th>Pending</th>
													<th>Rejected</th>
													
												</tr>
												<tr>
													<td><?php echo $database->get_name('topic','id',$_POST['topic'],'topic'); ?></td>
													<td><?php echo $rowl1['cnt']; ?></td>
													<td><?php echo $rowl1v['cnt']; ?></td>
													<td><?php echo $rowl1p['cnt']; ?></td>
													<td><?php echo $rowl1r['cnt']; ?></td>
												</tr>
												

											</thead>
											<tbody>
												
											</tbody>
										</table>
									</div>
									<?php
									$sqll1vt=$database->query("select count(id) as cnt from createquestion where estatus='1' and find_in_set(".$_POST['chapter'].",chapter) >0  and vstatus1='1' and find_in_set(".$_REQUEST['topic'].",topic) >0 ");
									$rowlvt=mysqli_fetch_array($sqll1vt);
									$sqll1vv=$database->query("select count(id) as cnt from createquestion where estatus='1' and find_in_set(".$_POST['chapter'].",chapter) >0 and vstatus1=1 and review_status='1' and vusername1='".$session->username."' and find_in_set(".$_REQUEST['topic'].",topic) >0");
									$rowl1vv=mysqli_fetch_array($sqll1vv);
									$sqll1vp=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$_POST['chapter'].",chapter) >0 and vstatus1='1' and review_status='0' and vusername1='".$session->username."'  and find_in_set(".$_REQUEST['topic'].",topic) >0 ");
									$rowl1vp=mysqli_fetch_array($sqll1vp);

									$wc=0;
									$woutc=0;
									$timing=0;
									$qtype=0;
									$comple=0;
									$qtheory=0;
									$usage=0;
									$class=0;
									$exam=0;
									$chapter=0;
									$topic=0;
									$rselrc=$database->query("select b.changes_count,b.changes from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1' and review_status='1'   and b.estatus='1' and a.id=b.question_id    and  a.vusername1='".$session->username."'  and b.message='1'    and find_in_set(".$_POST['chapter'].",chapter) >0  and find_in_set(".$_POST['topic'].",topic) >0 group by a.id");
									$rowcn=mysqli_num_rows($rselrc);
									if($rowcn>0) {
										while($row_rselrc=mysqli_fetch_array($rselrc)){
											if($row_rselrc['changes_count']!=0){
												$wc++;
											}
											if($row_rselrc['changes_count']==0){
												$woutc++;
											}


											if($row_rselrc['changes']!='No Changes'){
												$changes=$row_rselrc['changes'];
												$data=explode("^",$changes);
												$a=1;
												
												if(count($data)>0){
													foreach($data as $dataa){
														$data1=explode("_",$dataa);
														if($data1[0]!=''){
															$new='';
															$old='';
															
															if($data1[0]=='complexity'){
																
																$comple++;
															}
															if($data1[0]=='timeduration'){
																
																$timing++;
																
															}
															if($data1[0]=='usageset'){
																
																$usage++;
															}
															if($data1[0]=='questiontheory'){
																
																$qtheory++;
															}
															if($data1[0]=='inputquestion'){
																
																$qtype++;
															}
															if($data1[0]=='class'){
																
																$class++;
															}
															if($data1[0]=='exam'){
																
																$exam++;
															}
															if($data1[0]=='chapter'){
																
																$chapter++;
															}
															if($data1[0]=='topic'){
																
																$topic++;
															}
															
															
															
															?>
																
															<?php
															
														
														$a++;
														}
													}
												}
											}
												
											
										}
														
									}
									
									
									?>
									<div class="table-responsive pt-3" >
										<h6 class="my-4"><b><u>Reviewer Questions Overview</u></b></h6>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>Topic</th>
													<th>Total Questions</th>
													<th>Reviewed Questions</th>
													<th>Pending</th>
													<th>Reviewed with corrections</th>
													<th>Reviewed with  out corrections</th>
													
												</tr>
												<tr>
													<td><?php echo $database->get_name('topic','id',$_POST['topic'],'topic'); ?></td>
													<td><?php echo $rowlvt['cnt']; ?></td>
													<td><?php echo $rowl1vv['cnt']; ?></td>
													<td><?php echo $rowl1vp['cnt']; ?></td>
													<td><?php echo $wc; ?></td>
													<td><?php echo $woutc; ?></td>
												</tr>
												

											</thead>
											<tbody>
												
											</tbody>
										</table>
									</div>
									<div class="table-responsive pt-3" >
										<h6 class="my-4"><b><u>Reviewed Questions With Corrections</u></b></h6>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>Topic</th>
													<th>Timing Issue</th>
													<th>QuestionType Issue</th>
													<th>Complexity Issue</th>
													<th>Question Theory Issue</th>
													<th>Usageset Issue</th>
													<th>Class Issue</th>
													<th>Exam Issue</th>
													<th>Chapter Issue</th>
													<th>Topic Issue</th>
													
												</tr>
												<tr>
													<td><?php echo $database->get_name('topic','id',$_POST['topic'],'topic'); ?></td>
													<td><?php echo $timing; ?></td>
													<td><?php echo $qtype; ?></td>
													<td><?php echo $comple; ?></td>
													<td><?php echo $qtheory; ?></td>
													<td><?php echo $usage; ?></td>
													<td><?php echo $class; ?></td>
													<td><?php echo $exam; ?></td>
													<td><?php echo $chapter; ?></td>
													<td><?php echo $topic; ?></td>
												</tr>
												

											</thead>
											<tbody>
												
											</tbody>
										</table>
									</div>

									<div class="table-responsive pt-3" >
									<h6 class="my-4"><b><u>Usageset wise Overview</u></b></h6>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th rowspan="2" colspan="2"></th>
												<th>Total</th>
												<th>Verified </th>
												<th>Verified (%)</th>
												<th>Pending</th>
												<th>Pending (%)</th>
												<th>Reviewed</th>
												<th>Reviewed (%)</th>
												<th>Review Pending</th>
												<th>Review Pending (%)</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th rowspan="6"><?php echo $database->get_name('topic','id',$_POST['topic'],'topic'); ?> Chapter</th>
											</tr>
											<?php
											$sqllo=$database->query("select * from question_useset where estatus='1' ");
											$i=1;
											while($rowllo=mysqli_fetch_array($sqllo)){
												$seltotal=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$_POST['chapter'].",chapter) >0   and find_in_set(".$_POST['topic'].",topic) >0 and usageset='".$rowllo['id']."'");
												$rowtotal=mysqli_fetch_array($seltotal);

												$seltotal1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$_POST['chapter'].",chapter) >0 and find_in_set(".$_POST['topic'].",topic) >0 and usageset='".$rowllo['id']."'");
												$rowtotal1=mysqli_fetch_array($seltotal1);
												
												$seltotalv=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1=1 and find_in_set(".$_POST['chapter'].",chapter) >0  and find_in_set(".$_POST['topic'].",topic) >0 and usageset='".$rowllo['id']."'");
												$rowtotalv=mysqli_fetch_array($seltotalv);

												$percentpv = $rowtotalv['cnt']/$rowtotal['cnt'];
												if(is_nan($percentpv))
													$percentpv=0;
												else
													$percentpv=$percentpv;
												$percent_vpv= number_format( $percentpv * 100) . '%';

												$seltotalp=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1=0 and find_in_set(".$_POST['chapter'].",chapter) >0 and find_in_set(".$_POST['topic'].",topic) >0 and usageset='".$rowllo['id']."'");
												$rowtotalp=mysqli_fetch_array($seltotalp);

												$percentpvp = $rowtotalp['cnt']/$rowtotal['cnt'];
												if(is_nan($percentpvp))
													$percentpvp=0;
												else
													$percentpvp=$percentpvp;
												$percent_vpvp= number_format( $percentpvp * 100) . '%';


												

												$seltotalr=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1=1 and review_status='1' and find_in_set(".$_POST['chapter'].",chapter) >0 and find_in_set(".$_POST['topic'].",topic) >0 and usageset='".$rowllo['id']."'");
												$rowtotalr=mysqli_fetch_array($seltotalr);

												$seltotalpr=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1=1 and review_status='0' and find_in_set(".$_POST['chapter'].",chapter) >0 and find_in_set(".$_POST['topic'].",topic) >0 and usageset='".$rowllo['id']."'");
												$rowtotalpr=mysqli_fetch_array($seltotalpr);

												$percentpr = $rowtotalr['cnt']/$rowtotal1['cnt'];
												if(is_nan($percentpr))
													$percentpr=0;
												else
													$percentpr=$percentpr;
												$percent_vr= number_format( $percentpr * 100) . '%';

												$percentpr1 = $rowtotalpr['cnt']/$rowtotal1['cnt'];
												if(is_nan($percentpr1))
													$percentpr1=0;
												else
													$percentpr1=$percentpr1;
												$percent_vr1= number_format( $percentpr1 * 100) . '%';
												?>
												<tr>
													<td><?php  if($rowllo['usageset']!=''){ echo $rowllo['usageset']; } else{ echo '0'; } ?></td>
													<td><?php  if($rowtotal['cnt']!=''){ echo $rowtotal['cnt']; } else{ echo '0'; } ?></td>
													<td><?php  if($rowtotalv['cnt']!=''){ echo $rowtotalv['cnt']; } else{ echo '0'; } ?></td>
													<td><?php echo $percent_vpv; ?></td>
													<td><?php  if($rowtotalp['cnt']!=''){ echo $rowtotalp['cnt']; } else{ echo '0'; } ?></td>
													<td><?php echo $percent_vpvp; ?></td>
													<td><?php  if($rowtotalr['cnt']!=''){ echo $rowtotalr['cnt']; } else{ echo '0'; } ?></td>
													<td><?php echo $percent_vr; ?></td>
													<td><?php  if($rowtotalpr['cnt']!=''){ echo $rowtotalpr['cnt']; } else{ echo '0'; } ?></td>
													<td><?php echo $percent_vr1; ?></td>
												<?php
											}
											?>
											</tr>
										</tbody>
									</table>
								</div>
							<?php
							}
							?>
							<?php
							if($_POST['comp_tquestion']=='complexity'){
								if($_POST['chapter']!='' && $_POST['topic']==''){
								?>	<div id="Horizontalvertical">
										<div class="table-responsive pt-3" >
											<table class="table table-bordered">
												<thead>
													<tr>
														<th rowspan="2" colspan="2">
															<div class="col-lg-6">
																<select id="typev" class="form-control" name="typev" onChange="setState('Horizontalvertical','<?php echo SECURE_PATH;?>fitmentreport/ajax.php','gettypev=1&typev='+$('#typev').val()+'&chapter=<?php echo $_POST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>')" >	
																	<option value='horizontal' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'horizontal'){ echo ' selected="selected"';}}else { echo 'selected';} ?>>Horizontal </option>
																	<option value='vertical' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'vertical'){ echo ' selected="selected"';}}?>>Vertical</option>
																</select>
															</div>
														</th>
														<th colspan="2"></th>
														<th colspan="2">Easy</th>
														<th colspan="2">Moderate</th>
														<th colspan="2">Difficult</th>
														<th colspan="2">Highly Difficult</th>
														
													</tr>
													<tr>
														<th>Total</th>
														<th>%</th>
														<th>Total</th>
														<th>%</th>
														<th>Total</th>
														<th>%</th>
														<th>Total</th>
														<th>%</th>
														<th>Total</th>
														<th>%</th>
													</tr>

												</thead>
												<tbody>
													<?php
													$j=1;
													$selchapter=$database->query("select * from chapter where estatus='1' ".$chaptercon." ");
													while($rowchapter=mysqli_fetch_array($selchapter)){
														$totval=0;
														$totval_per=0;
														?>
														<tr>
															<th rowspan="6"><?php echo $rowchapter['chapter']; ?> Chapter Summary</th>
														</tr>
														<?php
														$sqllo=$database->query("select * from question_useset where estatus='1' ");
														$i=1;
														while($rowllo=mysqli_fetch_array($sqllo)){
															$seltot=$database->query("select count(id) as cnt from createquestion where estatus='1'  and usageset='".$rowllo['id']."' and find_in_set(".$rowchapter['id'].",chapter)>0  ");
															$rowtot=mysqli_fetch_array($seltot);

															$seltot1=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowchapter['id'].",chapter)>0 and usageset!='0'  ");
															$rowtot1=mysqli_fetch_array($seltot1);
															
															
															$pertot = $rowtot['cnt']/$rowtot1['cnt'];
															if(is_nan($pertot))
																$pertot=0;
															else
																$pertot=$pertot;
															$percenttot= number_format( $pertot * 100) . '%';
															$pecento= number_format( $pertot * 100);
															$totval=$totval+$rowtot['cnt'];
															$totval_per=$totval_per+$pecento;
															?>
																<tr>
																	<td><?php echo $rowllo['usageset']; ?></td>
																	<td><?php echo $rowtot['cnt']; ?></td>
																	<td><?php echo $percenttot; ?></td>
																	<?php
																	$sqllo1=$database->query("select * from complexity where estatus='1' ");
																	while($rowllo1=mysqli_fetch_array($sqllo1)){
																		
																		$sel=$database->query("select count(id) as cnt from createquestion where estatus='1'  and complexity='".$rowllo1['id']."' and usageset='".$rowllo['id']."' and find_in_set(".$rowchapter['id'].",chapter)>0  ");
																		$row=mysqli_fetch_array($sel);
																		
																		$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$rowchapter['id'].",chapter)>0  and usageset='".$rowllo['id']."' and complexity!=0  ");
																		$row1=mysqli_fetch_array($sel1);
																		$percentp = $row['cnt']/$row1['cnt'];
																		if(is_nan($percentp))
																			$percentp=0;
																		else
																			$percentp=$percentp;
																		$percent_vp= number_format( $percentp * 100) . '%';
																		?>

																	<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $rowchapter['id'];?>&usageset=<?php echo $rowllo['id'];?>&complexity=<?php echo $rowllo1['id'];?>")' ><?php  if($row['cnt']!=''){ echo $row['cnt']; } else{ echo '0'; } ?></a></td>
																	<td><?php echo $percent_vp; ?></td>
																	<?php } ?>
																	
																</tr>
															<?php
															
														$i++;
														}
														?>
															<tr>
															<td>Totals</td>
															<td><?php echo $totval; ?></td>
															<td><?php echo $totval_per; ?></td>
															<?php
															$sqllo1=$database->query("select * from complexity where estatus='1' ");
															while($rowllo1=mysqli_fetch_array($sqllo1)){
																
																$sel=$database->query("select count(id) as cnt from createquestion where estatus='1'  and complexity='".$rowllo1['id']."' and usageset!=0 and find_in_set(".$rowchapter['id'].",chapter)>0  ");
																$row=mysqli_fetch_array($sel);
																
																$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$rowchapter['id'].",chapter)>0  and usageset!=0 and complexity!=0  ");
																$row1=mysqli_fetch_array($sel1);
																$percentp = $row['cnt']/$row1['cnt'];
																if(is_nan($percentp))
																	$percentp=0;
																else
																	$percentp=$percentp;
																$percent_vp= number_format( $percentp * 100) . '%';
															?>
																
																<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $_POST['chapter'];?>&complexity=<?php echo $rowllo1['id'];?>")' ><?php echo $row['cnt']; ?></a></td>
																<td><?php echo $percent_vp; ?></td>
															<?php  } ?>
														</tr>
														<?php
													}
														?>
														
													
												</tbody>
											</table>
										</div>
									</div>
								<?php
							
										$sellc=$database->query("select * from fitment_data where estatus='1' and type='1' and comp_tquestion='1' and chapter='".$_POST['chapter']."' and username='".$session->username."'");
										$rowlcount1=mysqli_num_rows($sellc);
										if($rowlcount1>0){
											?>
												<div class="table-responsive pt-4">
													<table class="table table-bordered"  id="dataTable">
														  <thead>
															<tr>
															  <th scope="col">Chapter</th>
															 <th scope="col">Super Complex</th>
															   <th scope="col">Complex</th>
															  <th scope="col">Moderate</th>
															  <th scope="col">Easy</th>
															  <th scope="col">Comments</th>
															  
															</tr>
														  </thead>
														  <tbody>
															<?php
															$i=1;
															
															while($rowl=mysqli_fetch_array($sellc)){
																
																if($rowl['cha_easy']=='1'){ $cha_easy='Ok'; }else{ $cha_easy='Needed'; }
																if($rowl['cha_moderate']=='1'){ $cha_moderate='Ok'; }else{ $cha_moderate='Needed'; }
																if($rowl['cha_difficult']=='1'){ $cha_difficult='Ok'; }else{ $cha_difficult='Needed'; }
																if($rowl['cha_hdifficult']=='1'){ $cha_hdifficult='Ok'; }else{ $cha_hdifficult='Needed'; }
															?>
																<tr>
																  <td><?php echo $database->get_name('chapter','id',$rowl['chapter'],'chapter'); ?></td>
																  <td><?php echo $cha_easy; ?> </td>
																  <td><?php echo $cha_moderate; ?> </td>
																   <td><?php echo $cha_difficult; ?> </td>
																   <td><?php echo $cha_hdifficult; ?> </td>
																   <td><?php echo $rowl['chapter_comments']; ?> </td>
																</tr>
															<?php } ?>
															
														  </tbody>
														</table>
												</div>
											<?php
										}else{
								?>
								<div class="req-ext-qus-form my-2">
									<h6 class="my-4"><b><u><?php echo $database->get_name('chapter','id',$_POST['chapter'],'chapter'); ?> Chapter Required Extra Questions</u></b></h6>
									<form>
										<?php
											if(isset($_REQUEST['errordisplay'])){
												if($_REQUEST['errordisplay']=='1'){
													if($_REQUEST['cha_hdifficult']=='2'){
														$style='';
													}else if($_REQUEST['cha_hdifficult']=='1'){
														$style="style='display:none;'";
													}else{
														$style="style='display:none;'";
													}
													if($_REQUEST['cha_difficult']=='2'){
														$style1='';
													}else{
														$style1="style='display:none;'";
													}
													if($_REQUEST['cha_moderate']=='2'){
														$style2='';
													}else{
														$style2="style='display:none;'";
													}
													if($_REQUEST['cha_easy']=='2'){
														$style3='';
													}else{
														$style3="style='display:none;'";
													}
												}
											}else{
												$style="style='display:none;'";
												$style1="style='display:none;'";
												$style2="style='display:none;'";
												$style3="style='display:none;'";
											}
											?>
										<div class="row">
											<div class="col-lg-5">
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Super Complex<span style="color:red">*</span></label>
													<div class="col-sm-8">
														
														<select id="cha_hdifficult"  name="cha_hdifficult" class="cha_hdifficult form-control"  onchange="selectfunction();">
															<option value=''>-Select--</option>
															<option value='1' <?php if(isset($_POST['cha_hdifficult'])){ if($_POST['cha_hdifficult'] == '1'){ echo ' selected="selected"';};}?>>Ok</option>
															<option value='2' <?php if(isset($_POST['cha_hdifficult'])){ if($_POST['cha_hdifficult'] == '2'){ echo ' selected="selected"';};}?>>Needed</option>

														</select>
														 
														<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_hdifficult'])){ echo $_SESSION['error']['cha_hdifficult'];}?></span>
													</div>
												</div>
											</div>
											<div class="col-lg-5" >
												<div class="form-group row cha_hdiff_count1" <?php echo $style; ?> >
													<label class="col-sm-4 col-form-label">How Many<span style="color:red">*</span></label>
													<div class="col-sm-8">
														
														<input type="text" name="cha_hdiff_count" id="cha_hdiff_count" class="form-control" value="<?php if(isset($_REQUEST['cha_hdiff_count'])){ echo $_REQUEST['cha_hdiff_count']; }else{ echo ''; } ?>" onkeypress="return onlyNumbers(event);" maxlength='4'>
														 
														<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_hdiff_count'])){ echo $_SESSION['error']['cha_hdiff_count'];}?></span>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-5">
												<div class="form-group row">
													<label class="col-sm-4 col-form-label"> Complex<span style="color:red">*</span></label>
													<div class="col-sm-8">
														
														<select id="cha_difficult"  name="cha_difficult" class="cha_difficult form-control" onchange="selectfunction1();">
															<option value=''>-Select--</option>
															<option value='1' <?php if(isset($_POST['cha_difficult'])){ if($_POST['cha_difficult'] == '1'){ echo ' selected="selected"';};}?>>Ok</option>
															<option value='2' <?php if(isset($_POST['cha_difficult'])){ if($_POST['cha_difficult'] == '2'){ echo ' selected="selected"';};}?>>Needed</option>

														</select>
														 
														<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_difficult'])){ echo $_SESSION['error']['cha_difficult'];}?></span>
													</div>
												</div>
											</div>
											<div class="col-lg-5" >
												<div class="form-group row cha_diff_count1" <?php echo $style1; ?> >
													<label class="col-sm-4 col-form-label">How Many<span style="color:red">*</span></label>
													<div class="col-sm-8">
														
														<input type="text" name="cha_diff_count" id="cha_diff_count" class="form-control" value="<?php if(isset($_REQUEST['cha_diff_count'])){ echo $_REQUEST['cha_diff_count']; }else{ echo ''; } ?>"  onkeypress="return onlyNumbers(event);" maxlength='4'>
														 
														<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_diff_count'])){ echo $_SESSION['error']['cha_diff_count'];}?></span>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-5">
												<div class="form-group row">
													<label class="col-sm-4 col-form-label"> Moderate<span style="color:red">*</span></label>
													<div class="col-sm-8">
														
														<select id="cha_moderate"  name="cha_moderate" class="cha_moderate form-control" onchange="selectfunction2();">
															<option value=''>-Select--</option>
															<option value='1' <?php if(isset($_POST['cha_moderate'])){ if($_POST['cha_moderate'] == '1'){ echo ' selected="selected"';};}?> >Ok</option>
															<option value='2' <?php if(isset($_POST['cha_moderate'])){ if($_POST['cha_moderate'] == '2'){ echo ' selected="selected"';};}?> >Needed</option>

														</select>
														 
														<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_moderate'])){ echo $_SESSION['error']['cha_moderate'];}?></span>
													</div>
												</div>
											</div>
											<div class="col-lg-5" >
												<div class="form-group row cha_modera_count1" <?php echo $style2; ?> >
													<label class="col-sm-4 col-form-label">How Many<span style="color:red">*</span></label>
													<div class="col-sm-8">
														
														<input type="text" name="cha_modera_count" id="cha_modera_count"  class="form-control" value="<?php if(isset($_REQUEST['cha_modera_count'])){ echo $_REQUEST['cha_modera_count']; }else{ echo ''; } ?>" onkeypress="return onlyNumbers(event);" maxlength='4'>
														 
														<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_modera_count'])){ echo $_SESSION['error']['cha_modera_count'];}?></span>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-5">
												<div class="form-group row">
													<label class="col-sm-4 col-form-label"> Easy<span style="color:red">*</span></label>
													<div class="col-sm-8">
														<select id="cha_easy"  name="cha_easy" class="cha_easy form-control" onchange="selectfunction3();">
															<option value=''>-Select--</option>
															<option value='1' <?php if(isset($_POST['cha_easy'])){ if($_POST['cha_easy'] == '1'){ echo ' selected="selected"';};}?> >Ok</option>
															<option value='2' <?php if(isset($_POST['cha_easy'])){ if($_POST['cha_easy'] == '2'){ echo ' selected="selected"';};}?> >Needed</option>

														</select>
														 
														<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_easy'])){ echo $_SESSION['error']['cha_easy'];}?></span>
													</div>
												</div>
											</div>
											<div class="col-lg-5" >
												<div class="form-group row cha_easy_count1" <?php echo $style3; ?> >
													<label class="col-sm-4 col-form-label">How Many<span style="color:red">*</span></label>
													<div class="col-sm-8">
														
														<input type="text" name="cha_easy_count"  id="cha_easy_count" class="form-control" value="<?php if(isset($_REQUEST['cha_easy_count'])){ echo $_REQUEST['cha_easy_count']; }else{ echo ''; } ?>" onkeypress="return onlyNumbers(event);" maxlength='4'>
														 
														<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_easy_count'])){ echo $_SESSION['error']['cha_easy_count'];}?></span>
													</div>
												</div>
											</div>
										</div>
										
										<div class="row">
											<div class="col-lg-5">
												<div class="form-group row">
													<label class="col-sm-4 col-form-label"> Comments <span style="color:red">*</span></label>
													<div class="col-sm-8">
														
														<textarea name="chapter_comments" rows="5" class="form-control" id="chapter_comments" ><?php if(isset($_POST['chapter_comments'])){ if($_POST['chapter_comments']!=''){ echo $_POST['chapter_comments']; }} ?> </textarea>
														<span class="error text-danger"><?php if(isset($_SESSION['error']['chapter_comments'])){ echo $_SESSION['error']['chapter_comments'];}?></span>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6 text-center">
												
													<a href="#" type="sbmit"
														class="btn btn-primary text-white" onClick="setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','validateForm=1&chapter=<?php echo $_POST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&cha_hdifficult='+$('#cha_hdifficult').val()+'&cha_hdiff_count='+$('#cha_hdiff_count').val()+'&cha_difficult='+$('#cha_difficult').val()+'&cha_diff_count='+$('#cha_diff_count').val()+'&cha_moderate='+$('#cha_moderate').val()+'&cha_modera_count='+$('#cha_modera_count').val()+'&cha_easy='+$('#cha_easy').val()+'&cha_easy_count='+$('#cha_easy_count').val()+'&chapter_comments='+$('#chapter_comments').val()+'')">Submit</a>
												
											</div>
										</div>
									
										
									</form>
								</div>
								<?php  } }   ?>
							<?php
								if($_POST['topic_report']=='1'){
								?><div class="table-responsive pt-5" >
									<table class="table table-bordered">
										<thead>
											<tr>
												<th rowspan="2" colspan="2"></th>
												<th colspan="2">Easy</th>
												<th colspan="2">Moderate</th>
												<th colspan="2">Difficult</th>
												<th colspan="2">Highly Difficult</th>
											</tr>
											<tr>
												<th>Total</th>
												<th>%</th>
												<th>Total</th>
												<th>%</th>
												<th>Total</th>
												<th>%</th>
												<th>Total</th>
												<th>%</th>
											</tr>

										</thead>
										<tbody>
											<?php
											$j=1;
											$seltopic=$database->query("select * from topic where estatus='1'".$chaptercon1."  ".$topiccon."");
											while($rowtopic=mysqli_fetch_array($seltopic)){
												?>
												<tr>
													<th rowspan="5" ><?php echo $rowtopic['topic']; ?> Summary</th>
												</tr>
												<?php
												$sqllo=$database->query("select * from question_useset where estatus='1' ");
												while($rowllo=mysqli_fetch_array($sqllo)){
													?>
														<tr>
															<td><?php echo $rowllo['usageset']; ?></td>
															<?php
															$sqllo1=$database->query("select * from complexity where estatus='1' ");
															while($rowllo1=mysqli_fetch_array($sqllo1)){
																
																
																$sel=$database->query("select count(id) as cnt from createquestion where estatus='1' and complexity='".$rowllo1['id']."' and usageset='".$rowllo['id']."'  and find_in_set(".$_POST['chapter'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0 ");
																$row=mysqli_fetch_array($sel);
																$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$_POST['chapter'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0 and usageset='".$rowllo['id']."' and complexity!=''  ");
																$row1=mysqli_fetch_array($sel1);
																$percentp = $row['cnt']/$row1['cnt'];
																if(is_nan($percentp))
																	$percentp=0;
																else
																	$percentp=$percentp;
																$percent_vp= number_format( $percentp * 100) . '%';
																?>

															<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $_POST['id'];?>&topic=<?php echo $rowtopic['id'];?>&usageset=<?php echo $rowllo['id'];?>&complexity=<?php echo $rowllo1['id'];?>")' ><?php  if($row['cnt']!=''){ echo $row['cnt']; } else{ echo '0'; } ?></a></td>
															<td><?php echo $percent_vp; ?></td>
															<?php } ?>
															
														</tr>
													<?php
													
												
												}
												?>
													
												
											<?php
												$j++;
											}
											?>
											
											
										</tbody>
									</table>
								</div>
								<?php
								}else{
									
									if($_POST['chapter']!='' && $_POST['topic']!=''){
										
									?>
										<div id="Horizontalvertical1">
											<div class="table-responsive pt-5" >
												<table class="table table-bordered">
													<thead>
														<tr>
															<th rowspan="2" colspan="2">
																<div class="col-lg-6">
																		<select id="typev" class="form-control" name="typev" onChange="setState('Horizontalvertical1','<?php echo SECURE_PATH;?>fitmentreport/ajax.php','gettypev1=1&typev='+$('#typev').val()+'&chapter=<?php echo $_POST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>')" >	
																			<option value='horizontal' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'horizontal'){ echo ' selected="selected"';}}else { echo 'selected';} ?>>Horizontal </option>
																			<option value='vertical' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'vertical'){ echo ' selected="selected"';}}?>>Vertical</option>
																		</select>
																	</div>
															</th>
															<th colspan="2"></th>
															<th colspan="2">Easy</th>
															<th colspan="2">Moderate</th>
															<th colspan="2">Difficult</th>
															<th colspan="2">Highly Difficult</th>
														</tr>
														<tr>
															<th>Total</th>
															<th>%</th>
															<th>Total</th>
															<th>%</th>
															<th>Total</th>
															<th>%</th>
															<th>Total</th>
															<th>%</th>
															<th>Total</th>
															<th>%</th>
														</tr>

													</thead>
													<tbody>
														<?php
														$j=1;
														$seltopic=$database->query("select * from topic where estatus='1'".$chaptercon1."  ".$topiccon."");
														while($rowtopic=mysqli_fetch_array($seltopic)){
															?>
															<tr>
																<th rowspan="5" ><?php echo $rowtopic['topic']; ?> Summary</th>
															</tr>
															<?php
															$totval=0;
															$totval_per=0;
															$sqllo=$database->query("select * from question_useset where estatus='1' ");
															while($rowllo=mysqli_fetch_array($sqllo)){
																$seltot=$database->query("select count(id) as cnt from createquestion where estatus='1'  and usageset='".$rowllo['id']."' and find_in_set(".$rowchapter['id'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0 ");
																	$rowtot=mysqli_fetch_array($seltot);

																	$seltot1=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowchapter['id'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0  and usageset!='0' ");
																	$rowtot1=mysqli_fetch_array($seltot1);
																	
																	
																	$pertot = $rowtot['cnt']/$rowtot1['cnt'];
																	if(is_nan($pertot))
																		$pertot=0;
																	else
																		$pertot=$pertot;
																	$percenttot= number_format( $pertot * 100) . '%';
																	$pecento= number_format( $pertot * 100);
																	$totval=$totval+$rowtot['cnt'];
																	$totval_per=$totval_per+$pecento;
																?>
																	<tr>
																		<td><?php echo $rowllo['usageset']; ?></td>
																		<td><?php echo $rowtot['cnt']; ?></td>
																		<td><?php echo $percenttot; ?></td>
																		<?php
																		$sqllo1=$database->query("select * from complexity where estatus='1' ");
																		while($rowllo1=mysqli_fetch_array($sqllo1)){
																			
																			
																			$sel=$database->query("select count(id) as cnt from createquestion where estatus='1' and complexity='".$rowllo1['id']."' and usageset='".$rowllo['id']."'  and find_in_set(".$_POST['chapter'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0 ");
																			$row=mysqli_fetch_array($sel);
																			$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$_POST['chapter'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0 and usageset='".$rowllo['id']."' and complexity!=''  ");
																			$row1=mysqli_fetch_array($sel1);
																			$percentp = $row['cnt']/$row1['cnt'];
																			if(is_nan($percentp))
																				$percentp=0;
																			else
																				$percentp=$percentp;
																			$percent_vp= number_format( $percentp * 100) . '%';
																			?>

																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $_POST['id'];?>&topic=<?php echo $rowtopic['id'];?>&usageset=<?php echo $rowllo['id'];?>&complexity=<?php echo $rowllo1['id'];?>")' ><?php  if($row['cnt']!=''){ echo $row['cnt']; } else{ echo '0'; } ?></a></td>
																		<td><?php echo $percent_vp; ?></td>
																		<?php } ?>
																		
																	</tr>
																<?php
																
															
															}
															?>
																
															
														<?php
															$j++;
														}
														?>
														
														
													</tbody>
												</table>
											</div>
										</div>
									<?php
										
								if(isset($_REQUEST['errordisplay1'])){
								if($_REQUEST['errordisplay1']=='1'){
									if($_REQUEST['topic_hdifficult']=='2'){
										$style='';
									}else if($_REQUEST['topic_hdifficult']=='1'){
										$style="style='display:none;'";
									}else{
										$style="style='display:none;'";
									}
									if($_REQUEST['topic_difficult']=='2'){
										$style1='';
									}else{
										$style1="style='display:none;'";
									}
									if($_REQUEST['topic_moderate']=='2'){
										$style2='';
									}else{
										$style2="style='display:none;'";
									}
									if($_REQUEST['topic_easy']=='2'){
										$style3='';
									}else{
										$style3="style='display:none;'";
									}
								}
							}else{
								$style="style='display:none;'";
								$style1="style='display:none;'";
								$style2="style='display:none;'";
								$style3="style='display:none;'";
							}
							?>
							<?php
							
									$sellc=$database->query("select * from fitment_data where estatus='1' and type='2' and comp_tquestion='1' and chapter='".$_POST['chapter']."' and topic='".$_POST['topic']."' and username='".$session->username."'");
										$rowlcount1=mysqli_num_rows($sellc);
										if($rowlcount1>0){
											?>
												<div class="table-responsive pt-4">
													<table class="table table-bordered"  id="dataTable">
														  <thead>
															<tr>
															  <th scope="col">Chapter</th>
															 <th scope="col">Super Complex</th>
															   <th scope="col">Complex</th>
															  <th scope="col">Moderate</th>
															  <th scope="col">Easy</th>
															  <th scope="col">Comments</th>
															  
															</tr>
														  </thead>
														  <tbody>
															<?php
															$i=1;
															
															while($rowl=mysqli_fetch_array($sellc)){
																
																if($rowl['cha_easy']=='1'){ $cha_easy='Ok'; }else{ $cha_easy='Needed'; }
																if($rowl['cha_moderate']=='1'){ $cha_moderate='Ok'; }else{ $cha_moderate='Needed'; }
																if($rowl['cha_difficult']=='1'){ $cha_difficult='Ok'; }else{ $cha_difficult='Needed'; }
																if($rowl['cha_hdifficult']=='1'){ $cha_hdifficult='Ok'; }else{ $cha_hdifficult='Needed'; }
															?>
																<tr>
																  <td><?php echo $database->get_name('chapter','id',$rowl['chapter'],'chapter'); ?></td>
																  <td><?php echo $cha_easy; ?> </td>
																  <td><?php echo $cha_moderate; ?> </td>
																   <td><?php echo $cha_difficult; ?> </td>
																   <td><?php echo $cha_hdifficult; ?> </td>
																   <td><?php echo $rowl['chapter_comments']; ?> </td>
																</tr>
															<?php } ?>
															
														  </tbody>
														</table>
												</div>
											<?php
										}else{
								?>
							<div class="req-ext-qus-form my-2">
                                <h6 class="my-4"><b><u><?php echo $database->get_name('topic','id',$_POST['topic'],'topic'); ?> Topic  Required Extra Questions</u></b></h6>
                                <form>
									<div class="row">
										<div class="col-lg-5">
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Super Complex<span style="color:red">*</span></label>
												<div class="col-sm-8">
													
													<select id="topic_hdifficult"  name="topic_hdifficult" class="topic_hdifficult form-control"  onchange="tselectfunction();">
														<option value=''>-Select--</option>
														<option value='1' <?php if(isset($_POST['topic_hdifficult'])){ if($_POST['topic_hdifficult'] == '1'){ echo ' selected="selected"';};}?>>Ok</option>
														<option value='2' <?php if(isset($_POST['topic_hdifficult'])){ if($_POST['topic_hdifficult'] == '2'){ echo ' selected="selected"';};}?>>Needed</option>

													</select>
													 
													<span class="error text-danger"><?php if(isset($_SESSION['error']['topic_hdifficult'])){ echo $_SESSION['error']['topic_hdifficult'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-5">
											<div class="form-group row topic_hdiff_count1" <?php echo $style; ?>>
												<label class="col-sm-4 col-form-label">How Many<span style="color:red">*</span></label>
												<div class="col-sm-8">
													
													<input type="text" name="topic_hdiff_count" id="topic_hdiff_count" class="form-control" value="<?php if(isset($_REQUEST['topic_hdiff_count'])){ echo $_REQUEST['topic_hdiff_count']; }else{ echo ''; } ?>" onkeypress="return onlyNumbers(event);" maxlength='4' >
													 
													<span class="error text-danger"><?php if(isset($_SESSION['error']['topic_hdiff_count'])){ echo $_SESSION['error']['topic_hdiff_count'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-5">
											<div class="form-group row">
												<label class="col-sm-4 col-form-label"> Complex<span style="color:red">*</span></label>
												<div class="col-sm-8">
													
													<select id="topic_difficult"  name="topic_difficult" class="topic_difficult form-control" onchange="tselectfunction1();">
														<option value=''>-Select--</option>
														<option value='1' <?php if(isset($_POST['topic_difficult'])){ if($_POST['topic_difficult'] == '1'){ echo ' selected="selected"';};}?>>Ok</option>
														<option value='2' <?php if(isset($_POST['topic_difficult'])){ if($_POST['topic_difficult'] == '2'){ echo ' selected="selected"';};}?>>Needed</option>

													</select>
													 
													<span class="error text-danger"><?php if(isset($_SESSION['error']['topic_difficult'])){ echo $_SESSION['error']['topic_difficult'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-5" >
											<div class="form-group row topic_diff_count1" <?php echo $style1; ?>>
												<label class="col-sm-4 col-form-label">How Many<span style="color:red">*</span></label>
												<div class="col-sm-8">
													
													<input type="text" name="topic_diff_count" id="topic_diff_count" class="form-control" value="<?php if(isset($_REQUEST['topic_diff_count'])){ echo $_REQUEST['topic_diff_count']; }else{ echo ''; } ?>"  onkeypress="return onlyNumbers(event);" maxlength='4'>
													 
													<span class="error text-danger"><?php if(isset($_SESSION['error']['topic_diff_count'])){ echo $_SESSION['error']['topic_diff_count'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-5">
											<div class="form-group row">
												<label class="col-sm-4 col-form-label"> Moderate<span style="color:red">*</span></label>
												<div class="col-sm-8">
													
													<select id="topic_moderate"  name="topic_moderate" class="topic_moderate form-control" onchange="tselectfunction2();">
														<option value=''>-Select--</option>
														<option value='1' <?php if(isset($_POST['topic_moderate'])){ if($_POST['topic_moderate'] == '1'){ echo ' selected="selected"';};}?> >Ok</option>
														<option value='2' <?php if(isset($_POST['topic_moderate'])){ if($_POST['topic_moderate'] == '2'){ echo ' selected="selected"';};}?> >Needed</option>

													</select>
													 
													<span class="error text-danger"><?php if(isset($_SESSION['error']['topic_moderate'])){ echo $_SESSION['error']['topic_moderate'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-5" >
											<div class="form-group row topic_modera_count1" <?php echo $style2; ?>>
												<label class="col-sm-4 col-form-label">How Many <span style="color:red">*</span></label>
												<div class="col-sm-8">
													
													<input type="text" name="topic_modera_count" id="topic_modera_count"  class="form-control" value="<?php if(isset($_REQUEST['topic_modera_count'])){ echo $_REQUEST['topic_modera_count']; }else{ echo ''; } ?>" onkeypress="return onlyNumbers(event);" maxlength='4'>
													 
													<span class="error text-danger"><?php if(isset($_SESSION['error']['topic_modera_count'])){ echo $_SESSION['error']['topic_modera_count'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-5">
											<div class="form-group row">
												<label class="col-sm-4 col-form-label"> Easy <span style="color:red">*</span></label>
												<div class="col-sm-8">
													<select id="topic_easy"  name="topic_easy" class="topic_easy form-control" onchange="tselectfunction3();">
														<option value=''>-Select--</option>
														<option value='1' <?php if(isset($_POST['topic_easy'])){ if($_POST['topic_easy'] == '1'){ echo ' selected="selected"';};}?>>Ok</option>
														<option value='2' <?php if(isset($_POST['topic_easy'])){ if($_POST['topic_easy'] == '2'){ echo ' selected="selected"';};}?>>Needed</option>

													</select>
													 
													<span class="error text-danger"><?php if(isset($_SESSION['error']['topic_easy'])){ echo $_SESSION['error']['topic_easy'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-5" >
											<div class="form-group row topic_easy_count1" <?php echo $style3; ?>>
												<label class="col-sm-4 col-form-label">How Many <span style="color:red">*</span></label>
												<div class="col-sm-8">
													
													<input type="text" name="topic_easy_count"  id="topic_easy_count" class="form-control" value="<?php if(isset($_REQUEST['topic_easy_count'])){ echo $_REQUEST['topic_easy_count']; }else{ echo ''; } ?>" onkeypress="return onlyNumbers(event);" maxlength='4'>
													 
													<span class="error text-danger"><?php if(isset($_SESSION['error']['topic_easy_count'])){ echo $_SESSION['error']['topic_easy_count'];}?></span>
												</div>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-lg-5">
											<div class="form-group row">
												<label class="col-sm-4 col-form-label"> Comments <span style="color:red">*</span></label>
												<div class="col-sm-8">
													
													<textarea name="topic_comments" rows="5" class="form-control" id="topic_comments" ><?php if(isset($_POST['topic_comments'])){ if($_POST['topic_comments']!=''){ echo $_POST['topic_comments']; }} ?> </textarea>
													<span class="error text-danger"><?php if(isset($_SESSION['error']['topic_comments'])){ echo $_SESSION['error']['topic_comments'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-6 text-center">
											
												
												<a href="#" type="sbmit"
													class="btn btn-primary text-white" onClick="setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','validateForm1=1&chapter=<?php echo $_POST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&topic_hdifficult='+$('#topic_hdifficult').val()+'&topic_hdiff_count='+$('#topic_hdiff_count').val()+'&topic_difficult='+$('#topic_difficult').val()+'&topic_diff_count='+$('#topic_diff_count').val()+'&topic_moderate='+$('#topic_moderate').val()+'&topic_modera_count='+$('#topic_modera_count').val()+'&topic_easy='+$('#topic_easy').val()+'&topic_easy_count='+$('#topic_easy_count').val()+'&topic_comments='+$('#topic_comments').val()+'')">Submit</a>
										</div>
									</div>
								
                                    
                                </form>
                            </div>
							<?php } }  }  ?>
							<?php
							}else if($_POST['comp_tquestion']=='inputquestion'){
								if($_POST['chapter']!='' && $_POST['topic']==''){
							?>
									<div id="Horizontalvertical2">
										<div class="table-responsive pt-5">
											 <div id="table-scroll" class="table-scroll">
												 <div class="table-wrap">
													<table class="main-table table table-bordered">
														<thead>
															<tr>
																<th class="fixed-side" rowspan="2" colspan="2">
																<th colspan="2"></th>	
																</th>
																<?php
																$sel=$database->query("select * from questiontype where estatus='1'");
																while($row=mysqli_fetch_array($sel)){
																?>
																	<th  colspan="2"><?php echo $row['questiontype']; ?></th>
																<?php
																}
																?>
																
															</tr>
																
															<tr>
																<th>Total</th>
																<th>%</th>
																<?php
																$sel=$database->query("select * from questiontype where estatus='1'");
																while($row=mysqli_fetch_array($sel)){
																?>
																	<th>Total</th>
																	<th>%</th>
																<?php } ?>
															</tr>

														</thead>
														<tbody>
															<?php
															$j=1;
															$selchapter=$database->query("select * from chapter where estatus='1' ".$chaptercon." ");
															while($rowchapter=mysqli_fetch_array($selchapter)){
																?>
																<tr>
																	<th rowspan="6" class="fixed-side" ><?php echo $rowchapter['chapter']; ?> Chapter Summary</th>
																</tr>
																<?php
																$sqllo=$database->query("select * from question_useset where estatus='1' ");
																$i=1;
																$totval=0;
																$totval_per=0;
																while($rowllo=mysqli_fetch_array($sqllo)){
																	$seltot=$database->query("select count(id) as cnt from createquestion where estatus='1'  and usageset='".$rowllo['id']."' and find_in_set(".$rowchapter['id'].",chapter)>0  ");
																	$rowtot=mysqli_fetch_array($seltot);

																	$seltot1=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowchapter['id'].",chapter)>0  and usageset!='0' ");
																	$rowtot1=mysqli_fetch_array($seltot1);
																	$pertot = $rowtot['cnt']/$rowtot1['cnt'];
																	if(is_nan($pertot))
																		$pertot=0;
																	else
																		$pertot=$pertot;
																	$percenttot= number_format( $pertot * 100) . '%';
																	$pecento= number_format( $pertot * 100);
																	$totval=$totval+$rowtot['cnt'];
																	$totval_per=$totval_per+$pecento;
																	?>
																		<tr>
																			<td class="fixed-side"><?php echo $rowllo['usageset']; ?></td>
																			<td><?php echo $rowtot['cnt']; ?></td>
																			<td><?php echo $percenttot; ?></td>
																			<?php
																			$sqllo1=$database->query("select * from questiontype where estatus='1' ");
																			while($rowllo1=mysqli_fetch_array($sqllo1)){
																				
																				$sel=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowllo1['id'].",inputquestion)>0  and usageset='".$rowllo['id']."' and find_in_set(".$rowchapter['id'].",chapter)>0  ");
																				$row=mysqli_fetch_array($sel);
																				
																				$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$rowchapter['id'].",chapter)>0  and usageset='".$rowllo['id']."' and inputquestion!=''  ");
																				$row1=mysqli_fetch_array($sel1);
																				$percentp = $row['cnt']/$row1['cnt'];
																				if(is_nan($percentp))
																					$percentp=0;
																				else
																					$percentp=$percentp;
																				$percent_vp= number_format( $percentp * 100) . '%';
																				?>

																			<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $_POST['chapter'];?>&usageset=<?php echo $rowllo['id'];?>&questiontype=<?php echo $rowllo1['id'];?>")' ><?php  if($row['cnt']!=''){ echo $row['cnt']; } else{ echo '0'; } ?></a></td>
																			<td><?php echo $percent_vp; ?></td>
																			<?php } ?>
																			
																		</tr>
																	<?php
																	
																$i++;
																}
																?>
																	<tr>
																	<td class="fixed-side">Totals</td>
																	<td><?php echo $totval; ?></td>
																	<td><?php echo $totval_per; ?></td>
																	<?php
																	$sqllo1=$database->query("select * from questiontype where estatus='1' ");
																	while($rowllo1=mysqli_fetch_array($sqllo1)){
																		
																		$sel=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowllo1['id'].",inputquestion)>0  and usageset!=0 and find_in_set(".$rowchapter['id'].",chapter)>0  ");
																		$row=mysqli_fetch_array($sel);
																		
																		$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$rowchapter['id'].",chapter)>0  and usageset!=0 and inputquestion!=''  ");
																		$row1=mysqli_fetch_array($sel1);
																		$percentp = $row['cnt']/$row1['cnt'];
																		if(is_nan($percentp))
																			$percentp=0;
																		else
																			$percentp=$percentp;
																		$percent_vp= number_format( $percentp * 100) . '%';
																	?>
																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $_POST['chapter'];?>&questiontype=<?php echo $rowllo1['id'];?>")' ><?php echo $row['cnt']; ?></a></td>
																		<td><?php echo $percent_vp; ?></td>
																	<?php  } ?>
																</tr>
																<?php
															}
																?>
																
															
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
									<div class="req-ext-qus-form my-2">
										
										<form>

										<?php
												
												
										if($session->userlevel!=9){
											if($_POST['chapter']!='' && $_POST['topic']==''){
											$sellc=$database->query("select * from fitment_data where estatus='1' and type='1' and comp_tquestion='2' and chapter='".$_POST['chapter']."' and username='".$session->username."'");
											$rowlcount1=mysqli_num_rows($sellc);
											if($rowlcount1>0){
												?>
													<div class="table-responsive pt-4">
														<table class="table table-bordered"  id="dataTable">
															  <thead>
																<tr>
																  <th scope="col">Chapter</th>
																<?php
																	$selt=$database->query("select * from questiontype where estatus='1'");
																	while($rowt=mysqli_fetch_array($selt)){
																		?>
																			<th scope="col"><?php echo $rowt['questiontype']; ?></th>
																		<?php
																	}
																	?>
																  <th scope="col">Comments</th>
																  
																</tr>
															  </thead>
															  <tbody>
																<?php
																$i=1;
																
																while($rowl=mysqli_fetch_array($sellc)){
																	
																	$qdata=json_decode($rowl['qtypedata'],true);
																?>
																	<tr>
																	  <td><?php echo $database->get_name('chapter','id',$rowl['chapter'],'chapter'); ?></td>
																	 <?php
																	if(count($qdata)>0){
																		foreach($qdata as $data){
																				if($data['type']=='1'){
																					$type="Ok";
																				}else if($data['type']=='2'){
																					$type="Needed";
																				}
																				?>
																					<td><?php echo $type; ?> </td>
																			<?php
																			}
																		}
																		?>
																	   <td><?php echo $rowl['chapter_comments']; ?> </td>
																	</tr>
																<?php } ?>
																
															  </tbody>
															</table>
													</div>
												<?php	
											}else{
											?>
											<h6 class="my-4"><b><u><?php echo $database->get_name('chapter','id',$_POST['chapter'],'chapter'); ?> Chapter  Required Extra Questions</u></b></h6>
											<div id ="qtypelist">
												<?php
												$kl=1;
												if(isset($_REQUEST['editform'])){
													$posdata1 = explode('^',rtrim($_POST['qtypedata']));
														foreach($posdata1 as $rowpost2)
														{
															if(strlen($rowpost2)>0)
															{
																$rowpost = explode('_',$rowpost2);
																$sql1=$database->query("select * from questiontype where estatus='1' and id='".$rowpost[0]."' ");
																$row1=mysqli_fetch_array($sql1);
																if($rowpost[1]=='2'){
																	$style="";
																}else{
																	$style="style='display:none;'";
																}
															?>	
																<div class="row qtypelist">
																	<div class="col-lg-5">
																		<div class="form-group row">
																			<label class="col-sm-4 col-form-label"><?php echo $row1['questiontype']; ?> <span style="color:red">*</span></label>
																			<div class="col-sm-8">
																				
																				<select id="cha_qtype<?php echo $rowpost[0]; ?>"  name="cha_qtype<?php echo $kl; ?>" class="cha_qtype form-control"  onchange="selectfunctioncha(<?php echo $rowpost[0]; ?>);">
																					<option value=''>-Select--</option>
																					<option value='1' <?php if(isset($rowpost[1])){ if($rowpost[1] == '1'){ echo ' selected="selected"';};}?>>Ok</option>
																					<option value='2' <?php if(isset($rowpost[1])){ if($rowpost[1] == '2'){ echo ' selected="selected"';};}?>>Needed</option>

																				</select>
																				 
																				<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_qtype'.$kl])){ echo $_SESSION['error']['cha_qtype'.$kl];}?></span>
																			</div>
																		</div>
																	</div>
																	<input type="hidden" id="qtype_id"  class="qtype_id" value="<?php echo $rowpost[0]; ?>" >
																	<div class="col-lg-5" >
																		<div class="form-group row cha_qtype_count<?php echo $rowpost[0]; ?>" <?php echo $style; ?> >
																			<label class="col-sm-4 col-form-label">How Many <span style="color:red">*</span></label>
																			<div class="col-sm-8">
																				
																				<input type="text" name="cha_qtype_count" id="cha_qtype_count" class="form-control cha_qtype_count" value="<?php if(isset($rowpost[2])){ echo $rowpost[2]; }else{ echo ''; } ?>" onkeypress="return onlyNumbers(event);" maxlength='4'>
																				 
																				<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_qtype_count_'.$kl])){ echo $_SESSION['error']['cha_qtype_count_'.$kl];}?></span>
																			</div>
																		</div>
																	</div>
																</div>
															<?php
																$kl++;
															}
														}
												}else{
												$style="style='display:none;'";
												$k=1;
												$sql=$database->query("select * from questiontype where estatus='1' ");
												while($row=mysqli_fetch_array($sql)){
											?>
													<div class="row qtypelist">
														<div class="col-lg-5">
															<div class="form-group row">
																<label class="col-sm-4 col-form-label"><?php echo $row['questiontype']; ?><span style="color:red">*</span></label>
																<div class="col-sm-8">
																	
																	<select id="cha_qtype<?php echo $row['id']; ?>"  name="cha_qtype<?php echo $k; ?>" class="cha_qtype form-control"  onchange="selectfunctioncha(<?php echo $row['id']; ?>);">
																		<option value=''>-Select--</option>
																		<option value='1' <?php if(isset($_POST['cha_qtype'])){ if($_POST['cha_qtype'] == '1'){ echo ' selected="selected"';};}?>>Ok</option>
																		<option value='2' <?php if(isset($_POST['cha_qtype'])){ if($_POST['cha_qtype'] == '2'){ echo ' selected="selected"';};}?>>Needed</option>

																	</select>
																	 
																	<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_qtype'][$k])){ echo $_SESSION['error']['cha_qtype'][$k];}?></span>
																</div>
															</div>
														</div>
														<input type="hidden" id="qtype_id"  class="qtype_id" value="<?php echo $row['id']; ?>" >
														<div class="col-lg-5" >
															<div class="form-group row cha_qtype_count<?php echo $row['id']; ?>" <?php echo $style; ?> >
																<label class="col-sm-4 col-form-label">How Many <span style="color:red">*</span></label>
																<div class="col-sm-8">
																	
																	<input type="text" name="cha_qtype_count" id="cha_qtype_count" class="form-control cha_qtype_count" value="<?php if(isset($_REQUEST['cha_qtype_count'])){ echo $_REQUEST['cha_qtype_count']; }else{ echo ''; } ?>" onkeypress="return onlyNumbers(event);" maxlength='4'>
																	 
																	<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_qtype_count_'.$k])){ echo $_SESSION['error']['cha_qtype_count_'.$k];}?></span>
																</div>
															</div>
														</div>
													</div>
												<?php $k++; }  } ?>

											</div>	
												
												<div class="row">
													<div class="col-lg-5">
														<div class="form-group row">
															<label class="col-sm-4 col-form-label"> Comments <span style="color:red">*</span></label>
															<div class="col-sm-8">
																
																<textarea name="chapter_comments" rows="5" class="form-control" id="chapter_comments" ><?php if(isset($_POST['chapter_comments'])){ if($_POST['chapter_comments']!=''){ echo $_POST['chapter_comments']; }} ?> </textarea>
																<span class="error text-danger"><?php if(isset($_SESSION['error']['chapter_comments'])){ echo $_SESSION['error']['chapter_comments'];}?></span>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-lg-6 text-center">
														
															<a href="#" type="sbmit"
																class="btn btn-primary text-white" onClick="setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','validateForm2=1&chapter=<?php echo $_POST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&qtypedata='+qtypedata()+'&chapter_comments='+$('#chapter_comments').val()+'')">Submit</a>
														
													</div>
												</div>
											
												
											</form>
									</div>
								<?php	 } } 
									} 
								
								if($_POST['topic_report']=='1'){
									?>
									<div class="table-responsive pt-5" >
										 
												<table class="table table-bordered main-table">
													<thead>
														<tr>
															<th rowspan="2" colspan="2"></th>
															<?php
															$sel=$database->query("select * from questiontype where estatus='1'");
															while($row=mysqli_fetch_array($sel)){
															?>
																<th colspan="2" ><?php echo $row['questiontype']; ?></th>
															<?php
															}
															?>
															
														</tr>
														<tr>
															<?php
															$sel=$database->query("select * from questiontype where estatus='1'");
															while($row=mysqli_fetch_array($sel)){
															?>
																<th>Total</th>
																<th>%</th>
															<?php } ?>
														</tr>

													</thead>
													<tbody>
														<?php
														$j=1;
														$seltopic=$database->query("select * from topic where estatus='1'".$chaptercon1."  ".$topiccon."");
														while($rowtopic=mysqli_fetch_array($seltopic)){
															?>
															<tr>
																<th rowspan="5" ><?php echo $rowtopic['topic']; ?> Summary</th>
															</tr>
															<?php
															$sqllo=$database->query("select * from question_useset where estatus='1' ");
															while($rowllo=mysqli_fetch_array($sqllo)){
																?>
																	<tr>
																		<td><?php echo $rowllo['usageset']; ?></td>
																		<?php
																		$sqllo1=$database->query("select * from questiontype where estatus='1' ");
																		while($rowllo1=mysqli_fetch_array($sqllo1)){
																			
																			$sel=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowllo1['id'].",inputquestion)>0  and usageset='".$rowllo['id']."'  and find_in_set(".$_POST['chapter'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0 ");
																			$row=mysqli_fetch_array($sel);
																			$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$_POST['chapter'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0 and usageset='".$rowllo['id']."' and inputquestion!=''  ");
																			$row1=mysqli_fetch_array($sel1);
																			$percentp = $row['cnt']/$row1['cnt'];
																			if(is_nan($percentp))
																				$percentp=0;
																			else
																				$percentp=$percentp;
																			$percent_vp= number_format( $percentp * 100) . '%';
																			?>

																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $_POST['chapter'];?>&topic=<?php echo $_POST['topic'];?>&usageset=<?php echo $rowllo['id'];?>&questiontype=<?php echo $rowllo1['id'];?>")' ><?php  if($row['cnt']!=''){ echo $row['cnt']; } else{ echo '0'; } ?></a></td>
																		<td><?php echo $percent_vp; ?></td>
																		<?php } ?>
																		
																	</tr>
																<?php
																
															
															}
															?>
																
															
														<?php
															$j++;
														}
														?>
													</tbody>
												</table>
											
									</div>
									<?php
								}else{
									if($_POST['chapter']!='' && $_POST['topic']!=''){

									?>
									<div class="table-responsive pt-5" >
										<div id="table-scroll" class="table-scroll">
											 <div class="table-wrap">
												<table class="table table-bordered main-table">
													<thead>
														<tr>
															<th class="fixed-side" rowspan="2" colspan="2" ></th>
															<th colspan="2"></th>	
															<?php
															$sel=$database->query("select * from questiontype where estatus='1'");
															while($row=mysqli_fetch_array($sel)){
															?>
																<th colspan="2" ><?php echo $row['questiontype']; ?></th>
															<?php
															}
															?>
															
														</tr>
														<tr>
															<th>Total</th>
															<th>%</th>
															<?php
															$sel=$database->query("select * from questiontype where estatus='1'");
															while($row=mysqli_fetch_array($sel)){
															?>
																<th>Total</th>
																<th>%</th>
															<?php } ?>
														</tr>

													</thead>
													<tbody>
														<?php
														$j=1;
														$seltopic=$database->query("select * from topic where estatus='1'".$chaptercon1."  ".$topiccon."");
														while($rowtopic=mysqli_fetch_array($seltopic)){
															?>
															<tr>
																<th rowspan="5" class="fixed-side" ><?php echo $rowtopic['topic']; ?> Summary</th>
															</tr>
															<?php
															$sqllo=$database->query("select * from question_useset where estatus='1' ");
															while($rowllo=mysqli_fetch_array($sqllo)){
																$seltot=$database->query("select count(id) as cnt from createquestion where estatus='1'  and usageset='".$rowllo['id']."' and find_in_set(".$rowtopic['chapter'].",chapter)>0 and find_in_set(".$rowtopic['id'].",topic)>0  ");
																$rowtot=mysqli_fetch_array($seltot);

																$seltot1=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowtopic['chapter'].",chapter)>0 and find_in_set(".$rowtopic['id'].",topic)>0  and usageset!='0' ");
																$rowtot1=mysqli_fetch_array($seltot1);
																$pertot = $rowtot['cnt']/$rowtot1['cnt'];
																if(is_nan($pertot))
																	$pertot=0;
																else
																	$pertot=$pertot;
																$percenttot= number_format( $pertot * 100) . '%';
																$pecento= number_format( $pertot * 100);
																$totval=$totval+$rowtot['cnt'];
																$totval_per=$totval_per+$pecento;
																?>
																	<tr>
																		<td class="fixed-side"><?php echo $rowllo['usageset']; ?></td>
																		<td><?php echo $rowtot['cnt']; ?></td>
																		<td><?php echo $percenttot; ?></td>
																		<?php
																		$sqllo1=$database->query("select * from questiontype where estatus='1' ");
																		while($rowllo1=mysqli_fetch_array($sqllo1)){
																			
																			$sel=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowllo1['id'].",inputquestion)>0  and usageset='".$rowllo['id']."'  and find_in_set(".$_POST['chapter'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0 ");
																			$row=mysqli_fetch_array($sel);
																			$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$_POST['chapter'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0 and usageset='".$rowllo['id']."' and inputquestion!=''  ");
																			$row1=mysqli_fetch_array($sel1);
																			$percentp = $row['cnt']/$row1['cnt'];
																			if(is_nan($percentp))
																				$percentp=0;
																			else
																				$percentp=$percentp;
																			$percent_vp= number_format( $percentp * 100) . '%';
																			?>

																		<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $_POST['chapter'];?>&topic=<?php echo $_POST['topic'];?>&usageset=<?php echo $rowllo['id'];?>&questiontype=<?php echo $rowllo1['id'];?>")' ><?php  if($row['cnt']!=''){ echo $row['cnt']; } else{ echo '0'; } ?></a></td>
																		<td><?php echo $percent_vp; ?></td>
																		<?php } ?>
																		
																	</tr>
																<?php
																
															
															}
															?>
																
															
														<?php
															$j++;
														}
														?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<?php
									}	}
								if($_REQUEST['chapter']!='' && $_REQUEST['topic']!=''){
									$sellc=$database->query("select * from fitment_data where estatus='1' and type='2' and comp_tquestion='2' and chapter='".$_POST['chapter']."' and topic='".$_POST['topic']."' and username='".$session->username."'");
									$rowlcount1=mysqli_num_rows($sellc);
									if($rowlcount1>0){
										?>
											<div class="table-responsive pt-4">
												<table class="table table-bordered"  id="dataTable">
													  <thead>
														<tr>
														  <th scope="col">Chapter</th>
														<?php
															$selt=$database->query("select * from questiontype where estatus='1'");
															while($rowt=mysqli_fetch_array($selt)){
																?>
																	<th scope="col"><?php echo $rowt['questiontype']; ?></th>
																<?php
															}
															?>
														  <th scope="col">Comments</th>
														  
														</tr>
													  </thead>
													  <tbody>
														<?php
														$i=1;
														
														while($rowl=mysqli_fetch_array($sellc)){
															
															$qdata=json_decode($rowl['qtypedata'],true);
														?>
															<tr>
															  <td><?php echo $database->get_name('chapter','id',$rowl['chapter'],'chapter'); ?></td>
															 <?php
															if(count($qdata)>0){
																foreach($qdata as $data){
																		if($data['type']=='1'){
																			$type="Ok";
																		}else if($data['type']=='2'){
																			$type="Needed";
																		}
																		?>
																			<td><?php echo $type; ?> </td>
																	<?php
																	}
																}
																?>
															   <td><?php echo $rowl['chapter_comments']; ?> </td>
															</tr>
														<?php } ?>
														
													  </tbody>
													</table>
											</div>
										<?php	
									}else{
								?>
									<h6 class="my-4"><b><u><?php echo $database->get_name('topic','id',$_POST['topic'],'topic'); ?> Required Extra Questions</u></b></h6>
									<div id ="qtypelist">
											<?php
											$kl=1;
											if(isset($_REQUEST['editform'])){
												$posdata1 = explode('^',rtrim($_POST['qtypedata']));
													foreach($posdata1 as $rowpost2)
													{
														if(strlen($rowpost2)>0)
														{
															$rowpost = explode('_',$rowpost2);
															$sql1=$database->query("select * from questiontype where estatus='1' and id='".$rowpost[0]."' ");
															$row1=mysqli_fetch_array($sql1);
															if($rowpost[1]=='2'){
																$style="";
															}else{
																$style="style='display:none;'";
															}
														?>	
															<div class="row qtypelist">
																<div class="col-lg-5">
																	<div class="form-group row">
																		<label class="col-sm-4 col-form-label"><?php echo $row1['questiontype']; ?> <span style="color:red">*</span></label>
																		<div class="col-sm-8">
																			
																			<select id="cha_qtype<?php echo $rowpost[0]; ?>"  name="cha_qtype<?php echo $kl; ?>" class="cha_qtype form-control"  onchange="selectfunctioncha(<?php echo $rowpost[0]; ?>);">
																				<option value=''>-Select--</option>
																				<option value='1' <?php if(isset($rowpost[1])){ if($rowpost[1] == '1'){ echo ' selected="selected"';};}?>>Ok</option>
																				<option value='2' <?php if(isset($rowpost[1])){ if($rowpost[1] == '2'){ echo ' selected="selected"';};}?>>Needed</option>

																			</select>
																			 
																			<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_qtype'.$kl])){ echo $_SESSION['error']['cha_qtype'.$kl];}?></span>
																		</div>
																	</div>
																</div>
																<input type="hidden" id="qtype_id"  class="qtype_id" value="<?php echo $rowpost[0]; ?>" >
																<div class="col-lg-5" >
																	<div class="form-group row cha_qtype_count<?php echo $rowpost[0]; ?>" <?php echo $style; ?> >
																		<label class="col-sm-4 col-form-label">How Many <span style="color:red">*</span></label>
																		<div class="col-sm-8">
																			
																			<input type="text" name="cha_qtype_count" id="cha_qtype_count" class="form-control cha_qtype_count" value="<?php if(isset($rowpost[2])){ echo $rowpost[2]; }else{ echo ''; } ?>" onkeypress="return onlyNumbers(event);" maxlength='4'>
																			 
																			<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_qtype_count_'.$kl])){ echo $_SESSION['error']['cha_qtype_count_'.$kl];}?></span>
																		</div>
																	</div>
																</div>
															</div>
														<?php
															$kl++;
														}
													}
											}else{
												$style="style='display:none;'";
											$k=1;
											$sql=$database->query("select * from questiontype where estatus='1' ");
											while($row=mysqli_fetch_array($sql)){
										?>
												<div class="row qtypelist">
													<div class="col-lg-5">
														<div class="form-group row">
															<label class="col-sm-4 col-form-label"><?php echo $row['questiontype']; ?></label>
															<div class="col-sm-8">
																
																<select id="cha_qtype<?php echo $row['id']; ?>"  name="cha_qtype<?php echo $k; ?>" class="cha_qtype form-control"  onchange="selectfunctioncha(<?php echo $row['id']; ?>);">
																	<option value=''>-Select--</option>
																	<option value='1' <?php if(isset($_POST['cha_qtype'])){ if($_POST['cha_qtype'] == '1'){ echo ' selected="selected"';};}?>>Ok</option>
																	<option value='2' <?php if(isset($_POST['cha_qtype'])){ if($_POST['cha_qtype'] == '2'){ echo ' selected="selected"';};}?>>Needed</option>

																</select>
																 
																<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_qtype'][$k])){ echo $_SESSION['error']['cha_qtype'][$k];}?></span>
															</div>
														</div>
													</div>
													<input type="hidden" id="qtype_id"  class="qtype_id" value="<?php echo $row['id']; ?>" >
													<div class="col-lg-5" >
														<div class="form-group row cha_qtype_count<?php echo $row['id']; ?>" <?php echo $style; ?> >
															<label class="col-sm-4 col-form-label">How Many</label>
															<div class="col-sm-8">
																
																<input type="text" name="cha_qtype_count" id="cha_qtype_count" class="form-control cha_qtype_count" value="<?php if(isset($_REQUEST['cha_qtype_count'])){ echo $_REQUEST['cha_qtype_count']; }else{ echo ''; } ?>" onkeypress="return onlyNumbers(event);" maxlength='4'>
																 
																<span class="error text-danger"><?php if(isset($_SESSION['error']['cha_qtype_count_'.$k])){ echo $_SESSION['error']['cha_qtype_count_'.$k];}?></span>
															</div>
														</div>
													</div>
												</div>
											<?php $k++; }  } ?>

										</div>	
											
											<div class="row">
												<div class="col-lg-5">
													<div class="form-group row">
														<label class="col-sm-4 col-form-label"> Comments</label>
														<div class="col-sm-8">
															
															<textarea name="chapter_comments" rows="5" class="form-control" id="chapter_comments" ><?php if(isset($_POST['chapter_comments'])){ if($_POST['chapter_comments']!=''){ echo $_POST['chapter_comments']; }} ?> </textarea>
															<span class="error text-danger"><?php if(isset($_SESSION['error']['chapter_comments'])){ echo $_SESSION['error']['chapter_comments'];}?></span>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6 text-center">
													
														<a href="#" type="sbmit"
															class="btn btn-primary text-white" onClick="setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','validateForm3=1&chapter=<?php echo $_POST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&qtypedata='+qtypedata()+'&chapter_comments='+$('#chapter_comments').val()+'')">Submit</a>
													
												</div>
											</div>
										
											
										</form>
								</div>
							<?php
									 }
								}
							}
							?>
							
							

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript">
  $('#dataTable').DataTable({
   
   
    
  });
   
</script>
	<?php
		unset($_SESSION['error']);
	}
	
	if(isset($_REQUEST['validateForm'])){
		$_SESSION['error'] = array();
		  $post = $session->cleanInput($_POST);
		  $id = 'NULL';
			
		  if(isset($post['editform'])){
			  $id = $post['editform'];
		  }

		  $field = 'cha_hdifficult';
			if(!$post['cha_hdifficult'] || strlen(trim($post['cha_hdifficult'])) == 0){
			  $_SESSION['error'][$field] = "* Super Complex  cannot be empty";
			}
			$field = 'cha_difficult';
			if(!$post['cha_difficult'] || strlen(trim($post['cha_difficult'])) == 0){
			  $_SESSION['error'][$field] = "* Complex cannot be empty";
			}
			$field = 'cha_moderate';
			if(!$post['cha_moderate'] || strlen(trim($post['cha_moderate'])) == 0){
			  $_SESSION['error'][$field] = "* Moderate cannot be empty";
			}
			$field = 'cha_easy';
			if(!$post['cha_easy'] || strlen(trim($post['cha_easy'])) == 0){
			  $_SESSION['error'][$field] = "* Easy cannot be empty";
			}
			$field = 'chapter_comments';
			if(!$post['chapter_comments'] || strlen(trim($post['chapter_comments'])) == 0){
			  $_SESSION['error'][$field] = "* Comments cannot be empty";
			}
			
			
		  
		  //Check if any errors exist
			if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
			?>
			<script type="text/javascript">
			  setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&errordisplay=1&comp_tquestion=complexity&chapter=<?php echo $post['chapter'];?>&topic=<?php echo $post['topic'];?>&cha_hdifficult=<?php echo $post['cha_hdifficult'];?>&cha_hdiff_count=<?php echo $post['cha_hdiff_count'];?>&cha_difficult=<?php echo $post['cha_difficult'];?>&cha_diff_count=<?php echo $post['cha_diff_count'];?>&cha_moderate=<?php echo $post['cha_moderate'];?>&cha_modera_count=<?php echo $post['cha_modera_count'];?>&cha_easy=<?php echo $post['cha_easy'];?>&cha_easy_count=<?php echo $post['cha_easy_count'];?>&chapter_comments=<?php echo $post['chapter_comments'];?>')
			</script>
		  <?php
			}
			else{
				$sel=$database->query("select id from fitment_data where estatus='1' and type='1' and comp_tquestion='1' and chapter='".$post['chapter']."' and username='".$session->username."'");
				$rowc=mysqli_num_rows($sel);
				if($rowc==0){
					$result=$database->query("insert fitment_data set type='1',comp_tquestion='1',chapter='".$post['chapter']."',cha_easy='".$post['cha_easy']."',cha_moderate='".$post['cha_moderate']."',cha_difficult='".$post['cha_difficult']."',cha_hdifficult='".$post['cha_hdifficult']."',cha_easy_count='".$post['cha_easy_count']."',cha_modera_count='".$post['cha_modera_count']."',cha_diff_count='".$post['cha_diff_count']."',cha_hdiff_count='".$post['cha_hdiff_count']."',chapter_comments='".$post['chapter_comments']."',username='".$session->username."',estatus='1',timestamp='".time()."'");
					if($result){
						?>
							<script type="text/javascript">
								alert("Chapter Data Submitted Successfully");
								setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&comp_tquestion=complexity');
							</script>
						<?php
						
					}else{
						?>
							<script type="text/javascript">
								alert("Chapter Data Submission Failed");
								setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&comp_tquestion=complexity');
							</script>
						<?php

					}
				}else{
					?>
					<script type="text/javascript">
						alert("Chapter Data Already Submitted");
						setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&comp_tquestion=complexity');
					</script>
				<?php

				}
			}
	}
	
	if(isset($_REQUEST['validateForm1'])){
		$_SESSION['error'] = array();
		  $post = $session->cleanInput($_POST);
		  $id = 'NULL';
			
		  if(isset($post['editform'])){
			  $id = $post['editform'];
		  }

		  $field = 'topic_hdifficult';
			if(!$post['topic_hdifficult'] || strlen(trim($post['topic_hdifficult'])) == 0){
			  $_SESSION['error'][$field] = "* Super Complex  cannot be empty";
			}
			$field = 'topic_difficult';
			if(!$post['topic_difficult'] || strlen(trim($post['topic_difficult'])) == 0){
			  $_SESSION['error'][$field] = "* Complex cannot be empty";
			}
			$field = 'topic_moderate';
			if(!$post['topic_moderate'] || strlen(trim($post['topic_moderate'])) == 0){
			  $_SESSION['error'][$field] = "* Moderate cannot be empty";
			}
			$field = 'topic_easy';
			if(!$post['topic_easy'] || strlen(trim($post['topic_easy'])) == 0){
			  $_SESSION['error'][$field] = "* Easy cannot be empty";
			}
			$field = 'topic_comments';
			if(!$post['topic_comments'] || strlen(trim($post['topic_comments'])) == 0){
			  $_SESSION['error'][$field] = "* Comments cannot be empty";
			}
			
			
		  
		  //Check if any errors exist
			if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
			?>
			<script type="text/javascript">
			  setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&errordisplay1=1&comp_tquestion=complexity&chapter=<?php echo $post['chapter'];?>&topic=<?php echo $post['topic'];?>&topic_hdifficult=<?php echo $post['topic_hdifficult'];?>&topic_hdiff_count=<?php echo $post['topic_hdiff_count'];?>&topic_difficult=<?php echo $post['topic_difficult'];?>&topic_diff_count=<?php echo $post['topic_diff_count'];?>&topic_moderate=<?php echo $post['topic_moderate'];?>&topic_modera_count=<?php echo $post['topic_modera_count'];?>&topic_easy=<?php echo $post['topic_easy'];?>&topic_easy_count=<?php echo $post['topic_easy_count'];?>&topic_comments=<?php echo $post['topic_comments'];?>');
			</script>
		  <?php
			}
			else{
				$sel=$database->query("select id from fitment_data where estatus='1' and type='2'  and comp_tquestion='1' and chapter='".$post['chapter']."' and topic='".$post['topic']."'  and username='".$session->username."' ");
				$rowc=mysqli_num_rows($sel);
				if($rowc==0){
					
					$result=$database->query("insert fitment_data set type='2',comp_tquestion='1',chapter='".$post['chapter']."',topic='".$post['topic']."',cha_easy='".$post['topic_easy']."',cha_moderate='".$post['topic_moderate']."',cha_difficult='".$post['topic_difficult']."',cha_hdifficult='".$post['topic_hdifficult']."',cha_easy_count='".$post['topic_easy_count']."',cha_modera_count='".$post['topic_modera_count']."',cha_diff_count='".$post['topic_diff_count']."',cha_hdiff_count='".$post['topic_hdiff_count']."',chapter_comments='".$post['topic_comments']."',username='".$session->username."',estatus='1',timestamp='".time()."'");
				
					if($result){
						?>
							<script type="text/javascript">
								alert("Topic Data Submitted Successfully");
								setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&comp_tquestion=complexity');
							</script>
						<?php
						
					}else{
						?>
							<script type="text/javascript">
								alert("Data Submission Failed.");
								setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&comp_tquestion=complexity');
							</script>
						<?php

					}
				}else{
					?>
							<script type="text/javascript">
								alert("Topic Data Already Submitted");
								setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&comp_tquestion=complexity');
							</script>
						<?php
				}
			}
	}

	if(isset($_REQUEST['validateForm2'])){
		$_SESSION['error'] = array();
		  $post = $session->cleanInput($_POST);
		  $id = 'NULL';
			
		  if(isset($post['editform'])){
			  $id = $post['editform'];
		  }
		$abcd=1;
		$list=explode("^",$post['qtypedata']);
		
		if(count($list)>0)
		{
			foreach ($list as $k)
			{
				$r = explode('_',$k);
				if($r[0]!=''){
					$sell=$database->query("select * from questiontype where estatus='1'  and id='".$r[0]."'");
					$rowl=mysqli_fetch_array($sell);
					if(strlen(trim($r['1'])) == 0 ){
					  $_SESSION['error']['cha_qtype'.$abcd] = $rowl['questiontype']." cannot be empty";
					}
					if($r[1]=='2'){
						if(strlen(trim($r['2'])) == 0 ){
						  $_SESSION['error']['cha_qtype_count_'.$abcd] = $rowl['questiontype']." Count cannot be empty";
						}
					}
				}
				$abcd++;
			}
			$field = 'chapter_comments';
			if(!$post['chapter_comments'] || strlen(trim($post['chapter_comments'])) == 0){
			  $_SESSION['error'][$field] = "* Comments cannot be empty";
			}
			if(count($_SESSION['error']) > 0 || $post['validateForm2'] == 2){
				
			?>
			<script type="text/javascript">
			  setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&editform=1&errordisplay3=1&comp_tquestion=inputquestion&chapter=<?php echo $post['chapter'];?>&topic=<?php echo $post['topic'];?>&qtypedata=<?php echo $post['qtypedata'];?>&chapter_comments=<?php echo $post['chapter_comments'];?>')
			</script>
		  <?php
			}
			else{
				$qdata=array();
				$jk = 0;
				if(count($list)>0)
				{
					foreach ($list as $k)
					{
						$r = explode('_',$k);
						if($r[0]!=''){
							$qdata[$jk]['id']=$r[0];
							$qdata[$jk]['type']=$r[1];
							$qdata[$jk]['count']=$r[2];
							$jk++;
						}
					}
				}
				$sel=$database->query("select id from fitment_data where estatus='1' and type='1' and comp_tquestion=2 and chapter='".$post['chapter']."' and topic='".$post['topic']."'  and username='".$session->username."' ");
				$rowc=mysqli_num_rows($sel);
				if($rowc==0){
					
					$result=$database->query("insert fitment_data set type='1',comp_tquestion='2',chapter='".$post['chapter']."',topic='".$post['topic']."',qtypedata='".json_encode($qdata)."',cha_easy='0',cha_moderate='0',cha_difficult='0',cha_hdifficult='0',cha_easy_count='0',cha_modera_count='0',cha_diff_count='0',cha_hdiff_count='0',chapter_comments='".$post['chapter_comments']."',username='".$session->username."',estatus='1',timestamp='".time()."'");
				
					if($result){
						?>
							<script type="text/javascript">
								alert("Topic Data Submitted Successfully");
								setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&comp_tquestion=inputquestion');
							</script>
						<?php
						
					}else{
						?>
							<script type="text/javascript">
								alert("Data Submission Failed.");
								setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&comp_tquestion=inputquestion');
							</script>
						<?php

					}
				}else{
					?>
							<script type="text/javascript">
								alert("Topic Data Already Submitted");
								setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&comp_tquestion=inputquestion');
							</script>
						<?php
				}
			}
		}
			
		 
	}
	if(isset($_REQUEST['validateForm3'])){
		$_SESSION['error'] = array();
		  $post = $session->cleanInput($_POST);
		  $id = 'NULL';
			
		  if(isset($post['editform'])){
			  $id = $post['editform'];
		  }
		$abcd=1;
		$list=explode("^",$post['qtypedata']);
		
		if(count($list)>0)
		{
			foreach ($list as $k)
			{
				$r = explode('_',$k);
				if($r[0]!=''){
					$sell=$database->query("select * from questiontype where estatus='1'  and id='".$r[0]."'");
					$rowl=mysqli_fetch_array($sell);
					if(strlen(trim($r['1'])) == 0 ){
					  $_SESSION['error']['cha_qtype'.$abcd] = $rowl['questiontype']." cannot be empty";
					}
					if($r[1]=='2'){
						if(strlen(trim($r['2'])) == 0 ){
						  $_SESSION['error']['cha_qtype_count_'.$abcd] = $rowl['questiontype']." Count cannot be empty";
						}
					}
				}
				$abcd++;
			}
			$field = 'chapter_comments';
			if(!$post['chapter_comments'] || strlen(trim($post['chapter_comments'])) == 0){
			  $_SESSION['error'][$field] = "* Comments cannot be empty";
			}
			if(count($_SESSION['error']) > 0 || $post['validateForm2'] == 2){
			?>
			<script type="text/javascript">
			  setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&editform=1&errordisplay3=1&comp_tquestion=inputquestion&chapter=<?php echo $post['chapter'];?>&topic=<?php echo $post['topic'];?>&qtypedata=<?php echo $post['qtypedata'];?>&chapter_comments=<?php echo $post['chapter_comments'];?>')
			</script>
		  <?php
			}
			else{
				$qdata=array();
				$jk = 0;
				if(count($list)>0)
				{
					foreach ($list as $k)
					{
						$r = explode('_',$k);
						if($r[0]!=''){
							$qdata[$jk]['id']=$r[0];
							$qdata[$jk]['type']=$r[1];
							$qdata[$jk]['count']=$r[2];
							$jk++;
						}
					}
				}
				$sel=$database->query("select id from fitment_data where estatus='1' and type='2' and comp_tquestion=2 and chapter='".$post['chapter']."' and topic='".$post['topic']."'  and username='".$session->username."' ");
				$rowc=mysqli_num_rows($sel);
				if($rowc==0){
					
					$result=$database->query("insert fitment_data set type='2',comp_tquestion='2',chapter='".$post['chapter']."',topic='".$post['topic']."',qtypedata='".json_encode($qdata)."',cha_easy='0',cha_moderate='0',cha_difficult='0',cha_hdifficult='0',cha_easy_count='0',cha_modera_count='0',cha_diff_count='0',cha_hdiff_count='0',chapter_comments='".$post['chapter_comments']."',username='".$session->username."',estatus='1',timestamp='".time()."'");
				
					if($result){
						?>
							<script type="text/javascript">
								alert("Topic Data Submitted Successfully");
								setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&comp_tquestion=inputquestion');
							</script>
						<?php
						
					}else{
						?>
							<script type="text/javascript">
								alert("Data Submission Failed.");
								setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&comp_tquestion=inputquestion');
							</script>
						<?php

					}
				}else{
					?>
							<script type="text/javascript">
								alert("Topic Data Already Submitted");
								setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process.php','tableDisplay=1&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&comp_tquestion=inputquestion');
							</script>
						<?php
				}
			}
		}
			
		 
	}
	?>
	<script>
		function selectfunction(){
			var cha_hdifficult=$('#cha_hdifficult').val();
			if(cha_hdifficult=='2'){
				$('.cha_hdiff_count1').show();
			}else{
				$('.cha_hdiff_count1').hide();
			}
		}
		function selectfunction1(){
			var cha_difficult=$('#cha_difficult').val();
			if(cha_difficult=='2'){
				$('.cha_diff_count1').show();
			}else{
				$('.cha_diff_count1').hide();
			}
		}
		function selectfunction2(){
			var cha_moderate=$('#cha_moderate').val();
			if(cha_moderate=='2'){
				$('.cha_modera_count1').show();
			}else{
				$('.cha_modera_count1').hide();
			}
		}
		function selectfunction3(){
			var cha_easy=$('#cha_easy').val();
			if(cha_easy=='2'){
				$('.cha_easy_count1').show();
			}else{
				$('.cha_easy_count1').hide();
			}
		}

		function tselectfunction(){
			var topic_hdifficult=$('#topic_hdifficult').val();
			if(topic_hdifficult=='2'){
				$('.topic_hdiff_count1').show();
			}else{
				$('.topic_hdiff_count1').hide();
			}
		}
		function tselectfunction1(){
			var topic_difficult=$('#topic_difficult').val();
			if(topic_difficult=='2'){
				$('.topic_diff_count1').show();
			}else{
				$('.topic_diff_count1').hide();
			}
		}
		function tselectfunction2(){
			var topic_moderate=$('#topic_moderate').val();
			if(topic_moderate=='2'){
				$('.topic_modera_count1').show();
			}else{
				$('.topic_modera_count1').hide();
			}
		}
		function tselectfunction3(){
			var topic_easy=$('#topic_easy').val();
			if(topic_easy=='2'){
				$('.topic_easy_count1').show();
			}else{
				$('.topic_easy_count1').hide();
			}
		}
	</script>
	
	<script>
		function selectfunctioncha(id){
			
			var cha_qtype=$('#cha_qtype'+id).val();
			
			if(cha_qtype=='2'){
				$('.cha_qtype_count'+id).show();
			}else{
				$('.cha_qtype_count'+id).hide();
			}
		}
		 function qtypedata(){
		var retval = '';
		i=0;
        $('#qtypelist .qtypelist').each(function(){
			retval+= $(this).find('.qtype_id').val()+'_'+$(this).find('.cha_qtype').val()+'_'+$(this).find('.cha_qtype_count').val()+'^';
		});
		
		return retval;
		
	}
	</script>
	
	<script type="text/javascript"> 
    
	function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode

		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;

		return true;
	}
</script>
<script type="text/javascript">
	function getFields()
    {
    	
		var ass='';
		$('.topic_report').each(function(element) {
		if($(this).is(':checked')) {
		 $('#'+$(this).attr('id')).val(1);
		  ass+=$(this).val()+",";
		 
		}else{
		  $('#'+$(this).attr('id')).val(0);
		  ass+=$(this).val()+",";
		 
		}
	});
}
</script>
<script>
    // Fixed table side jquery library
    jQuery(document).ready(function () {
      jQuery(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');
    });
  </script>
