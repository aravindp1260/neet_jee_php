<style type="text/css">
.btn-group-sm>.btn, .btn-sm {
    padding: .2rem .2rem;}
    i.fa.fa-key.black {
    padding: 0.3rem 0.1rem;
}
    </style>
<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');

ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}
?>
<script>
$(document).ready(function(){
  //alert("sdfgh"+userlevelval);
  $('#userlevel').change(function(){
    var userlevelval = $("#userlevel option:selected").val();
    
    if(userlevelval == 3 || userlevelval == 2 || userlevelval == 1){
    $('.department').show();
    $('.designation').show();
    
  }
  else{
    $('.department').hide();
    $('.designation').hide();
  }
  });
  
});
</script>

<?php
//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
    $data_sel = $database->query("SELECT * FROM webinars WHERE id = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
	$_POST = array_merge($_POST,$data);
	$datetime=$_POST['datetime'];
	
	$_POST['date']=date("d-m-Y",$datetime);
	$_POST['time']=date('H:i',$datetime);
 ?>
 <script type="text/javascript">
 $('#adminForm').slideDown();
 </script>
 <?php
    }
 }
 ?>
<style>
  .spinner-border.w_1 {
    width: 1rem;height: 1rem;
  }
</style>
<script>
getFields5();
getFields6();
getFields1();
$('#subject').selectpicker2();
$('#chapter').selectpicker1();
</script>
<script>
chapterfunction();
//radiofunction('<?php echo $_POST['ppaper']; ?>');
function chapterfunction(){
	var chapter=$('#chapter').val();
	//alert(chapter);
	if(chapter.length >0){
		$('#chapview').show();

	}else{
		$('#chapview').hide();
	}

}
</script>
<script>
	$(function () {
		$('.datepicker').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});

	
	</script>
	
 <div class="col-lg-12 col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group row">
			  <label class="col-sm-4 col-form-label">Date<span style="color:red;">*</span></label>
				<div class="col-sm-8">
				  <input type="text" name="date" class="form-control datepicker" placeholder="Select Date" id="date" value="<?php if(isset($_POST['date'])){ echo $_POST['date'];}?>" autocomplete="off">
				  <span class="text-danger"><?php if(isset($_SESSION['error']['date'])){ echo $_SESSION['error']['date'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
	
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Time<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<input type="time" name="time" class="form-control"  id="time" value="<?php if(isset($_POST['time'])){ echo $_POST['time'];} ?>" autocomplete="off"  >
					<span class="text-danger"><?php if(isset($_SESSION['error']['time'])){ echo $_SESSION['error']['time'];}?></span>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	
		<div class="col-lg-12"  >
		
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Title<span style="color:red;">*</span></label>
				<div class="col-sm-10">
					<input type="text" name="title" class="form-control" placeholder="Enter Title" id="title" value="<?php if(isset($_POST['title'])){ echo $_POST['title'];}else{ echo ''; }?>" autocomplete="off" >
					<span class="text-danger"><?php if(isset($_SESSION['error']['title'])){ echo $_SESSION['error']['title'];}?></span>
				</div>
			</div>
		
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12" >
			<div class="form-group row">
				<label class="col-sm-2 col-form-label"> Description</label>
				<div class="col-sm-10">

					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								<div id="editorContainer">
									<div id="toolbarLocation"></div>
									<textarea id="description" class="faqcustomdata wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['description'])) { echo urldecode($_POST['description']); }else{  } ?></textarea>

								</div>
							</div>

						</div>
					</div>


				</div>
			</div>
		</div>

	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
					<label class="col-md-2 col-form-label"> Upload Image<span style="color:red;">*</span></label>
					<div class="col-md-10">
						
			
						<div id="file-uploader2" style="display:inline">
							<noscript>
								<p>Please enable JavaScript to use file uploader.</p>
								or put a simple form for upload here
							</noscript>
			
						</div>
						<script>
			
							function createUploader(){
			
								var uploader = new qq.FileUploader({
									element: document.getElementById('file-uploader2'),
									action: '<?php echo SECURE_PATH;?>frame/js/upload/php2.php?upload=image&upload_type=multiple',
									debug: true,
									multiple:true
								});
							}
			
							createUploader();
			
			
			
						
			
						</script>	
						<input type="hidden" name="image" id="image" value="<?php if(isset($_POST['image'])) { echo $_POST['image']; } ?>"/>
						<?php
						if(isset($_POST['image']))
						{
							if($_POST['image']!=''){
						?>
								<?php if(isset($_POST['image'])) { echo $_POST['image']; } ?>
						<?php
							}
			
						}
						?>
			
						<span class="error" style="color:red;" ><?php if(isset($_SESSION['error']['image'])){ echo $_SESSION['error']['image'];}?></span>
					</div>
				</div>
	</div>
		
		
	<script type="text/javascript" >
		createEditorInstance("en", {});
	</script>
	<script>
		function rand(){

			var data2;
			data2 = '{ "description" : "' + encodeURIComponent(tinymce.get('description').getContent())+ '"}';



			$.ajax({
				type: 'POST',
				url: '<?php echo SECURE_PATH;?>webinar/img.php',
				data: data2,
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',

				success: function (result,xhr) {
					console.log("reply");

					setState('adminForm','<?php echo SECURE_PATH;?>webinar/process.php','validateForm=1&date='+$('#date').val()+'&time='+$('#time').val()+'&title='+$('#title').val()+'&image='+$('#image').val()+'<?php if(isset($_POST['editform'])){ echo '&editform='.$_POST['editform'];}?><?php if(isset($_POST['page'])){ echo '&page='.$_POST['page'];}?>')
				},
				error: function(e){

					console.log("ERROR: ", e);
				}
			});
		}
		</script>
    <div class="form-group row pl-3">
		
        <div class="col-lg-6">
            <a class="radius-20 btn btn-theme px-5" style="cursor:pointer" onClick="rand()">Submit</a>
        </div>
       
    </div>
</div>
<script type="text/javascript"> 
    function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }
</script>
<?php
unset($_SESSION['error']);
}


//Process and Validate POST data
if(isset($_POST['validateForm'])){
	$_SESSION['error'] = array();
  $post = $session->cleanInput($_POST);
  $id = 'NULL';
	
  if(isset($post['editform'])){
	  $id = $post['editform'];
  }

	$field = 'date';
	if(!$post['date'] || strlen(trim($post['date'])) == 0){
	  $_SESSION['error'][$field] = "* Date cannot be empty";
	}
	 $field = 'time';
	if(!$post['time'] || strlen(trim($post['time'])) == 0){
	  $_SESSION['error'][$field] = "* Time cannot be empty";
	}

	$field = 'title';
	if(!$post['title'] || strlen(trim($post['title'])) == 0){
	  $_SESSION['error'][$field] = "* Title cannot be empty";
	}
	 $field = 'description';
	if(!$_SESSION['description'] || strlen(trim($_SESSION['description'])) == 0){
	  $_SESSION['error'][$field] = "* Description cannot be empty";
	}
	
	

  //Check if any errors exist
	if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
		
	?>
    <script type="text/javascript">
      $('#adminForm').slideDown();
      setState('adminForm','<?php echo SECURE_PATH;?>webinar/process.php','addForm=1&date=<?php echo $post['date'];?>&time=<?php echo $post['time'];?>&title=<?php echo $post['title'];?>&description=<?php echo $_SESSION['description'];?>&image=<?php echo $post['image'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?>')
    </script>
  <?php
	}
	else{
		if($id=='NULL')
		{
			$date=$post['date'];
			$time=$post['time'];
			$datetime=$date." ".$time;
			$datetime1=strtotime($datetime);
			$desc=str_replace("'","&#39;",$_SESSION['description']);
		mysqli_query($database->connection,"INSERT webinars SET datetime='".$datetime1."',title='".$post['title']."',description='".$desc."',image='".$post['image']."',estatus='1',timestamp='".time()."'");
	  
	 ?>
	  <div class="col-lg-12 col-md-12">
		<div class="form-group">
			<div class="alert alert-success">
			<i class="fa fa-thumbs-up fa-2x"></i> Webinar Data Created Successfully!
			</div>
			<script type="text/javascript">
  animateForm('<?php echo SECURE_PATH;?>webinar/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
		</div>
	</div>
	 <?php
    }
		else
		{
			$date=$post['date'];
			$time=$post['time'];
			$datetime=$date." ".$time;
			$datetime1=strtotime($datetime);
			$desc=str_replace("'","&#39;",$_SESSION['description']);
			//echo "Update webinars SET datetime='".$datetime1."',title='".$post['title']."',description='".$desc."',image='".$post['image']."' where id='".$id."'";
      mysqli_query($database->connection,"Update webinars SET datetime='".$datetime1."',title='".$post['title']."',description='".$desc."',image='".$post['image']."' where id='".$id."'");
	  ?>
	  <div class="col-lg-12 col-md-12">
		<div class="form-group">
			<div class="alert alert-success">
			<i class="fa fa-thumbs-up fa-2x"></i> Webinar Data Updated Successfully!
			</div>
			<script type="text/javascript">
			animateForm('<?php echo SECURE_PATH;?>webinar/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
			</script>
		</div>
	</div>
	  <?php
    }
	}
}
  ?>
 <?php
//Delete users
if(isset($_GET['rowDelete'])){
	mysqli_query($database->connection,"update webinars set estatus='0' where id='".$_GET['rowDelete']."'");
?>
<div class="alert alert-success">Webinar deleted successfully!</div>
<script type="text/javascript">
  animateForm('<?php echo SECURE_PATH;?>webinar/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
<?php
}


//Display bulkreport
if(isset($_GET['tableDisplay'])){
	//Pagination code
  $limit=50;
  if(isset($_GET['page']))
  {
    $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
    $page=$_GET['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
  //Search Form
?>
<?php
  $tableName = 'webinars';
  $condition = "estatus=1";//"userlevel = '8' OR userlevel = '9' OR userlevel = '7' OR userlevel = '6'";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  //$query_string = $_SERVER['QUERY_STRING'];
  // $pagination = $session->showPagination(SECURE_PATH."users/process.php?tableDisplay=1&",$tableName,$start,$limit,$page,$condition);
  $q = "SELECT * FROM $tableName ".$condition." ORDER BY timestamp DESC";
  $result_sel = $database->query($q);
  $numres = mysqli_num_rows($result_sel);
 $query = "SELECT * FROM $tableName ".$condition." ORDER BY timestamp DESC";
  
  
  //echo $query;
  $data_sel = $database->query($query);
  if(($start+$limit) > $numres){
	 $onpage = $numres;
	 }
	 else{
	  $onpage = $start+$limit;
	 }
  if($numres > 0){
	?>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">Webinar Details</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered"  width="100%" cellspacing="0" id="dataTable">
									<thead>
										<tr>
											<th class="text-left" nowrap>Date & Time</th>
											  <th class="text-left" nowrap>Title</th>
											  <th class="text-left" nowrap>Description</th>
											  <th class="text-left" nowrap>Image</th>
											  <th class="text-left" nowrap>Created On</th>
											  <th class="text-left" nowrap>Actions</th>
											</tr>
									</thead>
									<tbody>
									<?php
									
									$i=1;
									while($value = mysqli_fetch_array($data_sel))
									{
										 $time=$value['timestamp'];
										 $datetime=$value['datetime']; 

									?>
									<tr>
										<td class="text-left" data-order="<?php echo $datetime; ?>"><?php echo date('d/m/Y h:i A',$datetime);?></td>
										<td class="text-left"><?php echo $value['title'];?></td>
										<td class="text-left"><?php echo urldecode($value['description']);?></td>
										<td><img src="<?php echo SECURE_PATH; ?>files/<?php echo $value['image']; ?>" width="30" height='20'></td>
										<td class="text-left" data-order="<?php echo $time; ?>"><?php echo date('d/m/Y',$time);?></td>
										<td nowrap>
											<div id="dummy"></div>
										  <a href="#" class="btn btn-sm btn-primary" onClick="setState('adminForm','<?php echo SECURE_PATH;?>webinar/process.php','addForm=2&editform=<?php echo $value['id'];?>&page=<?php echo $_GET['page']; ?>')"><i
												  class="material-icons md-16 pt-1">edit</i></a>
										  <a href="#" class="btn btn-sm btn-danger" onClick="confirmDelete('adminForm','<?php echo SECURE_PATH;?>webinar/process.php','rowDelete=<?php echo $value['id'];?>')"><i
												  class="material-icons md-16 pt-1">delete</i></a>
										
										  
										</td>
									  </tr>
									<?php
									$i++;
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>

  </section>
 
	<?php
	}

	
	?>
<script type="text/javascript">
  $('#dataTable').DataTable({
    "pageLength": 50,
    "order": [[ 4, 'desc' ]]
    
  });
   
</script>
<?php }
?>


