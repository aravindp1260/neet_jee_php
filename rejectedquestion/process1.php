<?php
include('../include/session.php');
error_reporting(0);
?>
<script src="../edut/js/wirisdisplay.js"></script>
<?php

if(isset($_REQUEST['viewDetails']))
{
	$sello=$database->query("select * from users where username='".$session->username."'");
	$rowese=mysqli_fetch_array($sello);
	if($rowese['userlevel']=='7'){
		$cond=" and username='".$_SESSION['username']."'";
	}else{
	}
	$sell=$database->query("select * from createquestion where estatus='1' and vstatus='2' and id='".$_REQUEST['id']."'".$cond."");
	$rowcount=mysqli_num_rows($sell);
	if($rowcount>0){
		$row=mysqli_fetch_array($sell);
		//$selldata=$database->query("select * from questionset where estatus='1' and question_id='".$_REQUEST['id']."'");
		//$rowdata=mysqli_fetch_array($selldata);
		if($row['list1type']=='roman'){
			$list1type="upper-roman";
		}else if($row['list1type']=='alphabets'){
			$list1type="upper-alpha";
		}else if($row['list1type']=='numbers'){
			$list1type="decimal";
		}else{
			$list1type="upper-alpha";
		}
		if($row['list2type']=='roman'){
			$list2type="upper-roman";
		}else if($row['list2type']=='alphabets'){
			$list2type="upper-alpha";
		}else if($row['list2type']=='numbers'){
			$list2type="decimal";
		}else{
			$list2type="upper-alpha";
		}
		if($row['vstatus']=='1'){
			$status="Verified";
		}else if($row['vstatus']=='2'){
			$status="Rejected";
		}else{
			$status="Pending";
		}
		?>
			<div class="questionreport mt-3" id="tabs" >
			
				 <nav>
					<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Overall Issues</a>
						
						<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">View</a>
					</div>
				</nav>

				<div class="tab-content" id="nav-tabContent">
					<div class="tab-pane fade show active py-3" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
						<div id="issuediv">
							
							<div class="d-flex justify-content-between align-items-center my-4">
								<div class="question-idname">
									<h6>Question Id: <?php echo $row['id']; ?></h6>
								</div>
								<div class="status-name">
									<h6>Status:<?php echo $status; ?> </h6>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-bordered">
										  <thead>
											<tr>
											  <th scope="col">Issue ID</th>
											  <th scope="col">Issue Section</th>
											  <th scope="col">Issue Type</th>
											  <th scope="col">Remarks</th>
											  <th scope="col">Status</th>
											</tr>
										  </thead>
										  <tbody>
											<?php
											$j=1;
											$sql=$database->query("select * from question_issues where estatus='1' and question_id='".$row['id']."'");
											$rcount=mysqli_num_rows($sql);
											while($rowd=mysqli_fetch_array($sql)){
												if($rowd['resolve_issue']=='1'){
													$status1="Resolved";
												}else{
													$status1="Pending";
												}
											?>
												<tr>
													<td><?php echo $rowd['issue_id']; ?></td>
													<td><?php echo $database->get_name('issue_section','id',$rowd['issue_section'],'issue_section'); ?></td>
													<td><?php echo $database->get_name('issue_type','id',$rowd['issue_type'],'issue_type'); ?></td>
													<td><?php echo $rowd['vremarks']; ?></td>
													<td><?php echo $status1; ?></td>
												</tr>
											<?php $j++; } ?>
										  </tbody>
										</table>
									</div>
								</div>
							</div>
							
							
						</div>
					</div>
			
					<div class="tab-pane fade py-3" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
				   
						<div class="d-flex justify-content-between align-items-left my-4">
							<div class="col-md-4 col-sm-12">
								<h6>Question Id:<?php echo $row['id']; ?> </h6>
							</div>
							
							
						</div>
						<div class="container">
		  				<div class="row">
		  					<?php
		  					$row['exam']=rtrim($row['exam'],',');
		  					$row['class']=rtrim($row['class'],',');
		  					$row['chapter']=rtrim($row['chapter'],',');
		  					$row['topic']=rtrim($row['topic'],',');
		  					
		  					if($row['class']=='1'){
		  						$class='XI';
		  					}else if($row['class']=='2'){
		  						$class='XII';
		  					}else  if($row['class']=='1,2'){
		  						$class='XI,XII';
		  					}
		  					if($row['exam']=='1'){
		  						$exam="NEET";
		  					}else if($row['exam']=='2'){
		  						$exam="JEE";
		  					}else if($row['exam']=='1,2'){
		  						$exam="NEET,JEE";
		  					}
		  					$chapter ='';
		  					$sql = $database->query("SELECT * FROM chapter WHERE estatus='1' and id IN(".$row['chapter'].")"); 
		  					while($row2=mysqli_fetch_array($sql)){
		  						$chapter .= $row2['chapter'].",";
		  					}
		  
		  						$topic ='';
		  						$k=1;
		  						$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$row['topic'].")"); 
		  						while($row1=mysqli_fetch_array($zSql1)){
		  							$topic .= $row1['topic'].",";
		  							$k++;
		  						}
		  						?>
		  					<div class="col-md-2">
		  						<h6>Class</h6>
		  						<p><?php echo $class; ?></p>
		  					</div>
		  					
		  					<div class="col-md-2">
		  						<h6>Exam</h6>
		  						<p><?php echo $exam; ?></p>
		  					</div>
		  					<div class="col-md-2">
		  						<h6>Subject</h6>
		  						<p><?php echo $database->get_name('subject','id',$row['subject'],'subject'); ?></p>
		  					</div>
		  					<div class="col-md-6">
		  						<h6>Chapter</h6>
		  						<p><?php echo rtrim($chapter,","); ?></p>
		  					</div>
		  					<?php
		  					if($row['qtype']=='5'){
		  						$type="Comprehension";
		  					?>
		  						<div class="col-md-6">
		  							<h6>Question Type</h6>
		  							<p><?php echo $type; ?></p>
		  						</div>
		  						<div class="col-md-12 mt-2 questionView">
		  							<h6>Explanation</h6>
		  							<div><?php echo urldecode($database->get_name('compquestion','id',$row['compquestion'],'compquestion')); ?></div>
		  						</div>
		  						<div class="col-md-12 mt-2 questionView">
		  							<h6>Question</h6>
		  							<div><?php echo urldecode($row['question']); ?></div>
		  							<div class="row">
		  
		  								<div class="col-md-1 py-2 ">
		  									(A)</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo urldecode($row['option1']); ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(B)</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo urldecode($row['option2']); ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(C)</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo urldecode($row['option3']); ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(D)</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo urldecode($row['option4']); ?>
		  								</div>
		  							</div>
		  						</div>
		  						<?php if($row['explanation']!=''){ ?>
		  							<div class="col-md-12 mt-2">
		  								<h6>Explanation</h6>
		  								<div><?php echo urldecode($row['explanation']); ?></div>
		  									
		  
		  							</div>
		  						<?php } ?>
		  						
		  						<div class="col-md-12 mt-2">
		  							<h6>Answer</h6>
		  							<p><?php echo rtrim($row['answer'],","); ?></p>
		  								
		  
		  						</div>
		  					<?php
		  					}else if($row['qtype']=='8'){
		  						$type="Integer";
		  						?>
		  						<div class="col-md-6">
		  							<h6>Question Type</h6>
		  							<p><?php echo $type; ?></p>
		  						</div>
		  						<div class="col-md-12 mt-2 questionView">
		  							<h6>Question</h6>
		  							<div><?php echo urldecode($row['question']); ?></div>
		  						</div>
		  						
		  						
		  						<div class="col-md-12 mt-2 questionView">
		  							<h6>answer</h6>
		  							<div><?php echo rtrim($row['answer'],","); ?></div>
		  						</div>
		  						<?php if($row['explanation']!=''){ ?>
		  							<div class="col-md-12 mt-2">
		  								<h6>Explanation</h6>
		  								<div><?php echo urldecode($row['explanation']); ?></div>
		  									
		  
		  							</div>
		  						<?php } ?>
		  					<?php
		  					}else if($row['qtype']=='3'){
		  						$type="Matching";
		  					
		  						$list1='';
		  						$list2='';
		  						$obj=json_decode($row['question'],true);
		  						?>
		  						<div class="col-md-6">
		  							<h6>Question Type</h6>
		  							<p><?php echo $type; ?></p>
		  						</div>
		  						
		  						<div class="col-md-12 mt-2 questionView">
		  							<h6>Question</h6>
		  							<?php echo urldecode($row['mat_question']); ?>
									<br />
		  							<div class="questionlist-types d-flex">
		  								<div>
		  								<h5>List1</h5>
		  									<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
		  										<?php
											foreach($obj as $qqlist)
											{
											
												if(strlen($qqlist['qlist1'])>0)
												{
													echo '<li style="width:150px;">'.urldecode($qqlist['qlist1']).'</li>';
												}
											}
											?>
		  									
		  									</ul>
		  								</div>
		  								<div class="pl-5">
		  									<h5>List2</h5>
		  									<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
		  										<?php
											foreach($obj as $qqlist2)
											{
											
												if(strlen($qqlist2['qlist2'])>0)
												{
													echo '<li style="width:150px;">'.urldecode($qqlist2['qlist2']).'</li>';
												}
											}
											?>
		  									</ol>
		  								</div>
		  							</div>
		  							<div class="row">
		  
		  								<div class="col-md-1 py-2 ">
		  									(A) 						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option1']; ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(B)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option2']; ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(C)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option3']; ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(D)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option4']; ?>
		  								</div>
		  							</div>
		  						</div>
		  						<?php if($row['explanation']!=''){ ?>
		  							<div class="col-md-12 mt-2">
		  								<h6>Explanation</h6>
		  								<div><?php echo urldecode($row['explanation']); ?></div>
		  									
		  
		  							</div>
		  						<?php } ?>
		  						
		  						<div class="col-md-12 mt-2">
		  							<h6>Answer</h6>
		  							<p><?php echo rtrim($row['answer'],","); ?></p>
		  								
		  
		  						</div>
		  					<?php
		  					}else if($row['qtype']=='9'){
		  						$type="Matrix";
		  						$list1='';
		  						$list2='';
		  						$obj=json_decode($row['question'],true);
		  						?>
		  						<div class="col-md-6">
		  							<h6>Question Type</h6>
		  							<p><?php echo $type; ?></p>
		  						</div>
		  						
		  						<div class="col-md-12 mt-2 questionView">
		  							<h6>Question</h6>
		  							<?php echo urldecode($row['mat_question']); ?>
									<br />
		  								<div class="questionlist-types d-flex">
		  									<div>
		  									<h5>List1</h5>
		  										<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
		  											<?php
													foreach($obj as $qqlist)
													{
													
														if(strlen($qqlist['qlist1'])>0)
														{
															echo '<li style="width:150px;">'.urldecode($qqlist['qlist1']).'</li>';
														}
													}
													?>
		  										
		  										</ul>
		  									</div>
		  									<div class="pl-5">
		  										<h5>List2</h5>
		  										<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
		  											<?php
													foreach($obj as $qqlist2)
													{
													
														if(strlen($qqlist2['qlist2'])>0)
														{
															echo '<li style="width:150px;">'.urldecode($qqlist2['qlist2']).'</li>';
														}
													}
													?>
		  										</ol>
		  									</div>
		  								</div>
		  							
		  							<div class="row">
		  
		  								<div class="col-md-1 py-2 ">
		  									(A) 						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option1']; ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(B)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option2']; ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(C)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option3']; ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(D)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option4']; ?>
		  								</div>
		  							</div>
		  						</div>
		  						<?php if($row['explanation']!=''){ ?>
		  							<div class="col-md-12 mt-2">
		  								<h6>Explanation</h6>
		  								<div><?php echo urldecode($row['explanation']); ?></div>
		  									
		  
		  							</div>
		  						<?php } ?>
		  						<?php if($row['expla_vlink']!=''){ ?>
		  							<div class="col-md-12 mt-2">
		  								<h6>Explanation Video Link</h6>
		  								<div><?php echo $row['expla_vlink']; ?></div>
		  									
		  
		  							</div>
		  						<?php } ?>
		  						<div class="col-md-12 mt-2">
		  							<h6>Answer</h6>
		  							<p><?php echo rtrim($row['answer'],","); ?></p>
		  								
		  
		  						</div>
		  					<?php
		  					}else{
		  						$type="General";
		  					?>
		  						<div class="col-md-3">
		  							<h6>Question Type</h6>
		  							<p><?php echo $type; ?></p>
		  						</div>
		  						
		  						<div class="col-md-12 mt-2 questionView">
		  							<h6>Question</h6>
		  							<div><?php echo urldecode($row['question']); ?></div>
		  							<div class="row">
		  
		  								<div class="col-md-1 py-2 ">
		  									(A) 						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo urldecode($row['option1']); ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(B)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo urldecode($row['option2']); ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(C)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo urldecode($row['option3']); ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(D)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo urldecode($row['option4']); ?>
		  								</div>
		  							</div>
		  						</div>
		  						<?php if($row['explanation']!=''){ ?>
		  							<div class="col-md-12 mt-2">
		  								<h6>Explanation</h6>
		  								<div><?php echo urldecode($row['explanation']); ?></div>
		  									
		  
		  							</div>
		  						<?php } ?>
		  						<?php if($row['expla_vlink']!=''){ ?>
		  							<div class="col-md-12 mt-2">
		  								<h6>Explanation Video Link</h6>
		  								<div><?php echo $row['expla_vlink']; ?></div>
		  									
		  
		  							</div>
		  						<?php } ?>
		  						<div class="col-md-12 mt-2">
		  							<h6>Answer</h6>
		  							<p><?php echo rtrim($row['answer'],","); ?></p>
		  								
		  
		  						</div>
		  					<?php
		  					}
		  					?>
		  					<?php if($row['complexity']!=''){ ?>
		  						<div class="col-md-12 mt-2">
		  							<h6>Complexity</h6>
		  							<p><?php echo $database->get_name('complexity','id',$row['complexity'],'complexity'); ?></p>
		  								
		  
		  						</div>
		  					<?php } ?>
		  					<?php if($row['timeduration']!=''){ ?>
		  						<div class="col-md-12 mt-2">
		  							<h6>Answering Time Duration</h6>
		  							<p><?php echo $row['timeduration']; ?></p>
		  								
		  
		  						</div>
		  					<?php } ?>
		  					<?php if($row['inputquestion']!=''){
		  					$zSql2 = $database->query("SELECT * FROM questiontype WHERE estatus='1' and id IN(".$row['inputquestion'].")"); 
		  					while($row2=mysqli_fetch_array($zSql2)){
		  						$questiontype .= $row2['questiontype'].",";
		  						$k++;
		  					}
		  					?>
		  						<div class="col-md-12 mt-2">
		  							<h6>Type of Question</h6>
		  							<p><?php echo rtrim($questiontype,","); ?></p>
		  								
		  
		  						</div>
		  					<?php } ?>
		  					<?php if($row['usageset']!=''){ ?>
		  						<div class="col-md-12 mt-2">
		  							<h6>Usage Set</h6>
		  							<p><?php echo $database->get_name('question_useset','id',$row['usageset'],'usageset'); ?></p>
		  								
		  
		  						</div>
		  					<?php } ?>
		  					<?php if($row['question_theory']!='' && $row['question_theory']!='0'){ ?>
		  						<div class="col-md-12 mt-2">
		  							<h6>Question Theory</h6>
		  							<p><?php echo $database->get_name('question_theory','id',$row['question_theory'],'question_theory'); ?></p>
		  								
		  
		  						</div>
		  					<?php } ?>
		  					
		  				</div>
		  			</div>
		  			
		  			 <div class="modal-footer">
		  			 
		  			
		  			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		  			
		  </div>
						
					</div>
				</div>
			</div>
		<!-- 	<div class="container">
		  				<div class="row">
		  					<?php
		  					$row['exam']=rtrim($row['exam'],',');
		  					$row['class']=rtrim($row['class'],',');
		  					$row['chapter']=rtrim($row['chapter'],',');
		  					$row['topic']=rtrim($row['topic'],',');
		  					
		  					if($row['class']=='1'){
		  						$class='XI';
		  					}else if($row['class']=='2'){
		  						$class='XII';
		  					}else  if($row['class']=='1,2'){
		  						$class='XI,XII';
		  					}
		  					if($row['exam']=='1'){
		  						$exam="NEET";
		  					}else if($row['exam']=='2'){
		  						$exam="JEE";
		  					}else if($row['exam']=='1,2'){
		  						$exam="NEET,JEE";
		  					}
		  					$chapter ='';
		  					$sql = $database->query("SELECT * FROM chapter WHERE estatus='1' and id IN(".$row['chapter'].")"); 
		  					while($row2=mysqli_fetch_array($sql)){
		  						$chapter .= $row2['chapter'].",";
		  					}
		  
		  						$topic ='';
		  						$k=1;
		  						$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$row['topic'].")"); 
		  						while($row1=mysqli_fetch_array($zSql1)){
		  							$topic .= $row1['topic'].",";
		  							$k++;
		  						}
		  						?>
		  					<div class="col-md-2">
		  						<h6>Class</h6>
		  						<p><?php echo $class; ?></p>
		  					</div>
		  					
		  					<div class="col-md-2">
		  						<h6>Exam</h6>
		  						<p><?php echo $exam; ?></p>
		  					</div>
		  					<div class="col-md-2">
		  						<h6>Subject</h6>
		  						<p><?php echo $database->get_name('subject','id',$row['subject'],'subject'); ?></p>
		  					</div>
		  					<div class="col-md-6">
		  						<h6>Chapter</h6>
		  						<p><?php echo rtrim($chapter,","); ?></p>
		  					</div>
		  					<?php
		  					if($row['qtype']=='5'){
		  						$type="Comprehension";
		  					?>
		  						<div class="col-md-6">
		  							<h6>Question Type</h6>
		  							<p><?php echo $type; ?></p>
		  						</div>
		  						<div class="col-md-12 mt-2 questionView">
		  							<h6>Explanation</h6>
		  							<div><?php echo urldecode($database->get_name('compquestion','id',$row['compquestion'],'compquestion')); ?></div>
		  						</div>
		  						<div class="col-md-12 mt-2 questionView">
		  							<h6>Question</h6>
		  							<div><?php echo $row['question']; ?></div>
		  							<div class="row">
		  
		  								<div class="col-md-1 py-2 ">
		  									(A)</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option1']; ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(B)</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option2']; ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(C)</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option3']; ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(D)</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option4']; ?>
		  								</div>
		  							</div>
		  						</div>
		  						<?php if($row['explanation']!=''){ ?>
		  							<div class="col-md-12 mt-2">
		  								<h6>Explanation</h6>
		  								<div><?php echo $row['explanation']; ?></div>
		  									
		  
		  							</div>
		  						<?php } ?>
		  						
		  						<div class="col-md-12 mt-2">
		  							<h6>Answer</h6>
		  							<p><?php echo rtrim($row['answer'],","); ?></p>
		  								
		  
		  						</div>
		  					<?php
		  					}else if($row['qtype']=='8'){
		  						$type="Integer";
		  						?>
		  						<div class="col-md-6">
		  							<h6>Question Type</h6>
		  							<p><?php echo $type; ?></p>
		  						</div>
		  						<div class="col-md-12 mt-2 questionView">
		  							<h6>Question</h6>
		  							<div><?php echo $row['question']; ?></div>
		  						</div>
		  						
		  						
		  						<div class="col-md-12 mt-2 questionView">
		  							<h6>answer</h6>
		  							<div><?php echo rtrim($row['answer'],","); ?></div>
		  						</div>
		  						<?php if($row['explanation']!=''){ ?>
		  							<div class="col-md-12 mt-2">
		  								<h6>Explanation</h6>
		  								<div><?php echo urldecode($row['explanation']); ?></div>
		  									
		  
		  							</div>
		  						<?php } ?>
		  					<?php
		  					}else if($row['qtype']=='3'){
		  						$type="Matching";
		  					
		  						$list1='';
		  						$list2='';
		  						$obj=json_decode($row['question'],true);
		  						foreach($obj as $rowpost2)
		  						{
		  							if(strlen($rowpost2)>0)
		  							{
		  								$rowpost = explode("_",$rowpost2);
		  								$list1.=$rowpost[0]."_";
		  								$list2.=$rowpost[1]."_";
		  							}
		  						}
		  						$qlist1 = explode("_",$list1);
		  						$qlist2 = explode("_",$list2);
		  						?>
		  						<div class="col-md-6">
		  							<h6>Question Type</h6>
		  							<p><?php echo $type; ?></p>
		  						</div>
		  						
		  						<div class="col-md-12 mt-2 questionView">
		  							<h6>Question</h6>
		  							
		  							<div class="questionlist-types d-flex">
		  								<div>
		  								<h5>List1</h5>
		  									<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
		  										<?php
		  										foreach($qlist1 as $qqlist)
		  										{
		  										
		  											if(strlen($qqlist)>0)
		  											{
		  												echo '<li style="width:150px;">'.$qqlist.'</li>';
		  											}
		  										}
		  										?>
		  									
		  									</ul>
		  								</div>
		  								<div class="pl-5">
		  									<h5>List2</h5>
		  									<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
		  										<?php
		  										foreach($qlist2 as $qqlist1)
		  										{
		  											if(strlen($qqlist1)>0)
		  											{
		  												echo '<li style="width:150px;">'.$qqlist1.'</li>';
		  											}
		  										}
		  										?>
		  									</ol>
		  								</div>
		  							</div>
		  							<div class="row">
		  
		  								<div class="col-md-1 py-2 ">
		  									(A) 						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option1']; ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(B)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option2']; ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(C)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option3']; ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(D)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option4']; ?>
		  								</div>
		  							</div>
		  						</div>
		  						<?php if($row['explanation']!=''){ ?>
		  							<div class="col-md-12 mt-2">
		  								<h6>Explanation</h6>
		  								<div><?php echo $row['explanation']; ?></div>
		  									
		  
		  							</div>
		  						<?php } ?>
		  						
		  						<div class="col-md-12 mt-2">
		  							<h6>Answer</h6>
		  							<p><?php echo rtrim($row['answer'],","); ?></p>
		  								
		  
		  						</div>
		  					<?php
		  					}else if($row['qtype']=='9'){
		  						$type="Matrix";
		  						$list1='';
		  						$list2='';
		  						$obj=json_decode($row['question'],true);
		  						foreach($obj as $rowpost2)
		  						{
		  							if(strlen($rowpost2)>0)
		  							{
		  								$rowpost = explode("_",$rowpost2);
		  								$list1.=$rowpost[0]."_";
		  								$list2.=$rowpost[1]."_";
		  							}
		  						}
		  						$qlist1 = explode("_",$list1);
		  						$qlist2 = explode("_",$list2);
		  						?>
		  						<div class="col-md-6">
		  							<h6>Question Type</h6>
		  							<p><?php echo $type; ?></p>
		  						</div>
		  						
		  						<div class="col-md-12 mt-2 questionView">
		  							<h6>Question</h6>
		  							
		  								<div class="questionlist-types d-flex">
		  									<div>
		  									<h5>List1</h5>
		  										<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
		  											<?php
		  											foreach($qlist1 as $qqlist)
		  											{
		  											
		  												if(strlen($qqlist)>0)
		  												{
		  													echo '<li style="width:150px;">'.$qqlist.'</li>';
		  												}
		  											}
		  											?>
		  										
		  										</ul>
		  									</div>
		  									<div class="pl-5">
		  										<h5>List2</h5>
		  										<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
		  											<?php
		  											foreach($qlist2 as $qqlist1)
		  											{
		  												if(strlen($qqlist1)>0)
		  												{
		  													echo '<li style="width:150px;">'.$qqlist1.'</li>';
		  												}
		  											}
		  											?>
		  										</ol>
		  									</div>
		  								</div>
		  							
		  							<div class="row">
		  
		  								<div class="col-md-1 py-2 ">
		  									(A) 						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option1']; ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(B)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option2']; ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(C)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option3']; ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(D)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo $row['option4']; ?>
		  								</div>
		  							</div>
		  						</div>
		  						<?php if($row['explanation']!=''){ ?>
		  							<div class="col-md-12 mt-2">
		  								<h6>Explanation</h6>
		  								<div><?php echo $row['explanation']; ?></div>
		  									
		  
		  							</div>
		  						<?php } ?>
		  						<?php if($row['expla_vlink']!=''){ ?>
		  							<div class="col-md-12 mt-2">
		  								<h6>Explanation Video Link</h6>
		  								<div><?php echo $row['expla_vlink']; ?></div>
		  									
		  
		  							</div>
		  						<?php } ?>
		  						<div class="col-md-12 mt-2">
		  							<h6>Answer</h6>
		  							<p><?php echo rtrim($row['answer'],","); ?></p>
		  								
		  
		  						</div>
		  					<?php
		  					}else{
		  						$type="General";
		  					?>
		  						<div class="col-md-3">
		  							<h6>Question Type</h6>
		  							<p><?php echo $type; ?></p>
		  						</div>
		  						
		  						<div class="col-md-12 mt-2 questionView">
		  							<h6>Question</h6>
		  							<div><?php echo urldecode($row['question']); ?></div>
		  							<div class="row">
		  
		  								<div class="col-md-1 py-2 ">
		  									(A) 						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo urldecode($row['option1']); ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(B)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo urldecode($row['option2']); ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(C)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo urldecode($row['option3']); ?>
		  								</div>
		  
		  								<div class="col-md-1 py-2 ">
		  									(D)						</div>
		  
		  								<div class="col-md-5 py-2 ">
		  									<?php echo urldecode($row['option4']); ?>
		  								</div>
		  							</div>
		  						</div>
		  						<?php if($row['explanation']!=''){ ?>
		  							<div class="col-md-12 mt-2">
		  								<h6>Explanation</h6>
		  								<div><?php echo urldecode($row['explanation']); ?></div>
		  									
		  
		  							</div>
		  						<?php } ?>
		  						<?php if($row['expla_vlink']!=''){ ?>
		  							<div class="col-md-12 mt-2">
		  								<h6>Explanation Video Link</h6>
		  								<div><?php echo $row['expla_vlink']; ?></div>
		  									
		  
		  							</div>
		  						<?php } ?>
		  						<div class="col-md-12 mt-2">
		  							<h6>Answer</h6>
		  							<p><?php echo rtrim($row['answer'],","); ?></p>
		  								
		  
		  						</div>
		  					<?php
		  					}
		  					?>
		  					<?php if($row['complexity']!=''){ ?>
		  						<div class="col-md-12 mt-2">
		  							<h6>Complexity</h6>
		  							<p><?php echo $database->get_name('complexity','id',$row['complexity'],'complexity'); ?></p>
		  								
		  
		  						</div>
		  					<?php } ?>
		  					<?php if($row['timeduration']!=''){ ?>
		  						<div class="col-md-12 mt-2">
		  							<h6>Answering Time Duration</h6>
		  							<p><?php echo $row['timeduration']; ?></p>
		  								
		  
		  						</div>
		  					<?php } ?>
		  					<?php if($row['inputquestion']!=''){
		  					$zSql2 = $database->query("SELECT * FROM questiontype WHERE estatus='1' and id IN(".$row['inputquestion'].")"); 
		  					while($row2=mysqli_fetch_array($zSql2)){
		  						$questiontype .= $row2['questiontype'].",";
		  						$k++;
		  					}
		  					?>
		  						<div class="col-md-12 mt-2">
		  							<h6>Type of Question</h6>
		  							<p><?php echo rtrim($questiontype,","); ?></p>
		  								
		  
		  						</div>
		  					<?php } ?>
		  					<?php if($row['usageset']!=''){ ?>
		  						<div class="col-md-12 mt-2">
		  							<h6>Usage Set</h6>
		  							<p><?php echo $database->get_name('question_useset','id',$row['usageset'],'usageset'); ?></p>
		  								
		  
		  						</div>
		  					<?php } ?>
		  					<?php if($row['question_theory']!='' && $row['question_theory']!='0'){ ?>
		  						<div class="col-md-12 mt-2">
		  							<h6>Question Theory</h6>
		  							<p><?php echo $database->get_name('question_theory','id',$row['question_theory'],'question_theory'); ?></p>
		  								
		  
		  						</div>
		  					<?php } ?>
		  					
		  				</div>
		  			</div>
		  			<div class="row">
		  				<div class="col-md-12">
		  					<div class="table-responsive">
		  						<table class="table table-bordered">
		  						  <thead>
		  							<tr>
		  							  <th scope="col">Issue ID</th>
		  							  <th scope="col">Issue Section</th>
		  							  <th scope="col">Issue Type</th>
		  							  <th scope="col">Remarks</th>
		  							  <th scope="col">Status</th>
		  							</tr>
		  						  </thead>
		  						  <tbody>
		  							<?php
		  							$j=1;
		  							$sqll=$database->query("select * from question_issues where estatus='1' and question_id='".$_REQUEST['id']."'");
		  							$rcount=mysqli_num_rows($sqll);
		  							while($rowl=mysqli_fetch_array($sqll)){
		  								if($rowl['resolve_issue']=='1'){
		  									$status="Resolved";
		  								}else{
		  									$status="Pending";
		  								}
		  							?>
		  								<tr>
		  									<td><?php echo $rowl['issue_id']; ?></td>
		  									<td><?php echo $database->get_name('issue_section','id',$rowl['issue_section'],'issue_section'); ?></td>
		  									<td><?php echo $database->get_name('issue_type','id',$rowl['issue_type'],'issue_type'); ?></td>
		  									<td><?php echo $rowl['vremarks']; ?></td>
		  									<td><?php echo $status; ?></td>
		  								</tr>
		  							<?php $j++; } ?>
		  						  </tbody>
		  						</table>
		  					</div>
		  				</div>
		  			</div>
		  			 <div class="modal-footer">
		  			 
		  			
		  			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		  			
		  </div>   -->                                             
		
	<?php 
		}else{
			echo "No Results Found";

		}
	}

 ?>