
<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
?>


	<?php
	if(isset($_REQUEST['addForm'])){
		
		$data_sel =  $database->query("select * from users where username='".$session->username."'");
		$row = mysqli_fetch_array($data_sel);
		
		if($_REQUEST['class']!=''){
			$_REQUEST['class']=$_REQUEST['class'];
		}else{
			$_REQUEST['class']="1";
		}

	
	?>
		<style>
			.subjectanalysis {
				padding: .75rem 1.25rem;
				margin-bottom: 0;
				background-color: rgba(0,0,0,.03);
				border-bottom: 1px solid rgba(0,0,0,.125);
			}
			.subject-wise-analysis.nav-pills .nav-link.active {
				background-color:#000;
				color:#fff;
			}
			.subject-analysis-content label, h6 {
				color:#000;
			}
			.subject-analysis-content .table td p {
				color:#000000c7;
				font-weight:bold;
			}
			.subject-analysis-content .table th, .subject-analysis-content .table td {
				font-size:12px !important;
				padding:0.7rem 0.1rem !important;
			}
		</style>
		<div class="subject-analysis-content white_block m_top10">
			<div class="background_yash">
				<div class="row">
					
					<div class="col-sm-2">
						<label class="text-uppercase font-weight-bold f_12">Class</label>
						
							<select   class="form-control chosen-select " id="class"   >
								<?php 
								$seclass=$database->query("select * from class where estatus='1' ");
								while($rowclass=mysqli_fetch_array($seclass)){ ?>
								
								<option value="<?php  echo $rowclass['id'];  ?>" <?php if(isset($_REQUEST['class'])){ if($_REQUEST['class']==$rowclass['id']){ echo 'selected';}else{}} ?>  ><?php echo $rowclass['class'];?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-sm-2">
						
						<label class="text-uppercase font-weight-bold f_12">Subject</label>
							<select   class="form-control " id="subject"    onchange="setState('chapterdiv','<?php echo SECURE_PATH;?>superdashboard/process.php','chapterdiv=1&subject='+$('#subject').val()+'&class=<?php echo $_REQUEST['class']; ?>&totalquestions=<?php echo $_REQUEST['totalquestions']; ?>')" >
							<?php $sesubject=$database->query("select * from subject where estatus='1' ");
							while($rowsubject=mysqli_fetch_array($sesubject)){ ?>
								
								<option value="<?php  echo $rowsubject['id'];  ?>" <?php if(isset($_REQUEST['subject'])){ if($_REQUEST['subject']==$rowsubject['id']){ echo 'selected';}else{}} ?>  ><?php echo $rowsubject['subject'];?></option>
							<?php } ?>
							</select>
					</div>
					<div class="col-sm-3">
						<label class="text-uppercase font-weight-bold f_12">Question Type</label>
						
						<select   class="form-control chosen-select " id="totalquestions"   >
							<option value='verified' <?php if(isset($_REQUEST['totalquestions'])){ if($_REQUEST['totalquestions']=='verified'){ echo 'selected';}else{ echo 'selected';}} ?> >Verified Question</option>
							<option value='notverified' <?php if(isset($_REQUEST['totalquestions'])){ if($_REQUEST['totalquestions']=='notverified'){ echo 'selected';}else{}} ?> >Not Verified</option>
							<option value='All' <?php if(isset($_REQUEST['totalquestions'])){ if($_REQUEST['totalquestions']=='All'){ echo 'selected';}else{}} ?> >All Questions</option>
					</select>
					</div>
					<div class="col-sm-3" id="chapterdiv">
						<label class="text-uppercase font-weight-bold f_12">Chapter</label>
						
						<select id="chapter" class="form-control chosen-select" >
							<option value=''>All</option>
							<?php
								$sqoll=$database->query("select * from chapter where estatus='1' and subject='".$_REQUEST['subject']."' ");
								while($rowoll=mysqli_fetch_array($sqoll)){ ?>
									<option value="<?php  echo $rowoll['id'];  ?>" <?php if(isset($_REQUEST['chapter'])){ if($_REQUEST['chapter']==$rowoll['id']){ echo 'selected';}else{}} ?>  ><?php echo $rowoll['chapter'];?></option>
								<?php } 
							?>
						</select>
					</div>
					<div class="col-sm-2" id="attributesOverveiw">
						<a class="btn btn-outline-success btn_bg_white text-uppercase m_top30" onclick="setState('adminForm','<?php echo SECURE_PATH;?>superdashboard/process.php','addForm=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&totalquestions='+$('#totalquestions').val()+'&chapter='+$('#chapter').val());" >SUBMIT</a>
					</div>
				</div>
			</div>
			<?php
			if($_REQUEST['class']!=''){
				$class=" AND FIND_IN_SET(".$_REQUEST['class'].",class) > 0 ";
			}else{
				$class=" AND class='1'";
			}
			if($_REQUEST['chapter']!=''){
				$chapter=" AND FIND_IN_SET(".$_REQUEST['chapter'].",chapter) >0  ";
			}else{
				$chapter="";
			}
			if($_REQUEST['topic']!=''){
				$topic=" AND FIND_IN_SET(".$_REQUEST['topic'].",topic) >0  ";
			}else{
				$topic="";
			}
			$subject=" and subject='".$_REQUEST['subject']."'";
			$data_sel =  $database->query("select * from users where username='".$session->username."'");
			$row = mysqli_fetch_array($data_sel);
			//$class2=" and class='".$row['subject']."'";
			$chapter2=" and chapter IN (".$row['chapter'].")";
			$review_status=" and review_status='1'";
			
			if($_REQUEST['totalquestions']=='All'){
				$con=$class.$subject.$chapter.$topic;

			}else if($_REQUEST['totalquestions']=='notverified'){
				$vstatus=" and vstatus1='0'";
				$con=$class.$subject.$chapter.$topic.$vstatus;

			}else if($_REQUEST['totalquestions']=='verified'){
				$vstatus=" and vstatus1='1'";
				$con=$class.$subject.$chapter.$topic.$vstatus;
				
			}else{
				$vstatus=" and vstatus1='1'";
				$con=$class.$subject.$chapter.$topic.$vstatus;

			}
			
			/*$totchaptersquev=$database->query("select count(*) from createquestion where estatus='1'".$con."");
			$rowchapterquev=mysqli_fetch_array($totchaptersquev);

			$totchaptersquevr=$database->query("select count(*) from createquestion where estatus='1' and review_status='0'".$con."");
			$rowchapterquevr=mysqli_fetch_array($totchaptersquevr);

			$totchaptersquevr1=$database->query("select count(*) from createquestion where estatus='1' and review_status='1'".$con."");
			$rowchapterquevr1=mysqli_fetch_array($totchaptersquevr1);

			$totchaptersquea=$database->query("select count(*) from createquestion where estatus='1'".$con."");
			$rowchapterquea=mysqli_fetch_array($totchaptersquea);*/
			if(isset($_POST['chapoverview'])){
				$ctitle=$database->get_name('chapter','id',$_POST['chapter'],'chapter');
			}else{
				$ctitle="All Chapters";
			}
			$tot=0;
			$totchap=$database->query("select count(id) from chapter where estatus='1' and class='".$_REQUEST['class']."'".$subject."");
			$rowchap=mysqli_fetch_array($totchap);

			$totchap2=$database->query("select * from chapter where estatus='1' and class='".$_REQUEST['class']."'".$subject."");
			while($rowchap1=mysqli_fetch_array($totchap2)){
				
				$totchap=$database->query("select id from review_chapters where estatus='1' and class='".$_REQUEST['class']."'  and subject='".$row['subject']."' and chapter='".$rowchap1['id']."' group by chapter");
				
				$rowchap1=mysqli_num_rows($totchap);
				$tot=$tot+$rowchap1;
			}
			$ptot=$rowchap[0]-$tot;

			$tott=0;
			$totchapt=$database->query("select * from topic where estatus='1' and class='".$_REQUEST['class']."' and chapter='".$_REQUEST['chapter']."'".$subject."");
			$rowchapt=mysqli_fetch_array($totchapt);
			
			while($rowchapt1=mysqli_fetch_array($totchapt)){
				$totchap=$database->query("select count(*) from review_chapters where estatus='1' and class='".$_REQUEST['class']."'  and subject='".$_REQUEST['subject']."' and chapter='".$rowchapt1['chapter']."' and topic='".$rowchapt1['id']."'");
				$rowchap1=mysqli_fetch_array($totchap);
				$tott=$tott+$rowchap1[0];
			}
			$ptott=$rowchapt[0]-$tott;
	
			?>
			<div class="row">
				<div class="col-sm-12">
					<div class="card m_top10">
						<h5 class="card-header text-center text-uppercase font-weight-600 subjectanalysis"><?php echo $database->get_name('subject','id',$_REQUEST['subject'],'subject'); ?> Subject Analysis</h5>
						<div class="card-body p-2">
							<h6 class="card-title text-uppercase font-weight-600">OverView</h6>    
							<div class="row">
								<div class="col-sm-3">
									<div class="br_yash">
										<h6 class="font-weight-600 text-uppercase font_14">Total</h6>
												<h4 class="m_top10"><?php if(isset($_REQUEST['chapter'])){ if($_REQUEST['chapter']!=''){ echo $rowchapt[0];  }else{ echo $rowchap[0]; }  }else{ echo $rowchap[0]; }?></h4>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="br_yash">
										<h6 class="font-weight-600 text-uppercase font_14">Chapters/Topics</h6>
												
												<h4 class="">
													<?php if(isset($_REQUEST['chapter'])){ if($_REQUEST['chapter']!=''){ echo $rowchapt[0];  }else{ echo $rowchap[0]; }  }else{ echo $rowchap[0]; }?>
												
									</div>
								</div>
								<div class="col-sm-3">
									<div class="br_yellow">
										<div class="row">
											<div class="col-sm-8">
												<h6 class="font-weight-600 text-uppercase font_14">Review Pending</h6>
											</div>
											<div class="col-sm-4 float-right">
												<img class="float-right icon_view" src="<?php echo SECURE_PATH;?>vendor/images/in progress.png">
											</div>
											<div class="col-sm-12">
												<h4 class="m_top10"><?php if(isset($_REQUEST['chapter'])){ if($_REQUEST['chapter']!=''){ echo $ptott;  }else{ echo $ptot; }  }else{ echo $ptot; }?></h4>
												
											</div>
										 </div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="br_green">
										<div class="row">
											<div class="col-sm-8">
												<h6 class="font-weight-600 text-uppercase font_14">Completed</h6>
											</div>
											<div class="col-sm-4 float-right">
												<img class="float-right icon_view" src="<?php echo SECURE_PATH;?>vendor/images/completed.png">
											</div>
											<div class="col-sm-12">
												<h4 class="m_top10"><?php if(isset($_REQUEST['chapter'])){ if($_REQUEST['chapter']!=''){ echo $tott;  }else{ echo $tot; }  }else{ echo $tot; }?></h4>
											</div>
										 </div>
									</div>
								</div>
							</div>
						</div>
					  </div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="card m_top10">
						<div class="card-header p-2" id="questionattribute">
							<div class="row">
								<div class="col-sm-12">
									
										
										<h5 class="mb-0">
											<a href='' class="d-flex justify-content-between align-items-center"
											data-toggle="collapse" data-target="#attributediv" aria-expanded="true"
											aria-controls="attributediv" onclick="setStateGet('questionattribute','<?php echo SECURE_PATH;?>superdashboard/process6.php','questionattribute=1&class=<?php echo $_REQUEST['class']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&totalquestions=<?php echo $_REQUEST['totalquestions']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>')" >
												<h5 class="card-title mb-0"><b>Attribute Overview</b></h5>
												<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
											</a>


										</h5>
									
																					
								</div>
							</div>
						</div>
					</div>	
				</div>		 
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<div class="card m_top10">
						<div class="card-header p-2" id="questionCategory">
							<div class="row">
								<div class="col-sm-12">
									
								
										<h5 class="mb-0">
											<a href='' class="d-flex justify-content-between align-items-center"
											data-toggle="collapse" data-target="#customdiv" aria-expanded="true"
											aria-controls="customdiv" onclick="setStateGet('questionCategory','<?php echo SECURE_PATH;?>superdashboard/process5.php','questionCategory=1&class=<?php echo $_REQUEST['class']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&totalquestions=<?php echo $_REQUEST['totalquestions']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>')" >
												<h5 class="card-title mb-0"><b>Question Category Vs Question types</b></h5>
												<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
											</a>
										</h5>
									
																					
								</div>
							</div>
						</div>
					</div>	
				</div>		 
			</div>														
			
			
		</div>
			
	<?php
	}
	if(isset($_REQUEST['chapterdiv'])){
		
		if(isset($_REQUEST['subject'])){
			if($_REQUEST['subject']!=""){
				$subject=" AND subject='".$_REQUEST['subject']."'";
			}else{
				$subject="";
			}
		}else{
		}
	
		if(isset($_REQUEST['class'])){
			if($_REQUEST['class']!=""){
				$class=" AND class='".$_REQUEST['class']."'";
			}else{
				$class="";
			}
		}else{
		}
	?>
		<label class="text-uppercase font-weight-bold f_12">Chapter</label>
						
			<select id="chapter" class="form-control chosen-select" >
				<option value=''>All</option>
				<?php
				
				
					$sqoll=$database->query("select * from chapter where estatus='1'".$class.$subject."");
					while($rowoll=mysqli_fetch_array($sqoll)){ ?>
						<option value="<?php  echo $rowoll['id'];  ?>" <?php if(isset($_REQUEST['chapter'])){ if($_REQUEST['chapter']==$rowoll['id']){ echo 'selected';}else{}} ?>  ><?php echo $rowoll['chapter'];?></option>
					<?php }
				?>
			</select>
	<?php
	}
	?>
		