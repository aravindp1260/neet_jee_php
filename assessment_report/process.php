<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
	$con = mysqli_connect("localhost","rspace","Rsp@2019","neetjee");
	//$con = mysqli_connect("localhost","root","","neetjee");
		function query($sql){
		global $con;
		 return mysqli_query($con,$sql);
		}

		$con1 = mysqli_connect("localhost","rspace","Rsp@2019","qsbank");
	//$con = mysqli_connect("localhost","root","","neetjee");
		function query1($sql){
		global $con1;
		 return mysqli_query($con1,$sql);
		}
?>


	<?php
	if(isset($_REQUEST['tableDisplay'])){
		
		
	
	?>
		<style>
			.subjectanalysis {
				padding: .75rem 1.25rem;
				margin-bottom: 0;
				background-color: rgba(0,0,0,.03);
				border-bottom: 1px solid rgba(0,0,0,.125);
			}
			.subject-wise-analysis.nav-pills .nav-link.active {
				background-color:#000;
				color:#fff;
			}
			.subject-analysis-content label, h6 {
				color:#000;
			}
			.subject-analysis-content .table td p {
				color:#000000c7;
				font-weight:bold;
			}
			.subject-analysis-content .table th, .subject-analysis-content .table td {
				font-size:12px !important;
				padding:0.7rem 0.1rem !important;
			}
		</style>
		<style>
	
	 </style>
	<script>
	$('.chapter').selectpicker1();
	 </script>
		<section class="content-area">
			<div class="container">
				<div class="row Data-Tables">
					<div class="col-xl-12 col-lg-12"> 
						<div class="card border-0 shadow mb-4">
							<div class="card-header py-3">
								<h6 class="m-0 font-weight-bold text-primary"> Assessment Report</h6>
							</div>
							<div class="card-body">
							
									<script type="text/javascript">


									  $('#dataTable').DataTable({
										"pageLength": 50,
										"order": [[ 0, 'desc' ]]
										
									  });

									</script>
									<div class="card border-0">
										<div class="card-body table-responsive table_data p-0">
											<table class="table table-bordered dashboard-table mb-0" id="dataTable">
												<thead class="thead-light">
													<tr>
														<th style="text-align:center;">Exam id</th>
														<th style="text-align:center;">Name</th>
														<th style="text-align:center;">Mobile</th>
														<th style="text-align:center;">Email</th>
														<th style="text-align:center;">Exam</th>
														<th style="text-align:center;">Exam Type</th>
														<th style="text-align:center;">Exam Duration</th>
														<th style="text-align:center;">Year</th>
														<th style="text-align:center;">Subject</th>
														<th style="text-align:center;">Start Time</th>
														<th style="text-align:center;">Questions Answered</th>
														<th style="text-align:center;">Correct</th>
														<th style="text-align:center;">Wrong</th>
														<th style="text-align:center;">Skipped</th>
														<th style="text-align:center;">Marks</th>
														<th style="text-align:center;">Exam Time</th>
														<th style="text-align:center;">Status</th>
													</tr>
													
												</thead>
											<tbody>
												<?php
												$k=1;
												$sql=query("SELECT * FROM `assist_examdetails`  where estatus='1' and timestamp>=1595849400 order by timestamp desc  ");
												while($value=mysqli_fetch_array($sql)){
													$time=$value['timestamp'];
													if($value['exam']=='1'){
														$exam="NEET";
													}else{
														$exam="JEE";
													}
													if($value['exam_short_full']=='1'){
														$exam_short_full="Quick Assessment";
													}else{
														$exam_short_full="Comprehensive Assessment";
													}
													if($value['is_completed']==1){
														$status="Started";
													}else if($value['is_completed']==2){
														$status="Completed";
													}else if($value['is_completed']==0){
														$status="Not Started";
													}
													$class="";
													$classdata=query1("select * from class where estatus='1' and id in (".$value['class_id'].")");
													while($row1=mysqli_fetch_array($classdata)){
														$class.=$row1['class'].",";
													}
													$class1=rtrim($class,",");
													$subject='';
													$subjectdata=query1("select * from subject where estatus='1' and id in (".$value['subject'].")");
													while($row11=mysqli_fetch_array($subjectdata)){
														$subject.=$row11['subject'].",";
													}
													$subject1=rtrim($subject,",");
													$data1=query("select count(id) as cnt from assessment_questions where estatus='1' and exam_session_id='".$value['id']."' and status!=0");
													$rowdata1=mysqli_fetch_array($data1);

													$answered=$rowdata1['cnt'];

													$data1c=query("select count(id) as cnt from assessment_questions where estatus='1' and exam_session_id='".$value['id']."' and status=2");
													$rowdata1c=mysqli_fetch_array($data1c);

													$correct=$rowdata1c['cnt'];

													$data1cw=query("select count(id) as cnt from assessment_questions where estatus='1' and exam_session_id='".$value['id']."' and status=1");
													$rowdata1cw=mysqli_fetch_array($data1cw);

													$wrong=$rowdata1cw['cnt'];

													$skipped=$value['total_questions']-$answered;


													$data1time=query("select sum(question_time) as time from assessment_questions where estatus='1' and exam_session_id='".$value['id']."' ");
													$rowdata1ctime=mysqli_fetch_array($data1time);
													$time=gmdate("H:i:s", $rowdata1ctime['time']);

													$marks=($correct*4)-$wrong;
													?>
													<tr>
														<td><?php echo $value['id']; ?></td>
														<td><?php echo $value['firstname']; ?></td>
														<td><?php echo $value['mobile']; ?></td>
														<td><?php echo $value['email']; ?></td>
														<td><?php echo $exam; ?></td>
														<td><?php echo $exam_short_full; ?></td>
														<td><?php echo $value['duration']; ?></td>
														<td><?php echo $class1; ?></td>
														<td><?php echo $subject1; ?></td>
														<td class="text-left" data-order="<?php echo $time; ?>"><?php echo date('d/m/Y H:i:s',$value['timestamp']);?></td>
														<td class="text-left"><?php echo $answered; ?></td>
														<td class="text-left"><?php echo $correct;  ?></td>
														<td class="text-left"><?php echo $wrong;  ?></td>
														<td class="text-left"><?php echo $skipped;  ?></td>
														<td class="text-left"><?php echo $marks; ?></td>
														<td class="text-left"><?php echo $time; ?></td>
														<td class="text-left"><?php echo $status; ?></td>
														
													</tr>
													<?php
													$k++;
												} ?>
											</tbody>
										</table>
									
									
									</div>	
								</div>
								
								
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>
		
			
	<?php
	}
	
	?>
	
