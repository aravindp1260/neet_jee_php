<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}

//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
    $data_sel = $database->query("SELECT * FROM video_prices WHERE id = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
    $_POST = array_merge($_POST,$data);
	
 ?>
 <script type="text/javascript">
 $('#adminForm').slideDown();
 </script>
 
 <?php
    }
 }
 ?>
	
	<script>
		$(function () {
			$('.datepicker1,.datepicker2,.datepicker3').datetimepicker({
			   format: 'DD-MM-YYYY'
			   
			});
		});

	</script>
	<!-- <script>
		
		editor = com.wiris.jsEditor.JsEditor.newInstance({'language': 'en'});
	                 editor.insertInto(document.getElementById('editorContainer'));
				 width:300;
				 height:500;
	</script> -->
	
  <div class="col-lg-12 col-md-12">
 
 	<div class="row">
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Class<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<div class="col-sm-8">
													
						<!-- <div class="custom-control custom-checkbox ">
							<input type="checkbox" id="customcheckboxInline3" name="customcheckboxInline3" class="class3 " onChange="getFields2();setState('aaa','<?php echo SECURE_PATH;?>videoprice/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'')"  value="<?php if(isset($_POST['class'])) {  if(in_array('1', explode(",",$_POST['class']))) { echo '1'; }else{ echo '1'; } } else { echo '1'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('1', explode(",",$_POST['class']))) { echo 'checked'; } } ?> >
							<label class="" for="customcheckboxInline3">XI</label>
						</div>
						<div class="custom-control custom-checkbox ">
							<input type="checkbox" id="customcheckboxInline4" name="customcheckboxInline3" class="class3 " onChange="getFields2();setState('aaa','<?php echo SECURE_PATH;?>videoprice/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'')"  value="<?php if(isset($_POST['class'])) {  if(in_array('2', explode(",",$_POST['class']))) { echo '2'; }else{ echo '2'; } } else { echo '2'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('2', explode(",",$_POST['class']))) { echo 'checked'; }} ?> >
							<label class="" for="customcheckboxInline4">XII</label>
						</div> -->
							<div class="form-check form-check-inline">
							  <input class="form-check-input class3" type="checkbox" id="inlineCheckbox1" value="1" onChange="getFields2();setState('aaa','<?php echo SECURE_PATH;?>videoprice/ajax.php','getsubject=1&class=1&exam='+$('#exam').val()+'')"  value="<?php if(isset($_POST['class'])) {  if(in_array('1', explode(",",$_POST['class']))) { echo '1'; }else{ echo '1'; } } else { echo '1'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('1', explode(",",$_POST['class']))) { echo 'checked'; } } ?>>
							  <label class="form-check-label" for="inlineCheckbox1">XI</label>
							</div>
							<div class="form-check form-check-inline">
							  <input class="form-check-input class3" type="checkbox" id="inlineCheckbox2" value="2"  onChange="getFields2();setState('aaa','<?php echo SECURE_PATH;?>videoprice/ajax.php','getsubject=1&class=2&exam='+$('#exam').val()+'')"  value="<?php if(isset($_POST['class'])) {  if(in_array('2', explode(",",$_POST['class']))) { echo '2'; }else{ echo '2'; } } else { echo '2'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('2', explode(",",$_POST['class']))) { echo 'checked'; }} ?>>
							  <label class="form-check-label" for="inlineCheckbox2">XII</label>
							</div>
							<input type="hidden" name="class" id="class"  value="<?php if(isset($_POST['class'])) { if($_POST['class']!=''){ echo $_POST['class']; }else { echo '0';} }else{ echo '0'; }?>" >
							<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['class'])){ echo $_SESSION['error']['class'];}?></span>
					</div>
					
				</div>
			</div>
		</div>
	
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Exam<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<select name="exam" class="form-control selectpicker1"  id="exam"  multiple  data-actions-box="true" data-live-search="true"  onChange="setStateGet('aaa','<?php echo SECURE_PATH;?>videoprice/ajax.php','getsubject=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'')">
						<option value=''>-Select-</option>
						<?php
						$row1 = $database->query("SELECT * FROM exam WHERE estatus='1' "); 
						while($data1 = mysqli_fetch_array($row1))
						{
						  ?>
						<option value="<?php echo $data1['id'];?>" <?php if(isset($_POST['exam'])) { if(in_array($data1['id'], explode(",",$_POST['exam']))) { echo 'selected="selected"'; } } ?>   ><?php echo $data1['exam'];?></option>
						<?php
						}
						?>
					</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['exam'])){ echo $_SESSION['error']['exam'];}?></span>
				</div>
			</div>
		</div>
		<?php
		if(isset($_POST['exam'])){
		if($_POST['exam']!=''){
			$examid='';
			$sql=$database->query("select * from exam where estatus='1' and id in (".$_POST['exam'].")");
			while($row=mysqli_fetch_array($sql)){
				$examid.=$row['subjects'].",";
			}
			$exam=explode(",",$examid);
			$exam1=array_unique($exam);
			$subject1=implode(",",$exam1);
			$subjectids=" AND id IN (".rtrim($subject1,",").")";
		}else{
			$subjectids=" AND id IN (1,2,3,4,5)";
		}
	}else{
		$subjectids=" AND id IN (1,2,3,4,5)";
	}
	?>
		<div class="col-lg-6" id="aaa">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Subject</label>
				<div class="col-sm-8">
					
					<select class="form-control " name="subject" value=""   id="subject" onChange="setState('ccc','<?php echo SECURE_PATH;?>videoprice/ajax.php','getchapter=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'')">
					<option value="">-select-</option>
					<?php
					$row = $database->query("select * from subject where estatus='1' ".$subjectids."");
					while($data = mysqli_fetch_array($row))
					{
						?>
					<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['subject'])) { if($_POST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['subject']);?></option>
					<?php
					}
					?>
				</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['exam_ids'])){ echo $_SESSION['error']['exam_ids'];}?></span>
				</div>
			</div>
		</div>
	
		<div class="col-lg-6" id="ccc">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Chapter</label>
				<div class="col-sm-8">
					<select class="form-control" name="chapter"  value=""    id="chapter"  >
						<?php
						$row = $database->query("select * from chapter where estatus='1' and  class IN  (".rtrim($_POST['class'],",").") and subject IN (".$_POST['subject'].") ");
						while($data = mysqli_fetch_array($row))
						{
							?>
						<option value="<?php echo $data['id']; ?>" <?php if(isset($_POST['chapter'])) { if($_POST['chapter']==$data['id']) { echo 'selected="selected"'; }  } ?> > <?php echo ucwords($data['chapter']);?></option>
						<?php
						}
						?>
					</select> 
					<span class="error text-danger"><?php if(isset($_SESSION['error']['subject_ids'])){ echo $_SESSION['error']['subject_ids'];}?></span>
				</div>
			</div>
		</div>
	
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Price<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="price"   autocomplete="off" id="price" value='<?php if(isset($_POST['price'])){ echo $_POST['price']; }else{ echo ''; }?>' >
					<span class="error text-danger"><?php if(isset($_SESSION['error']['price'])){ echo $_SESSION['error']['price'];}?></span>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group row">
				<a class="radius-20 btn btn-theme px-5" onClick="setState('adminForm','<?php echo SECURE_PATH;?>videoprice/process.php','validateForm=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&price='+$('#price').val()+'&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>')"> Save</a>
			</div>
		</div>
	</div>
	
	
	
</div>

<?php
unset($_SESSION['error']);
}
?>

<?php
if(isset($_POST['validateForm'])){
	
	$_SESSION['error'] = array();
	
	$post = $session->cleanInput($_POST);

	 	$id = 'NULL';
		  	if(isset($post['editform'])){
	  $id = $post['editform'];

	}
	$field = 'price';
	if(!$post['price'] || strlen(trim($post['price'])) == 0){
	  $_SESSION['error'][$field] = "* Price  cannot be empty";
	}
	$field = 'class';
	if(!$post['class'] || strlen(trim($post['class'])) == 0){
	  $_SESSION['error'][$field] = "* Class  cannot be empty";
	}
	 $field = 'exam';
	if(!$post['exam'] || strlen(trim($post['exam'])) == 0){
	  $_SESSION['error'][$field] = "* Exam cannot be empty";
	}
	
	if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
	?>
    <script type="text/javascript">
      $('#adminForm').slideDown();
      setState('adminForm','<?php echo SECURE_PATH;?>videoprice/process.php','addForm=1&class=<?php echo $post['class'];?>&exam=<?php echo $post['exam'];?>&subject=<?php echo $post['subject'];?>&chapter=<?php echo $post['chapter'];?>&price=<?php echo $post['price'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?><?php if(isset($_POST['from_page'])){ echo '&from_page='.$post['from_page'];}?>')
    </script>
  <?php
	}
	else{
		if($post['subject']!=''){
				$subject=$post['subject'];
			}else{
				$subject=0;
			}
			if($post['chapter']!=''){
				$chapter=$post['chapter'];
			}else{
				$chapter=0;
			}
			$class=rtrim($post['class'],",");
		if($id=='NULL')
		{
			$result=$database->query("insert video_prices set class='".$class."',exam='".$post['exam']."',subject='".$subject."',chapter='".$chapter."',price='".$post['price']."',estatus='1',username='".$session->username."',timestamp='".time()."'");
			$lid = mysqli_insert_id($database->connection);

			
			

		if($lid!="" && $lid!=0 && $lid!=NULL){
			
			
			
			?>
				<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Video Price Created  Successfully!</div>
				
					<script type="text/javascript">
				
					animateForm('<?php echo SECURE_PATH;?>videoprice/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
				</script>
			<?php
			}else{
					?>
				<div class="alert alert-danger"><i class="fa fa-thumbs-up fa-2x"></i> Video  Price Creation Failed!</div>
				
					<script type="text/javascript">
				
					animateForm('<?php echo SECURE_PATH;?>videoprice/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
				</script>
			<?php
			}
			
		}else{
			
			$result=$database->query("update video_prices set class='".$class."',exam='".$post['exam']."',subject='".$subject."',chapter='".$chapter."',price='".$post['price']."',estatus='1',username='".$session->username."'  where id='".$id."'");
			if($result){
				?>
					<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Video Price Details Updated  Successfully!</div>
					
						<script type="text/javascript">
					
						animateForm('<?php echo SECURE_PATH;?>videoprice/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
					</script>
			<?php
			}else{
					?>
				<div class="alert alert-danger"><i class="fa fa-thumbs-up fa-2x"></i>  Video Price  Creation Failed!</div>
				
					<script type="text/javascript">
				
					animateForm('<?php echo SECURE_PATH;?>videoprice/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
				</script>
			<?php
			}
		}
	}
		

?>

 
	<?php
	

}
?>
<?php

//Display bulkreport
if(isset($_GET['tableDisplay'])){
	//Pagination code
  $limit=50;
  if(isset($_GET['page']))
  {
    $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
    $page=$_GET['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
  //Search Form
?>
<?php
  $tableName = 'video_prices';
  $condition = "estatus='1'";//"userlevel = '8' OR userlevel = '9' OR userlevel = '7' OR userlevel = '6'";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  //$query_string = $_SERVER['QUERY_STRING'];
  $pagination = $session->showPagination(SECURE_PATH."videoprice/process.php?tableDisplay=1&",$tableName,$start,$limit,$page,$condition);
  $q = "SELECT * FROM $tableName ".$condition." ";
 $result_sel = $database->query($q);
  $numres = mysqli_num_rows($result_sel);
  $query = "SELECT * FROM $tableName ".$condition."  order by id asc";
  
  $data_sel = $database->query($query);
  if(($start+$limit) > $numres){
	 $onpage = $numres;
	 }
	 else{
	  $onpage = $start+$limit;
	 }
  if($numres > 0){
	  
	?>
		  <script type="text/javascript">


  $('#dataTable').DataTable({
    "pageLength": 50
    
    
  });

</script>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">Video Price Data</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" id="dataTable">
									<thead>
										<tr>
											<th class="text-left" nowrap>Sr.No.</th>
											<th class="text-left" nowrap>Class</th>
											<th class="text-left" nowrap>Exam</th>
											<th class="text-left" nowrap>Subject</th>
											<th class="text-left" nowrap>Chapter</th>
											<th class="text-left" nowrap>Price</th>
											<th class="text-left" nowrap>Options</th>
										</tr>
									</thead>
									<tbody>
									<?php
										$k=1;
									  while($value = mysqli_fetch_array($data_sel))
									  {
										 $class='';
										$sqlclass=$database->query("select * from class where estatus='1' and id in (".rtrim($value['class'],",").")");
										while($rowclass=mysqli_fetch_array($sqlclass)){
											$class.=$rowclass['class'].",";
										}
										 $exam='';
										$sqlexam=$database->query("select * from exam where estatus='1' and id in (".rtrim($value['exam'],",").")");
										while($rowexams=mysqli_fetch_array($sqlexam)){
											$exam.=$rowexams['exam'].",";
										}
										$sqlsubject=$database->query("select * from subject where estatus='1' and id in (".$value['subject'].")");
										$rowsubject=mysqli_fetch_array($sqlsubject);

										$sqlchapter=$database->query("select * from chapter where estatus='1' and id in (".$value['chapter'].")");
										$rowchapter=mysqli_fetch_array($sqlchapter);
									  ?>
										<tr>
										<td class="text-left"><?php echo $k;?></td>
										<td class="text-left" nowrap><?php echo rtrim($class,",");?></td>
										<td class="text-left" nowrap><?php echo rtrim($exam,",");?></td>
										<td class="text-left" nowrap><?php echo $rowsubject['subject'];?></td>
										<td class="text-left" nowrap><?php echo $rowchapter['chapter'];?></td>
										<td class="text-left" nowrap><?php echo $value['price'];?></td>
										<td class="text-left" nowrap><a class=""  onClick="setState('adminForm','<?php echo SECURE_PATH;?>videoprice/process.php','addForm=2&editform=<?php echo $value['id'];?>')"><span style="color:blue;"><i class="fa fa-pencil-square-o" style="color:blue;"></i></a>&nbsp;&nbsp;&nbsp;
										<a class=" " onClick="confirmDelete('adminForm','<?php echo SECURE_PATH;?>videoprice/process.php','rowDelete=<?php echo $value['id'];?>')"><i class="fa fa-trash" style="color:red;"></i></a></td>
									  </tr>
														<?php
									$k++;
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  </section>
 
	<?php
	}
	else{
	?>
		
  <?php
	}

}
if(isset($_REQUEST['getsubject'])){
	if(isset($_REQUEST['exam'])){
		if($_REQUEST['exam']!=''){
			$examid='';
			$sql=$database->query("select * from exam where estatus='1' and id in (".$_REQUEST['exam'].")");
			while($row=mysqli_fetch_array($sql)){
				$examid.=$row['subjects'].",";
			}
			$exam=explode(",",$examid);
			$exam1=array_unique($exam);
			$subject=implode(",",$exam1);
			$subjectids=" AND id IN (".rtrim($subject,",").")";
		}else{
			$subjectids=" AND id IN (1,2,3,4,5)";
		}
	}else{
		$subjectids=" AND id IN (1,2,3,4,5)";
	}
	?>
		<div class="form-group row">
			<label class="col-sm-4 col-form-label">Subjects<span style="color:red;">*</span></label>
			<div class="col-sm-8">
				<select name="subject_ids" class="selectpicker3" id="subject_ids" multiple data-actions-box="true" data-live-search="true" >
					
					<?php
						echo "SELECT * FROM subject WHERE estatus='1' ".$subjectids."";
					$row1 = $database->query("SELECT * FROM subject WHERE estatus='1' ".$subjectids.""); 
					while($data1 = mysqli_fetch_array($row1))
					{
					  ?>
					<option value="<?php echo $data1['id'];?>" <?php if(isset($_POST['subject_ids'])) { if(in_array($data1['id'], $_POST['subject_ids'])) { echo 'selected="selected"'; } } ?>   ><?php echo $data1['subject'];?></option>
					<?php
					}
					?>
				</select>
				<span class="error text-danger"><?php if(isset($_SESSION['error']['subject_ids'])){ echo $_SESSION['error']['subject_ids'];}?></span>
			</div>
		</div>
	<?php
}
?>
	<?php
if(isset($_GET['rowDelete'])){
	$database->query("update video_prices set estatus='0'  WHERE id = '".$_GET['rowDelete']."'");
	?>
    <div class="alert alert-success"> Video Price deleted successfully!</div>

  <script type="text/javascript">

animateForm('<?php echo SECURE_PATH;?>videoprice/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
   <?php
}
?>

  <script>
 $(".selectpicker1").selectpicker('refresh');
 $(".selectpicker2").selectpicker('refresh');
  $(".selectpicker3").selectpicker('refresh');
 $(".selectpicker4").selectpicker('refresh');
</script>
<script>
function getFields2()
{
    var ass='';
    $('.class3').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#class').val(ass);
	});
        
        

    
}
</script>



