<style type="text/css">
.btn-group-sm>.btn, .btn-sm {
    padding: .2rem .2rem;}
    i.fa.fa-key.black {
    padding: 0.3rem 0.1rem;
}
    </style>
<?php
ini_set('display_errors','0');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');

error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}
?>
<script>
$(document).ready(function(){
  //alert("sdfgh"+userlevelval);
  $('#userlevel').change(function(){
    var userlevelval = $("#userlevel option:selected").val();
    
    if(userlevelval == 3 || userlevelval == 2 || userlevelval == 1){
    $('.department').show();
    $('.designation').show();
    
  }
  else{
    $('.department').hide();
    $('.designation').hide();
  }
  });
  
});
</script>
<script type="text/javascript">
    function validate(){
		
		 var count_err = 0;
		 var a=$('#exam_type').val();
		if(($('#exam_type').length)==0 || $('#exam_type').val()=='' || $('#exam_type').val()=='0'|| $('#exam_type').val()==null){
			 count_err=count_err+1;
            $('#exam_type_error').show();
		}else{ 
			$('#exam_type_error').hide();
		}

		/*if(($('#sub_exam_type').length)==0 || $('#sub_exam_type').val()=='' || $('#sub_exam_type').val()=='0' || $('#sub_exam_type').val()==null){
            count_err=count_err+1;
            $('#sub_exam_type_error').show();
		}else{ $('#sub_exam_type_error').hide();}*/

		/*if(($('#exam_paper').length)==0 || $('#exam_paper').val()=='' || $('#exam_paper').val()=='0' || $('#exam_paper').val()==null){
            count_err=count_err+1;
            $('#exam_paper_error').show();
		}else{ $('#exam_paper_error').hide();}*/

		if(($('#email_template_id').length)==0 || $('#email_template_id').val()=='' || $('#email_template_id').val()=='0' || $('#email_template_id').val()==null){
            count_err=count_err+1;
            $('#email_template_id_error').show();
		}else{ $('#email_template_id_error').hide();}

		if(($('#file').length)==0 || $('#file').val()=='' || $('#file').val()=='0' || $('#file').val()==null){
            count_err=count_err+1;
            $('#file_error').show();
		}else{ $('#file_error').hide();}
		
		if(count_err==0){
			
			$('#messageDetails').modal('show');
		  
			setState('viewDetails','<?php echo SECURE_PATH; ?>send_email_template/process1.php','viewDetails=1&exam_type='+$('#exam_type').val()+'&sub_exam_type='+$('#sub_exam_type').val()+'&exam_paper='+$('#exam_paper').val()+'&email_template_id='+$('#email_template_id').val()+'&file='+$('#file').val()+'');
		}
		
        
    }
</script>
<?php
//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
    $data_sel = $database->query1("SELECT * FROM email_template_details WHERE id = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
    $_POST = array_merge($_POST,$data);
 ?>
 <script type="text/javascript">
 $('#adminForm').slideDown();
 </script>
 <?php
    }
 }
 if(isset($_POST['errordisplay'])){
	$_POST['email_text']=$_SESSION['email_text'];
 }else{
	 $_POST['email_text']='';
 }
 ?>


	<div class="col-lg-12 col-md-12" id="formdata">
		<div class="row">
		
		
			

			<div class="col-lg-6" >
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Exam</label>
					<div class="col-sm-8">
						<select class="form-control"   name="exam_type" id="exam_type"  onchange="examdatadiv();setState('exampaperdiv','<?php echo SECURE_PATH;?>send_email_template/process.php','getexams=1&exam_type='+$('#exam_type').val()+'');" >
							<option value=''>-Select-</option>
							<?php
							$selexam =$database->query("select * from exam where estatus='1' and id IN (1,2) ");
							
							while($data = mysqli_fetch_array($selexam))
							{
								?>
								<option value="<?php echo $data['id'];?>"   <?php if(isset($_POST['exam_type'])) { if($_POST['exam_type']==$data['id']) { echo 'selected="selected"'; } } ?> ><?php echo $data['exam'];?></option>
							<?php
							}
							?>
						</select>
						<span class="error text-danger" id="exam_type_error" style="display:none;" >Please Select Exam Type</span>
					</div>
				</div>
			</div>
			<?php
			if(isset($_POST['exam_type'])){
				if($_POST['exam_type']=='2'){
					$stylediv="";
				}else{
					$stylediv="style='display:none;'";
				}
			}else{
				$stylediv="style='display:none;'";
			}
			?>
			<div class="col-lg-6" id="subexamtype" <?php echo $stylediv; ?> >
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Mains/Advance</label>
					<div class="col-sm-8">
						<select class="form-control"   name="sub_exam_type" id="sub_exam_type"  onchange="setState('exampaperdiv','<?php echo SECURE_PATH;?>send_email_template/process.php','getexams=1&exam_type='+$('#exam_type').val()+'&sub_exam_type='+$('#sub_exam_type').val()+'');">
							<option value=''>-Select-</option>	
							<option value="1" <?php if(isset($_POST['sub_exam_type'])) { if($_POST['sub_exam_type']=='1') { echo 'selected="selected"'; }echo ''; } ?>   >Mains</option>
							<option value="2" <?php if(isset($_POST['sub_exam_type'])) { if($_POST['sub_exam_type']=='2') { echo 'selected="selected"'; }echo ''; } ?>   >Advance</option>sub_exam_type
						</select>
						<span class="error text-danger" id="sub_exam_type_error" style="display:none;">Please Select Sub Exam Type</span>
					</div>
				</div>
			</div>
			<?php
			if(isset($_POST['exam_type'])){
				if($_POST['exam_type']!=''){
					$con=" and exam_type='".$_POST['exam_type']."'";
				}else{
					$con="";
				}
			}else{
				$con="";
			}
			if(isset($_POST['sub_exam_type'])){
				if($_POST['sub_exam_type']!=''){
					$con1=" and sub_exam_type='".$_POST['sub_exam_type']."'";
				}else{
					$con1="";
				}
			}else{
				$con1="";
			}
			?>
			<div class="col-lg-6" id="exampaperdiv"  >
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Exam Paper</label>
					<div class="col-sm-8">
						<select name="exam_paper" class="form-control exam_paper" id="exam_paper" >
							<option value=''>-select-</option>
							<?php
							$sql=$database->query1("select * from exam_paper where estatus='1' and institution_id='19' and publish='1' ".$con.$con1."");
							while($rowsql=mysqli_fetch_array($sql)){ ?>
								<option value="<?php echo $rowsql['id'];?>"   <?php if(isset($_POST['exam_paper'])) { if($_POST['exam_paper']==$rowsql['id']) { echo 'selected="selected"'; } } ?> ><?php echo $rowsql['exam_name'];?></option>
							<?php }
							?>
						</select>
						<span class="error text-danger" id="exam_paper_error" style="display:none;">Please Select Exam Paper</span>
					</div>
				</div>
			</div>

			<div class="col-lg-6"   >
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Email Template</label>
					<div class="col-sm-8">
						<select name="text" class="form-control email_template_id" id="email_template_id" >
							<option value=''>-select-</option>
							<?php
							$sql=$database->query1("select * from email_template where estatus='1' ");
							while($rowsql=mysqli_fetch_array($sql)){ ?>
								<option value="<?php echo $rowsql['id'];?>"   <?php if(isset($_POST['email_template_id'])) { if($_POST['email_template_id']==$rowsql['id']) { echo 'selected="selected"'; } } ?> ><?php echo $rowsql['email_title'];?></option>
							<?php }
							?>
						</select>
						<span class="error text-danger" id="email_template_id_error" style="display:none;">Please Select Email Template</span>
					</div>
				</div>
			</div>

			<div class="col-lg-6" id="exceldiv" >
				<div class="form-group row" >
									
					<label class="col-sm-4 col-form-label">Excel Upload<span style="color:red;">*</span></label>
						<div class="col-sm-8">
							
							<div id="file-uploader2" style="display:inline">		
								<noscript>			
									<p>Please enable JavaScript to use file uploader.</p>
									<!-- or put a simple form for upload here -->
								</noscript>  
					  
							</div>
							<script> 
							 
								function createUploader(){   
									   
									var uploader = new qq.FileUploader({
										element: document.getElementById('file-uploader2'),
										action: '<?php echo SECURE_PATH;?>frame/js/upload/php.php?upload=file&width=580&height=480',
										debug: true,
										multiple:false
									});           
								}
								
								createUploader();
							</script>
							<input type="hidden" name="file" id="file" value="<?php if(isset($_POST['file'])){ echo $_POST['file'];}?>" />
							<span class="error text-danger" id="file_error" style="display:none;">Please Upload Excel Upload Document</span>
							
							<?php
							  if(isset($_POST['file'])){
								if($_POST['file']!=''){
									?>
								   <a href="<?php echo SECURE_PATH."files/".$_POST['file'];?>" target="_blank" >View Document</a>
									<?php
								}
							  }
							?>
							
						</div>
					</div>
				</div>

			</div>
			<!-- <div class="col-lg-12">
				<a class="radius-20 btn btn-theme px-5" style="cursor:pointer" onClick="setState('adminForm','<?php echo SECURE_PATH;?>send_email_template/process.php','validateForm=1&exam_type='+$('#exam_type').val()+'&sub_exam_type='+$('#sub_exam_type').val()+'&exam_paper='+$('#exam_paper').val()+'&email_template_id='+$('#email_template_id').val()+'&file='+$('#file').val()+'<?php if(isset($_POST['editform'])){ echo '&editform='.$_POST['editform'];}?><?php if(isset($_POST['from_page'])){ echo '&from_page='.$_POST['from_page'];}?><?php if(isset($_POST['page'])){ echo '&page='.$_POST['page'];}?>');">Preview</a>
			</div> -->
			<div class="col-md-12 text-center">
				<a class="btn btn-primary text-white " onclick="validate()">Preview</a>
				</div>
		</div>
		
	</div>
<script type="text/javascript"> 
    function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }
</script>
<?php
unset($_SESSION['error']);
}


//Process and Validate POST data
if(isset($_POST['validateForm'])){
	?>
		<script>
		$('#messageDetails').modal('hide');
	</script>
	<?php
	$_SESSION['error'] = array();
  $post = $session->cleanInput($_POST);
  $id = 'NULL';
	
  if(isset($post['editform'])){
	  $id = $post['editform'];
  }

  $field = 'exam_type';
	if(!$post['exam_type'] || strlen(trim($post['exam_type'])) == 0){
	  $_SESSION['error'][$field] = "* Exam Type name cannot be empty";
	}
	/* $field = 'sub_exam_type';
	if(!$post['sub_exam_type'] || strlen(trim($post['sub_exam_type'])) == 0){
	  $_SESSION['error'][$field] = "* Sub Exam Type cannot be empty";
	}
	 $field = 'exam_paper';
	if(!$post['exam_paper'] || strlen(trim($post['exam_paper'])) == 0){
	  $_SESSION['error'][$field] = "* Exam Paper cannot be empty";
	}*/
	 $field = 'email_template_id';
	if(!$post['email_template_id'] || strlen(trim($post['email_template_id'])) == 0){
	  $_SESSION['error'][$field] = "* Exam Template Id cannot be empty";
	}
	 $field = 'file';
	if(!$post['file'] || strlen(trim($post['file'])) == 0){
	  $_SESSION['error'][$field] = "* File cannot be empty";
	}
	
  //Check if any errors exist
	if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
		//print_r($_SESSION);
	?>
    <script type="text/javascript">
      $('#adminForm').slideDown();
      setState('adminForm','<?php echo SECURE_PATH;?>send_email_template/process.php','addForm=1&errordisplay=1&exam_type=<?php echo $post['exam_type'];?>&sub_exam_type=<?php echo $post['sub_exam_type'];?>&exam_paper=<?php echo $post['exam_paper'];?>&email_template_id=<?php echo $post['email_template_id'];?>&file=<?php echo $post['file'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?>')
    </script>
  <?php
	}
	else{
		$file = "https://admin.rizee.in/files/".$post['file'];
		if($id=='NULL')
		{
		
		$batchid=$session->generateRandStr(6);
		
		$sqld=$database->query1("select id,email_text from email_template where estatus='1' and id='".$post['email_template_id']."'");
		$rowd=mysqli_fetch_array($sqld);
		$result=$database->query1("INSERT email_template_details SET batch_id='".$batchid."',exam_type='".$post['exam_type']."',sub_exam_type='".$post['sub_exam_type']."',exam_paper_id='".$post['exam_paper']."',email_template_id='".$post['email_template_id']."',file='".$post['file']."',estatus='1',timestamp='".time()."'");

		$sqld=$database->query1("select id,email_text from email_template where estatus='1' and id='".$post['email_template_id']."'");
		$rowd=mysqli_fetch_array($sqld);
		$email_text=urldecode($rowd['email_text']);
		$pattern = "/[#]/";
		$components = preg_split($pattern, $email_text);
		if($result){
			if($post['file']!=''){
				$sheet_no=1;
				$mobile12[]='';
				$filename=$post['file'];
				$filename = '../files/'.$filename;
				$inputFileName = $filename;
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
				$objPHPExcel->setActiveSheetIndex($sheet_no-1);
				if(isset($objPHPExcel)){
					$sheetData = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
					$columncount=lettersToNumber($sheetData);
					$from=2;
					$highestColumm = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
					$highestRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
					$to=$highestRow;
					
					$jk=0;
					
					for($i=$from;$i<=$to; $i++){
						
						$qdata=array();
						for($k=0;$k<$columncount;$k++){
							$val=$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($k, 1)->getValue();
							$dataval=$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($k, $i)->getValue();
								$qdata[$k]['column_name']=$val;
								$qdata[$k]['value']=$dataval;
								
							}
							
					
						
						
						$database->query1("INSERT email_template_students SET batch_id='".$batchid."',mobile='".$qdata[0]['value']."',excel_data='".json_encode($qdata)."',email_status=0,estatus='1',timestamp='".time()."'");	
						$jk++;
						}
					}		
				?>
					<div class="col-lg-12 col-md-12">
		<div class="form-group">
			<div class="alert alert-success">
			<i class="fa fa-thumbs-up fa-2x"></i> Email Template Saved Successfully!
			</div>
		</div>
	</div>
	
	  <script type="text/javascript">
	  alert("Email Template Saved Successfully!");
				setStateGet('adminForm','<?php echo SECURE_PATH;?>send_email_template/process.php','addForm=1');
			</script>
					<?php
			}
		}
	  
	 ?>
	  
	 <?php
    }
		else
		{
			$database->query1("update email_template_details SET batchid='".$batchid."',exam_type='".$post['exam_type']."',sub_exam_type='".$post['sub_exam_type']."',exam_paper_id='".$post['exam_paper_id']."',email_template_id='".$post['email_template_id']."',file='".$post['file']."' where id='".$id."'");
		
	  ?>
	  <div class="col-lg-12 col-md-12">
		<div class="form-group">
			<div class="alert alert-success">
			<i class="fa fa-thumbs-up fa-2x"></i> Email Template Updated successfully!
			</div>
		</div>
	</div>
	<script type="text/javascript">
      animateForm('<?php echo SECURE_PATH;?>send_email_template/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
  </script>
	  <?php
    }
  ?>
 
  
 
	<?php
  }
}


//Display bulkreport
if(isset($_GET['tableDisplay'])){
	?>
	
	<?php
	//Pagination code
  $limit=50;
  if(isset($_GET['page']))
  {
    $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
    $page=$_GET['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
  //Search Form
?>
<?php
  $tableName = 'email_template_details';
  $condition = "estatus=1";//"userlevel = '8' OR userlevel = '9' OR userlevel = '7' OR userlevel = '6'";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  //$query_string = $_SERVER['QUERY_STRING'];
  $pagination = $session->showPagination(SECURE_PATH."send_email_template/process.php?tableDisplay=1&",$tableName,$start,$limit,$page,$condition);
  $q = "SELECT * FROM $tableName ".$condition." ";
 $result_sel = $database->query1($q);
  $numres = mysqli_num_rows($result_sel);
  $query = "SELECT * FROM $tableName ".$condition." ";
  
  $data_sel = $database->query1($query);
  if(($start+$limit) > $numres){
	 $onpage = $numres;
	 }
	 else{
	  $onpage = $start+$limit;
	 }
  if($numres > 0){
	  
	?>
 <script type="text/javascript">


  $('#dataTable').DataTable({
    "pageLength": 50,
    "order": [[ 1, 'asc' ]]
    
  });

</script>
	

  	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">Email Template Details</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered dashboard-table" width="100%" cellspacing="0"  id="dataTable">
									<thead>
										<tr>
											<th class="text-left">Sr.No.</th>
											<th class="text-left">Email Template</th>
											<th class="text-left">Batch ID</th>
											<th class="text-left">Exam Type </th>
											<th class="text-left">Exam Paper</th>
											<th class="text-left">Date & Time</th>
											<th class="text-left">Options</th>
										</tr>
									</thead>
									<tbody>
									<?php
									
									$k=1;
									while($value = mysqli_fetch_array($data_sel))
									{
										$sel=$database->query1("select * from email_template where estatus='1' and id='".$value['email_template_id']."'");
										$rowl=mysqli_fetch_array($sel);

										$sel1=$database->query1("select * from exam where estatus='1' and id='".$value['exam_type']."'");
										$rowl1=mysqli_fetch_array($sel1);
										$sel2=$database->query1("select * from exam_paper where estatus='1' and id='".$value['exam_paper_id']."'");
										$rowl2=mysqli_fetch_array($sel2);
										
									?>
										<tr>
										<td class="text-left"><?php echo $k;?></td>
												<td class="text-left" nowrap><?php echo $rowl['email_title'] ?></td>
												<td class="text-left" nowrap><?php echo $value['batch_id'] ?></td>
												<td class="text-left" nowrap><?php echo $rowl1['exam']; ?></td>
												<td class="text-left" nowrap><?php echo $rowl2['exam_name'];?></td>
												<td class="text-left" data-order="<?php echo $value['timestamp'] ?>"><?php echo date('d/m/Y',$value['timestamp']);?></td>
												<td><a data-toggle="modal" data-target="#exampleModal" class="" style="color:green;" onClick="setState('viewuseretails','<?php echo SECURE_PATH;?>send_email_template/ajax1.php','viewdetails=<?php echo $value['id'];?>&batch_id=<?php echo $value['batch_id'] ?>')"><u>Users</u></a></td>
											</tr>
									<?php
									$k++;
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
  </section>
  
	<?php
	}
	else{
	?>
		<div class="text-danger text-center">No Results Found</div>
  <?php
	}

}
if(isset($_REQUEST['getchapter']))
{	
	$_REQUEST['chapter']=explode(",",$_REQUEST['chapter']);
	?>
		
			<label class="col-lg-4 col-form-label">Chapter<span style="color:red;">*</span></label>
			<div class="col-lg-6 ml-5">
				<select name="chapter" class="selectpicker1" id="chapter" multiple data-actions-box="true" data-live-search="true"  onchange="chapterfunction();setState('chapview','<?php echo SECURE_PATH;?>users/process.php','getchaptercview=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'')">
					<!-- <option value="All"   >All</option> -->
					<?php
					//echo "SELECT * FROM `chapter` WHERE estatus='1' AND class IN(".rtrim($_REQUEST['class'],',').")   AND subject IN(".rtrim($_REQUEST['subject'],',').") ";
					$i=1;
					$count=0;
					$row1 = $database->query("SELECT * FROM `chapter` WHERE estatus='1' AND class IN(".rtrim($_REQUEST['class'],',').")   AND subject IN(".rtrim($_REQUEST['subject'],',').")  "); 
					while($data1 = mysqli_fetch_array($row1))
					{
						$userd=$database->query("select * from users where valid='1' and FIND_IN_SET(".$data1['id'].",`chapter`)");
						$rowdcount=mysqli_num_rows($userd);
					  ?>
					<option value="<?php echo $data1['id'];?>"  <?php if(isset($_REQUEST['chapter'])) {if(in_array($data1['id'], $_REQUEST['chapter'])) { echo 'selected="selected"'; } } ?> ><?php echo $data1['chapter'];?></option>
					<?php
					 $i++;
					}
					?>
				</select>
				
				<span class="text-danger"><?php if(isset($_SESSION['error']['chapter'])){ echo $_SESSION['error']['chapter'];}?></span>
			</div>
		   
		
    <?php
}?>
<?php
if(isset($_REQUEST['getchaptercview']))
{	
	$chapter=str_replace("All,","",$_REQUEST['chapter']);
	if($chapter!=''){
		$con=" AND id IN(".rtrim($chapter,',').") ";
	}else{
		$con=" AND id IN(".rtrim($post['chapter'],',').") ";
	}
	
	?>
		
					<?php
					$i=1;
					$count=0;
					$row1 = $database->query("SELECT * FROM `chapter` WHERE estatus='1' ".$con.""); 
					while($data1 = mysqli_fetch_array($row1))
					{
						$userd=$database->query("select * from users where valid='1' and FIND_IN_SET(".$data1['id'].",`chapter`)");
						$rowdcount=mysqli_num_rows($userd);
					 
						  $count=$count+$rowdcount;
					 $i++;
					}
					
					?>
					<a href='#'  style="cursor:pointer;color: #007bff;" title="Print"  data-toggle="modal" data-target="#messageDetails1" onClick="setStateGet('viewDetails1','<?php echo SECURE_PATH;?>users/process1.php','viewDetails1=1&chapter=<?php echo $_REQUEST['chapter']; ?>');"><?php echo $count; ?> Users</a>
		
    <?php
} ?>
		<?php
	if(isset($_REQUEST['getexamset']))
	{	
		$_REQUEST['qset']=explode(",",$_REQUEST['qset']);
	?>
		<label for="inputTopi" class="label">Set ID</label>
		<select class="form-control selectpicker4" multiple data-live-search="true" name="qset" value=""  id="qset"  >
			<?php
			
			$sel=$database->query("select * from previous_questions where estatus='1' and year in (".$_POST['year'].")  ORDER by id DESC");
			while($row=mysqli_fetch_array($sel)){
				$sel1=$database->query("select * from previous_sets where estatus='1' and pid='".$row['id']."'");
				while($row1=mysqli_fetch_array($sel1)){
				?>
					
					<option value="<?php echo $row1['id'];?>"  <?php if(isset($_REQUEST['qset'])) {if(in_array($row1['id'], $_REQUEST['qset'])) { echo 'selected="selected"'; } } ?> ><?php echo $row1['qset'];?></option>
			<?php
				}
			}
			?>
	</select>
    <?php
}?>

<script>
function selectfunction(){
	
	var userlevel=$('#userlevel').val();
	var ppaper=$('#ppaper').val();
	if(userlevel=='6' || userlevel=='7' || userlevel=='8' || userlevel=='2' || userlevel=='3'){
		$('.subjectdiv').show();
		if(userlevel=='7'){
			$('.contentd').show();
			$('.contentd1').show();
			$('.previous1').show();
			if(ppaper=='1'){
					$('.previous2').show();
				}else{
					$('.previous2').hide();
				}
			$('.nochapter1').show();
			$('.update_chapter1').hide();
			$('.lecturerdashboard1').hide();
			$('.questionsearch1').hide();
			$('.questionanalysis1').hide();
			$('.previous2').hide();
		}else if(userlevel=='8'){
			$('.contentd').show();
			$('.contentd1').hide();
			$('.previous1').show();
			$('.update_chapter1').show();
			if(ppaper=='1'){
					$('.previous2').show();
				}else{
					$('.previous2').hide();
				}
			$('.nochapter1').hide();
			$('.lecturerdashboard1').hide();
			$('.questionsearch1').hide();
			$('.questionanalysis1').hide();
		
		}else if(userlevel=='3'){
			$('.contentd').show();
			$('.contentd1').hide();
			$('.previous1').show();
			$('.update_chapter1').show();
			if(ppaper=='1'){
					$('.previous2').show();
				}else{
					$('.previous2').hide();
				}
			$('.nochapter1').hide();
			$('.lecturerdashboard1').hide();
			$('.questionsearch1').show();
			$('.questionanalysis1').hide();
		
		}else if(userlevel=='2'){
			$('.contentd').show();
			$('.contentd1').hide();
			$('.previous1').show();
			$('.update_chapter1').show();
			$('.question_wchapter1').show();
				if(ppaper=='1'){
					$('.previous2').show();
				}else{
					$('.previous2').hide();
				}
			$('.nochapter1').hide();
			$('.lecturerdashboard1').show();
			$('.questionsearch1').show();
			$('.questionanalysis1').show();
		
		}else{
			$('.contentd').hide();
			$('.previous1').hide();
			$('.previous2').hide();
			$('.contentd1').hide();
			$('.nochapter1').hide();
			$('.update_chapter1').hide();
			$('.lecturerdashboard1').hide();
			$('.questionsearch1').hide();
			$('.questionanalysis1').hide();
		}

	}else{
		$('.subjectdiv').hide();
        $('.contentd').hide();
		$('.previous1').hide();
		$('.previous2').hide();
		$('.nochapter1').hide();
		$('.update_chapter1').hide();
		$('.lecturerdashboard1').hide();
		$('.questionsearch1').hide();
		$('.questionanalysis1').hide();
		

	}
}
</script>

	<script type="text/javascript">


function getFields1()
{
    var ass='';
    $('.class3').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#class').val(ass);
	});
        
        

    
}
</script>
<script type="text/javascript">
	function getFields5()
    {
    	
    var ass='';
    $('.classn').each(function(element) {
    if($(this).is(':checked')) {
     $('#'+$(this).attr('id')).val(1);
      ass+=$(this).val()+",";
	 
    }else{
      $('#'+$(this).attr('id')).val(0);
      ass+=$(this).val()+",";
	 
    }
//alert(ass);

  });
        
        

    
}
	function getFields6()
    {
    	var userlevel=$('#userlevel').val();
    $('.ppaper').each(function(element) {
    if($(this).is(':checked')) {
     $('#'+$(this).attr('id')).val(1);
	 if(userlevel=='8' || userlevel=='7' || userlevel=='3' || userlevel=='2'){
		$('.previous2').show();
	 }else{
		$('.previous2').hide();
	 }

    }else{
      $('#'+$(this).attr('id')).val(0);
     if(userlevel=='8' || userlevel=='7' || userlevel=='3' || userlevel=='2'){
		$('.previous2').hide();
	  }else{
		$('.previous2').hide();
	 }
    }
//alert(ass);

  });
        
        

    
}
	function getFields7()
    {
    	var userlevel=$('#userlevel').val();
    $('.attributes').each(function(element) {
    if($(this).is(':checked')) {
     $('#'+$(this).attr('id')).val(1);
	 

    }else{
      $('#'+$(this).attr('id')).val(0);
    
    }
//alert(ass);

  });
        
        

    
}

	function getFields8()
    {
		var userlevel=$('#userlevel').val();
		$('.update_chapter').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }

	function getFields9()
    {
		$('.lecturerdashboard').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }

	function getFields10()
    {
		$('.questionsearch').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	function getFields11()
    {
		$('.questionanalysis').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	function getFields12()
    {
		$('.nochapter').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	function getFields13()
    {
		$('.ppaper_analysis').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	function getFields14()
    {
		$('.question_forum').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	function getFields15()
    {
		$('.institute_epaper').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	
	function getFields16()
    {
		$('.attributeoverview').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	function getFields17()
    {
		$('.fitment_report').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	function getFields18()
    {
		$('.exam_paper_generation').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
    </script>
<script>
 $(".selectpicker1").selectpicker('refresh');
 $(".selectpicker2").selectpicker('refresh');
  $(".selectpicker3").selectpicker('refresh');
 $(".selectpicker4").selectpicker('refresh');
</script>
<script>
$('#chapter').on('change', function(){
		var thisObj = $(this);
    var isAllSelected = thisObj.find('option[value="All"]').prop('selected');
    var lastAllSelected = $(this).data('all');
    var selectedOptions = (thisObj.val())?thisObj.val():[];
    	var allOptionsLength = thisObj.find('option[value!="All"]').length;
     
     console.log(selectedOptions);
      var selectedOptionsLength = selectedOptions.length;
     
    if(isAllSelected == lastAllSelected){
    
    if($.inArray("All", selectedOptions) >= 0){
    	selectedOptionsLength -= 1;      
    }
        	
      if(allOptionsLength <= selectedOptionsLength){
      
      thisObj.find('option[value="All"]').prop('selected', true);
      isAllSelected = true;
      }else{       
      	thisObj.find('option[value="All"]').prop('selected', false);
         isAllSelected = false;
      }
      
    }else{   		
    	thisObj.find('option').prop('selected', isAllSelected);
    }
   
		$(this).data('all', isAllSelected);
		setStateGet('chapview','<?php echo SECURE_PATH;?>users/process.php','getchaptercview=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'')
});

	function examdatadiv(){
		var exam_type=$("#exam_type").val();
		if(exam_type==2){
			$("#subexamtype").show();
		}else{
			$("#subexamtype").hide();
		}
	}
</script>
<?php
if(isset($_POST['exam_type'])){
	if($_POST['exam_type']!=''){
		$con=" and exam_type='".$_POST['exam_type']."'";
	}else{
		$con="";
	}
	if(isset($_POST['sub_exam_type'])){
		if($_POST['sub_exam_type']!=''){
			$con1=" and sub_exam_type='".$_POST['sub_exam_type']."'";
		}else{
			$con1="";
		}
	}else{
		$con1="";
	}
	
	?>
		<div class="form-group row">
			<label class="col-sm-4 col-form-label">Exam Paper</label>
			<div class="col-sm-8">
				<select name="exam_paper" class="form-control exam_paper" id="exam_paper" >
					<option value=''>-select-</option>
					<?php
					$sql=$database->query1("select * from exam_paper where estatus='1' and institution_id='19' and publish='1'".$con.$con1."");
					while($rowsql=mysqli_fetch_array($sql)){ ?>
						<option value="<?php echo $rowsql['id'];?>"   <?php if(isset($_POST['exam_paper'])) { if($_POST['exam_paper']==$rowsql['id']) { echo 'selected="selected"'; } } ?> ><?php echo $rowsql['exam_name'];?></option>
					<?php }
					?>
				</select>
				<span class="error text-danger"><?php if(isset($_SESSION['error']['exam_paper'])){ echo $_SESSION['error']['exam_paper'];}?></span>
			</div>
		</div>
	<?php
}
?>
 <?php
 function lettersToNumber($letters){
    $alphabet = range('A', 'Z');
    $number = 0;

    foreach(str_split(strrev($letters)) as $key=>$char){
        $number = $number + (array_search($char,$alphabet)+1)*pow(count($alphabet),$key);
    }
    return $number;
}
?>