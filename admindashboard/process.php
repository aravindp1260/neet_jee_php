<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>indexout.php','loginForm=1');
	</script>
<?php
}
?>


	<?php
	if(isset($_REQUEST['addForm'])){
		?>
	<script>
		$('#exam').selectpicker();
		 $('#verifydiv').click(function(e) {  
		  //alert(1);
		});
	</script>	
		

                    <!-- Dataentry-Overview Start -->
			<?php $_REQUEST['exam1']=explode(",",$_REQUEST['exam']); ?>
			<div class="form-group mb-1 text-right">
				<select   class="selectpicker" id="exam" multiple  onchange="setStateGet('adminForm','<?php echo SECURE_PATH;?>admindashboard/process.php','addForm=1&getdataentry=1&exam='+$('#exam').val()+'')" >
				<?php $exam=$database->query("select * from exam where estatus='1'");
					while($rowexam=mysqli_fetch_array($exam)){ ?>
						
						<option value="<?php echo $rowexam['id'];?>" <?php if(isset($_REQUEST['exam1'])) { if(in_array($rowexam['id'], $_REQUEST['exam1'])) { echo 'selected="selected"'; } } ?>  ><?php echo $rowexam['exam'];?></option>
					<?php } ?>
				</select>
			</div>
			<div class="card data-entry-overview border-0 shadow-sm my-3 table-responsive">
				 <div class="card-header" id="dataentryoverview">
					<a href="" class="d-flex justify-content-between align-items-center"
						 data-toggle="collapse" data-target="#dataentrydiv" aria-expanded="true"
                                    aria-controls="dataentrydiv" >
						<h5 class="card-title mb-0"><b>Data Entry Overview</b></h5>
						<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
					</a>
				</div>
				<div class="collapse show" id="dataentrydiv"  aria-labelledby="dataentryoverview" >
					<div class="card-body" >
						
						<div class="d-flex justify-content-between dataoverviewtable">
							<?php


							if(isset($_REQUEST['getdataentry'])){
								if($_REQUEST['exam']!=''){
									$con=" AND id IN (".$_REQUEST['exam'].")";
									$connd=" AND exam LIKE '%".$_REQUEST['exam']."%'";
								}else{
									$con="";
								}
								$sql=$database->query("select * from class where estatus='1'");
								$i=1;
								while($rowl=mysqli_fetch_array($sql)){ 
									$dataetotal=$database->query("select class_count from syllabus_totals_exam where   class ='".$rowl['id']."'".$connd." limit 0,1 ");
									//echo "select count(*) from createquestion where   estatus='1' and class LIKE '%".$rowl['id']."%'".$connd."";
									$rowdataetotal=mysqli_fetch_array($dataetotal);
									if($i=='1'){
										$style="mr-1";
									}else{
										$style="";
									}
									
								?>
								<div class="col-lg-6 col-md-6 col-sm-12 my-2">
									<table class="table table-bordered <?php echo $style; ?> mb-0 bg-white">
										<thead>
											<tr>
												<th class="bg-gray-color" colspan="4">
													<h5><b>Class <?php echo $rowl['class']; ?></b></h5>
												</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th colspan="4">
													<div class="d-flex justify-content-between">
														<h6 class="text-dark"><b>Total Questions</b></h6>
														<h5><?php  if($rowdataetotal!=''){ echo $rowdataetotal[0]; } else{ echo '0'; } ?></h5>
													</div>
												</th>
											</tr>
											<?php
											$sqll1=$database->query("select * from exam where estatus='1'".$con."");
											while($rowll1=mysqli_fetch_array($sqll1)){ 
												$dataetotalneet=$database->query("select exam_count from syllabus_totals_exam where  class ='".$rowl['id']."' and exam = '".$rowll1['id']."' limit 0,1 ");
												$rowdataetotalneet=mysqli_fetch_array($dataetotalneet);
													$colspan="4";
												
												?>
												<tr> 
													
													<th class="bg-gray-color" colspan="<?php echo $colspan; ?>">
														<div class="d-flex justify-content-between">
															<h6 class="text-dark"><b><?php echo $rowll1['exam']; ?></b></h6>
															<h5><?php  if($rowdataetotalneet!=''){ echo $rowdataetotalneet[0]; } else{ echo '0'; } ?></h5>
														</div>
													</th>
												</tr>
												<tr>
												<td class="d-flex justify-content-between p-0">
											<?php
												/*if($rowll1['id']=='1'){
													
													$data3 = $database->query("select * from subject where estatus=1 and id!='4'");
												}else if($rowll1['id']=='2'){
													$data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
												}else  if($rowll1['id']=='1,2'){
													$data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
												}else{
													$data3 = $database->query("select * from subject where estatus=1");
												}*/
												$data3 = $database->query("select * from subject where estatus=1 and id in (".$rowll1['subjects'].") ");
												while($row3=mysqli_fetch_array($data3)){ 
													$dataetotalneet=$database->query("select subject_count from syllabus_totals_exam where    class ='".$rowl['id']."' and exam LIKE '%".$rowll1['id']."%' and subject='".$row3['id']."' limit 0,1 ");
													$rowdataetotal=mysqli_fetch_array($dataetotalneet);
												?>
													
														
															<div class="item-td"><h6><?php echo $row3['subject']; ?></h6></div>
													
													
												<?php
												}
												echo '</td>';
												echo '</tr>';
												echo '<tr>';
													echo '<td class="d-flex justify-content-between p-0">';
												/*if($rowll1['id']=='1'){
													
													$data4 = $database->query("select * from subject where estatus=1 and id!='4'");
												}else if($rowll1['id']=='2'){
													$data4 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
												}else  if($rowll1['id']=='1,2'){
													$data4 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
												}else{
													$data4 = $database->query("select * from subject where estatus=1");
												}*/
												$data4 = $database->query("select * from subject where estatus=1 and  id in (".$rowll1['subjects'].") ");
												$rowcount=mysqli_num_rows($data4);
												while($row4=mysqli_fetch_array($data4)){
													$dataetotalneet=$database->query("select subject_count from syllabus_totals_exam where    class='".$rowl['id']."' and exam ='".$rowll1['id']."' and subject='".$row4['id']."' limit 0,1 ");
													$rowdataetotal=mysqli_fetch_array($dataetotalneet);
												?>
													
														<div class="item-td">
															<h6><?php  if($rowdataetotal!=''){ echo $rowdataetotal[0]; } else{ echo '0'; } ?></h6>
														</div>
													
												<?php
												}
												echo '</td>';
												echo '</tr>';
											}
											?>
										</tbody>
									</table>
								</div>
								<?php
									$i++;
								}
							}else{
								$sql=$database->query("select * from class where estatus='1'");
								$i=1;
								while($rowl=mysqli_fetch_array($sql)){ 
									$dataetotal=$database->query("select class_count from syllabus_totals_exam where class ='".$rowl['id']."'");
									$rowdataetotal=mysqli_fetch_array($dataetotal);
									if($i=='1'){
										$style="mr-1";
									}else{
										$style="";
									}
								?>
							<div class="col-lg-6 col-md-6 col-sm-12 my-2">
								<table class="table table-bordered <?php echo $style; ?> mb-0 bg-white">
									<thead>
										<tr>
											<th class="bg-gray-color" colspan="4">
												<h5><b>Class <?php echo $rowl['class']; ?></b></h5>
											</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th colspan="4">
												<div class="d-flex justify-content-between">
													<h6 class="text-dark"><b>Total Questions</b></h6>
													<h5><?php  if($rowdataetotal!=''){ echo $rowdataetotal[0]; } else{ echo '0'; } ?></h5>
												</div>
											</th>
										</tr>
										<?php
										$sqll1=$database->query("select * from exam where estatus='1'");
										while($rowll1=mysqli_fetch_array($sqll1)){ 
											$dataetotalneet=$database->query("select exam_count from syllabus_totals_exam where class ='".$rowl['id']."' and exam='".$rowll1['id']."' limit 0,1 ");
											$rowdataetotalneet=mysqli_fetch_array($dataetotalneet);
												$colspan="4";
											
											?>
											<tr> 
												
												<th class="bg-gray-color" colspan="<?php echo $colspan; ?>">
													<div class="d-flex justify-content-between">
														<h6 class="text-dark"><b><?php echo $rowll1['exam']; ?></b></h6>
														<h5><?php  if($rowdataetotalneet!=''){ echo $rowdataetotalneet[0]; } else{ echo '0'; } ?></h5>
													</div>
												</th>
											</tr>
											<tr>
											<td class="d-flex justify-content-between p-0">
										<?php
											/*if($rowll1['id']=='1'){
												
												$data3 = $database->query("select * from subject where estatus=1 and id!='4'");
											}else if($rowll1['id']=='2'){
												$data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
											}else  if($rowll1['id']=='1,2'){
												$data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
											}else{
												$data3 = $database->query("select * from subject where estatus=1");
											}*/
											$data3 = $database->query("select * from subject where estatus=1 and id in (".$rowll1['subjects'].") ");
											while($row3=mysqli_fetch_array($data3)){ 
												$dataetotalneet=$database->query("select subject_count from syllabus_totals_exam where class ='".$rowl['id']."' and exam='".$rowll1['id']."' and subject='".$row3['id']."' limit 0,1");
												$rowdataetotal=mysqli_fetch_array($dataetotalneet);
											?>
												
													
														<div class="item-td"><h6><?php echo $row3['subject']; ?></h6></div>
												
												
											<?php
											}
											echo '</td>';
											echo '</tr>';
											echo '<tr>';
												echo '<td class="d-flex justify-content-between p-0">';
											/*if($rowll1['id']=='1'){
												
												$data4 = $database->query("select * from subject where estatus=1 and id!='4'");
											}else if($rowll1['id']=='2'){
												$data4 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
											}else  if($rowll1['id']=='1,2'){
												$data4 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
											}else{
												$data4 = $database->query("select * from subject where estatus=1");
											}*/
											$data4 = $database->query("select * from subject where estatus=1 and  id in (".$rowll1['subjects'].") ");
											$rowcount=mysqli_num_rows($data4);
											while($row4=mysqli_fetch_array($data4)){
												$dataetotalneet=$database->query("select subject_count from syllabus_totals_exam where class ='".$rowl['id']."' and exam='".$rowll1['id']."' and subject='".$row4['id']."'  order by topic desc limit 0,1");
												$rowdataetotal=mysqli_fetch_array($dataetotalneet);
											?>
												
													<div class="item-td">
														<!-- <h6><?php  if($rowdataetotal!=''){ echo $rowdataetotal[0]; } else{ echo '0'; } ?></h6> -->
														<h6><?php  if($rowdataetotal!=''){ echo $rowdataetotal[0]; } else{ echo '0'; } ?></h6>
													</div>
												
											<?php
											}
											echo '</td>';
											echo '</tr>';
										}
										?>
									</tbody>
								</table>
							</div>
							<?php
											$i++;
							}

							}
							?>
						</div>
						
					</div>
				</div>
			</div>
			
			<!-- Dataentry-Overview End -->

			<!-- verification-Overview Start -->





			<div class="card sverification-overview border-0 shadow-sm my-3 table-responsive" id="sverificationOverview">
				
				<div class="card-header" id="sverification">
				   <a href="" class="d-flex justify-content-between align-items-center"
					  data-toggle="collapse" data-target="#sverifydiv" aria-expanded="true"
					  aria-controls="sverifydiv"  onclick="setStateGet('sverificationOverview','<?php echo SECURE_PATH;?>admindashboard/process9.php','sverificationOverview=1')">
					   <h5 class="card-title mb-0"><b>Subject Wise Overview</b></h5>
					   <h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
				   </a>
			   </div>
   
		   </div>

		 <div class="card verification-overview border-0 shadow-sm my-3 table-responsive" id="verificationOverview">
				
               

				    <div class="card-header" id="verification">
						<a href="" class="d-flex justify-content-between align-items-center"
						   data-toggle="collapse" data-target="#verifydiv" aria-expanded="true"
						   aria-controls="verifydiv"  onclick="setStateGet('verificationOverview','<?php echo SECURE_PATH;?>admindashboard/process1.php','verificationOverview=1&exam='+$('#exam').val()+'')">
							<h5 class="card-title mb-0"><b>Verification Overview</b></h5>
							<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
						</a>
					</div>

			</div>
			
			
			 <div class="card timeline-overview border-0 shadow-sm my-3 table-responsive" id="timelineOverview">
               

				  <div class="card-header" id="timelineview">
						<a href="" class="d-flex justify-content-between align-items-center"
						   data-toggle="collapse" data-target="#timelinediv" aria-expanded="true"
						   aria-controls="timelinediv" onclick="setStateGet('timelineOverview','<?php echo SECURE_PATH;?>admindashboard/process2.php','timelineOverview=1&exam='+$('#exam').val()+'')" >
							<h5 class="card-title mb-0"><b>Timeline Overview</b></h5>
							<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
						</a>
					</div>

			</div>
			

			
			<div class="card attribute-overview border-0 shadow-sm my-3 table-responsive" id="attributesOverveiw">
               
				 <div class="card-header" id="attributeoverview">
					<a href="" class="d-flex justify-content-between align-items-center"
					   data-toggle="collapse" data-target="#attributediv" aria-expanded="true"
					   aria-controls="attributediv" onclick="setStateGet('attributesOverveiw','<?php echo SECURE_PATH;?>admindashboard/process3.php','attributesOverveiw=1&exam='+$('#exam').val())" >
						<h5 class="card-title mb-0"><b>Attribute Overview</b></h5>
						<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
					</a>
				</div>

			</div>
			
			<div class="datewise-navtabs mb-4" id="topicOverview">
                
				<div class="card-header" id="subjectoverview">
					<a href="" class="d-flex justify-content-between align-items-center"
					   data-toggle="collapse" data-target="#subjectchapdiv" aria-expanded="true"
					   aria-controls="subjectchapdiv" onclick="setStateGet('topicOverview','<?php echo SECURE_PATH;?>admindashboard/process4.php','topicOverview=1&exam='+$('#exam').val())" >
						<h5 class="card-title mb-0"><b>Subject ,Chapter & Topic wise Overview</b></h5>
						<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
					</a>
				</div>
				

            </div>
			 
			 <div class="card timeline-overview border-0 shadow-sm my-3 table-responsive" id="customOverview">
               
				<div class="card-header" id="customoverview">
					<a href="" class="d-flex justify-content-between align-items-center"
					   data-toggle="collapse" data-target="#customdiv" aria-expanded="true"
					   aria-controls="customdiv" onclick="setStateGet('customOverview','<?php echo SECURE_PATH;?>admindashboard/process5.php','customOverview=1&exam='+$('#exam').val()+'')" >
						<h5 class="card-title mb-0"><b>Custom Overview</b></h5>
						<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
					</a>
				</div>
			</div>
			<div class="card attribute-overview border-0 shadow-sm my-3 table-responsive" id="previousPaper">
                
				<div class="card-header" id="previouspaper">
					<a href="" class="d-flex justify-content-between align-items-center"
					   data-toggle="collapse" data-target="#previousdiv" aria-expanded="true"
					   aria-controls="previousdiv" onclick="setStateGet('previousPaper','<?php echo SECURE_PATH;?>admindashboard/process7.php','previousAnalysis=1&exam='+$('#exam').val()+'')" >
						<h5 class="card-title mb-0"><b>Previous Paper Analysis</b></h5>
						<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
					</a>
				</div>
			</div> 

			 <div class="card attribute-overview border-0 shadow-sm my-3 table-responsive" id="pendingquestion">
               
				<div class="card-header" id="pendingquestion">
					<a href="" class="d-flex justify-content-between align-items-center"
					   data-toggle="collapse" data-target="#pendingdiv" aria-expanded="true"
					   aria-controls="pendingdiv" onclick="setStateGet('pendingquestion','<?php echo SECURE_PATH;?>admindashboard/process8.php','pendingquestion=1&exam='+$('#exam').val()+'')" >
						<h5 class="card-title mb-0"><b>Pending Question Range -Chapter Count</b></h5>
						<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
					</a>
				</div>
			</div>
			<!--Custom Overview End-->
		<?php
		 
	}





//Display bulkreport
if(isset($_GET['tableDisplay'])){
	
	

}
?>
<script>
 $(".selectpicker1").selectpicker('refresh');
  $(".selectpicker3").selectpicker('refresh');
  $(".selectpicker4").selectpicker('refresh');
   $(".selectpicker5").selectpicker('refresh');
</script>