<?php

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");
/*define("DB_SERVER","localhost");
define("DB_USER","root");
define("DB_PWD","");
define("DB_NAME","qsbank");*/
$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = "Without topic Linkage Questions report".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";

header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
						<th style="text-align:center;">Sr.No.</th>
						<th style="text-align:center;">Question Id</th>
						<th style="text-align:center;">Question Type</th>
						<th style="text-align:center;">Subject</th>
						<th style="text-align:center;">Chapter</th>
						<th style="text-align:center;">Topic</th>
							
						</tr>
                        <?php
                             $k=1;
                            $sel=query("SELECT id,class,subject,chapter,topic,inputquestion FROM createquestion where estatus='1' and find_in_set(1,inputquestion)>0  and subject in (1,2) order by subject,chapter asc  ");
                            while($row = mysqli_fetch_array($sel)){
                                $selsubject=query("SELECT *  FROM subject WHERE estatus=1 and id='".$row['subject']."' ORDER BY id ASC");
								$rowsubject = mysqli_fetch_array($selsubject);
							
								


								$input='';
								$selinput=query("SELECT *  FROM questiontype WHERE estatus=1 and id in (".$row['inputquestion'].") ORDER BY id ASC");
								while($rowinput = mysqli_fetch_array($selinput)){
									$input.=$rowinput['questiontype'].",";
								}
								
								$selchapter=query("SELECT *  FROM chapter WHERE estatus=1 and id in (".$row['chapter'].") ORDER BY id ASC");
								while($rowchapter = mysqli_fetch_array($selchapter)){
									$seltopic=query("SELECT *  FROM topic WHERE estatus=1 and find_in_set(".$rowchapter['id'].",chapter)>0 and id in (".$row['topic'].") ORDER BY id ASC");
									$rowctopic=mysqli_num_rows($seltopic);
									if($rowctopic>0){
										while($rowtopic = mysqli_fetch_array($seltopic)){
										echo "<tr>";
											?>	
											
												<td><?php echo $k;?></td>
												<td><?php echo $row['id'];?></td>
												<td><?php echo rtrim($input,",") ?></td>
												<td><?php echo $rowsubject['subject'];?></td>
												
												<td><?php echo $rowchapter['chapter']; ?></td>
												<td><?php echo $rowtopic['topic']; ?></td>
												
												
												
											<?php
												
											echo "</tr>";
											$k++;
										}
									}else{
										echo "<tr>";
											?>	
											
												<td><?php echo $k;?></td>
												<td><?php echo $row['id'];?></td>
												<td><?php echo rtrim($input,",") ?></td>
												<td><?php echo $rowsubject['subject'];?></td>
												
												<td><?php echo $rowchapter['chapter']; ?></td>
												<td style="color:Red">No Topic</td>
												
												
												
											<?php
												
											echo "</tr>";
											$k++;
									}
								}	

									
									
							
                                                            
									           
                                       
                            }

                        ?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>