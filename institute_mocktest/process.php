<?php
ini_set('display_errors','0');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');

ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}

//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
    $data_sel = $database->query1("SELECT * FROM institute_mocktests WHERE id = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
    $_POST = array_merge($_POST,$data);
	
 ?>
 <script type="text/javascript">
 $('#adminForm').slideDown();
 </script>
 
 <?php
    }
 }
 ?>
	
	<script>
		$(function () {
			$('.datepicker1,.datepicker2,.datepicker3').datetimepicker({
			   format: 'DD-MM-YYYY'
			   
			});
		});

	</script>
	<!-- <script>
		
		editor = com.wiris.jsEditor.JsEditor.newInstance({'language': 'en'});
	                 editor.insertInto(document.getElementById('editorContainer'));
				 width:300;
				 height:500;
	</script> -->
	
  <div class="col-lg-12 col-md-12">
 
 	<div class="row">
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Institute<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<select name="institute_id" id="institute_id" class="form-control">
						<option value=''>-Select-</option>
						<?php
						$sql=$database->query("select * from institute where estatus='1'");
						while($row=mysqli_fetch_array($sql)){
						?>
							<option value="<?php echo $row['id'];?>" <?php if(isset($_POST['institute_id'])) { if($row['id']==$_POST['institute_id']) { echo 'selected="selected"'; } } ?>   ><?php echo $row['institute_name'];?></option>
						<?php
						}
						?>
					</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['institute_name'])){ echo $_SESSION['error']['institute_name'];}?></span>
				</div>
			</div>
		</div>
	
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Exam<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<select name="exam_id" class="form-control" id="exam_id"   onchange="examdatatype();setStateGet('mocktestdiv','<?php echo SECURE_PATH;?>institute_mocktest/process.php','getmocktest=1&exam_id='+$('#exam_id').val()+'')">
						<option value=''>-Select-</option>
						<?php
						$row1 = $database->query("SELECT * FROM exam WHERE estatus='1' "); 
						while($data1 = mysqli_fetch_array($row1))
						{
						  ?>
						<option value="<?php echo $data1['id'];?>" <?php if(isset($_POST['exam_id'])) { if($data1['id']==$_POST['exam_id']) { echo 'selected="selected"'; } } ?>   ><?php echo $data1['exam'];?></option>
						<?php
						}
						?>
					</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['exam_id'])){ echo $_SESSION['error']['exam_id'];}?></span>
				</div>
			</div>
		</div>
		<?php
		if(isset($_POST['exam_id'])){
			if($_POST['exam_id']=='2'){
				$stylet='';
				if(isset($_POST['sub_exam_type'])){
					if($_POST['sub_exam_type']=='1'){
						$stylett='';
					}else{
						$stylett='style="display:none;"';
					}
				}else{
					$stylett='style="display:none;"';
				}

			}else{
				$stylet='style="display:none;"';
				$stylett='style="display:none;"';
			}
		}else{
			$stylet='style="display:none;"';
			$stylett='style="display:none;"';
		}
		?>
		<div class="col-lg-6"   id="examdivdata" <?php echo $stylet; ?>>
			<div class="form-group row"  >
				<label class="col-sm-4 col-form-label">Sub Exam Type<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<div class="form-group">
						<select  name="sub_exam_type" id="sub_exam_type" class="form-control  sub_exam_type"  onchange="setStateGet('mocktestdiv','<?php echo SECURE_PATH;?>institute_mocktest/process.php','getmocktest=1&exam_id='+$('#exam_id').val()+'&sub_exam_type='+$('#sub_exam_type').val()+'')">
							<option value=''>-Select-</option>	
							<option value="1" <?php if(isset($_POST['sub_exam_type'])) { if($_POST['sub_exam_type']=='1') { echo 'selected="selected"'; }echo ''; } ?>   >Mains</option>
							<option value="2" <?php if(isset($_POST['sub_exam_type'])) { if($_POST['sub_exam_type']=='2') { echo 'selected="selected"'; }echo ''; } ?>   >Advance</option>sub_exam_type
						</select>
						
						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['sub_exam_type'])){ echo $_SESSION['error']['sub_exam_type'];}?></span>
					</div>
				</div>
			</div>
		
		
		</div>
		<div class="col-lg-6" id="mocktestdiv">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Mocktest Exam<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<select name="mocktest_id" class="form-control selectpicker2" id="mocktest_id"  onchange="examdatatype();" multiple data-actions-box="true" data-live-search="true" >
						
						<?php
						$row1 = $database->query1("SELECT * FROM exam_paper WHERE estatus='1' and institution_id='19' and type='test_series'"); 
						while($data1 = mysqli_fetch_array($row1))
						{
						  ?>
						<option value="<?php echo $data1['id'];?>" <?php if(isset($_POST['mocktest_id'])) { if($data1['id']==$_POST['mocktest_id']) { echo 'selected="selected"'; } } ?>   ><?php echo $data1['exam_name'];?></option>
						<?php
						}
						?>
					</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['mocktest_id'])){ echo $_SESSION['error']['mocktest_id'];}?></span>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group row">
				<a class="radius-20 btn btn-theme px-5" onClick="setState('adminForm','<?php echo SECURE_PATH;?>institute_mocktest/process.php','validateForm=1&institute_id='+$('#institute_id').val()+'&exam_id='+$('#exam_id').val()+'&sub_exam_type='+$('#sub_exam_type').val()+'&mocktest_id='+$('#mocktest_id').val()+'&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>')"> Save</a>
			</div>
		</div>
	</div>
	
	
	
</div>

<?php
unset($_SESSION['error']);
}
?>

<?php
if(isset($_POST['validateForm'])){
	
	$_SESSION['error'] = array();
	
	$post = $session->cleanInput($_POST);

	 	$id = 'NULL';
		  	if(isset($post['editform'])){
	  $id = $post['editform'];

	}
	$field = 'institute_id';
	if(!$post['institute_id'] || strlen(trim($post['institute_id'])) == 0){
	  $_SESSION['error'][$field] = "* Institute  cannot be empty";
	}
	
	 $field = 'exam_id';
	if(!$post['exam_id'] || strlen(trim($post['exam_id'])) == 0){
	  $_SESSION['error'][$field] = "* Exam cannot be empty";
	}
	 $field = 'mocktest_id';
	if(!$post['mocktest_id'] || strlen(trim($post['mocktest_id'])) == 0){
	  $_SESSION['error'][$field] = "* Mocktest Exam cannot be empty";
	}
	if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
		print_r($_SESSION);
	?>
    <script type="text/javascript">
      $('#adminForm').slideDown();
      setState('adminForm','<?php echo SECURE_PATH;?>institute_mocktest/process.php','addForm=1&institute_id=<?php echo $post['institute_id'];?>&exam_id=<?php echo $post['exam_id'];?>&sub_exam_type=<?php echo $post['sub_exam_type'];?>&mocktest_id=<?php echo $post['mocktest_id'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?><?php if(isset($_POST['from_page'])){ echo '&from_page='.$post['from_page'];}?>')
    </script>
  <?php
	}
	else{
		
		if($id=='NULL')
		{
			$mocktest_counts=explode(",",$post['mocktest_id']);
			if(count($mocktest_counts)>0){
				foreach($mocktest_counts as $mocktestdata){
					$sql=$database->query1("select * from exam_paper where estatus='1' and id='".$mocktestdata."'");
					$row=mysqli_fetch_array($sql);
					$sql1=$database->query1("select * from institute_mocktests where estatus='1' and institute_id='".$post['institute_id']."' and mocktest_id='".$mocktestdata."'");
					$row1=mysqli_num_rows($sql1);
					if($row1==0){
					
						$result=$database->query1('insert institute_mocktests set institute_id="'.$post['institute_id'].'",exam_id="'.$post['exam_id'].'",sub_exam_type="'.$post['sub_exam_type'].'",mocktest_id="'.$mocktestdata.'",class_id="'.$row['class_id'].'",mains_2021="'.$row['mains_2021'].'",username="'.$session->username.'",estatus="1",timestamp="'.time().'"');
					}else{
						$result=$database->query1('update institute_mocktests set institute_id="'.$post['institute_id'].'",exam_id="'.$post['exam_id'].'",sub_exam_type="'.$post['sub_exam_type'].'",mocktest_id="'.$mocktestdata.'",class_id="'.$row['class_id'].'",mains_2021="'.$row['mains_2021'].'" where estatus="1" and institute_id="'.$post['institute_id'].'" and mocktest_id="'.$mocktestdata.'"');
					}

				
				}
				?>
					
					<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Institute Updation  Successfully!</div>
					
						<script type="text/javascript">
					
						animateForm('<?php echo SECURE_PATH;?>institute_mocktest/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
					</script>
				
			<?php
				}	
				
			}else{

				$mocktest_counts=explode(",",$post['mocktest_id']);
				if(count($mocktest_counts)>0){
					foreach($mocktest_counts as $mocktestdata){
			
						$sql=$database->query1("select * from exam_paper where estatus='1' and id='".$mocktestdata."'");
						$row=mysqli_fetch_array($sql);

						$sql1=$database->query1("select * from institute_mocktests where estatus='1' and institute_id='".$post['institute_id']."' and mocktest_id='".$mocktestdata."' and id='".$id."'");
						$row1=mysqli_num_rows($sql1);
						if($row1>0){
							
							$result=$database->query1('update institute_mocktests set institute_id="'.$post['institute_id'].'",exam_id="'.$post['exam_id'].'",sub_exam_type="'.$post['sub_exam_type'].'",mocktest_id="'.$mocktestdata.'",class_id="'.$row['class_id'].'",mains_2021="'.$row['mains_2021'].'" where id="'.$id.'"');
						}else{
							
							$result=$database->query1('insert institute_mocktests set institute_id="'.$post['institute_id'].'",exam_id="'.$post['exam_id'].'",sub_exam_type="'.$post['sub_exam_type'].'",mocktest_id="'.$mocktestdata.'",class_id="'.$row['class_id'].'",mains_2021="'.$row['mains_2021'].'",username="'.$session->username.'",estatus="1",timestamp="'.time().'"');
						}
					}
					
					
					
				
				?>
					
					<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Institute Updation  Successfully!</div>
					
						<script type="text/javascript">
					
						animateForm('<?php echo SECURE_PATH;?>institute_mocktest/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
					</script>
				
			<?php
				}	
				
			}

				
				
			
	}
		

?>

 
	<?php
	

}
?>
<?php

//Display bulkreport
if(isset($_GET['tableDisplay'])){
	//Pagination code
  $limit=50;
  if(isset($_GET['page']))
  {
    $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
    $page=$_GET['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
  //Search Form
?>
<?php
  $tableName = 'institute_mocktests';
  $condition = "estatus='1'";//"userlevel = '8' OR userlevel = '9' OR userlevel = '7' OR userlevel = '6'";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  //$query_string = $_SERVER['QUERY_STRING'];
  $pagination = $session->showPagination(SECURE_PATH."institute_mocktest/process.php?tableDisplay=1&",$tableName,$start,$limit,$page,$condition);
  $q = "SELECT * FROM $tableName ".$condition." ";
 $result_sel = $database->query1($q);
  $numres = mysqli_num_rows($result_sel);
  $query = "SELECT * FROM $tableName ".$condition."  order by id asc";
  
  $data_sel = $database->query1($query);
  if(($start+$limit) > $numres){
	 $onpage = $numres;
	 }
	 else{
	  $onpage = $start+$limit;
	 }
  if($numres > 0){
	  
	?>
		  <script type="text/javascript">


  $('#dataTable').DataTable({
    "pageLength": 50
    
    
  });

</script>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">Institute Mocktest Data</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" id="dataTable">
									<thead>
										<tr>
											<th class="text-left" nowrap>Sr.No.</th>
											<th class="text-left" nowrap>Institute Name</th>
											<th class="text-left" nowrap>Exam Type</th>
											<th class="text-left" nowrap>Exam Name</th>
											<th class="text-left" nowrap>Options</th>
										</tr>
									</thead>
									<tbody>
									<?php
										$k=1;
									  while($value = mysqli_fetch_array($data_sel))
									  {
										$institute_sql=$database->query("select * from institute where estatus='1' and id='".$value['institute_id']."'");  
										$rowsql=mysqli_fetch_array($institute_sql);
										$exam_sql=$database->query1("select * from exam where estatus='1' and id='".$value['exam_id']."'");  
										$rowexam=mysqli_fetch_array($exam_sql);
										$exam_sql1=$database->query1("select * from exam_paper where estatus='1' and id='".$value['mocktest_id']."'");  
										$rowexam1=mysqli_fetch_array($exam_sql1);
									  ?>
														<tr>
														<td class="text-left"><?php echo $k;?></td>
										<td class="text-left" nowrap><?php echo $rowsql['institute_name'];?></td>
										<td class="text-left" nowrap><?php echo $rowexam['exam'];?></td>
										<td class="text-left" nowrap><?php echo $rowexam1['exam_name'];?></td>
										<td class="text-left" nowrap><a class=""  onClick="setState('adminForm','<?php echo SECURE_PATH;?>institute_mocktest/process.php','addForm=2&editform=<?php echo $value['id'];?>')"><span style="color:blue;"><i class="fa fa-pencil-square-o" style="color:blue;"></i></a>&nbsp;&nbsp;&nbsp;
										<a class=" " onClick="confirmDelete('adminForm','<?php echo SECURE_PATH;?>institute_mocktest/process.php','rowDelete=<?php echo $value['id'];?>')"><i class="fa fa-trash" style="color:red;"></i></a></td>
									  </tr>
														<?php
									$k++;
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  </section>
 
	<?php
	}
	else{
	?>
		
  <?php
	}

}

?>
	<?php
if(isset($_GET['rowDelete'])){
	$database->query1("update institute_mocktests set estatus='0'  WHERE id = '".$_GET['rowDelete']."'");
	?>
    <div class="alert alert-success"> Mocktest deleted successfully!</div>

  <script type="text/javascript">

animateForm('<?php echo SECURE_PATH;?>institute_mocktest/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
   <?php
}
	if(isset($_REQUEST['getmocktest'])){
		if($_REQUEST['exam_id']!=''){
			$examcon=" and exam_type='".$_REQUEST['exam_id']."'";
		}else{
			$examcon="";
		}
		if(isset($_REQUEST['sub_exam_type'])){
			if($_REQUEST['sub_exam_type']!=''){
				$subexamcon=" and sub_exam_type='".$_REQUEST['sub_exam_type']."'";
			}else{
				$subexamcon=" ";
			}
		}else{
			$subexamcon=" ";
		}
		
		?>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Mocktest Exam<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<select name="mocktest_id" class="form-control selectpicker2" id="mocktest_id"  onchange="examdatatype();" multiple data-actions-box="true" data-live-search="true" >
						
						<?php
						$row1 = $database->query1("SELECT * FROM exam_paper WHERE estatus='1' and institution_id='19' and  type='test_series' ".$examcon.$subexamcon.""); 
						while($data1 = mysqli_fetch_array($row1))
						{
						  ?>
						<option value="<?php echo $data1['id'];?>" <?php if(isset($_POST['mocktest_id'])) { if($data1['id']==$_POST['mocktest_id']) { echo 'selected="selected"'; } } ?>   ><?php echo $data1['exam_name'];?></option>
						<?php
						}
						?>
					</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['mocktest_id'])){ echo $_SESSION['error']['mocktest_id'];}?></span>
				</div>
			</div>
		<?php

	}
?>

  <script>
 $(".selectpicker1").selectpicker('refresh');
 $(".selectpicker2").selectpicker('refresh');
  $(".selectpicker3").selectpicker('refresh');
 $(".selectpicker4").selectpicker('refresh');
 function examdatatype(){
		var exam=$("#exam_id").val();
		if(exam=='2'){
			$('#examdivdata').show();
			
			
		}else{
			$('#examdivdata').hide();
			
		}
	}
</script>



