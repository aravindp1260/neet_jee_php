<?php

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");

ini_set('memory_limit', '-1');
ini_set('post_max_size', '100000M');
ini_set('upload_max_filesize', '100000M');
ini_set('max_input_time ', 100000);
ini_set('max_execution_time', 9000);
/*define("DB_SERVER","localhost");
define("DB_USER","root");
define("DB_PWD","");
define("DB_NAME","qsbank");*/
$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = " Total Questions report".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";
header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
						
						 	<th style="text-align:center;">Sr.No.</th>
							<th style="text-align:center;">Subject</th>
							<th style="text-align:center;">Chapter</th>
							<th style="text-align:center;">Topic</th>
							<th style="text-align:center;">Total Questions</th>
							<th style="text-align:center;">Easy</th>
							<th style="text-align:center;">Moderate</th>
							<th style="text-align:center;">Difficult</th>
							<th style="text-align:center;">Very Difficult</th>
							<th style="text-align:center;">Total Practice Questions</th>
							<th style="text-align:center;">Total Exam Questions</th>
							<th style="text-align:center;">P-Easy</th>
							<th style="text-align:center;">P-Moderate</th>
							<th style="text-align:center;">P-Difficult</th>
							<th style="text-align:center;">P-Very Difficult</th>
							<th style="text-align:center;">E-Easy</th>
							<th style="text-align:center;">E-Moderate</th>
							<th style="text-align:center;">E-Difficult</th>
							<th style="text-align:center;">E-Very Difficult</th>

							
						</tr>
                        <?php
                           	$k=1;
							$selltot=query("select * from topic where  estatus='1'   order by subject,id asc   ");
							while($rowl=mysqli_fetch_array($selltot)){
								$selsubject=query("SELECT *  FROM subject WHERE estatus=1 and id='".$rowl['subject']."' ORDER BY id ASC");
								$rowsubject = mysqli_fetch_array($selsubject);
								$selchapter=query("SELECT *  FROM chapter WHERE estatus=1 and id='".$rowl['chapter']."' ORDER BY id ASC");
								$rowchapter = mysqli_fetch_array($selchapter);
								$seltot=query("select id from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",topic)>0  ");
								$rowtot=mysqli_num_rows($seltot);
								
								$seltote=query("select id from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",topic)>0 and complexity='1' ");
								$rowtote=mysqli_num_rows($seltote);
								$seltotm=query("select id from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",topic)>0 and complexity='2' ");
								$rowtotm=mysqli_num_rows($seltotm);
								$seltotd=query("select id from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",topic)>0  and complexity='3' ");
								$rowtotd=mysqli_num_rows($seltotd);
								$seltothd=query("select id from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",topic)>0  and complexity='5' ");
								$rowtothd=mysqli_num_rows($seltothd);

								$seltotpr=query("select id from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",topic)>0 and usageset='1' ");
								$rowtotpr=mysqli_num_rows($seltotpr);
								$seltotexam=query("select id from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",topic)>0 and usageset!='' and usageset!='1' ");
								$rowtotexam=mysqli_num_rows($seltotexam);

								$seltotpe=query("select id from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",topic)>0 and complexity='1' and usageset='1' ");
								$rowtotpe=mysqli_num_rows($seltotpe);
								$seltotpm=query("select id from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",topic)>0 and complexity='2' and usageset='1'");
								$rowtotpm=mysqli_num_rows($seltotpm);
								$seltotpd=query("select id from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",topic)>0  and complexity='3' and usageset='1'");
								$rowtotpd=mysqli_num_rows($seltotpd);
								$seltotphd=query("select id from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",topic)>0  and complexity='5' and usageset='1'");
								$rowtotphd=mysqli_num_rows($seltotphd);


								$seltotee=query("select id from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",topic)>0 and complexity='1' and usageset!='1' and usageset!=''");
								$rowtotee=mysqli_num_rows($seltotee);
								$seltotem=query("select id from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",topic)>0 and complexity='2' and usageset!='1' and usageset!=''");
								$rowtotem=mysqli_num_rows($seltotem);
								$seltoted=query("select id from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",topic)>0  and complexity='3' and usageset!='1' and usageset!=''");
								$rowtoted=mysqli_num_rows($seltoted);
								$seltotehd=query("select id from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",topic)>0  and complexity='5' and usageset!='1' and usageset!=''");
								$rowtotehd=mysqli_num_rows($seltotehd);

								echo "<tr>";
										?>	
										
											<td><?php echo $k;?></td>
											<td ><?php echo $rowsubject['subject'];?></td>
											<td><?php echo $rowchapter['chapter']; ?></td>
											<td><?php echo $rowl['topic']; ?></td>
											<td ><?php echo $rowtot;?></td>
											<td ><?php echo $rowtote;?></td>
											<td ><?php echo $rowtotm;?></td>
											<td ><?php echo $rowtotd;?></td>
											<td ><?php echo $rowtothd;?></td>
											<td ><?php echo $rowtotpr;?></td>
											<td ><?php echo $rowtotexam;?></td>
											<td ><?php echo $rowtotpe;?></td>
											<td ><?php echo $rowtotpm;?></td>
											<td ><?php echo $rowtotpd;?></td>
											<td ><?php echo $rowtotphd;?></td>
											<td ><?php echo $rowtotee;?></td>
											<td ><?php echo $rowtotem;?></td>
											<td ><?php echo $rowtoted;?></td>
											<td ><?php echo $rowtotehd;?></td>
											
										<?php
										
										echo "</tr>";
										
								$k++;
                                                            
									           
									}
							
						
						                        ?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>