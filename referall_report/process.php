<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
?>


	<?php
	if(isset($_REQUEST['tableDisplay'])){
		
		$con = mysqli_connect("localhost","rspace","Rsp@2019","neetjee");
		//$con = mysqli_connect("localhost","root","","neetjee");
		function query($sql){
		global $con;
		 return mysqli_query($con,$sql);
		}
	
	?>
		<style>
			.subjectanalysis {
				padding: .75rem 1.25rem;
				margin-bottom: 0;
				background-color: rgba(0,0,0,.03);
				border-bottom: 1px solid rgba(0,0,0,.125);
			}
			.subject-wise-analysis.nav-pills .nav-link.active {
				background-color:#000;
				color:#fff;
			}
			.subject-analysis-content label, h6 {
				color:#000;
			}
			.subject-analysis-content .table td p {
				color:#000000c7;
				font-weight:bold;
			}
			.subject-analysis-content .table th, .subject-analysis-content .table td {
				font-size:12px !important;
				padding:0.7rem 0.1rem !important;
			}
		</style>
		<style>
	
	 </style>
	<script>
	$('.chapter').selectpicker1();
	 </script>
	  <script>
function search_report() {
		
			var user_type = $('#user_type').val();
			
			setState('adminTable','<?php echo SECURE_PATH;?>referall_report/process.php','tableDisplay=1&user_type='+user_type+'');
		}
	</script>
		<section class="content-area">
			<div class="container">
				<div class="row Data-Tables">
					<div class="col-xl-12 col-lg-12"> 
						<div class="card border-0 shadow mb-4">
							<div class="card-header py-3">
								<h6 class="m-0 font-weight-bold text-primary"> Referral Codes Report</h6>
							</div>
							<div class="card-body">
							
									<script type="text/javascript">


									  $('#dataTable').DataTable({
										"pageLength": 50,
										"order": [[ 1, 'asc' ]]
										
									  });

									</script>
									<div class="row">
										
										<div class="col-md-4 ml-auto pl-3">
											<div class=" form-group form-inline pl-3 ml-1">
											<label>User Type: </label>
											<select class="form-control alibaba col-md-8" id="user_type" name="user_type" onchange="search_report();">
												<option value="">-Select-</option>
												<option value="6" <?php if(isset($_POST['user_type'])){ if($_POST['user_type'] == 6){ echo ' selected="selected"';};}?>>Reviewer</option>
												<option value="3" <?php if(isset($_POST['user_type'])){ if($_POST['user_type'] == 3){ echo ' selected="selected"';};}?>>Lecturer</option>
												<option value="2" <?php if(isset($_POST['user_type'])){ if($_POST['user_type'] == 2){ echo ' selected="selected"';};}?>>Student</option>
												<option value="1" <?php if(isset($_POST['user_type'])){ if($_POST['user_type'] == 1){ echo ' selected="selected"';};}?>>Admin</option>
											</select>
											</div>
										</div>
										
									</div>
									<div class="card border-0">
										<div class="card-body table-responsive table_data p-0">
											<table class="table table-bordered dashboard-table mb-0" id="dataTable">
												<thead class="thead-light">
													<tr>
														<th scope="col" class="text-center" >Username</th>
														<th scope="col" class="text-center">Type</th>
														<th scope="col" class="text-center">Referral Id</th>
														<th scope="col" class="text-center">No.Of Times Used</th>
														<th scope="col" class="text-center">Last Used On</th>
														
													</tr>
													
												</thead>
											<tbody>
												<?php
												if(isset($_REQUEST['user_type'])){
													if($_REQUEST['user_type']!=''){
														$user_type=" AND b.user_type='".$_REQUEST['user_type']."'";
													}else{
														$user_type="";
													}
												}else{
													$user_type="";
												}
												$k=1;
												
												$sel=query("SELECT a.* FROM `student_transactions` as a inner join referral_codes  as b on a.referral_id=b.id and a.referral_id!='' ".$user_type." group by a.referral_id  order by a.id desc");
												while($row=mysqli_fetch_array($sel)){
													$time=$row['timestamp'];
													$sel1=query("select * from referral_codes where  id='".$row['referral_id']."'");
													$row1=mysqli_fetch_array($sel1);

													$sel2=query("select count(id) as cnt from `student_transactions` where  referral_id='".$row['referral_id']." '");
													$row2=mysqli_fetch_array($sel2);
													if($row1['user_type']=='1'){
														$lec=$row1['lec_user'];
														$type="Admin";
													}else if($row1['user_type']=='3'){
														$lec=$row1['lec_user'];
														$type="Lecturer";
													}else if($row1['user_type']=='6'){
														$lec=$row1['lec_user'];
														$type="Reviewer";
													}else if($row1['user_type']=='2'){
														$lec=$row1['lec_user'];
														$type="Student";
													}else{
														$lec="Admin";
														$type="Admin";
													}
												?>
													<tr>
														<td><?php echo $lec;?></td>
														<td><?php echo $type; ?></td>
														<td><?php echo $row1['referral_code']; ?></td>
														<td><?php echo $row2['cnt']; ?></td>
														<td class="text-left" data-order="<?php echo $time; ?>"><?php echo date('d/m/Y',$row['timestamp']);?></td>
													</tr>
												<?php $k++; } ?>
											</tbody>
										</table>
									
									
									</div>	
								</div>
								
								
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>
		
			
	<?php
	}
	
	?>
	
