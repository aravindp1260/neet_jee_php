<?php
include("database.php");
//include("mailer.php");
include("form.php");
date_default_timezone_set('Asia/Kolkata');
class Session
{
   var $username;     //Username given on sign-up
   var $userid;       //Random value generated on current login
   var $userlevel;    //The level to which the user pertains
   var $time;         //Time user was last active (page loaded)
   var $logged_in;    //True if user is logged in, false otherwise
   var $userinfo = array();  //The array holding all user info
   var $url;          //The page url current being viewed
   var $referrer;     //Last recorded site page viewed
   var $peoples_rep1 = array(1=>'Zilla Parishad Chairman',
                             2=>'Member of Parliament',
                             3=>'MLA',
                             4=>'ZPTC',
                             5=>'Mandal Parishad President',
                             6=>'MPTC',
                             7=>'Sarpanch',
                             8=>'Ward Member');


   /* Class constructor */
   function Session(){
      $this->time = time();
      $this->startSession();
   }

   
   function startSession(){
      global $database;  //The database connection
      session_start();   //Tell PHP to start the session

      /* Determine if user is logged in */
      $this->logged_in = $this->checkLogin();

      /**
       * Set guest value to users not logged in, and update
       * active guests table accordingly.
       */
      if(!$this->logged_in){
         $this->username = $_SESSION['username'] = GUEST_NAME;
         $this->userlevel = GUEST_LEVEL;
         $database->addActiveGuest($_SERVER['REMOTE_ADDR'], $this->time);
      }
      /* Update users last active timestamp */
      else{
         $database->addActiveUser($this->username, $this->time);
      }

      /* Remove inactive visitors from database */

     if(date('i') % 20 == 0){
      $database->removeInactiveUsers();
      $database->removeInactiveGuests();
      }


   /* Set referrer page */
      if(isset($_SESSION['url'])){
         $this->referrer = $_SESSION['url'];
      }else{
         $this->referrer = "/";
      }

      /* Set current url */
      $this->url = $_SESSION['url'] = $_SERVER['PHP_SELF'];
   }

   /**
    * checkLogin - Checks if the user has already previously
    * logged in, and a session with the user has already been
    * established. Also checks to see if user has been remembered.
    * If so, the database is queried to make sure of the user's
    * authenticity. Returns true if the user has logged in.10/31/2019
    */
   function checkLogin(){
      global $database;  //The database connection
      /* Check if user has been remembered */
      if(isset($_COOKIE['cookname']) && isset($_COOKIE['cookid']))
	  {
         $this->username = $_SESSION['username'] = $_COOKIE['cookname'];
         $this->userid   = $_SESSION['userid']   = $_COOKIE['cookid'];
      }

      /* Username and userid have been set and not guest */
      if(isset($_SESSION['username']) && isset($_SESSION['userid']) &&
         $_SESSION['username'] != GUEST_NAME){
			 /* Confirm that username and userid are valid */
			 if($database->confirmUserID($_SESSION['username'], $_SESSION['userid']) != 0)
			 {
				/* Variables are incorrect, user not logged in */
				unset($_SESSION['username']);
				unset($_SESSION['userid']);
				return false;
			 }
			/* User is logged in, set class variables */
			 $this->userinfo  = $database->getUserInfo($_SESSION['username']);
			 $this->username  = $this->userinfo['username'];
			 $this->userid    = $this->userinfo['userid'];
			 $this->userlevel = $this->userinfo['userlevel'];

			/* auto login hash expires in three days */
			 if($this->userinfo['hash_generated'] < (time() - (60*60*24*3))){
				/* Update the hash */
				 $database->updateUserField($this->userinfo['username'], 'hash', $this->generateRandID());
				 $database->updateUserField($this->userinfo['username'], 'hash_generated', time());
			}

         return true;
      }
      /* User not logged in */
      else{
         return false;
      }
   }

   /**
    * login - The user has submitted his username and password
    * through the login form, this function checks the authenticity
    * of that information in the database and creates the session.
    * Effectively logging in the user if all goes well.
    */
   function login($subuser, $subpass, $subremember){
      global $database, $form;  //The database and form object

      /* Username error checking */
      $field = "user";  //Use field name for username
	  $q = "SELECT valid FROM ".TBL_USERS." WHERE username='$subuser'";
	  $valid = $database->query($q);
	  $valid = mysqli_fetch_array($valid);

      if(!$subuser || strlen($subuser = trim($subuser)) == 0){
         $form->setError($field, "* Username not entered");
      }
      else{
         /* Check if username is not alphanumeric */
         if(!ctype_alnum($subuser)){
            $form->setError($field, "* Username not alphanumeric");
         }
      }

      /* Password error checking */
      $field = "pass";  //Use field name for password
      if(!$subpass){
         $form->setError($field, "* Password not entered");
      }

      /* Return if form errors exist */
      if($form->num_errors > 0){
         return false;
      }

      /* Checks that username is in database and password is correct */
      $subuser = stripslashes($subuser);
      $result = $database->confirmUserPass($subuser, $subpass);

      /* Check error codes */
      if($result == 1){
         $field = "user";
         $form->setError($field, "* Username not found");
      }
      else if($result == 2){
         $field = "pass";
         $form->setError($field, "* Invalid password");
      }

      /* Return if form errors exist */
      if($form->num_errors > 0){
         return false;
      }


      if(EMAIL_WELCOME){
      	if($valid['valid'] == 0){
      		$form->setError($field, "* User's account has not yet been confirmed.");
      	}
      }

      /* Return if form errors exist */
      if($form->num_errors > 0){
         return false;
      }



      /* Username and password correct, register session variables */
      $this->userinfo  = $database->getUserInfo($subuser);
      $this->username  = $_SESSION['username'] = $this->userinfo['username'];
      $this->userid    = $_SESSION['userid']   = $this->generateRandID();
      $this->userlevel = $this->userinfo['userlevel'];

      /* Insert userid into database and update active users table */
      $database->updateUserField($this->username, "userid", $this->userid);
      $database->addActiveUser($this->username, $this->time);
      $database->removeActiveGuest($_SERVER['REMOTE_ADDR']);

      /**
       * This is the cool part: the user has requested that we remember that
       * he's logged in, so we set two cookies. One to hold his username,
       * and one to hold his random value userid. It expires by the time
       * specified in constants.php. Now, next time he comes to our site, we will
       * log him in automatically, but only if he didn't log out before he left.
       */
      if($subremember){
         setcookie("cookname", $this->username, time()+COOKIE_EXPIRE, COOKIE_PATH);
         setcookie("cookid",   $this->userid,   time()+COOKIE_EXPIRE, COOKIE_PATH);
      }

      /* Login completed successfully */
      return true;
   }

   /**
    * logout - Gets called when the user wants to be logged out of the
    * website. It deletes any cookies that were stored on the users
    * computer as a result of him wanting to be remembered, and also
    * unsets session variables and demotes his user level to guest.
    */
   function logout(){
      global $database;  //The database connection
      /**
       * Delete cookies - the time must be in the past,
       * so just negate what you added when creating the
       * cookie.
       */
      if(isset($_COOKIE['cookname']) && isset($_COOKIE['cookid'])){
         setcookie("cookname", "", time()-COOKIE_EXPIRE, COOKIE_PATH);
         setcookie("cookid",   "", time()-COOKIE_EXPIRE, COOKIE_PATH);
      }

      /* Unset PHP session variables */
      unset($_SESSION['username']);
      unset($_SESSION['userid']);

      /* Reflect fact that user has logged out */
      $this->logged_in = false;

      /**
       * Remove from active users table and add to
       * active guests tables.
       */
      $database->removeActiveUser($this->username);
      $database->addActiveGuest($_SERVER['REMOTE_ADDR'], $this->time);

      /* Set user level to guest */
      $this->username  = GUEST_NAME;
      $this->userlevel = GUEST_LEVEL;
   }

   /**
    * register - Gets called when the user has just submitted the
    * registration form. Determines if there were any errors with
    * the entry fields, if so, it records the errors and returns
    * 1. If no errors were found, it registers the new user and
    * returns 0. Returns 2 if registration failed.
    */
   function register($subuser, $subpass, $subemail, $subname){

      global $database, $form, $mailer;  //The database, form and mailer object

      /* Username error checking */
      $field = "user";  //Use field name for username
      if(!$subuser || strlen($subuser = trim($subuser)) == 0){
         $form->setError($field, "* Username not entered");
      }
      else{
         /* Spruce up username, check length */
         $subuser = stripslashes($subuser);
         if(strlen($subuser) < 5){
            $form->setError($field, "* Username below 5 characters");
         }
         else if(strlen($subuser) > 30){
            $form->setError($field, "* Username above 30 characters");
         }
         /* Check if username is not alphanumeric */
         else if(!ctype_alnum($subuser)){
            $form->setError($field, "* Username not alphanumeric");
         }
         /* Check if username is reserved */
         else if(strcasecmp($subuser, GUEST_NAME) == 0){
            $form->setError($field, "* Username reserved word");
         }
         /* Check if username is already in use */
         else if($database->usernameTaken($subuser)){
            $form->setError($field, "* Username already in use");
         }
         /* Check if username is banned */
         else if($database->usernameBanned($subuser)){
            $form->setError($field, "* Username banned");
         }
      }

      /* Password error checking */
      $field = "pass";  //Use field name for password
      if(!$subpass){
         $form->setError($field, "* Password not entered");
      }
      else{
         /* Spruce up password and check length*/
         $subpass = stripslashes($subpass);
         if(strlen($subpass) < 4){
            $form->setError($field, "* Password too short");
         }
         /* Check if password is not alphanumeric */
         else if(!ctype_alnum(($subpass = trim($subpass)))){
            $form->setError($field, "* Password not alphanumeric");
         }
         /**
          * Note: I trimmed the password only after I checked the length
          * because if you fill the password field up with spaces
          * it looks like a lot more characters than 4, so it looks
          * kind of stupid to report "password too short".
          */
      }

      /* Email error checking */
      $field = "email";  //Use field name for email
      if(!$subemail || strlen($subemail = trim($subemail)) == 0){
         $form->setError($field, "* Email not entered");
      }
      else{
         /* Check if valid email address */
         if(filter_var($subemail, FILTER_VALIDATE_EMAIL) == FALSE){
            $form->setError($field, "* Email invalid");
         }
         /* Check if email is already in use */
         if($database->emailTaken($subemail)){
            $form->setError($field, "* Email already in use");
         }

         $subemail = stripslashes($subemail);
      }

      /* Name error checking */
	  $field = "name";
	  if(!$subname || strlen($subname = trim($subname)) == 0){
	     $form->setError($field, "* Name not entered");
	  } else {
	     $subname = stripslashes($subname);
	  }

      $randid = $this->generateRandID();

      /* Errors exist, have user correct them */
      if($form->num_errors > 0){
         return 1;  //Errors with form
      }
      /* No errors, add the new account to the */
      else{
         if($database->addNewUser($subuser, md5($subpass), $subemail, $randid, $subname)){
            if(EMAIL_WELCOME){
               $mailer->sendWelcome($subuser,$subemail,$subpass,$randid);
            }
            return 0;  //New user added succesfully
         }else{
            return 2;  //Registration attempt failed
         }
      }
   }

   /**
    * editAccount - Attempts to edit the user's account information
    * including the password, which it first makes sure is correct
    * if entered, if so and the new password is in the right
    * format, the change is made. All other fields are changed
    * automatically.
    */
   function editAccount($subcurpass, $subnewpass, $subemail, $subname){
      global $database, $form;  //The database and form object
      /* New password entered */
      if($subnewpass){
         /* Current Password error checking */
         $field = "curpass";  //Use field name for current password
         if(!$subcurpass){
            $form->setError($field, "* Current Password not entered");
         }
         else{
            /* Check if password too short or is not alphanumeric */
            $subcurpass = stripslashes($subcurpass);
            if(strlen($subcurpass) < 4 ||
               !preg_match("^([0-9a-z])+$", ($subcurpass = trim($subcurpass)))){
               $form->setError($field, "* Current Password incorrect");
            }
            /* Password entered is incorrect */
            if($database->confirmUserPass($this->username,md5($subcurpass)) != 0){
               $form->setError($field, "* Current Password incorrect");
            }
         }

         /* New Password error checking */
         $field = "newpass";  //Use field name for new password
         /* Spruce up password and check length*/
         $subpass = stripslashes($subnewpass);
         if(strlen($subnewpass) < 4){
            $form->setError($field, "* New Password too short");
         }
         /* Check if password is not alphanumeric */
         else if(!preg_match("^([0-9a-z])+$", ($subnewpass = trim($subnewpass)))){
            $form->setError($field, "* New Password not alphanumeric");
         }
      }
      /* Change password attempted */
      else if($subcurpass){
         /* New Password error reporting */
         $field = "newpass";  //Use field name for new password
         $form->setError($field, "* New Password not entered");
      }

      /* Email error checking */
      $field = "email";  //Use field name for email
      if($subemail && strlen($subemail = trim($subemail)) > 0){
         /* Check if valid email address */
         if(filter_var($subemail, FILTER_VALIDATE_EMAIL) == FALSE){
            $form->setError($field, "* Email invalid");
         }
         $subemail = stripslashes($subemail);
      }

      /* Name error checking */
	  $field = "name";
	  if(!$subname || strlen($subname = trim($subname)) == 0){
	     $form->setError($field, "* Name not entered");
	  } else {

	     $subname = stripslashes($subname);
	  }

      /* Errors exist, have user correct them */
      if($form->num_errors > 0){
         return false;  //Errors with form
      }

      /* Update password since there were no errors */
      if($subcurpass && $subnewpass){
         $database->updateUserField($this->username,"password",md5($subnewpass));
      }

      /* Change Email */
      if($subemail){
         $database->updateUserField($this->username,"email",$subemail);
      }

      /* Change Name */
      if($subname){
         $database->updateUserField($this->username,"name",$subname);
      }

      /* Success! */
      return true;
   }

   /**
    * isAdmin - Returns true if currently logged in user is
    * an administrator, false otherwise.
    */
   function isAdmin(){
      return ($this->userlevel == ADMIN_LEVEL ||
              $this->username  == ADMIN_NAME);
   }

   /**
    * isAuthor - Returns true if currently logged in user is
    * an author or an administrator, false otherwise.
    */
   function isAuthor(){
      return ($this->userlevel == AUTHOR_LEVEL ||
              $this->userlevel == ADMIN_LEVEL);
   }

   /**
    * generateRandID - Generates a string made up of randomized
    * letters (lower and upper case) and digits and returns
    * the md5 hash of it to be used as a userid.
    */
   function generateRandID(){
      return md5($this->generateRandStr(16));
   }

   /**
    * generateRandStr - Generates a string made up of randomized
    * letters (lower and upper case) and digits, the length
    * is a specified parameter.
    */
   function generateRandStr($length){
      $randstr = "";
      for($i=0; $i<$length; $i++){
         $randnum = mt_rand(0,61);
         if($randnum < 10){
            $randstr .= chr($randnum+48);
         }else if($randnum < 36){
            $randstr .= chr($randnum+55);
         }else{
            $randstr .= chr($randnum+61);
         }
      }
      return $randstr;
   }

   function cleanInput($post = array()) {
       foreach($post as $k => $v){
            $post[$k] = trim(htmlspecialchars($v));
         }
         return $post;
   }
	/*Pagination code*/
	 function showPagination($pagename,$tbl_name,$start,$limit,$page,$condition){

		$database = new MySQLDB;
        //your table name
    // How many adjacent pages should be shown on each side?
    $adjacents = 2;

    /*
       First get total number of rows in data table.
       If you have a WHERE clause in your query, make sure you mirror it here.
    */
	//echo "SELECT COUNT(*) as num FROM $tbl_name $condition ";
    $query = "SELECT COUNT(*) as num FROM $tbl_name $condition ";

   // echo $query;

    $total_pages = mysqli_fetch_array($database->query($query));
//        echo $total_pages['num'].',';
    $total_pages = $total_pages['num'];




    /* Setup vars for query. */
    $targetpage = $pagename;     //your file name  (the name of this file)
                                //how many items to show per page




    /* Setup page vars for display. */
    if ($page == 0) $page = 1;                    //if no page var is given, default to 1.
    $prev = $page - 1;                            //previous page is page - 1
    $next = $page + 1;                            //next page is page + 1
    $lastpage = ceil($total_pages/$limit);        //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;                        //last page minus 1



    /*
        Now we apply our rules and draw the pagination object.
        We're actually saving the code to a variable in case we want to draw it more than once.
    */
    $pagination = "";
    if($lastpage > 1)
    {
        $pagination .= "<div class=\"pagination\">";
        //previous button
        if ($page > 1){

		    $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$prev."')\"> previous</a>";
		}
        else{
            $pagination.= "<span class=\"disabled\"> previous</span>";
		}
        //pages
        if ($lastpage < 7 + ($adjacents * 2))    //not enough pages to bother breaking it up
        {
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<span class=\"current\">$counter</span>";
                else
                    $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
            }
        }
        elseif($lastpage > 5 + ($adjacents * 2))    //enough pages to hide some
        {
            //close to beginning; only hide later pages
            if($page < 1 + ($adjacents * 2))
            {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a   onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
                }
                $pagination.= "...";
                $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$lpm1."')\">$lpm1</a>";
                $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$lastpage."')\">$lastpage</a>";
            }
            //in middle; hide some front and some back
            elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
            {
                $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=1')\">1</a>";
                $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=2')\">2</a>";
                $pagination.= "...";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
                }
                $pagination.= "...";
                $pagination.= "<a >$lpm1</a>";
                $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$lastpage."')\">$lastpage</a>";
            }
            //close to end; only hide early pages
            else
            {
                $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=1')\">1</a>";
                $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=2')\">2</a>";
                $pagination.= "...";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
                }
            }
        }

        //next button
        if ($page < $counter - 1)
            $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$next."')\">next </a>";
        else
            $pagination.= "<span class=\"disabled\">next  </span>";
        $pagination.= "</div>\n";
    }

//        echo $pagination.'hello';

    return $pagination;
	}
   

   function showPaginationnew($pagename,$tbl_name,$start,$limit,$page,$condition,$query){

		$database = new MySQLDB;
        //your table name
    // How many adjacent pages should be shown on each side?
    $adjacents = 2;

    /*
       First get total number of rows in data table.
       If you have a WHERE clause in your query, make sure you mirror it here.
    */
	//echo "SELECT COUNT(*) as num FROM $tbl_name $condition ";
   // $query = "SELECT COUNT(*) as num FROM $tbl_name $condition ";

   
    $total_pages = mysqli_num_rows($database->query1($query));
//        echo $total_pages['num'].',';
   // $total_pages = $total_pages['num'];




    /* Setup vars for query. */
    $targetpage = $pagename;     //your file name  (the name of this file)
                                //how many items to show per page




    /* Setup page vars for display. */
    if ($page == 0) $page = 1;                    //if no page var is given, default to 1.
    $prev = $page - 1;                            //previous page is page - 1
    $next = $page + 1;                            //next page is page + 1
    $lastpage = ceil($total_pages/$limit);        //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;                        //last page minus 1



    /*
        Now we apply our rules and draw the pagination object.
        We're actually saving the code to a variable in case we want to draw it more than once.
    */
    $pagination = "";
    if($lastpage > 1)
    {
        $pagination .= "<div class=\"pagination\">";
        //previous button
        if ($page > 1){

		    $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$prev."')\"> previous</a>";
		}
        else{
            $pagination.= "<span class=\"disabled\"> previous</span>";
		}
        //pages
        if ($lastpage < 7 + ($adjacents * 2))    //not enough pages to bother breaking it up
        {
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<span class=\"current\">$counter</span>";
                else
                    $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
            }
        }
        elseif($lastpage > 5 + ($adjacents * 2))    //enough pages to hide some
        {
            //close to beginning; only hide later pages
            if($page < 1 + ($adjacents * 2))
            {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a   onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
                }
                $pagination.= "...";
                $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$lpm1."')\">$lpm1</a>";
                $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$lastpage."')\">$lastpage</a>";
            }
            //in middle; hide some front and some back
            elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
            {
                $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=1')\">1</a>";
                $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=2')\">2</a>";
                $pagination.= "...";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
                }
                $pagination.= "...";
                $pagination.= "<a >$lpm1</a>";
                $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$lastpage."')\">$lastpage</a>";
            }
            //close to end; only hide early pages
            else
            {
                $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=1')\">1</a>";
                $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=2')\">2</a>";
                $pagination.= "...";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
                }
            }
        }

        //next button
        if ($page < $counter - 1)
            $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$next."')\">next </a>";
        else
            $pagination.= "<span class=\"disabled\">next  </span>";
        $pagination.= "</div>\n";
    }

//        echo $pagination.'hello';

    return $pagination;
	}
	 /*Pagination code*/
	 function showPagination99($pagename,$tbl_name,$start,$limit,$page,$query){
		$database = new MySQLDB;
        //your table name
    // How many adjacent pages should be shown on each side?
    $adjacents = 3;

    /*
       First get total number of rows in data table.
       If you have a WHERE clause in your query, make sure you mirror it here.
    */
	//echo "SELECT COUNT(*) as num FROM $tbl_name $condition ";
   // $query = "SELECT COUNT(*) as num FROM $tbl_name $condition ";

   	
    $total_pages = mysqli_fetch_array($database->query($query));
     //echo $total_pages['num'].',';
    $total_pages = $total_pages['num'];

	

    /* Setup vars for query. */
    $targetpage = $pagename;     //your file name  (the name of this file)
                                //how many items to show per page




    /* Setup page vars for display. */
    if ($page == 0) $page = 1;                    //if no page var is given, default to 1.
    $prev = $page - 1;                            //previous page is page - 1
    $next = $page + 1;                            //next page is page + 1
    $lastpage = ceil($total_pages/$limit);        //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;                        //last page minus 1



    /*
        Now we apply our rules and draw the pagination object.
        We're actually saving the code to a variable in case we want to draw it more than once.
    */
    $pagination = "";
    if($lastpage > 1)
    {
        $pagination .= "<div class=\"pagination\">";
        //previous button
        if ($page > 1){

		    $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$prev."')\"> previous</a>";
		}
        else{
            $pagination.= "<span class=\"disabled\"> previous</span>";
		}
        //pages
        if ($lastpage < 7 + ($adjacents * 2))    //not enough pages to bother breaking it up
        {
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<span class=\"current\">$counter</span>";
                else
                    $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
            }
        }
        elseif($lastpage > 5 + ($adjacents * 2))    //enough pages to hide some
        {
            //close to beginning; only hide later pages
            if($page < 1 + ($adjacents * 2))
            {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a   onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
                }
                $pagination.= "...";
                $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$lpm1."')\">$lpm1</a>";
                $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$lastpage."')\">$lastpage</a>";
            }
            //in middle; hide some front and some back
            elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
            {
                $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=1')\">1</a>";
                $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=2')\">2</a>";
                $pagination.= "...";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
                }
                $pagination.= "...";
                $pagination.= "<a >$lpm1</a>";
                $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$lastpage."')\">$lastpage</a>";
            }
            //close to end; only hide early pages
            else
            {
                $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=1')\">1</a>";
                $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=2')\">2</a>";
                $pagination.= "...";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
                }
            }
        }

        //next button
        if ($page < $counter - 1)
            $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$next."')\">next </a>";
        else
            $pagination.= "<span class=\"disabled\">next  </span>";
        $pagination.= "</div>\n";
    }

//        echo $pagination.'hello';
	return $pagination;
	}
	/*Pagination code*/
	 function showPagination2($pagename,$tbl_name,$start,$limit,$page,$condition){

		$database = new MySQLDB;
        //your table name
    // How many adjacent pages should be shown on each side?
    $adjacents = 3;




    /*
       First get total number of rows in data table.
       If you have a WHERE clause in your query, make sure you mirror it here.
    */
    $query = "SELECT COUNT(*) as num FROM $tbl_name $condition ";
    $total_pages = mysqli_fetch_array($database->query($query));
    $total_pages = $total_pages['num'];



    /* Setup vars for query. */
    $targetpage = $pagename;     //your file name  (the name of this file)
                                //how many items to show per page




    /* Setup page vars for display. */
    if ($page == 0) $page = 1;                    //if no page var is given, default to 1.
    $prev = $page - 1;                            //previous page is page - 1
    $next = $page + 1;                            //next page is page + 1
    $lastpage = ceil($total_pages/$limit);        //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;                        //last page minus 1

    /*
        Now we apply our rules and draw the pagination object.
        We're actually saving the code to a variable in case we want to draw it more than once.
    */
    $pagination = "";
    if($lastpage > 1)
    {
        $pagination .= "<div class=\"pagination\">";
        //previous button
        if ($page > 1){

		    $pagination.= "<a onclick=\"setStateGet('adminTable2','".$targetpage."','page=".$prev."')\">&laquo; previous</a>";
		}
        else{
            $pagination.= "<span class=\"disabled\">&laquo; previous</span>";
		}
        //pages
        if ($lastpage < 7 + ($adjacents * 2))    //not enough pages to bother breaking it up
        {
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<span class=\"current\">$counter</span>";
                else
                    $pagination.= "<a  onclick=\"setStateGet('adminTable2','".$targetpage."','page=".$counter."')\">$counter</a>";
            }
        }
        elseif($lastpage > 5 + ($adjacents * 2))    //enough pages to hide some
        {
            //close to beginning; only hide later pages
            if($page < 1 + ($adjacents * 2))
            {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a   onclick=\"setStateGet('adminTable2','".$targetpage."','page=".$counter."')\">$counter</a>";
                }
                $pagination.= "...";
                $pagination.= "<a onclick=\"setStateGet('adminTable2','".$targetpage."','page=".$lpm1."')\">$lpm1</a>";
                $pagination.= "<a  onclick=\"setStateGet('adminTable2','".$targetpage."','page=".$lastpage."')\">$lastpage</a>";
            }
            //in middle; hide some front and some back
            elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
            {
                $pagination.= "<a  onclick=\"setStateGet('adminTable2','".$targetpage."','page=1')\">1</a>";
                $pagination.= "<a onclick=\"setStateGet('adminTable2','".$targetpage."','page=2')\">2</a>";
                $pagination.= "...";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a onclick=\"setStateGet('adminTable2','".$targetpage."','page=".$counter."')\">$counter</a>";
                }
                $pagination.= "...";
                $pagination.= "<a >$lpm1</a>";
                $pagination.= "<a onclick=\"setStateGet('adminTable2','".$targetpage."','page=".$lastpage."')\">$lastpage</a>";
            }
            //close to end; only hide early pages
            else
            {
                $pagination.= "<a onclick=\"setStateGet('adminTable2','".$targetpage."','page=1')\">1</a>";
                $pagination.= "<a  onclick=\"setStateGet('adminTable2','".$targetpage."','page=2')\">2</a>";
                $pagination.= "...";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a  onclick=\"setStateGet('adminTable2','".$targetpage."','page=".$counter."')\">$counter</a>";
                }
            }
        }

        //next button
        if ($page < $counter - 1)
            $pagination.= "<a  onclick=\"setStateGet('adminTable2','".$targetpage."','page=".$next."')\">next &raquo;</a>";
        else
            $pagination.= "<span class=\"disabled\">next  &raquo;</span>";
        $pagination.= "</div>\n";
    }


    return $pagination;

	}
   function showPaginationneet($pagename,$tbl_name,$start,$limit,$page,$condition,$query){
      //your table name
  // How many adjacent pages should be shown on each side?
  $adjacents = 3;

  /*
     First get total number of rows in data table.
     If you have a WHERE clause in your query, make sure you mirror it here.
  */
  $query1 = query($query);

    $total_pages = mysqli_fetch_array($query1);
  $total_pages = $total_pages['num'];

 

  /* Setup vars for query. */
  $targetpage = $pagename;     //your file name  (the name of this file)
                              //how many items to show per page




  /* Setup page vars for display. */
  if ($page == 0) $page = 1;                    //if no page var is given, default to 1.
  $prev = $page - 1;                            //previous page is page - 1
  $next = $page + 1;                            //next page is page + 1
  $lastpage = ceil($total_pages/$limit);        //lastpage is = total pages / items per page, rounded up.
  $lpm1 = $lastpage - 1;                        //last page minus 1



  /*
      Now we apply our rules and draw the pagination object.
      We're actually saving the code to a variable in case we want to draw it more than once.
  */
  $pagination = "";
  if($lastpage > 1)
  {
      $pagination .= "<div class=\"pagination\">";
      //previous button
      if ($page > 1){

        $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$prev."')\"> previous</a>";
    }
      else{
          $pagination.= "<span class=\"disabled\"> previous</span>";
    }
      //pages
      if ($lastpage < 7 + ($adjacents * 2))    //not enough pages to bother breaking it up
      {
          for ($counter = 1; $counter <= $lastpage; $counter++)
          {
              if ($counter == $page)
                  $pagination.= "<span class=\"current\">$counter</span>";
              else
                  $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
          }
      }
      elseif($lastpage > 5 + ($adjacents * 2))    //enough pages to hide some
      {
          //close to beginning; only hide later pages
          if($page < 1 + ($adjacents * 2))
          {
              for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
              {
                  if ($counter == $page)
                      $pagination.= "<span class=\"current\">$counter</span>";
                  else
                      $pagination.= "<a   onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
              }
              $pagination.= "...";
              $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$lpm1."')\">$lpm1</a>";
              $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$lastpage."')\">$lastpage</a>";
          }
          //in middle; hide some front and some back
          elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
          {
              $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=1')\">1</a>";
              $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=2')\">2</a>";
              $pagination.= "...";
              for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
              {
                  if ($counter == $page)
                      $pagination.= "<span class=\"current\">$counter</span>";
                  else
                      $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
              }
              $pagination.= "...";
              $pagination.= "<a >$lpm1</a>";
              $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$lastpage."')\">$lastpage</a>";
          }
          //close to end; only hide early pages
          else
          {
              $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=1')\">1</a>";
              $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=2')\">2</a>";
              $pagination.= "...";
              for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
              {
                  if ($counter == $page)
                      $pagination.= "<span class=\"current\">$counter</span>";
                  else
                      $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
              }
          }
      }

      //next button
      if ($page < $counter - 1)
          $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$next."')\">next </a>";
      else
          $pagination.= "<span class=\"disabled\">next  </span>";
      $pagination.= "</div>\n";
  }

//        echo $pagination.'hello';
 return $pagination;
 }
   function showPagination1($pagename,$tbl_name,$start,$limit,$page,$condition,$numres){

      $database = new MySQLDB;
      //your table name
      // How many adjacent pages should be shown on each side?
      $adjacents = 3;
		/*
         First get total number of rows in data table.
         If you have a WHERE clause in your query, make sure you mirror it here.
      */
      $query = "SELECT COUNT(*) as num FROM $tbl_name $condition ";
      $total_pages = mysql_fetch_array($database->query($query));
      $total_pages = $numres;



      /* Setup vars for query. */
      $targetpage = $pagename;     //your file name  (the name of this file)
      //how many items to show per page




      /* Setup page vars for display. */
      if ($page == 0) $page = 1;                    //if no page var is given, default to 1.
      $prev = $page - 1;                            //previous page is page - 1
      $next = $page + 1;                            //next page is page + 1
      $lastpage = ceil($total_pages/$limit);        //lastpage is = total pages / items per page, rounded up.
      $lpm1 = $lastpage - 1;                        //last page minus 1

      /*
          Now we apply our rules and draw the pagination object.
          We're actually saving the code to a variable in case we want to draw it more than once.
      */
      $pagination = "";
      if($lastpage > 1)
      {
         $pagination .= "<div class=\"pagination\">";
         //previous button
         if ($page > 1){

            $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$prev."')\">&laquo; previous</a>";
         }
         else{
            $pagination.= "<span class=\"disabled\">&laquo; previous</span>";
         }
         //pages
         if ($lastpage < 7 + ($adjacents * 2))    //not enough pages to bother breaking it up
         {
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
               if ($counter == $page)
                  $pagination.= "<span class=\"current\">$counter</span>";
               else
                  $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
            }
         }
         elseif($lastpage > 5 + ($adjacents * 2))    //enough pages to hide some
         {
            //close to beginning; only hide later pages
            if($page < 1 + ($adjacents * 2))
            {
               for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
               {
                  if ($counter == $page)
                     $pagination.= "<span class=\"current\">$counter</span>";
                  else
                     $pagination.= "<a   onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
               }
               $pagination.= "...";
               $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$lpm1."')\">$lpm1</a>";
               $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$lastpage."')\">$lastpage</a>";
            }
            //in middle; hide some front and some back
            elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
            {
               $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=1')\">1</a>";
               $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=2')\">2</a>";
               $pagination.= "...";
               for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
               {
                  if ($counter == $page)
                     $pagination.= "<span class=\"current\">$counter</span>";
                  else
                     $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
               }
               $pagination.= "...";
               $pagination.= "<a >$lpm1</a>";
               $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=".$lastpage."')\">$lastpage</a>";
            }
            //close to end; only hide early pages
            else
            {
               $pagination.= "<a onclick=\"setStateGet('adminTable','".$targetpage."','page=1')\">1</a>";
               $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=2')\">2</a>";
               $pagination.= "...";
               for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
               {
                  if ($counter == $page)
                     $pagination.= "<span class=\"current\">$counter</span>";
                  else
                     $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$counter."')\">$counter</a>";
               }
            }
         }

         //next button
         if ($page < $counter - 1)
            $pagination.= "<a  onclick=\"setStateGet('adminTable','".$targetpage."','page=".$next."')\">next &raquo;</a>";
         else
            $pagination.= "<span class=\"disabled\">next  &raquo;</span>";
         $pagination.= "</div>\n";
      }


      return $pagination;


   }

/*End Pagination*/


	function checkInput($pan)
	{

	   if (filter_var($pan, FILTER_VALIDATE_INT, array("options" => array("min_range"=>10000, "max_range"=>1999999))) === false && strlen($pan)<8) {
	   return 1;
	  }
		else
		{
		 return 0;
		}
	}
	function checkInput1($pan)
	{

	   if (filter_var($pan, FILTER_VALIDATE_INT, array("options" => array("min_range"=>1, "max_range"=>20))) === false) {
	   return 1;
	  }else
		{
			return 0;
		}
	}

	function sendsms($user,$pass,$msg,$sender,$mobile){
		global $database;
		$user='prisap';
		$pass='p31131';
		$mobile = '91'.substr($mobile,-10);
		$url = "http://bsms.entrolabs.com/spanelv2/api.php?username=".$user."&password=".$pass."&to=".$mobile."&from=".$sender."&message=".urlencode($msg);    //Store data into URL variable
		return $ret = @file($url);
	}

	function sendsms2($user,$pass,$msg,$sender,$mobile){

		global $database;
		$mobile = substr($mobile,-10);

		// Textlocal account details
		$username = 'entrolabsindia@gmail.com';
		$hash = $pass;

		// Message details
		$numbers = array($mobile);
		$sender = urlencode($sender);
		$message = rawurlencode($msg);

		$numbers = implode(',', $numbers);

		// Prepare data for POST request
		$data = array('username' => $username, 'apiKey' => 'ftlb3cldZs4-TIkUEBnUpgbwQHD7xFlr4wGX7iMXMB', 'numbers' => $numbers, "sender" => $sender, "message" => $message);


		// Send the POST request with cURL
		$ch = curl_init('https://api.textlocal.in/send/');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);


		// $database->query("INSERT INTO mlog VALUES(NULL, 'TEST SMS: ".$msg." - ".$mobile."' , '".$response."', '".time()."')");


		// Process your response here
		return $response;

	}



	/*imageResize - Resizes images of all types to the specified dimentions.
	 Preserves image alpha channel information also*/

	function image_resize($src, $dst, $width, $height, $crop=0){

		if(!list($w, $h) = getimagesize($src)) return "Unsupported picture type!";

		$type = strtolower(substr(strrchr($src,"."),1));
		if($type == 'jpeg') $type = 'jpg';
		switch($type){
			case 'bmp': $img = imagecreatefromwbmp($src); break;
			case 'gif': $img = imagecreatefromgif($src); break;
			case 'jpg': $img = imagecreatefromjpeg($src); break;
			case 'png': $img = imagecreatefrompng($src); break;
			default : return "Unsupported picture type!";
		}

		//resize
		if(is_array($crop)){
			// $_SESSION['crop'] = 'Is here.. reading crop array';
			$ratio = max($width/$w, $height/$h);

			if($w < $width or $h < $height){
				//$_SESSION['crop'].= "Picture is too small!".$ratio;
			}

			$h = $crop['height'];
			$x = $crop['x'];
			$w = $crop['width'];
			$y= $crop['y'];

		}
		else{
			if($w < $width and $h < $height) return "Picture is too small!";
			$ratio = min($width/$w, $height/$h);
			$width = $w * $ratio;
			$height = $h * $ratio;
			$x = 0;
			$y=0;
		  }

		$new = imagecreatetruecolor($width, $height);

	  // preserve transparency
	  if($type == "gif" or $type == "png"){
		imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
		imagealphablending($new, false);
		imagesavealpha($new, true);
	  }

	// $_SESSION['crop'].= "x:".$x."--y:".$y."--W:".$w."--H:".$h."--Width:".$width."--Height:".$height;
	  imagecopyresampled($new, $img, 0, 0, $x, $y, $width, $height, $w, $h);

	  switch($type){
		case 'bmp': imagewbmp($new, $dst); break;
		case 'gif': imagegif($new, $dst); break;
		case 'jpg': imagejpeg($new, $dst); break;
		case 'png': imagepng($new, $dst); break;
	  }
	  return true;
	}


	//Functions for all Includes
	function commonAdminJS(){
	?>
		<script type="text/javascript" src="<?php echo SECURE_PATH;?>frame/assets/js/modernizr.min.js"></script>
	<?php
	}

	function commonJS(){
	?>
		<script src="<?php echo SECURE_PATH;?>vendor/jquery/jquery.min.js"></script>
	   <script type="text/javascript" src="<?php echo SECURE_PATH;?>frame/js/nprogress.js"></script>
      <script type="text/javascript" src="<?php echo SECURE_PATH;?>frame/js/ajaxfunction.js"></script>
      <script type="text/javascript" src="<?php echo SECURE_PATH;?>frame/js/upload/fileuploader.js"></script>
	  <!-- 	<script src="https://www.wiris.net/demo/editor/editor"></script> 	-->
	   <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
	
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
   <script type="text/javascript" src="<?php echo SECURE_PATH;?>edut/tinymce4/tinymce.min.js"></script>
   <link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
   <script type="text/javascript" src="<?php echo SECURE_PATH;?>edut/js/wirislib.js"></script>
 

	

        <?php
   }
   function commonJS1(){
      ?>
         <script src="<?php echo SECURE_PATH;?>vendor/jquery/jquery.min.js"></script>
         <script type="text/javascript" src="<?php echo SECURE_PATH;?>frame/js/nprogress.js"></script>
         <script type="text/javascript" src="<?php echo SECURE_PATH;?>frame/js/ajaxfunction.js"></script>
         <script type="text/javascript" src="<?php echo SECURE_PATH;?>frame/js/upload/fileuploader.js"></script>
        <!-- 	<script src="https://www.wiris.net/demo/editor/editor"></script> 
         <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
         -->
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
      <script type="text/javascript" src="<?php echo SECURE_PATH;?>edut/tinymce4/tinymce.min.js"></script>
      <link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
      
           <?php
      }
	function commonFooterAdminJS(){
   ?>
      
		<!-- Bootstrap core JavaScript
      <script src="<?php echo SECURE_PATH;?>vendor/jquery/jquery.min.js"></script>-->
    <script src="<?php echo SECURE_PATH;?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?php echo SECURE_PATH;?>vendor/jquery-easing/jquery.easing.min.js"></script>

   

    <!-- Page level plugin JavaScript-->
    <script src="<?php echo SECURE_PATH;?>vendor/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo SECURE_PATH;?>vendor/datatables/dataTables.bootstrap4.js"></script>
	 

	 <!-- bootstrap-datetimepicker JS -->
    <script src="<?php echo SECURE_PATH;?>vendor/bootstrap-datetimepicker/js/moment.min.js"></script>
    <script src="<?php echo SECURE_PATH;?>vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="<?php echo SECURE_PATH;?>vendor/app/js/admin.js"></script>
   <!-- Demo scripts for this page-->
   <script src="<?php echo SECURE_PATH;?>vendor/app/js/custom-js/datatables-demo.js"></script> 
    

	 <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <!-- jQuery Custom Scroller CDN -->
    <script src="<?php echo SECURE_PATH;?>vendor/jquery-mousewheel-scrollbar/js/jquery.mCustomScrollbar.min.js"></script>

	<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script type="text/javascript"
        src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
		 <script src="<?php echo SECURE_PATH;?>vendor/app/js/bootstrap-multiselect.js"></script>
       <script type="text/javascript" src="<?php echo SECURE_PATH;?>plupload-master/js/plupload.full.min.js"></script>
		 <script type="text/javascript" src="<?php echo SECURE_PATH;?>plupload-master/js/jquery.ui.plupload/jquery.ui.plupload.js"></script>

   <?php
   }
   function commonHeader(){
   ?>
   <header class="header-area">
      <nav class="navbar navbar-expand navbar-light bg-admin">
         <div class="container">
            <button class="btn btn-light btn-circle text-theme order-1 order-sm-0" id="sidebarCollapse">
            <i class="material-icons text-theme md-18">more_vert</i>
            <!-- <i class="material-icons text-theme md-18">list</i> -->
            </button>
            <ul class="navbar-nav ml-auto ml-md-0">
                  <li class="nav-item dropdown no-arrow">
                     <a class="nav-link dropdown-toggle pt-1" href="#" id="userDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?php echo SECURE_PATH;?>vendor/images/user-profile.png" width="35" alt="profile-user" class="rounded-circle">
                        <div class="d-none d-xl-inline-block"><?php echo $this->username;?></div>
                     </a>
                     <div class="dropdown-menu dropdown-menu-right logout" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="#"><i class="material-icons">settings</i> Settings</a>
                        <a class="dropdown-item" href="#"><i class="material-icons">style</i> Activity Log</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo SECURE_PATH;?>indexout.php"><i
                              class="material-icons">exit_to_app</i>
                        Logout</a>
                     </div>
                  </li>
            </ul>
         </div>
      </nav>
</header>
            
   <?php
   }
	function commonAdminCSS(){
   ?>
      <link href="<?php echo SECURE_PATH;?>frame/js/upload/fileuploader.css" rel="stylesheet">
		<link href="<?php echo SECURE_PATH;?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="<?php echo SECURE_PATH;?>vendor/app/css/style.css" rel="stylesheet">
		 <link href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">

    <link href=" https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="<?php echo SECURE_PATH;?>vendor/jquery-mousewheel-scrollbar/css/jquery.mCustomScrollbar.min.css">

      <!-- bootstrap-datetimepicker CSS -->
      <link rel="stylesheet" href="<?php echo SECURE_PATH;?>vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css">

      <!-- Page level plugin CSS-->
      <link href="<?php echo SECURE_PATH;?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
      <link href="<?php echo SECURE_PATH;?>frame/css/nprogress.css" rel="stylesheet">
	   <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
		<link href="<?php echo SECURE_PATH;?>vendor/app/css/bootstrap-multiselect.css" rel="stylesheet">
	   <!-- Input Typehead-->
   


	   

   <?php
   }

	//check trough id
	function checkId($trough,$panchayat)
	{
		global $database;
		$t=$database->query("select id from swm_troughs where panchayat='".$panchayat."' and trough_id='".$trough."'");
		if(mysqli_num_rows($t)>0)
		{
		  $new_trough=$trough++;
		return $this->checkId($new_trough,$panchayat);
		}
			return $trough;
	}

	//function to get valid time
	function getValidTime($district)
	{
		global $database;
	   $date=time();


		$r=$database->query("select valid_time from valid_times where district='".$district."' and UNIX_TIMESTAMP(STR_TO_DATE(valid_time, '%d-%m-%Y'))<'".time()."' order by year desc");
		 $r1=mysqli_fetch_array($r);

				$year=strtotime($r1['valid_time'].' +1 year');

		  return date('d-m-Y',$year);


	}

	function gdc_title(){
	?>	

	    <title>RIZEE - The Perfect Guide</title>

	<?php	
	}

	function floorToFraction($number, $denominator = 1)
	{
		$x = $number * $denominator;
		$x = floor($x);
		$x = $x / $denominator;
		return $x;
	}

	function ceilToFraction($number, $denominator = 1)
	{
		$original  = $x = $number * $denominator;

		$x = ceil($x);

		$x = $x / $denominator;

		if(($x-$original) >= 0.25){
		  $x = $this->floorToFraction($number,$denominator);
		}

		return $x;
	}


	function findUniqueItem($food,$used,$index){

     if(in_array($food[$index]['id'],$used)){
		 $index++;

		if($index >= count($food)){
		  return 0;
		}
		else{
	   	  return $this->findUniqueItem($food,$used,$index);
		}
	 }
	 else{

	   return $index;
	 }


	}
	function checkHid($hid){
		global $database;

		$new_hid = $hid;

		$check_sel = $database->query("SELECT hid FROM house_tax WHERE hid = '".$hid."'");

	  if(mysqli_num_rows($check_sel) > 0){
			$new_hid = $hid.rand(111,999);
			$this->checkHid($new_hid);
	  }

		return $new_hid;
	}


	function checkMutation($hid,$panchayat){
		global $database;
		$new_hid = $hid;

		$mutation = 'M'.$panchayat.sprintf('%04d',$hid);


		$check_sel = $database->query("SELECT mutation_id FROM mutation_applications WHERE mutation_id = '".$mutation."'");

		if(mysqli_num_rows($check_sel) > 0){
			$hid++;
			return $this->checkMutation($hid,$panchayat);
		}

		return $mutation;
	}
	function checkGrievance($hid,$panchayat){
		global $database;
		$new_hid = $hid;
		$mutation = 'G'.$panchayat.sprintf('%04d',$hid);
		$check_sel = $database->query("SELECT issue_id FROM `grievances`  WHERE issue_id = '".$mutation."'");

		if(mysqli_num_rows($check_sel) > 0){
			$hid++;
			return $this->checkGrievance($hid,$panchayat);
		}

	return $mutation;
	}


	function checkRti($hid,$panchayat){
	  global $database;

	   $new_hid = $hid;

	   $mutation = 'RTI'.$panchayat.sprintf('%04d',$hid);


	  $check_sel = $database->query("SELECT rti_id FROM `rti_applications`  WHERE rti_id = '".$mutation."'");

	  if(mysqli_num_rows($check_sel) > 0){
			$hid++;
			return $this->checkRti($hid,$panchayat);
	  }

		return $mutation;
	}


	function activity($log,$user,$display){
	   global $database;

	  // $database->query("INSERT INTO log VALUES(NULL,'".$log." - IP:".$_SERVER['REMOTE_ADDR']."','".$user."','".time()."',".$display.",0)");
     $database->query("INSERT INTO user_log VALUES(NULL,'".$user."','".time()."','')");
	}




	function number_to_words($number){


		$no = round($number);
	   $point = round($number - $no, 2) * 100;
	   $hundred = null;
	   $digits_1 = strlen($no);
	   $i = 0;
	   $str = array();
	   $words = array('0' => '', '1' => 'one', '2' => 'two',
		'3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
		'7' => 'seven', '8' => 'eight', '9' => 'nine',
		'10' => 'ten', '11' => 'eleven', '12' => 'twelve',
		'13' => 'thirteen', '14' => 'fourteen',
		'15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
		'18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
		'30' => 'thirty', '40' => 'forty', '50' => 'fifty',
		'60' => 'sixty', '70' => 'seventy',
		'80' => 'eighty', '90' => 'ninety');
	   $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
	   while ($i < $digits_1) {
		 $divider = ($i == 2) ? 10 : 100;
		 $number = floor($no % $divider);
		 $no = floor($no / $divider);
		 $i += ($divider == 10) ? 1 : 2;
		 if ($number) {

			$counter = count($str);
			$plural = null;//(($counter = count($str)) && $number > 9) ? 's' : null;


			$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
			$str [] = ($number < 21) ? $words[$number] .
				" " . $digits[$counter] . $plural . " " . $hundred
				:
				$words[floor($number / 10) * 10]
				. " " . $words[$number % 10] . " "
				. $digits[$counter] . $plural . " " . $hundred;
		 } else $str[] = null;
	  }
	  $str = array_reverse($str);
	  $result = implode('', $str);
	  $points = ($point) ?
		"." . $words[$point / 10] . " " .
			  $words[$point = $point % 10] : '';
	  return $result  ;

	}
	function validateAadhar($aadhar){

		$aadharArray = array();



		for($i=0;$i < strlen($aadhar); $i++){
		   $aadharArray[$i] = substr($aadhar,$i,1);
		}

		if(!$this->CheckArrayByVerhoeffAlogirithm($aadharArray))
		return false;

		else if($aadharArray[0] == 1 || $aadharArray[0] == 0)
		return false;

		elseif($aadhar == '222222222222' || $aadhar == '333333333333' || $aadhar == '444444444444' || $aadhar == '555555555555' || $aadhar == '666666666666' || $aadhar == '777777777777' || $aadhar == '888888888888' || $aadhar == '999999999999')
		return false;

		else{
			 $part1 = substr($aadhar,0,4);
			 $part2 = substr($aadhar,4,4);
			 $part3 = substr($aadhar,8,4);

			 if($part1 == $part2 || $part1 == $part3 || $part2 == $part3)
			 return false;

		}

		return true;
	}
	function CheckArrayByVerhoeffAlogirithm($aadharArray){


				$op = array();
				$op[0] =  array( 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 );
				$op[1] =    array(  1, 2, 3, 4, 0, 6, 7, 8, 9, 5 );
				$op[2] =    array(  2, 3, 4, 0, 1, 7, 8, 9, 5, 6 );
				$op[3] =   array(  3, 4, 0, 1, 2, 8, 9, 5, 6, 7 );
				$op[4] =    array(  4, 0, 1, 2, 3, 9, 5, 6, 7, 8 );
				$op[5] =    array(  5, 9, 8, 7, 6, 0, 4, 3, 2, 1 );
				$op[6] =    array(  6, 5, 9, 8, 7, 1, 0, 4, 3, 2 );
			   $op[7] =    array(  7, 6, 5, 9, 8, 2, 1, 0, 4, 3 );
				$op[8] =    array(  8, 7, 6, 5, 9, 3, 2, 1, 0, 4 );
				$op[9] =    array(  9, 8, 7, 6, 5, 4, 3, 2, 1, 0 );

				$F = array();
				$F[0] =   array(  0, 1, 2, 3, 4, 5, 6, 7, 8, 9 );
				$F[1] =    array(  1, 5, 7, 6, 2, 8, 3, 0, 9, 4 );
				for ($i = 2; $i < 8; $i++)
				{
					for (  $j = 0; $j < 10; $j++)
						$F[$i][$j] = $F[$i - 1][$F[1][$j]];
				}




				// First we need to reverse the order of the input digits
				$reversedInput = array();
				for ($i = 0; $i < count($aadharArray); $i++)
					$reversedInput[$i] = $aadharArray[count($aadharArray) - ($i + 1)];

				$check = 0;
				for ($i = 0; $i < count($reversedInput); $i++)
					$check = $op[$check][$F[$i % 8][$reversedInput[$i]]];

				return ($check == 0);
	}


function getAadharInfo($aadhar){
	global $database;
	 $soap_request  = '<soapenv:Envelope   xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://srdhuidinfoservices/ecentric/com/xsd"><soapenv:Header /><soapenv:Body><xsd:getAadhaarInfo><xsd:UID>'.$aadhar.'</xsd:UID><xsd:EID>?</xsd:EID></xsd:getAadhaarInfo></soapenv:Body></soapenv:Envelope>';

  $header = array(
    "Content-type: text/xml;charset=\"utf-8\"",
    "Accept: text/xml",
    "Cache-Control: no-cache",
    "Pragma: no-cache",

    "Content-length: ".strlen($soap_request),
  );

  $soap_do = curl_init();
  curl_setopt($soap_do, CURLOPT_URL, "http://aadharinfo.ap.gov.in/SRDHAadhaarService/services/Services?wsdl" );
  curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
  curl_setopt($soap_do, CURLOPT_TIMEOUT,        10);
  curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
  curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
  curl_setopt($soap_do, CURLOPT_POST,           true );
  curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $soap_request);
  curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $header);


 $output = curl_exec($soap_do);


 $p1 = explode('<ns:return>',$output);

 if(count($p1) > 1){
 $p2 = explode('</ns:return>',$p1[1]);

  $xml_data =  htmlspecialchars_decode($p2[0]);


 $aadhardata = $this->xml2array($xml_data);
 return ($aadhardata[0]);

}
else{

	 $check_aadhar = $database->query("SELECT * FROM citizens WHERE aadhar = '".$aadhar."'");
			   if(mysqli_num_rows($check_aadhar) > 0){
				   $c_data = mysqli_fetch_assoc($check_aadhar);
				  $aadhardata[10][0]['value'] = $c_data['full_name'];
				  $aadhardata[2][0]['value'] = $c_data['father_name'];
				  $aadhardata[11][0]['value'] = $c_data['mobile'];

				  if($c_data['gender'] == 1)
				    $aadhardata[7][0]['value'] = 'M';
				  else if($c_data['gender'] == 0)
				    $aadhardata[7][0]['value'] = 'F';


				  return $aadhardata;
			   }

	else return array();
}
}

//Send app notioficcations
	function send_notifications($reg_ids, $message) {

   define("GOOGLE_API_KEY", "AIzaSyARRLCDdbRdOXCAlMogX78WT-GczH0VCkg");


        // Set POST variables
        $url = 'https://gcm-http.googleapis.com/gcm/send';

        $fields = array(
            'registration_ids' => ($reg_ids),
            'data' => ($message),
        );

        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );

        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE)
		{
            die('Curl failed: ' . curl_error($ch));
        }


        // Close connection
        curl_close($ch);

		return $result;

    }
    function GetFeild($tbl,$disp,$fld1,$val1,$fld2,$val2,$fld3,$val3,$fld4,$val4,$ord){
		
		if($fld1!="")
			$cond1=" AND ".$fld1."='".$val1."'";
		else
			$cond1="";

		if($fld2!="")
			$cond2=" AND ".$fld2."='".$val2."'";
		else
			$cond2="";

		if($fld3!="")
			$cond3=" AND ".$fld3."='".$val3."'";
		else
			$cond3="";

		if($fld4!="")
			$cond4=" AND ".$fld4."='".$val4."'";
		else
			$cond4="";

		if($ord!=""){
			$ord=" ORDER BY ".$ord;
		} else {
			$ord="";
		}

		$result=$database->query("SELECT ".$disp." FROM ".$tbl." WHERE slno!='' AND estatus='1'".$cond1.$cond2.$cond3.$cond4.$ord);	
		$count = $result->fetch_assoc();
		return $count[0];
	}
	function xml2array($xml){
		$opened = array();
		$opened[1] = 0;
		$xml_parser = xml_parser_create();
		xml_parse_into_struct($xml_parser, $xml, $xmlarray);
		$array = array_shift($xmlarray);
		unset($array["level"]);
		unset($array["type"]);
		$arrsize = sizeof($xmlarray);
		for($j=0;$j<$arrsize;$j++){
			$val = $xmlarray[$j];
			switch($val["type"]){
				case "open":
					$opened[$val["level"]]=0;
				case "complete":
					$index = "";
					for($i = 1; $i < ($val["level"]); $i++)
						$index .= "[" . $opened[$i] . "]";
					$path = explode('][', substr($index, 1, -1));
					$value = &$array;
					foreach($path as $segment)
						$value = &$value[$segment];
					$value = $val;
					unset($value["level"]);
					unset($value["type"]);
					if($val["type"] == "complete")
						$opened[$val["level"]-1]++;
				break;
				case "close":
				   if(isset($opened[$val["level"]-1])){
					$opened[$val["level"]-1]++;
				   }
					unset($opened[$val["level"]]);
				break;
			}
		}
		return $array;
	}

	function displayAadhar($aadhar){

		$a4 = substr($aadhar,0,4);

		$a2 = substr($aadhar,-1,2);

		return $a4.'XXXXXX'.$a2;

	}

	function displayMobile($mobile){

		$a4 = substr($mobile,0,4);

		$a2 = substr($mobile,-1,2);

		return $a4.'XXXXX'.$a2;
	}

};


/**
 * Initialize session object - This must be initialized before
 * the form object because the form uses session variables,
 * which cannot be accessed unless the session has started.
 */
$session = new Session;

/* Initialize form object */
$form = new Form;

?>
