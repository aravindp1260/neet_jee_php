<?php

define('SECURE_PATH','http://'.$_SERVER['HTTP_HOST'].'/');

     include('include/session.php');

header('Access-Control-Allow-Origin: *');  
header('Content-Type: application/json');

ini_set('display_errors', 'On');
error_reporting(E_ALL);


function filterqa($str){
  $str = urldecode($str);
  $res = str_replace("&nbsp;","",$str);

  return $res= getAllMath($res);

}

function getAllMath($text){



  $result = "";
  $part1 = explode("<math",$text);

  if(count($part1) > 0){

      //$result.= strip_tags($part1[0],'<p><img>');
  $result.= $part1[0];
      foreach($part1 as $first){
          $part2 = explode("</math>",$first);

          if(count($part2) > 1){
              $mml = "<math ".$part2[0]."</math>";


              $svg = json_decode(file_get_contents('http://imgsl.mylearningplus.in/neetjee/edut/tinymce4/plugins/tiny_mce_wiris/integration/showimage.php?mml='.urlencode($mml).'&lang=en'),true);


              $alt = "MathML Question";
              if(isset($svg['result']['alt']))
                  $alt = $svg['result']['alt'];
              $img= '<img src="data:image/svg+xml;base64,'.base64_encode($svg['result']['content']).'" data-mathml="'.str_replace('"','&quot;',$mml).'" class="Wirisformula" role="math" alt="'.$alt.'" style="vertical-align: -5px; height: '.$svg['result']['height'].'px; width: '.$svg['result']['width'].'px;"></img>';

              //$result.= $img.strip_tags($part2[1],'<p><img>');
      $result.= $img.$part2[1];
          }
      }
  }

  return $result;
}


function getAllMath1($text){



  $result = "";
  $part1 = explode("<math",$text);

  if(count($part1) > 0){

      //$result.= strip_tags($part1[0],'<p><img><b><br>');
      $result.= $part1[0];
      foreach($part1 as $first){
          $part2 = explode("</math>",$first);

          if(count($part2) > 1){
              $mml = "<math ".$part2[0]."</math>";
             
              $fields = array(
                  'mml' => $mml,
                  'lang'=>urlencode('en')
          );
          
          
          $url="http://imgsl.mylearningplus.in/neetjee/edut/tinymce4/plugins/tiny_mce_wiris/integration/showimagenew.php";
          
         $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         $retval = curl_exec($ch);
        curl_close($ch);
  
        $svg = json_decode($retval,true);


        $alt = "MathML Question";
        if(isset($svg['result']['alt']))
            $alt = $svg['result']['alt'];



       // $img= '<img src="'.($svg['result']['content']).'"  class="Wirisformula" role="math" alt="'.$alt.'" style="vertical-align: -5px; " width="'.$svg['result']['width'].'" height="'.$svg['result']['height'].'"></img>';
       $img= "<img src='".($svg['result']['content'])."'  class='Wirisformula' role='math' alt='.$alt.' style='vertical-align: -5px; ' width='".$svg['result']['width']."' height='".$svg['result']['height']."'></img>";
        //$result.= $img.strip_tags($part2[1],'<p><img>');
        $result.= $img.$part2[1];
         
          }
      }
  }

  return $result;
}


$jsonData = array();

$jsonData['incoming'] = date('d M`y H:i:s');

if(isset($_REQUEST["uploadFile"])){

	$filename = rand().'.png';

	if( move_uploaded_file($_FILES["image"]["tmp_name"], 'files/'.$filename)){
		$jsonData['result'] = 'success';
	}else{
		$jsonData['result'] = 'failed';
	}

	$jsonData['filename'] = $filename;
}



if(isset($_REQUEST["uploadImage"])){
$jsonData['uploadImage'] = true;

$ext = explode('.',$_FILES['file']["name"]);

$filename = uniqid().'.'.end($ext);

$jsonData['filename'] = $filename;


 if( move_uploaded_file($_FILES["file"]["tmp_name"], 'files/'.$filename)){
  $jsonData['result'] = 'success';

 
 }else{

     $image_parts = explode(";base64,", $_REQUEST['stream']);

     $image_base64 = base64_decode($image_parts[1]);

     $file = 'files/' .$filename;

     try{

         file_put_contents($file, $image_base64);
  $jsonData['result'] = 'success';


     }
     catch(Exception $e) {
         echo 'Message: ' .$e->getMessage();
     }
}
}


if(isset($_REQUEST['getQuestion'])){

     $jsonData["getQuestion"] = true;
$jsonData['status'] = 'failed';
$jsonData['data'] = Array();


global $database;

   $question_sel = $database->query("SELECT cq.id, cq.compquestion,cq.mat_question,cq.question,cq.option1,cq.option2,cq.option3,cq.option4,cq.option5, cq.explanation,cq.inputquestion,cq.qtype,cq.list1type,cq.list2type FROM  createquestion cq WHERE  cq.id = ".$_REQUEST['qid']);


   if(mysqli_num_rows($question_sel) > 0){
        $jsonData['status'] = 'success';

       $qdata = mysqli_fetch_assoc($question_sel);

       if($qdata['qtype']=='5'){
            $comp_sel=$database->query("select * from compquestion where id='".$qdata['compquestion']."'");
            $comprow=mysqli_fetch_array($comp_sel);
            $jsonData['data']['compquestion'] = filterqa($comprow['compquestion']);
       }

         $jsonData['data']['id'] = $qdata['id'];
        
         $jsonData['data']['mat_question'] = filterqa($qdata['mat_question']);
         $jsonData['data']['question'] = filterqa($qdata['question']);
         $jsonData['data']['option1'] = filterqa($qdata['option1']);
         $jsonData['data']['option2'] = filterqa($qdata['option2']);
         $jsonData['data']['option3'] = filterqa($qdata['option3']);
         $jsonData['data']['option4'] = filterqa($qdata['option4']);
         $jsonData['data']['option5'] = filterqa($qdata['option5']);
         $jsonData['data']['explanation'] = filterqa($qdata['explanation']);
         $jsonData['data']['inputquestion'] = $qdata['inputquestion'];
         $jsonData['data']['list1type'] = $qdata['list1type'];
         $jsonData['data']['list2type'] = $qdata['list2type'];
            
}
/*$question_sel=$database->query("select a.question_id as id,a.question,a.option1,a.option2,a.option3,a.option4,a.option5,a.explanation,a.compquestion,a.mat_question,b.inputquestion,b.list1type,b.list2type from questiondata_app1  as a inner join createquestion as b on a.estatus='1' and b.estatus='1' and  a.question_id=b.id  and a.question_id='".$_REQUEST['qid']."'  order by b.timestamp DESC ");


   if(mysqli_num_rows($question_sel) > 0){
$jsonData['status'] = 'success';

           $qdata = mysqli_fetch_assoc($question_sel);

           $jsonData['data']['id'] = $qdata['id'];
           $jsonData['data']['compquestion'] = $qdata['compquestion'];
           $jsonData['data']['mat_question'] = $qdata['mat_question'];
           $jsonData['data']['question'] = $qdata['question'];
           $jsonData['data']['option1'] = $qdata['option1'];
           $jsonData['data']['option2'] = $qdata['option2'];
           $jsonData['data']['option3'] = $qdata['option3'];
           $jsonData['data']['option4'] = $qdata['option4'];
           $jsonData['data']['option5'] = $qdata['option5'];
           $jsonData['data']['explanation'] = $qdata['explanation'];
           $jsonData['data']['inputquestion'] = $qdata['inputquestion'];
           $jsonData['data']['list1type'] = $qdata['list1type'];
           $jsonData['data']['list2type'] = $qdata['list2type'];
            
}*/
   

}

 

echo json_encode($jsonData);

?>