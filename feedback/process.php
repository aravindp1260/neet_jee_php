<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
?>


	<?php
	if(isset($_REQUEST['tableDisplay'])){
		
		$con = mysqli_connect("localhost","rspace","Rsp@2019","neetjee");
		function query($sql){
		global $con;
		 return mysqli_query($con,$sql);
		}
	
	?>
		<style>
			.subjectanalysis {
				padding: .75rem 1.25rem;
				margin-bottom: 0;
				background-color: rgba(0,0,0,.03);
				border-bottom: 1px solid rgba(0,0,0,.125);
			}
			.subject-wise-analysis.nav-pills .nav-link.active {
				background-color:#000;
				color:#fff;
			}
			.subject-analysis-content label, h6 {
				color:#000;
			}
			.subject-analysis-content .table td p {
				color:#000000c7;
				font-weight:bold;
			}
			.subject-analysis-content .table th, .subject-analysis-content .table td {
				font-size:12px !important;
				padding:0.7rem 0.1rem !important;
			}
		</style>
		<style>
	
	 </style>
	<script>
	$('.chapter').selectpicker1();
	 </script>
		<section class="content-area">
			<div class="container">
				<div class="row Data-Tables">
					<div class="col-xl-12 col-lg-12"> 
						<div class="card border-0 shadow mb-4">
							<div class="card-header py-3">
								<h6 class="m-0 font-weight-bold text-primary">Feedback</h6>
							</div>
							<div class="card-body">
							
									<script type="text/javascript">


									  $('#dataTable').DataTable({
										"pageLength": 50,
										"order": [[ 1, 'asc' ]]
										
									  });

									</script>
									<div class="card border-0">
										<div class="card-body table-responsive table_data p-0">
											<table class="table table-bordered dashboard-table mb-0" id="dataTable">
												<thead class="thead-light">
													<tr>
														<th scope="col" class="text-center" >Student Name</th>
														<th scope="col" class="text-center" >Date</th>
														<th scope="col" class="text-center">Mobile</th>
														<th scope="col" class="text-center">Modules</th>
														<th scope="col" class="text-center">Rating</th>
														<th scope="col" class="text-center">Type</th>
														<th scope="col" class="text-center">Feedback</th>
														<th scope="col" class="text-center">Additional_information</th>
														
													</tr>
													
												</thead>
											<tbody>
												<?php
												$k=1;
												$sel=query("select * from student_feedback where estatus='1'");
												while($row=mysqli_fetch_array($sel)){
													$time=$row['timestamp'];
													$sel1=query("select * from student_users where  mobile='".$row['mobile']."'");
													$row1=mysqli_fetch_array($sel1);
													$modules='';
													$sel2=query("select * from app_modules where estatus='1' and  id in (".$row['modules'].")");
													while($row2=mysqli_fetch_array($sel2)){
														$modules.=$row2['module_name']."<br />";
														
													}
													$modulesd=trim($modules,",");
												?>
													<tr>
														<td><?php echo $row1['name'];?></td>
														<td class="text-left" data-order="<?php echo $time; ?>"><?php echo date('d/m/Y',$row['timestamp']);?></td>
														<td><?php echo $row['mobile']; ?></td>
														<td class="text-left" style="width:18px;"><?php echo rtrim($modulesd,","); ?></td>
														<td><?php echo $row['rating']; ?></td>
														<td><?php echo $row['type']; ?></td>
														<td><?php echo $row['feedback']; ?></td>
														<td><?php echo $row['additional_information']; ?></td>
													</tr>
												<?php $k++; } ?>
											</tbody>
										</table>
									
									
									</div>	
								</div>
								
								
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>
		
			
	<?php
	}
	
	?>
	
