<?php
include('../include/session.php');

ini_set('MAX_EXECUTION_TIME', '-1');
ini_set('display_errors',"0");

//error_reporting(E_ALL);

if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}

?>

	<script>
	$('#fromdate').datetimepicker();
	$('#todate').datetimepicker();
</script>
	
<?php

//Display bulkreport
if(isset($_REQUEST['tableDisplay'])){
	//Pagination code
 $limit=25;
if(isset($_REQUEST['page']))
{
	$start = ($_REQUEST['page'] - 1) * $limit;     //first item to display on this page
	$page=$_REQUEST['page'];
}
else
{
	$start = 0;      //if no page var is given, set start to 0
$page=0;
}
  //Search Form
?>
<?php
$tableName = 'institute_notifications';
  $condition = "";
  if(isset($_GET['keyword'])){
  }
  if(isset($_REQUEST['fromdate'])){
	  if($_REQUEST['fromdate']!='' && $_REQUEST['todate']!=''){
		  $datecon=" AND timestamp between ".strtotime($_REQUEST['fromdate'])." and ".strtotime($_REQUEST['todate'])."";
		$datecon1=" AND a.timestamp between ".strtotime($_REQUEST['fromdate'])." and ".strtotime($_REQUEST['todate'])."";
	  }else if($_REQUEST['fromdate']!='' && $_REQUEST['todate']==''){
		  $datecon=" AND timestamp between ".strtotime($_REQUEST['fromdate']." 00:00:00")." and ".strtotime($_REQUEST['fromdate']." 23:59:59")."";
		  $datecon1=" AND a.timestamp between ".strtotime($_REQUEST['fromdate']." 00:00:00")." and ".strtotime($_REQUEST['fromdate']." 23:59:59")."";

	  } else if($_REQUEST['fromdate']=='' && $_REQUEST['todate']!=''){
		  $datecon=" AND timestamp between ".strtotime($_REQUEST['todate']." 00:00:00")." and ".strtotime($_REQUEST['todate']." 23:59:59")."";
			$datecon1=" AND a.timestamp between ".strtotime($_REQUEST['todate']." 00:00:00")." and ".strtotime($_REQUEST['todate']." 23:59:59")."";
	  }else{
		
			
		$fdate = strtotime(date('d-m-Y',strtotime("-7 days")));;
		$tdate = time();
		$datecon=" and timestamp between ".$fdate." and ".$tdate."";
		$datecon1=" and a.timestamp between ".$fdate." and ".$tdate."";
		$_REQUEST['fromdate']=date("m/d/Y g:i A",$fdate);
		$_REQUEST['todate']=date("m/d/Y g:i A",$tdate);
	  }
  }else{
	 $fdate = strtotime(date('d-m-Y',strtotime("-7 days")));;
		$tdate = time();
		$datecon=" and timestamp between ".$fdate." and ".$tdate."";
		$datecon1=" and a.timestamp between ".$fdate." and ".$tdate."";
		$_REQUEST['fromdate']=date("m/d/Y g:i A",$fdate);
		$_REQUEST['todate']=date("m/d/Y g:i A",$tdate);
  }
  if(isset($_REQUEST['notification_title'])){
	  if($_REQUEST['notification_title']!=''){
		  if($_REQUEST['notification_title']=='Day 01 Notification'){
			$dayids='';
			$sqln=$database->query1("select id,day from institute_notifications_days where estatus='1' and day='1' ");
			while($rown=mysqli_fetch_array($sqln)){
				$dayids.=$rown['id'].",";
			}
			$title=" AND notification_day_id in (".rtrim($dayids,",").")";
			$title1=" AND a.notification_day_id in (".rtrim($dayids,",").")";

		}else if($_REQUEST['notification_title']=='Day 02 Notification'){
			$dayids='';
			$sqln=$database->query1("select id,day from institute_notifications_days where estatus='1' and day='2' ");
			while($rown=mysqli_fetch_array($sqln)){
				$dayids.=$rown['id'].",";
			}
			$title=" AND notification_day_id in (".rtrim($dayids,",").")";
			$title1=" AND a.notification_day_id in (".rtrim($dayids,",").")";
		}else if($_REQUEST['notification_title']=='Day 03 Notification'){
			$dayids='';
			$sqln=$database->query1("select id,day from institute_notifications_days where estatus='1' and day='3' ");
			while($rown=mysqli_fetch_array($sqln)){
				$dayids.=$rown['id'].",";
			}
			$title=" AND notification_day_id in (".rtrim($dayids,",").")";
			$title1=" AND a.notification_day_id in (".rtrim($dayids,",").")";
		}else{
			$title=" AND title like '%".$_REQUEST['notification_title']."%'";
			$title1=" AND a.title like '%".$_REQUEST['notification_title']."%'";
		  }
		 
	  }else{
		   $title="";
		    $title1="";
	  }
  }else{
	  $title="";
	  $title1="";
  }
     if(isset($_REQUEST['manual_automation'])){
	  if($_REQUEST['manual_automation']!=''){
		  if($_REQUEST['manual_automation']=='manual'){
			$manual_automation=" AND is_admin='1' ";
			$manual_automation1=" AND a.is_admin='1' ";
		  }else if($_REQUEST['manual_automation']=='automation'){
			$manual_automation=" AND is_admin!='1' ";
			$manual_automation1=" AND a.is_admin!='1' ";
		  }else{
			  $manual_automation="";
			  $manual_automation1="";
		  }
	  }else{
		   $manual_automation="";
		    $manual_automation1="";
	  }
  }else{
	  $manual_automation="";
	   $manual_automation1="";
  }
	?>

	<?php

	$condition="  estatus='1' ".$datecon.$title.$manual_automation."";
	$condition1="  a.estatus='1' ".$datecon1.$title1.$manual_automation1."";
	if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
	}
	//$query="select * from ".$tableName."  ".$condition." order by timestamp desc LIMIT $start,$limit";
	$query="SELECT id,title,notification_day_id,timestamp,is_admin,student_ids FROM ((SELECT id,title,notification_day_id,timestamp,is_admin,student_ids FROM institute_notifications  ".$condition." and notification_day_id=0 and is_admin!='4') UNION ALL (SELECT id,title,notification_day_id,timestamp,is_admin,student_ids FROM institute_notifications  ".$condition." and notification_day_id=0 and is_admin='4' GROUP BY DATE(FROM_UNIXTIME(timestamp))) UNION ALL (SELECT a.id as id,a.title as title,b.notification_day_id as notification_day_id,a.timestamp as timestamp,a.is_admin as is_admin,a.student_ids as student_ids FROM institute_notifications as a inner join notification_log as b on ".$condition1." and a.notification_day_id!=0 and a.id=b.notification_id and a.notification_day_id=b.notification_day_id group by b.batch_id))t order by timestamp desc limit $start,$limit";
	//echo $query;
	$query2="SELECT id,title,notification_day_id,timestamp,is_admin,student_ids FROM ((SELECT id,title,notification_day_id,timestamp,is_admin,student_ids FROM institute_notifications  ".$condition." and notification_day_id=0 and is_admin!='4') UNION ALL (SELECT id,title,notification_day_id,timestamp,is_admin,student_ids FROM institute_notifications  ".$condition." and notification_day_id=0 and is_admin='4' GROUP BY DATE(FROM_UNIXTIME(timestamp)))  UNION ALL (SELECT a.id as id,a.title as title,b.notification_day_id as notification_day_id,a.timestamp as timestamp,a.is_admin as is_admin,a.student_ids as student_ids FROM institute_notifications as a inner join notification_log as b on ".$condition1." and a.notification_day_id!=0 and a.id=b.notification_id and a.notification_day_id=b.notification_day_id group by b.batch_id))t order by timestamp desc ";
	
	$q="SELECT id,title,notification_day_id,timestamp,is_admin,student_ids FROM ((SELECT id,title,notification_day_id,timestamp,is_admin,student_ids FROM institute_notifications  ".$condition." and notification_day_id=0 and is_admin!='4') UNION ALL (SELECT id,title,notification_day_id,timestamp,is_admin,student_ids FROM institute_notifications  ".$condition." and notification_day_id=0 and is_admin='4' GROUP BY DATE(FROM_UNIXTIME(timestamp))) UNION ALL (SELECT a.id as id,a.title as title,b.notification_day_id as notification_day_id,a.timestamp as timestamp,a.is_admin as is_admin,a.student_ids as student_ids FROM institute_notifications as a inner join notification_log as b on ".$condition1." and a.notification_day_id!=0 and a.id=b.notification_id and a.notification_day_id=b.notification_day_id group by b.batch_id))t order by timestamp desc";
	
  $result_sel = $database->query1($q);
  $numres = mysqli_num_rows($result_sel);

	$data_sel = $database->query1($query);
	
$pagination = $session->showPaginationnew(SECURE_PATH."notification_report/process.php?tableDisplay=1&fromdate=".$_REQUEST['fromdate']."&todate=".$_REQUEST['todate']."&notification_title=".$_REQUEST['notification_title']."",$tableName,$start,$limit,$page,$condition,$q);
 

 
	?>
	
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">Notification Details</h6>
						</div>

						<div class="card-body">
							
							
							
							<div class="row">
									<div class="col-lg-6">
										<div class="form-group row">
										  <label class="col-sm-4 col-form-label">From Date</label>
											<div class="col-sm-8">
											 
												<input type="text" name="date" id="fromdate" class="form-control"  value="<?php if(isset($_REQUEST['fromdate'])){ echo $_REQUEST['fromdate'];}else{}?>"  >
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group row">
										  <label class="col-sm-4 col-form-label">To Date</label>
											<div class="col-sm-8">
											 
												<input type="text" name="date" id="todate" class="form-control"  value="<?php if(isset($_REQUEST['todate'])){ echo $_REQUEST['todate'];}else{}?>"  >
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group row">
										  <label class="col-sm-4 col-form-label">Manual/Automation</label>
											<div class="col-sm-8">
											 
												<select id="manual_automation" class="form-control" name="manual_automation" >
													<option value=''>-Select-</option>
													<option value='manual' <?php if(isset($_REQUEST['manual_automation'])){  if($_REQUEST['manual_automation']=='manual'){ echo 'selected'; }else{ echo ''; }}?> >Manual</option>
													<option value='automation' <?php if(isset($_REQUEST['manual_automation'])){  if($_REQUEST['manual_automation']=='automation'){ echo 'selected'; }else{ echo ''; }}?>>Automation</option>
												</select>
											</div>
										</div>
									</div>
									
									<div class="col-lg-8">
										<div class="form-group row">
										  <label class="col-sm-3 col-form-label">Notifiation Title</label>
											<div class="col-sm-9">
											 
												<input type="text" name="notification_title" id="notification_title" class="form-control" autocomplete="off" value="<?php if(isset($_REQUEST['notification_title'])){ echo $_REQUEST['notification_title'];}else{}?>">
											</div>
										</div>
									</div>
									
									

									
								
									<div class="col-lg-3 pl-5">
										<div class="form-group row ">
											<a class="radius-20 btn btn-theme px-5 text-center" onClick="setState('adminTable','<?php echo SECURE_PATH;?>notification_report/process.php','tableDisplay=1&fromdate='+$('#fromdate').val()+'&todate='+$('#todate').val()+'&notification_title='+$('#notification_title').val()+'&manual_automation='+$('#manual_automation').val()+'')"> Search</a>
										</div>
									</div>
								</div>

							
							<?php  
								//echo $query;
								if(($start+$limit) > $numres){
									$onpage = $numres;
								}
								else{
									$onpage = $start+$limit;
								}
								if($numres > 0){ 
									
								?>
								
								<div class="my-3 footer-pagination d-flex justify-content-between align-items-right">
									<p></p>
									<p class="mb-0">Showing <?php echo ($start+1); ?> To <?php echo ($onpage); ?> results out of <?php echo $numres; ?></p>
								
								</div>
								<div class="table-responsive">
									<table class="table table-bordered"  width="100%" cellspacing="0" id="dataTable">
										<thead>
											<tr>
												<th rowspan="2">Sr.No</th>
												<th rowspan="2">Notification Title</th>
												<th rowspan="2">Automation / Manual</th>
												<th rowspan="2">Date</th>
												<th rowspan="2">Start Time</th>
												<th rowspan="2">End Time</th>
												<th rowspan="2">Sent To People</th>
												<th colspan="2">Received People</th>
												<th rowspan="2">Seen People</th>
											</tr>
											<tr>
												<th>Mobile</th>
												<th>Web</th>
												
											</tr>
											
										</thead>
										<tbody>
										<?php
										if(isset($_REQUEST['page'])){
											
											if($_REQUEST['page']==1)
											$i=1;
											else
											$i=(($_REQUEST['page']-1)*25)+1;
											}else $i=1;
											
										 while($value = mysqli_fetch_array($data_sel))
										  {
											
											 if($value['is_admin']=='1' && $value['title']!='Welcome to Rizee - The Perfect Guide'){
												 $type="Manual";
											 }else{
												  $type="Automation";
											 }
											$date1=date("m/d/Y",$value['timestamp']);
											if($value['notification_day_id']!='' && $value['notification_day_id']!='0' && $value['is_admin']=='2'){
												
												$sqllog=$database->query1("select batch_id from notification_log where estatus='1' and notification_id='".$value['id']."'");
												$rowlog=mysqli_fetch_array($sqllog);

												

												$sqlnoti1=$database->query1("select day from institute_notifications_days where estatus='1' and id='".$value['notification_day_id']."'");
												$rownoti1=mysqli_fetch_array($sqlnoti1);
												$mobilec=0;
												$sql_noti=$database->query1("SELECT a.id as id,b.mobile as mobile FROM institute_notifications as a inner join student_notifications as b on a.estatus='1' and  a.id=b.notification_id and a.timestamp between ".strtotime($date1." 00:00:00")." and ".strtotime($date1." 23:59:59")."  and a.notification_day_id='".$value['notification_day_id']."' ");
												$row_noti=mysqli_num_rows($sql_noti);
												while($row_notid=mysqli_fetch_array($sql_noti)){
													$sqlch=$database->query1("select id from student_users where estatus='1' and mobile='".$row_notid['mobile']."' and notification_token!=''");
													$rowch=mysqli_num_rows($sqlch);
													if($rowch>0){
														$mobilec++;
													}

												}
												$sql_seen=$database->query1("SELECT a.id as id FROM institute_notifications as a inner join student_notifications as b on a.estatus='1' and  a.id=b.notification_id and a.timestamp between ".strtotime($date1." 00:00:00")." and ".strtotime($date1." 23:59:59")."  and a.notification_day_id='".$value['notification_day_id']."'and b.seen_status='1' ");
												$row_seen=mysqli_num_rows($sql_seen);
												if($row_seen>0){
													$seencnt=$row_seen;
												}else{
													$seencnt=0;
												}
												$sqlnotisent=$database->query1("select id from institute_notifications where estatus='1' and notification_day_id='".$value['notification_day_id']."' and timestamp between ".strtotime($date1." 00:00:00")." and ".strtotime($date1." 23:59:59")." ");
												$cnt=mysqli_num_rows($sqlnotisent);

												if($rownoti1['day']=='1'){
													$title='Day 01 Notification';
												}else if($rownoti1['day']=='2'){
													$title='Day 02 Notification';
												}else if($rownoti1['day']=='3'){
													$title='Day 03 Notification';
												}else{
													
													$title=$value['title'];
												}
												$sql_notification1=$database->query1("SELECT b.timestamp as timestamp FROM institute_notifications as a inner join student_notifications as b on a.estatus='1' and  a.id=b.notification_id and a.timestamp between ".strtotime($date1." 00:00:00")." and ".strtotime($date1." 23:59:59")."  and a.notification_day_id='".$value['notification_day_id']."'  order by b.id asc limit 0,1");
												$row_notification1=mysqli_fetch_array($sql_notification1);
												$sql_notification11=$database->query1("SELECT b.timestamp as timestamp FROM institute_notifications as a inner join student_notifications as b on a.estatus='1' and  a.id=b.notification_id and a.timestamp between ".strtotime($date1." 00:00:00")." and ".strtotime($date1." 23:59:59")."  and a.notification_day_id='".$value['notification_day_id']."'  order by b.id desc limit 0,1");
												$row_notification11=mysqli_fetch_array($sql_notification11);
											}else{
												if($value['is_admin']=='4'){
													$title=$value['title'];
													$sqlnotisent=$database->query1("select id from institute_notifications where estatus='1' and is_admin='4' and title='".$value['title']."' and timestamp between ".strtotime($date1." 00:00:00")." and ".strtotime($date1." 23:59:59")." ");
													$cnt=mysqli_num_rows($sqlnotisent);

													$sql_notification1=$database->query1("SELECT b.timestamp as timestamp FROM institute_notifications as a inner join student_notifications as b on a.estatus='1' and a.is_admin='4' and title='".$value['title']."' and  a.id=b.notification_id and a.timestamp between ".strtotime($date1." 00:00:00")." and ".strtotime($date1." 23:59:59")." order by b.id asc limit 0,1");
													$row_notification1=mysqli_fetch_array($sql_notification1);
													$sql_notification11=$database->query1("SELECT b.timestamp as timestamp FROM institute_notifications as a inner join student_notifications as b on a.estatus='1' and a.is_admin='4' and title='".$value['title']."' and  a.id=b.notification_id and a.timestamp between ".strtotime($date1." 00:00:00")." and ".strtotime($date1." 23:59:59")." order by b.id desc limit 0,1");
													$row_notification11=mysqli_fetch_array($sql_notification11);
													$mobilec=0;
													$sql_noti=$database->query1("SELECT a.id as id,b.mobile as mobile FROM institute_notifications as a inner join student_notifications as b on a.estatus='1' and a.is_admin='4' and title='".$value['title']."' and  a.id=b.notification_id and a.timestamp between ".strtotime($date1." 00:00:00")." and ".strtotime($date1." 23:59:59")."  ");
													$row_noti=mysqli_num_rows($sql_noti);
													while($row_notid=mysqli_fetch_array($sql_noti)){
														$sqlch=$database->query1("select id from student_users where estatus='1' and mobile='".$row_notid['mobile']."' and notification_token!=''");
														$rowch=mysqli_num_rows($sqlch);
														if($rowch>0){
															$mobilec++;
														}

													}

													$sql_seen=$database->query1("SELECT a.id as id FROM institute_notifications as a inner join student_notifications as b on a.estatus='1' and a.is_admin='4' and title='".$value['title']."' and  a.id=b.notification_id and a.timestamp between ".strtotime($date1." 00:00:00")." and ".strtotime($date1." 23:59:59")." and b.seen_status='1' ");
													$row_seen=mysqli_num_rows($sql_seen);
													if($row_seen>0){
														$seencnt=$row_seen;
													}else{
														$seencnt=0;
													}
													

												}else{
													$mobilec=0;
													$sql_noti=$database->query1("select id,mobile from student_notifications where estatus='1' and notification_id='".$value['id']."' ");
													$row_noti=mysqli_num_rows($sql_noti);
													while($row_notid=mysqli_fetch_array($sql_noti)){
														$sqlch=$database->query1("select id from student_users where estatus='1' and mobile='".$row_notid['mobile']."' and notification_token!=''");
														$rowch=mysqli_num_rows($sqlch);
														if($rowch>0){
															$mobilec++;
														}

													}
													
													$title=$value['title'];
													$ids=explode(",",$value['student_ids']);
													$cnt=count($ids);


													$sql_seen=$database->query1("select id from student_notifications where estatus='1' and notification_id='".$value['id']."' and seen_status='1'");
													$row_seen=mysqli_num_rows($sql_seen);
													if($row_seen>0){
														$seencnt=$row_seen;
													}else{
														$seencnt=0;
													}
													$sql_notification1=$database->query1("select id,timestamp from student_notifications where estatus='1' and notification_id='".$value['id']."' order by id asc limit 0,1");
													$row_notification1=mysqli_fetch_array($sql_notification1);
													$sql_notification11=$database->query1("select id,timestamp from student_notifications where estatus='1' and notification_id='".$value['id']."' order by id desc limit 0,1");
													$row_notification11=mysqli_fetch_array($sql_notification11);
												}
											}
											$mobilecnt=$mobilec;
											if($mobilecnt>0){
												$mobilecnt=$mobilecnt;
											}else{
												$mobilecnt=0;
											}
											$webcnt=$row_noti-$mobilecnt;
											if($webcnt>0){
												$webcnt=$webcnt;
											}else{
												$webcnt=0;
											}

											
											?>
											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $title; ?></td>
												<td><?php echo $type; ?></td>
												<td><?php echo date("d/m/Y H:i:s",$value['timestamp']); ?></td>
												<td><?php echo date("d/m/Y H:i:s",$row_notification1['timestamp']); ?></td>
												<td><?php echo date("d/m/Y H:i:s",$row_notification11['timestamp']); ?></td>
												<td><a style="cursor:pointer;color:#156a92;" data-toggle="modal" data-target="#exampleModal"onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>notification_report/prnt.php','viewdetails=1&id=<?php echo $value['id'];?>')"><?php echo $cnt; ?></a></td>
												
												<td><a style="cursor:pointer;color:#156a92;" data-toggle="modal" data-target="#exampleModal1"onClick="setStateGet('viewDetails1','<?php echo SECURE_PATH;?>notification_report/prnt1.php','viewdetails1=1&id=<?php echo $value['id'];?>')"><?php echo $mobilecnt; ?></a></td>
												<td><a style="cursor:pointer;color:#156a92;" data-toggle="modal" data-target="#exampleModal1"onClick="setStateGet('viewDetails1','<?php echo SECURE_PATH;?>notification_report/prnt3.php','viewdetails1=1&id=<?php echo $value['id'];?>')"><?php echo $webcnt; ?></a></td>
												<td><a style="cursor:pointer;color:#156a92;" data-toggle="modal" data-target="#exampleModal2"onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>notification_report/prnt2.php','viewdetails2=1&id=<?php echo $value['id'];?>')"><?php echo $seencnt; ?></a></td>
												
											</tr>
											<?php

											$i++;
										  }	
										  ?>
											
										</tbody>
									</table>
								</div>
								<div class="my-3 footer-pagination d-flex justify-content-between align-items-center">
									<p class="mb-0">Showing <?php echo ($start+1); ?> To <?php echo ($onpage); ?> results out of <?php echo $numres; ?></p>
									<?php echo $pagination ;?>
											
								</div>
							</div>
							<?php
							}
							else{
							?>
								<div class="text-danger text-center">No Results Found</div>
						  <?php
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
  </section>
 
	<?php

}

	?>
	
	<script type="text/javascript"> 
    function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }
</script>
