<?php
include('../include/session.php');

ini_set('MAX_EXECUTION_TIME', '-1');
ini_set('display_errors',"0");
include('../mongophp/vendor/autoload.php');
//error_reporting(E_ALL);

if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}

// phpinfo();
//mongodb://rspace:Rsp%402020!@127.0.0.1:27017/rizee
$user = "rspace";
$pwd = 'Rsp@2020';
$db = 'rizee';

$conn=mysqli_connect('localhost','rspace','Rsp@2019','neetjee') ;
//$conn=mysqli_connect('localhost','root','','neetjee') ;

function query($sql)
{
    global $conn;
    return mysqli_query($conn,$sql);
}

    try {

        $manager = new MongoDB\Driver\Manager("mongodb://rspace:Rsp%402020!@10.142.0.5:27017/rizee");
		

    }
    catch (MongoDB\Driver\Exception\Exception $e) {

        $filename = basename(__FILE__);

        echo "The $filename script has experienced an error.\n";
        echo "It failed with the following exception:\n";

        echo "Exception:", $e->getMessage(), "\n";
        echo "In file:", $e->getFile(), "\n";
        echo "On line:", $e->getLine(), "\n";
    }


?>


<script>
	$(function () {
		$('.datepicker').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});

	</script>
		
	

<?php

//Display bulkreport
if(isset($_GET['tableDisplay'])){
	//Pagination code
  $limit=50;
  if(isset($_REQUEST['page']))
  {
    $start = ($_REQUEST['page'] - 1) * $limit;     //first item to display on this page
    $page=$_REQUEST['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
if(isset($_REQUEST['exam_id'])){
	if($_REQUEST['exam_id']!=''){
		$idcon=" And id='".$_REQUEST['exam_id']."'";
		$idcon1=" And schedule_exam_id='".$_REQUEST['exam_id']."'";
	}else{
		$idcon="";
		$idcon1="";
	}
}else{
	$idcon="";
	$idcon1="";
}
   $collection =  new \MongoDB\Collection($manager,$db, 'exam_registrations');

   $data_sel = $collection->find();

$data = array();
//echo "Printing ".$collection->count()." rows";

$rows = $collection->find()->toArray();

	?>
	 <script type="text/javascript">


$('#dataTable').DataTable({
  "pageLength": 25,
  "order": [[ 3, 'desc' ]]
  
});

</script>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">Mock Test Report Details</h6>
						</div>

						<div class="card-body">
						<div class="row ">
								<div class="col-md-12">
									<label class="m-0 font-weight-bold">Overall Mocktest Summary</label>
									<table class="table table-bordered mt-2">
										<thead>
										<tr align="center">
												<th>Registered</th>
												<th>Attempted</th>
												<th>Directly Attempted</th>
												<th>Completed</th>
												
												
											</tr>
							
										</thead>
										<tbody>
											
											<?php
												
											// $sqlneet=query("select * from exam_paper where estatus='1' and institution_id='19' and type='test_series' and exam_type='1' and publish='1' ");
											//while($rowneet=mysqli_fetch_array($sqlneet)){
												$sqlattempt=query("select id from student_exam_sessions where estatus='1' and type='schedule_exam' and sub_type='test_series' group by mobile");
												$rowattempt=mysqli_num_rows($sqlattempt);
												$sqlcomplete=query("select id from student_exam_sessions where estatus='1' and type='schedule_exam' and sub_type='test_series' and is_completed='1'  group by mobile");
												$rowcomplete=mysqli_num_rows($sqlcomplete);
												$con1 = array();
												$collection1 =  new \MongoDB\Collection($manager,$db, 'exam_registrations');

												$regd = $collection1->count($con1);
												$data_sel = $collection1->find($con1);
												$regdn=0;
												foreach($data_sel as $datasel2){
													$sqlnattempt=query("select id from student_exam_sessions where estatus='1' and schedule_exam_id='".$datasel2->exam_id."' and mobile='".$datasel2->username."' and sub_type='test_series' ");
													$rownattempt=mysqli_num_rows($sqlnattempt);
													if($rownattempt>0){

														$regdn=$regdn+1;
													}
												}	
												
												$rownatattempt=$regd-$regdn;
												if($rownatattempt>0){
													$rownatattempt=$rownatattempt;
												}else{
													$rownatattempt=0;
												}
												$regd_attempt=$regd-$rownatattempt;
												$regd_direct_attempt=$rowattempt-$regd_attempt;
												?>
												<tr> 
													<td class="text-center"><?php echo $regd; ?></td>
													<td class="text-center"><?php echo $regd_attempt; ?></td>
													<td class="text-center"><?php echo $regd_direct_attempt; ?></td>
													<td class="text-center"><?php echo $rowcomplete; ?></td>
													
												</tr>
											 <?php
										//	}
											
												?>
											
										</tbody>
									</table>
								</div>
								
							</div>
							
							<div class="row ">
								<div class="col-md-12">
									<label class="m-0 font-weight-bold">NEET Mocktest Summary</label>
									<table class="table table-bordered mt-2">
										<thead>
										<tr align="center">
												<th>Mocktest Name</th>
												<th>Schedule Date</th>
												<th>Registered</th>
												<th>Completed Register Users</th>
												<th>Not Attempted</th>
												<th>Completed Direct Users</th>
												<th>Total Attempted</th>
												<th>Completed</th>
												
												
											</tr>
							
										</thead>
										<tbody>
											
											<?php
												
											 $sqlneet=query("select * from exam_paper where estatus='1' and institution_id='19' and type='test_series' and exam_type='1' and publish='1' ");
											while($rowneet=mysqli_fetch_array($sqlneet)){
												$sqlattempt=query("select id from student_exam_sessions where estatus='1' and schedule_exam_id='".$rowneet['id']."' and sub_type='test_series' group by mobile");
												$rowattempt=mysqli_num_rows($sqlattempt);
												$sqlcomplete=query("select id from student_exam_sessions where estatus='1' and schedule_exam_id='".$rowneet['id']."' and sub_type='test_series' and is_completed='1'  group by mobile");
												$rowcomplete=mysqli_num_rows($sqlcomplete);
												$con1 = array("exam_id" => (int) $rowneet['id']);
												$collection1 =  new \MongoDB\Collection($manager,$db, 'exam_registrations');

												$regd = $collection1->count($con1);
												$data_sel = $collection1->find($con1);
												$regdn=0;
												foreach($data_sel as $datasel2){
													$sqlnattempt=query("select id from student_exam_sessions where estatus='1' and schedule_exam_id='".$datasel2->exam_id."' and mobile='".$datasel2->username."' and sub_type='test_series' ");
													$rownattempt=mysqli_num_rows($sqlnattempt);
													if($rownattempt>0){

														$regdn=$regdn+1;
													}
												}	
												
												$rownatattempt=$regd-$regdn;
												if($rownatattempt>0){
													$rownatattempt=$rownatattempt;
												}else{
													$rownatattempt=0;
												}
												$regd_attempt=$regd-$rownatattempt;
												$regd_direct_attempt=$rowattempt-$regd_attempt;
												$sqlattempttotal=query("select id from exam_status where exam_paper_id='".$rowneet['id']."' and type='schedule_exam' group by mobile");
												$rowattempttotal=mysqli_num_rows($sqlattempttotal);
												?>
												<tr> 
													<td><a onclick="setStateGet('tabledata','<?php echo SECURE_PATH;?>mocktest_report/ajax.php','tabledata=1&exam_id=<?php echo $rowneet['id']; ?>')" style="cursor:pointer;color: #3c71ab;"><?php echo $rowneet['exam_name']; ?></a></td>
													<td><?php echo date('d-M-Y H:i:s',$rowneet['start_time']); ?></td>
													<td class="text-center"><?php echo $regd; ?></td>
													<td class="text-center"><?php echo $regd_attempt; ?></td>
													<td class="text-center"><?php echo $rownatattempt; ?></td>
													<td class="text-center"><?php echo $regd_direct_attempt; ?></td>
													<td class="text-center"><?php echo $rowattempttotal; ?></td>
													<td class="text-center"><?php echo $rowcomplete; ?></td>
													
												</tr>
											 <?php
											}
											
												?>
											
										</tbody>
									</table>
								</div>
								
							</div>
							<div class="row ">
								<div class="col-md-12">
									<label class="m-0 font-weight-bold">JEE Mocktest Summary</label>
									<table class="table table-bordered mt-2">
										<thead>
										<tr align="center">
												<th>Mocktest Name</th>
												<th>Schedule Date</th>
												<th>Registered</th>
												<th>Completed Register Users</th>
												<th>Not Attempted</th>
												<th>Completed Direct Users</th>
												<th>Total Attempted</th>
												<th>Completed</th>
												
											</tr>
							
										</thead>
										<tbody>
											
												<?php
											
											 
											 $sqljee=query("select * from exam_paper where estatus='1' and institution_id='19' and type='test_series' and exam_type='2' and publish='1' ");
											while($rowjee=mysqli_fetch_array($sqljee)){
												$sqlattempt1=query("select id from student_exam_sessions where estatus='1' and schedule_exam_id='".$rowjee['id']."' and sub_type='test_series' group by mobile");
												$rowattempt1=mysqli_num_rows($sqlattempt1);
												$sqlcomplete1=query("select id from student_exam_sessions where estatus='1' and schedule_exam_id='".$rowjee['id']."' and sub_type='test_series' and is_completed='1'  group by mobile");
												$rowcomplete1=mysqli_num_rows($sqlcomplete1);
												$con2 = array("exam_id" => (int) $rowjee['id']);
												$collection3 =  new \MongoDB\Collection($manager,$db, 'exam_registrations');

												$regd1 = $collection3->count($con2);
												$data_sel1 = $collection3->find();
												$regd2=0;
												foreach($data_sel1 as $datasel2){
													if($datasel2['exam_id']==$rowjee['id']){
														$sqlnattempt=query("select id from student_exam_sessions where estatus='1' and schedule_exam_id='".$datasel2->exam_id."' and mobile='".$datasel2->username."' and sub_type='test_series' ");
														$rownattempt=mysqli_num_rows($sqlnattempt);
														if($rownattempt>0){
															$regd2=$regd2+1;
														}
													}
												
												}

												
												$rownatattempt1=$regd1-$regd2;
												if($rownatattempt1>0){
													$rownatattempt1=$rownatattempt1;
												}else{
													$rownatattempt1=0;
												}

												
												$regd_attempt1=$regd1-$rownatattempt1;
												$regd_direct_attempt1=$rowattempt1-$regd_attempt1;
												$sqlattempttotal=query("select id from exam_status where exam_paper_id='".$rowjee['id']."' and type='schedule_exam' group by mobile");
												$rowattempttotal=mysqli_num_rows($sqlattempttotal);
												?>
												<tr> 
													<td ><a onclick="setStateGet('tabledata','<?php echo SECURE_PATH;?>mocktest_report/ajax.php','tabledata=1&exam_id=<?php echo $rowjee['id']; ?>')" style="cursor:pointer;color: #3c71ab;"><?php echo $rowjee['exam_name']; ?></a></td>
													<td><?php echo date('d-M-Y H:i:s',$rowjee['start_time']); ?></td>
													<td class="text-center"><?php echo $regd1; ?></td>
													<td class="text-center"><?php echo $regd_attempt1; ?></td>
													<td class="text-center"><?php echo $rownatattempt1; ?></td>
													<td class="text-center"><?php echo $regd_direct_attempt1; ?></td>
													<td class="text-center"><?php echo $rowattempttotal; ?></td>
													<td class="text-center"><?php echo $rowcomplete1; ?></td>
													

												</tr>
											 <?php
											}									?>
											
										</tbody>
									</table>
								</div>
								
							</div>
							
							
							 
								
								
								<div class="table-responsive" id="tabledata">
									<!-- <div class="d-flex justify-content-between text-right">
										<a></a>
										<a href="<?php echo SECURE_PATH;?>mocktest_report/excel.php?exam_id=<?php echo $_REQUEST['exam_id']; ?>" style="float:right"  target="_blank" title="Export To Excel" ><img src="../vendor/images/excelicon.png" width="25px " ></img></a>

									</div> -->
									<!-- <table class="table table-bordered"  width="100%" cellspacing="0" id="dataTable">
										<thead>
											<tr>
												<th>Mobile</th>
												<th>Name</th>
												<th>Exam Type</th>
												<th>User Created on</th>
												<th>Plan Id</th>
												<th>Source</th>
												<th>Exam Name</th>
												<th>Exam Date</th>
												<th>Correct</th>
												<th>Wrong</th>
												<th>Not answered</th>
												<th>Total Questions</th>
												<th>Correct Marks</th>
												<th>Negative Marks</th>
												<th>Total Marks</th>
												<th>Date</th>
												
											</tr>
											
										</thead>
										<tbody>
										<?php
											
										//$rowcnt=mysqli_num_rows($data_sel);
										 //echo $rowcnt;

										  /*foreach ($rows as $row) {
											if(strlen($idcon1)==0){	
												$exam = query("SELECT * FROM exam_paper WHERE id=".$row->exam_id."  and estatus='1' ".$idcon."");
												$examdatacnt=mysqli_num_rows($exam);
												$examdata = mysqli_fetch_assoc($exam);
												$user = query("SELECT * FROM student_users WHERE mobile=".$row->username);
												$usercnt=mysqli_num_rows($user);
												if($usercnt>0 && $examdatacnt>0){
												$userdata = mysqli_fetch_assoc($user);
												
												$sqlrow=query("select id from `student_users` where estatus='1' and exam_id='".$row->exam_type."'  " );
												$rowe=mysqli_num_rows($sqlrow);
												$sqlexam=query("select id,correct,wrong,not_answered,total_questions,timestamp from `student_exam_sessions` where estatus='1' and mobile='".$userdata['mobile']."' and schedule_exam_id='".$row->exam_id."' and sub_type='test_series'" );
												$rowexam=mysqli_fetch_array($sqlexam);
												
												$sqlexam1=query("select id from `student_questions` where estatus='1' and mobile='".$userdata['mobile']."' and extra_question='0' and exam_session_id='".$rowexam['id']."' and status='2'" );
														$rowexam1=mysqli_num_rows($sqlexam1);
													$examtype = query("SELECT * FROM exam WHERE id=".$examdata['exam_type']."");
													$examdata1 = mysqli_fetch_assoc($examtype);
											
											
													
													$plan = query("SELECT * FROM student_plans WHERE id=".$userdata['current_plan_id']);
													$plandata = mysqli_fetch_assoc($plan);
													if($userdata['userlevel'] == 1)
														$user_type = "Institute User";
														else
															$user_type = "Registered User";
														
														
														
															$correct=$rowexam1*4;
														
														
															$sqlexam2=query("select id from `student_questions` where estatus='1' and mobile='".$userdata['mobile']."' and extra_question='0' and exam_session_id='".$rowexam['id']."' and schedule_exam_id='".$row->exam_id."' and status='1'" );
															$rowexam2=mysqli_num_rows($sqlexam2);
															$wrong=$rowexam2;
															$tmarks=$correct-$wrong;	
	
															$sqlc=query("select id from student_questions where estatus='1' and mobile='".$userdata['mobile']."' and extra_question='0' and schedule_exam_id='".$row->exam_id."'  and exam_session_id='".$rowexam['id']."' " );
															$rowc=mysqli_num_rows($sqlc);
															$nt=$rowc-($rowexam1+$wrong);
															if($nt>0){
																$nt1=$nt;
															}else{
																$nt1=0;
															}
															if($rowexam['timestamp']!=''){
																$timestamp=$rowexam['timestamp'];
																$timestamp1=date('d-M-Y H:i:s',$rowexam['timestamp']);
															}else{
																$timestamp='';
																$timestamp1='';
															}
															$correct=$rowexam1*4;
															$sqlexam1=query("select id from `student_questions` where estatus='1' and mobile='".$userdata['mobile']."' and extra_question='0' and exam_session_id='".$rowexam['id']."' and schedule_exam_id='".$row->exam_id."' and status='2'" );
															$rowexam1=mysqli_num_rows($sqlexam1);
															if($rowexam['id']!=''){
																
																$rowexam2=$rowexam1;
																$wrong2=$wrong;
																$nt2=$nt1;
																$rowc2=$rowc;
																
																$correct2=$correct;
																$tmarks=$correct-$wrong;
															}else{
																$rowexam2=0;
																$wrong2=0;
																$nt2=0;
																$rowc2=0;
																$correct2=0;
																$tmarks=0;
															}

														
														
													?>
													<tr>
														<td><?php echo $userdata['mobile']; ?></td>
														<td><?php echo $userdata['name']; ?></td>
														<td><?php echo $examdata1['exam']; ?></td>
														
														<td data-order="<?php echo $userdata['timestamp']; ?>"><?php echo date('d-M-Y H:i:s',$userdata['timestamp']); ?></td>
														<td><?php echo $plandata['plan_name']; ?></td>
														<?php
													
														if($userdata['source']=='2'){
															$srcsel=query("select * from registration_src where  username='".$userdata['mobile']."'");
															$rowc=mysqli_num_rows($srcsel);
															if($rowc>0){
																$rowsel=mysqli_fetch_array($srcsel);
																?>
																<td class="text-left"><?php echo $rowsel['src']; ?></th>		
																<?php
															}else{
																?>
																<td class="text-left">Other</th>		
																<?php
															}
														}else if($userdata['source']=='0'){
															?>
															<td class="text-left">Web</th>		
															<?php
														}else if($userdata['source']=='1'){
															?>
															<td class="text-left">Mobile</th>		
															<?php
														}else{
															?>
															<td class="text-left"></th>		
															<?php
														}

															
															?>
														<td><?php echo $examdata['exam_name']; ?></td>
														<td><?php echo date('d-M-Y H:i:s',$examdata['start_time']); ?></td>
														<td><?php echo $rowexam2; ?></td>
														<td><?php echo $wrong2; ?></td>
														<td><?php echo $nt2; ?></td>
														<td><?php echo $rowc2; ?></td>
														<td><?php echo $correct2; ?></td>
														<td><?php echo $wrong2; ?></td>
														<td><?php echo $tmarks; ?></td>
														<td data-order="<?php echo $timestamp1; ?>"><?php echo $timestamp1; ?></td>
														
														</tr>
													<?php
													
												}
											}else{
												$exam = query("SELECT * FROM exam_paper WHERE id=".$row->exam_id."   and estatus='1' ");
												$examdata = mysqli_fetch_assoc($exam);
												if($row->exam_id==$_REQUEST['exam_id']){
													$user = query("SELECT * FROM student_users WHERE mobile=".$row->username);
													$userdata = mysqli_fetch_assoc($user);
													$sqlrow=query("select id from `student_users` where estatus='1' and exam_id='".$row->exam_type."'  " );
													$rowe=mysqli_num_rows($sqlrow);
													$sqlexam=query("select id,correct,wrong,not_answered,total_questions,timestamp from `student_exam_sessions` where estatus='1' and mobile='".$userdata['mobile']."' and schedule_exam_id='".$_REQUEST['exam_id']."' and sub_type='test_series'" );
													$rowexam=mysqli_fetch_array($sqlexam);
														
													
													$examtype = query("SELECT * FROM exam WHERE id=".$examdata['exam_type']."");
													$examdata1 = mysqli_fetch_assoc($examtype);
											
											
													$sqlexam1=query("select id from `student_questions` where estatus='1' and mobile='".$userdata['mobile']."' and extra_question='0' and exam_session_id='".$rowexam['id']."' and status='2'" );
														$rowexam1=mysqli_num_rows($sqlexam1);
													$plan = query("SELECT * FROM student_plans WHERE id=".$userdata['current_plan_id']);
													$plandata = mysqli_fetch_assoc($plan);
													if($userdata['userlevel'] == 1)
														$user_type = "Institute User";
														else
															$user_type = "Registered User";
														
														
														
														
														
														$sqlexam2=query("select id from `student_questions` where estatus='1' and mobile='".$userdata['mobile']."' and extra_question='0' and exam_session_id='".$rowexam['id']."' and schedule_exam_id='".$row->exam_id."' and status='1'" );
														$rowexam2=mysqli_num_rows($sqlexam2);
														$wrong=$rowexam2;
														$tmarks=$correct-$wrong;	

														$sqlc=query("select id from student_questions where estatus='1' and mobile='".$userdata['mobile']."' and extra_question='0' and schedule_exam_id='".$row->exam_id."'  and exam_session_id='".$rowexam['id']."' " );
														$rowc=mysqli_num_rows($sqlc);
														$nt=$rowc-($rowexam1+$wrong);
														if($nt>0){
															$nt1=$nt;
														}else{
															$nt1=0;
														}
														if($rowexam['timestamp']!=''){
															$timestamp=$rowexam['timestamp'];
															$timestamp1=date('d-M-Y H:i:s',$rowexam['timestamp']);
														}else{
															$timestamp='';
															$timestamp1='';
														}
														$correct=$rowexam1*4;
														$sqlexam1=query("select id from `student_questions` where estatus='1' and mobile='".$userdata['mobile']."' and extra_question='0' and exam_session_id='".$rowexam['id']."' and schedule_exam_id='".$row->exam_id."' and status='2'" );
														$rowexam1=mysqli_num_rows($sqlexam1);
														if($rowexam['id']!=''){
															
															$rowexam2=$rowexam1;
															$wrong2=$wrong;
															$nt2=$nt1;
															$rowc2=$rowc;
															
															$correct2=$correct;
															$tmarks=$correct-$wrong;
														}else{
															$rowexam2=0;
															$wrong2=0;
															$nt2=0;
															$rowc2=0;
															$correct2=0;
															$tmarks=0;
														}
													?>
													<tr>
														<td><?php echo $userdata['mobile']; ?></td>
														<td><?php echo $userdata['name']; ?></td>
														<td><?php echo $examdata1['exam']; ?></td>
														
														<td data-order="<?php echo $row->timestamp; ?>"><?php echo date('d-M-Y H:i:s',$row->timestamp); ?></td>
														<td><?php echo $plandata['plan_name']; ?></td>
														<?php
													
														if($userdata['source']=='2'){
															$srcsel=query("select * from registration_src where  username='".$userdata['mobile']."'");
															$rowc=mysqli_num_rows($srcsel);
															if($rowc>0){
																$rowsel=mysqli_fetch_array($srcsel);
																?>
																<td class="text-left"><?php echo $rowsel['src']; ?></th>		
																<?php
															}else{
																?>
																<td class="text-left">Other</th>		
																<?php
															}
														}else if($userdata['source']=='0'){
															?>
															<td class="text-left">Web</th>		
															<?php
														}else if($userdata['source']=='1'){
															?>
															<td class="text-left">Mobile</th>		
															<?php
														}else{
															?>
															<td class="text-left"></th>		
															<?php
														}

																
															?>
														<td><?php echo $examdata['exam_name']; ?></td>
														<td><?php if($rowexam['id']!=''){ echo $rowexam2; }else{ echo '0'; } ?></td>
														<td><?php echo $wrong2; ?></td>
														<td><?php echo $nt2; ?></td>
														<td><?php echo $rowc2; ?></td>
														<td><?php echo $correct2; ?></td>
														<td><?php echo $wrong2; ?></td>
														<td><?php echo $tmarks; ?></td>
														<td data-order="<?php echo $timestamp1; ?>"><?php echo $timestamp1; ?></td>
														
														</tr>
													<?php
												}	
											}	
										}*/
										?>
										</tbody>
									</table> -->
								</div>
								
							</div>
							<?php
							
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
  </section>
 
	<?php

}

?>