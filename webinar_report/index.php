<?php
    error_reporting(0);
    include('../include/session.php');
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
		
    	?>
    	<script type="text/javascript">
			alert("User with the same username logged in to another browser");
			location.replace("<?php echo SECURE_PATH;?>admin/");
		</script>
		<?php
    }
    else
    {
    ?>
		
		<style>
         
		.note-popover.popover {
			max-width: none;
			display: none;
		}
		.note-editor {
			position: relative;
			padding: 1px;
		}
	
         </style>
		<style type="text/css">
		
		.vendor-list.table tbody td,,
		.vendor-list.table tbody th {
		/* word-break: keep-all; */
		width: 10% !important;
		}

		button:nth-of-type(1).active{
		color: #fff !important;
		background-color: #6c757d !important;
		border-color: #6c757d !important; 
		}
		.slabel{
		display:block !important
		}
		</style>
		
		
		<div class="content-wrapper">
			<section class="admindashboard">
                <div class="container-fluid p-0">
                    
					<div class="form-group mb-0" id="adminTable">
							<div class="col-sm-8">
								<script type="text/javascript">
									setStateGet('adminTable','<?php echo SECURE_PATH;?>webinar_report/process.php','tableDisplay=1');
								</script>
							</div>
						</div>
                   
                </div>
            </section>
			<section class="admindashboard">
                <div class="container-fluid p-0">
                    
					<div class="form-group mb-0" id="chapterreport">
							<div class="col-sm-8">
								<script type="text/javascript">
									setStateGet('chapterreport','<?php echo SECURE_PATH;?>webinar_report/process.php','getreport=1');
								</script>
							</div>
						</div>
                   
                </div>
            </section>
            
			

           
        </div>	
<?php
    
    }
?>